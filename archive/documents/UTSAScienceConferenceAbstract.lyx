#LyX 2.1 created this file. For more info see http://www.lyx.org/
\lyxformat 474
\begin_document
\begin_header
\textclass IEEEtran
\begin_preamble
% for subfigures/subtables
\usepackage[caption=false,font=footnotesize]{subfig}
\end_preamble
\options journal
\use_default_options false
\maintain_unincluded_children false
\language english
\language_package default
\inputencoding default
\fontencoding global
\font_roman default
\font_sans default
\font_typewriter default
\font_math auto
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100
\font_tt_scale 100
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command bibtex
\index_command default
\float_placement tbh
\paperfontsize default
\spacing single
\use_hyperref true
\pdf_title "Your Title"
\pdf_author "Your Name"
\pdf_bookmarks true
\pdf_bookmarksnumbered true
\pdf_bookmarksopen true
\pdf_bookmarksopenlevel 1
\pdf_breaklinks false
\pdf_pdfborder true
\pdf_colorlinks false
\pdf_backref false
\pdf_pdfusetitle false
\pdf_quoted_options "pdfpagelayout=OneColumn, pdfnewwindow=true, pdfstartview=XYZ, plainpages=false"
\papersize default
\use_geometry false
\use_package amsmath 1
\use_package amssymb 1
\use_package cancel 0
\use_package esint 0
\use_package mathdots 1
\use_package mathtools 0
\use_package mhchem 1
\use_package stackrel 0
\use_package stmaryrd 0
\use_package undertilde 0
\cite_engine basic
\cite_engine_type default
\biblio_style plain
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\justification true
\use_refstyle 0
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation default
\quotes_language english
\papercolumns 2
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Title
BigScan: A Multi-Tenant Massively Parallel Cloud-based DBaaS
\end_layout

\begin_layout Author
David Holland, Dr.
 Weining Zhang 
\begin_inset Newline newline
\end_inset

Computer Science, UTSA 
\begin_inset Newline newline
\end_inset

{njn130@my., Weining.Zhang@}utsa.edu
\end_layout

\begin_layout Abstract
Efficient processing of huge unstructured data sets (Big Data) in the Cloud
 using commodity servers and storage has become a new paradigm for computation
 [Map/Reduce, Hadoop] .
 However, processing large relational data sets remains problematic without
 special hardware.
 This problem can be characterized by investigating the performance of SQL
 JOINs over massive data sets.
 Any efficient DBaaS (Database as a Service) need a fast JOIN.
 To solve the slow performance of JOINs the authors propose investigating
 different JOIN algorithms that make specific use of a Cloud's storage managemen
t and networking service layers.
 To simplify, we consider read/only DBaaS (Database as a Service) operations.
 As such, DBaaS operators must exploit Cloud parallelism while minimizing
 inter-processor communication latencies.
 
\end_layout

\begin_layout Abstract
A demonstration prototype utilizes middle-ware for OpenStack’s object storage
 management system, Swift.
 A network of light weight, disposable VMs (ZeroCloud middle-ware,ZeroVM
 by RackSpace) provide in situ computation for data deployed on Swift's
 storage nodes.
 Our proposed DBaaS algorithms and SQL-like execution plans harness the
 synergism of this storage/computation architecture to speed-up critical
 DBaaS operations, e.g.
 the iconic JOIN.
 Validating the prototype emphasizes empiricism; using benchmarks, which
 are compared to identical data used to evaluate Google’s BigQuery, an important
 DBaaS predecessor [Dremel].
 Bench-marking large data sets over thousands of VMs in the OpenStack Cloud
 ensures credibility.
 Benchmarks show several improvements over BigQuery.
 Research areas needed to evolve future DBaaS performance by blending best
 features of SQL and No-SQL are intimated.
\end_layout

\begin_layout Keywords
JOIN, DBaaS, Cloud, OpenStack, Swift, ZeroVM, ZeroCloud, RackSpace
\end_layout

\end_body
\end_document

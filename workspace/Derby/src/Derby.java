/*

   Derby - Class SimpleApp

   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

 */

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;


/**
 * <p>
 * This sample program is a minimal Java application showing JDBC access to a
 * Derby database.</p>
 * <p>
 * Instructions for how to run this program are
 * given in <A HREF=example.html>example.html</A>, by default located in the
 * same directory as this source file ($DERBY_HOME/demo/programs/simple/).</p>
 * <p>
 * Derby applications can run against Derby running in an embedded
 * or a client/server framework.</p>
 * <p>
 * When Derby runs in an embedded framework, the JDBC application and Derby
 * run in the same Java Virtual Machine (JVM). The application
 * starts up the Derby engine.</p>
 * <p>
 * When Derby runs in a client/server framework, the application runs in a
 * different JVM from Derby. The connectivity framework (in this case the Derby
 * Network Server) provides network connections. The client driver is loaded
 * automatically.</p>
 */
public class Derby
{
    /* the default framework is embedded, protocol is  dbc:derby: */
    private String framework = "embedded";
    private String protocol = "jdbc:derby:";
    private String dbName = null; // the name of the database
    private String tableName = null;

    
     
    public Derby(String framework, String protocol, String dbName, String tableName) {
    	
		super();

		this.framework = framework;
		this.protocol = protocol;
		this.dbName = dbName;
		this.tableName = tableName;
	}

	/**
     * <p>
     * Starts the demo by creating a new instance of this class and running
     * the <code>go()</code> method.</p>
     * <p>
     * When you run this application, you may give one of the following
     * arguments:
     *  <ul>
          <li><code>embedded</code> - default, if none specified. Will use
     *        Derby's embedded driver. This driver is included in the derby.jar
     *        file.</li>
     *    <li><code>derbyclient</code> - will use the Derby client driver to
     *        access the Derby Network Server. This driver is included in the
     *        derbyclient.jar file.</li>
     *  </ul>
     * <p>
     * When you are using a client/server framework, the network server must
     * already be running when trying to obtain client connections to Derby.
     * This demo program will will try to connect to a network server on this
     * host (the localhost), see the <code>protocol</code> instance variable.
     * </p>
     * <p>
     * When running this demo, you must include the correct driver in the
     * classpath of the JVM. See <a href="example.html">example.html</a> for
     * details.
     * </p>
     * @param args This program accepts one optional argument specifying which
     *        connection framework (JDBC driver) to use (see above). The default
     *        is to use the embedded JDBC driver.
     */
    public static void main(String[] args)
    {
    	String framework = args[0];
    	String protocol =  args[1];
    	String dbName = args[2];
    	String tableName = args[3];
    	System.out.println("args:  framework-> " + framework + ", protocol-> " + protocol
    						+ ", dbName-> " + dbName + ", tableName-> " + tableName);
        Derby derby = new Derby(framework, protocol, dbName, tableName);
        derby.go(args);
        System.out.println( dbName + " finished");
    }

    /**
     * <p>
     * Starts the actual demo activities. This includes 
     * making a connection to Derby (automatically loading the driver).
     * <p>
     * 
     *
     * @param args - Optional argument specifying which framework or JDBC driver
     *        to use to connect to Derby. Default is the embedded framework,
     *        see the <code>main()</code> method for details.
     * @see #main(String[])
     */
    void go(String[] args)
    {
        /* parse the arguments to determine which framework is desired*/
        //parseArguments(args);

        System.out.println(dbName + " starting in " + framework + " mode");

        /* We will be using Statement and PreparedStatement objects for
         * executing SQL. These objects, as well as Connections and ResultSets,
         * are resources that should be released explicitly after use, hence
         * the try-catch-finally pattern used below.
         * We are storing the Statement and Prepared statement object references
         * in an array list for convenience.
         */
        Connection conn = null;
        ArrayList<Statement> statements = new ArrayList<Statement>(); // list of Statements, PreparedStatements
        Statement s;
        ResultSet rs = null;
        try
        {
            Properties props = new Properties(); // connection properties
            // providing a user name and password is optional in the embedded and derbyclient frameworks
            //props.put("user", "user1");
            //props.put("password", "user1");

            /* By default, the schema APP will be used when no username is
             * provided.
             * Otherwise, the schema name is the same as the user name (in this
             * case "user1" or USER1.)
             *
             * Note that user authentication is off by default, meaning that any
             * user can connect to your database using any password. To enable
             * authentication, see the Derby Developer's Guide.
             */

            /*
             * This connection specifies create=true in the connection URL to
             * cause the database to be created when connecting for the first
             * time. To remove the database, remove the directory derbyDB (the
             * same as the database name) and its contents.
             *
             * The directory <dbName> will be created under the directory that
             * the system property derby.system.home points to, or the current
             * directory (user.dir) if derby.system.home is not set.
             */
            conn = DriverManager.getConnection(protocol + dbName
                    + ";create=false", props);

            System.out.println("Connected to database " + dbName);

            // We want to control transactions manually. Autocommit is on by
            // default in JDBC.
            conn.setAutoCommit(false);

            /* Creating a statement object that we can use for running various
             * SQL statements commands against the database.*/
             s = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, 
                    ResultSet.CONCUR_READ_ONLY);
            //s = conn.createStatement();
            statements.add(s);

            int totalBytesInRow = estimateRowSizeInBytes(tableName, conn);
            System.out.println("Total estimated bytes in a single row of the table " + tableName 
            		+ " is " + totalBytesInRow);
            
            /*
               We select the rows and verify the results.
             */
            rs = s.executeQuery("SELECT * FROM " + tableName);
            
            int last = getRowsInResultSet(rs);
            System.out.println("At last row  in ResultSet:  #" + last);
            
            //mover cursor to first row in ResultSet
            rs.first();
            if(rs.isFirst())
            	System.out.println("Back at first row  in ResultSet ");
            
            System.out.println("Total bytes in ResultSet:  " + (totalBytesInRow * last));
            
            double gigaByte = 1024 * 1024 * 1024;
            
            int partitions = (int) Math.ceil((totalBytesInRow * last) / gigaByte);
            System.out.println("Number of 1 gigabyte paritions needed : " + partitions);     	
            
            
            ResultSetMetaData meta = rs.getMetaData();
            int columns = meta.getColumnCount();
            String types = "";
            for(int i = 1; i <= columns; i++) {
            	types = types + ", " + meta.getColumnTypeName(i);
            	//System.out.println(i +"th Column Schema: " + meta.getSchemaName(i));
  
            }
            
            System.out.println("TYPES: " + types);     
            
           
            /*
             * In embedded mode, an application should shut down the database.
             * If the application fails to shut down the database,
             * Derby will not perform a checkpoint when the JVM shuts down.
             * This means that it will take longer to boot (connect to) the
             * database the next time, because Derby needs to perform a recovery
             * operation.
             *
             * It is also possible to shut down the Derby system/engine, which
             * automatically shuts down all booted databases.
             *
             * Explicitly shutting down the database or the Derby engine with
             * the connection URL is preferred. This style of shutdown will
             * always throw an SQLException.
             *
             * Not shutting down when in a client environment, see method
             * Javadoc.
             */

            if (framework.equals("embedded"))
            {
                try
                {
                    // the shutdown=true attribute shuts down Derby
                    DriverManager.getConnection("jdbc:derby:;shutdown=true");

                    // To shut down a specific database only, but keep the
                    // engine running (for example for connecting to other
                    // databases), specify a database in the connection URL:
                    //DriverManager.getConnection("jdbc:derby:" + dbName + ";shutdown=true");
                }
                catch (SQLException se)
                {
                    if (( (se.getErrorCode() == 50000)
                            && ("XJ015".equals(se.getSQLState()) ))) {
                        // we got the expected exception
                        System.out.println("Derby shut down normally");
                        // Note that for single database shutdown, the expected
                        // SQL state is "08006", and the error code is 45000.
                    } else {
                        // if the error code or SQLState is different, we have
                        // an unexpected exception (shutdown failed)
                        System.err.println("Derby did not shut down normally");
                        printSQLException(se);
                    }
                }
            }
        }
        catch (SQLException sqle)
        {
            printSQLException(sqle);
        } finally {
            // release all open resources to avoid unnecessary memory usage

            // ResultSet
            try {
                if (rs != null) {
                    rs.close();
                    rs = null;
                }
            } catch (SQLException sqle) {
                printSQLException(sqle);
            }

            // Statements and PreparedStatements
            int i = 0;
            while (!statements.isEmpty()) {
                // PreparedStatement extend Statement
                Statement st = (Statement)statements.remove(i);
                try {
                    if (st != null) {
                        st.close();
                        st = null;
                    }
                } catch (SQLException sqle) {
                    printSQLException(sqle);
                }
            }

            //Connection
            try {
                if (conn != null) {
                    conn.close();
                    conn = null;
                }
            } catch (SQLException sqle) {
                printSQLException(sqle);
            }
        }
    } /// end method go

    /* Returns size of ResultSet. Must support TYPE_SCROLL_INSENSITIVE, 
     * ResultSet.CONCUR_READ_ONLY  cursor types; Resets cursor at start 
     * of ResultSet.
     */
    public static int getRowsInResultSet(ResultSet rs) throws SQLException {
        
    	int lastRow = 0;
        //mover cursor to last row in ResultSet
        if(rs.last())
        	lastRow = rs.getRow();
        
        //rs.beforeFirst();  //put cursor back at front of ResultSet
        
        return lastRow;
    	
    }
    
    
    /* Estimates the size of row in bytes of a table based upon DatabaseMetaData info, 
     * which provides the size of field sizes in bytes using a field's data type.
     * 
     *   INTEGER takes 4 bytes
     *   DOUBLE takes 8 bytes
     *   CHAR takes 2 bytes per char(UTF8 encoding), given by Col. SIZE
     *   VARCHAR takes 2 bytes per char(UTF8 encoding), given by its Col. SIZE
     *   TIME length of string representation, given by Col. SIZE, usually 8
     *   DATE length of string mm/dd/yyyy, given by Col. SIZE, usually 10
     */
    public static int estimateRowSizeInBytes(String tableName, Connection conn) throws SQLException {
    	
    	 // get data base metadata
        DatabaseMetaData metaData = conn.getMetaData();
        int bytes  = 0;
        int size = 0;
        int dataType = 0;
        // get columns
        ResultSet cols = metaData.getColumns(null, null, tableName, "%");
        while (cols.next()) {
        	// 1: catalog: String
            // 2: schema: String
            // 3: table name: String
            // 4: column name: String
            // 5: data type: int, mapped to java.sql.types which is numeric code.
        	//		INTEGER: 4, VARCHAR: 12, CHAR: 1, DOUBLE: 8, DATE: 91, TIME: 92
            // 6: type name: String (CHAR, VARCHAR, TIMESTAMP, ...)
        	// 7: size: int
            System.out.println("col. name: " + cols.getString(4) 
            		+ ", data type: " + cols.getInt(5)
            		+ ", type name: " + cols.getString(6)
            		+ ", size: " + cols.getInt(7));
            
            dataType = cols.getInt(5);
            //calculate total bytes needed to store row
            if (dataType == 1) {   			//CHAR
            	size = cols.getInt(7);
            	bytes += size * 2;
            } else if (dataType == 12) { 	// VARCHAR
            	size = cols.getInt(7);
            	bytes += size * 2;
            } else if (dataType == 4) {
            	bytes += 4;
            } else if (dataType == 8) {
            	bytes += 8;
            } else if (dataType == 91) {
            	size = cols.getInt(7);
            	bytes += size * 2;
            } else if (dataType == 92) {
            	size = cols.getInt(7);
            	bytes +=  size * 2;
            } else {
            	System.out.println("UNKNOWN TYPE: " + dataType);
            }
            
        }
        cols.close();
        //System.out.println("total bytes needed to store row: " + bytes);
        
        return bytes;
    	
    }
    
    /**
     * Reports a data verification failure to System.err with the given message.
     *
     * @param message A message describing what failed.
     */
    @SuppressWarnings("unused")
	private void reportFailure(String message) {
        System.err.println("\nData verification failed:");
        System.err.println('\t' + message);
    }

    /**
     * Prints details of an SQLException chain to <code>System.err</code>.
     * Details included are SQL State, Error code, Exception message.
     *
     * @param e the SQLException from which to print details.
     */
    public static void printSQLException(SQLException e)
    {
        // Unwraps the entire exception chain to unveil the real cause of the
        // Exception.
        while (e != null)
        {
            System.err.println("\n----- SQLException -----");
            System.err.println("  SQL State:  " + e.getSQLState());
            System.err.println("  Error Code: " + e.getErrorCode());
            System.err.println("  Message:    " + e.getMessage());
            // for stack traces, refer to derby.log or uncomment this:
            //e.printStackTrace(System.err);
            e = e.getNextException();
        }
    }

    /**
     * Parses the arguments given and sets the values of this class's instance
     * variables accordingly - that is, which framework to use, the name of the
     * JDBC driver class, and which connection protocol to use. The
     * protocol should be used as part of the JDBC URL when connecting to Derby.
     * <p>
     * If the argument is "embedded" or invalid, this method will not change
     * anything, meaning that the default values will be used.</p>
     * <p>
     * @param args JDBC connection framework, either "embedded" or "derbyclient".
     * Only the first argument will be considered, the rest will be ignored.
     */
    private void parseArguments(String[] args)
    {
    	
        if (args.length > 0) {
            if (args[0].equalsIgnoreCase("derbyclient"))
            {
                framework = "derbyclient";
                protocol = "jdbc:derby://localhost:1527/";
            }
        }
    	
    } // method
    
} // class

1		COMPUTE		ORDER		tracks.Composer, <										2	null	2
2		COMPUTE		PROJECT		tracks.Composer, tracks.Name							3	null
3		COMPUTE		JOIN		tracks.GenreId = genres.GenreId							4	5		2	4
4		COMPUTE		SELECT		tracks.UnitPrice < 1.0 AND tracks.Composer IS NOT NULL	6	null
5		COMPUTE		SELECT		genres.Name = 'Classical' OR genres.Name = 'Opera'		7	null
6		DATA_ONLY	${HOME}/databases/chinook.db	tracks	[TrackId INTEGER PRIMARY KEY; Name NVARCHAR(200); AlbumId INTEGER; MediaTypeId INTEGER; GenreId INTEGER; Composer NVARCHAR(220); Milliseconds INTEGER; Bytes INTEGER; UnitPrice NUMERIC(10,2)]
7		DATA_ONLY	${HOME}/databases/chinook.db	genres	[GenreId INTEGER PRIMAR; Name NVARCHAR(120)]
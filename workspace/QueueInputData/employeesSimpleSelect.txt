% BENCH MARK AGGREGATE SUM 
% All employees that make less than $200,000 annually
%
% SELECT salaries.salary, salaries.emp_no FROM salaries WHERE salaries.salary < 200000;  
% 
#
#	Type		Op		Predicate															L	R	Degree	Probe(JOIN)
1	COMPUTE		SELECT	salaries.salary, salaries.emp_no WHERE salaries.salary < 200000		2	null	6
2	DATA_ONLY	${HOME}/databases/employees6SalariesPartitions/employees.db	[salariesPart_1,salariesPart_2,salariesPart_3,salariesPart_4,salariesPart_5,salariesPart_6]	[emp_no int(11); salary int(11); from_date date; to_date date]
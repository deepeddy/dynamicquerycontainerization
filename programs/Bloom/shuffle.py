#!python
# -*- coding: utf-8 -*-
import os
import csv
import operator
import sys
import traceback
import cPickle
import io
import re
from random import Random 

JOIN_COUNT = int(os.environ['JOIN_COUNT'])
NODE_NAME = os.environ['SCRIPT_NAME']
KEY_COL_INDEX = int(os.environ['KEY_COL_INDEX'])
RELATION_TAG = os.environ['RELATION_TAG']
IO_BUFFER_SIZE = (2<<18) + 8

#DECLARE List of connection handles to store handles to all join nodes 
JoinConnectionHandles = []


#READING PARAMETER INTO PYTHON SCRIPT FROM JSON FILE USING:  
#	
#		Python fragment
#		os.environ['someParameter']
#
#
#		JSON fragment
#		"exec": {
#            "path": "file://python:python",
#			 "env": {
#				"someParameter": 4
#			  }
#        }
#
#
#ALSO CONSTRUCT WHICH SEQUENCER TO CONNECT TO USING:	os.environ['SCRIPT_NAME']
#AND A MODULO AND STRING FUNCTIONS TO FORM THE PATH TO A SPECIFIC SEQUENCER USING THE CONNECTION IDS
#THAT ARE GUARANTEED TO BE PROVIDED BY THE ZEROVM NETWORK LAYER. SEE THE JSON SERVLET DOC

def is_match(regex, text):
	pattern = re.compile(regex)
	return pattern.search(text) is not None

def csv_to_list(csv_file, delimiter=','):
	""" 
	Reads in a CSV file and returns the contents as list,
	where every row is stored as a sublist, and each element
	in the sublist represents 1 cell in the table.
  
	"""
	#sys.stderr.write(csv_file)
	with open(csv_file, 'rb', buffering=(2<<18) + 8) as fp:
		reader = csv.reader(fp, delimiter=delimiter)
		csv_list = list(reader)
		fp.close()
		return csv_list
      
	#cmp=lambda x,y: cmp(x.lower(), y.lower())
    
def sortCmp(x,y): 
	if x.isdigit() and y.isdigit():
		x=str(x)
		y=str(y)
	return cmp(x,y)

  
#sorts a list of rows/tuples by one of its columns/fields
# col must an int
def sort_by_column(rows, col, reverse=False):
  	
    
	if not isinstance(col, int ):  
		sys.stderr.write("*****FATAL**************COL INDEX NOT AN INTEGER ************************")
		sys.stderr.flush()
		sys.exit(0)
	else:
		rows = sorted(rows, cmp=lambda x,y: sortCmp(x,y), key=operator.itemgetter(col), reverse=reverse)

	return rows
  
#hash function that maps/SHUFFLES a tupple to a particular join nodeID based upon
#the value of the  key located in column 'col'
#the hash is then mapped to a node based upon modulo the number of join nodes avaliale
def mapTupple(tupple, col, modulo_base):
	keyValue = tupple[col]
	#node connections start at 1, {1 .... modulo_base}
	nodeId = (hash(keyValue) % modulo_base) + 1
    
	#sys.stderr.write ("\ntupple: " + str(tupple) + '\n')
	#sys.stderr.write ("key_col " + str(col) + ", key_value " + keyValue + '\n')
	#sys.stderr.flush()
    
	return nodeId
 
#use a Python set to simulate a BloomFilter
#must first create set of all keys, 
#then send them to the BloomFilter master, which returns a list of tupples not needed
#by any other node in the join, i.e. don't need to be included in the shuffle 
def filterByBloom(listOfTupples, key_col_index):
  
	keySet = set()
    
   	for tupple in listOfTupples:	
		keySet.add(tupple[key_col_index])
	        
	#open channel to Bloom Master and send keySet
	listChannelDirectory('/dev/out')
	#BloomChannel = open('/dev/out/bloom', 'ab', buffering = IO_BUFFER_SIZE)
	BloomChannel = open('/dev/out/bloom', 'a')
	sys.stderr.write("filterByBloom: node " + NODE_NAME + ", connected to: " + str(BloomChannel) + '\n')
	sys.stderr.flush()
	cPickle.dump(keySet,BloomChannel)
	sys.stderr.write("filterByBloom: node " + NODE_NAME + ", dumped keySet of size: " 
                     + str(len(keySet))+ " : " + str(sorted(keySet, cmp=None, key=None, reverse=False)) + '\n'
                     + " to: " + str(BloomChannel) + '\n')
	BloomChannel.flush()
	BloomChannel.close()
    
	sys.stderr.write(NODE_NAME + " status with channel: " + str(BloomChannel) + '\n')
	sys.stderr.flush()  
    
    
    #open response channel and wait for Bloom Master to return discard key set
	listChannelDirectory('/dev/in')
	#BloomChannel = open('/dev/in/bloom', 'rb', buffering = IO_BUFFER_SIZE)
	BloomChannel = open('/dev/in/bloom', 'r')
	sys.stderr.write("filterbyBloom: node " + NODE_NAME + ", connected to: " 
                     + str(BloomChannel) + '\n')
	sys.stderr.flush()
	keyDifferenceSet = set()
	try:
		sys.stderr.write("filterByBloom: node " + NODE_NAME + ", waiting response from BLOOM: " 
                         + " from: " + str(BloomChannel) + '\n')
		sys.stderr.flush()
		keyDifferenceSet = cPickle.load(BloomChannel)
		sys.stderr.write("filterByBloom: node " + NODE_NAME + ", loaded keyDifferenceSet: " + str(keyDifferenceSet) + '\n' 
                         + " to: " + str(BloomChannel) + '\n')
		sys.stderr.flush()
		BloomChannel.close()
	except EOFError:
		BloomChannel.close()
    
	if not keyDifferenceSet:
		sys.stderr.write("filterbyBloom: node " + NODE_NAME + ", returned EMPTY filter set from: " + str(BloomChannel) + '\n')
		sys.stderr.flush() 
		return
	else:
		for tupple in listOfTupples:
			if(tupple[key_col_index] in keyDifferenceSet):
				listOfTupples.remove(tupple)
        
	#return filtered set of tupples
  


#SHUFFLE step, iterate thru, writing each tupple to a join node based upon its key,
# all tupples with the same key are shipped to the same join node
def shuffle(listOfTupples, key_col_index, JOIN_COUNT, handles):
  
	debugIndex = 0
    
	for tupple in listOfTupples:
		joinNodeId = mapTupple(tupple, key_col_index, JOIN_COUNT)
		#shuffle tupple to correct join node, tag tupple for later joining identification,
		#join must identify which relation tupple comes from
		
    
		#append RELATION type tag to end of tupple
		tupple.append(NODE_NAME)
        
		'''
		sys.stderr.write(str(tupple[-1]) + ":, shuffleNodeId is: " + str(joinNodeId) + ", writing to: " 
                          + str(handles[joinNodeId - 1]) + '\n')
		sys.stderr.flush()
		'''
      
		cPickle.dump(tupple,handles[joinNodeId - 1])
		#JoinConnectionHandles[joinNodeId - 1].write(str(tupple) + '\n')
		#sys.stderr.write ("\nWRITTEN tupple: " + str(tupple) + '\n')
		#sys.stderr.flush();
      
		'''
		debugIndex += 1
		#if (debugIndex > 30000):
		if (debugIndex > 100):
			break;
		'''
            
def listChannelDirectory(directory):
	sys.stderr.write("LIST DIRECTORY CHANNELS\n")
    
	for file in os.listdir(directory): 
		sys.stderr.write(NODE_NAME + ": " + directory + " contains: " + file + '\n')
        
	sys.stderr.flush()
  
            
            
#OPEN LIST OF OUTPUT  CHANNELS
#list of handles for channels, type of channel of nodeType, e.g. "shuffle", "overflow".
#see manifest
def openOutputChannels(handles, nodeType):
  
	sys.stderr.write("OPEN OUTPUT CHANNELS\n")
    
	for out_file in os.listdir('/dev/out'):
      
  		#sys.stderr.write("Inspecting file: " + in_file + '\n')
  
		if (is_match(nodeType, out_file)):
			fp = open(os.path.join('/dev/out', out_file), 'ab', buffering = IO_BUFFER_SIZE)
			handles.append(fp)
			sys.stderr.write("node " + NODE_NAME + ", connected to: " + str(handles[-1]) + '\n')
        
	sys.stderr.flush()
          
#OPEN handles to all join nodes
def openJoinConnections(handles):
  
	# channels listed in order from {1 .... JOIN_COUNT}
	for out_file in os.listdir('/dev/out'):
      
		fp = open(os.path.join('/dev/out', out_file), 'ab', buffering = IO_BUFFER_SIZE)
		handles.append(fp)
        
		sys.stderr.write("node " + NODE_NAME + ", connected to: " + str(handles[-1]) + '\n')
		sys.stderr.flush()

          
#send EOF messges to all joins connected to this node
def closeConnections(handles):
  
	for fp in handles:
		#cPickle.dump("EOF "+ NODE_NAME, fp)
		#handles[i].write("EOF "+ NODE_NAME + '\n')
		#fp.write("EOF " + NODE_NAME + '\n')
		fp.flush()
		fp.close()
        
		#DEBUG
		sys.stderr.write("node type is: " + NODE_NAME + ", disconnected from: " +
                       str(fp) + '\n')
		sys.stderr.flush();
      
  
#list open channels     
def listOpenChannels(channels):
	sys.stderr.write("LIST STATUS OF CHANNELS\n")
	if not channels:
		sys.stderr.write(NODE_NAME + " open channel list is EMPTY: " + str(channels) + '\n')
		return
	else:
		for channel in channels:
			sys.stderr.write(string.upper(NODE_NAME) + " status with channel: " + str(channel) + '\n')
            
	sys.stderr.flush()   
    
      
class BloomFilter:
	# http://en.wikipedia.org/wiki/Bloom_filter

	def __init__(self, num_bytes, num_probes, iterable=()):
		self.array = bytearray(num_bytes)
		self.num_probes = num_probes
		self.num_bins = num_bytes * 8
		self.update(iterable)

	def get_probes(self, key):
		random = Random(key).random
		return (int(random() * self.num_bins) for _ in range(self.num_probes))

	def update(self, keys):
		for key in keys:
			for i in self.get_probes(key):
				self.array[i//8] |= 2 ** (i%8)

	def __contains__(self, key):
		return all(self.array[i//8] & (2 ** (i%8)) for i in self.get_probes(key))
      
#END  FUNCTION DEFs *************************************************         

	
                  
#GET TUPPLES from the .csv file, a list of lists(tupples), where every line is a list of csv formatted fields         
listOfTupples = csv_to_list('/dev/input', ",")

'''
#test:  add a tupple that will not be matched
if NODE_NAME == 'shuffle_R-1':
	testTupple = ['goof','poof', 'hoop', '999999']
	listOfTupples.append(testTupple)
'''

#filter out unneeded tupples using a Bloom Filter
filterByBloom(listOfTupples, KEY_COL_INDEX)

#sys.stderr.write("TEST if tuppple : " + str(testTupple) + " has been filtered " + '\n')
#sys.stderr.write("TEST if tuppple in list: " + str(testTupple in listOfTupples) + '\n')
#sys.stderr.flush()

'''
#check is filter is working
lastKey = listOfTupples[-1][KEY_COL_INDEX]
sys.stderr.write(lastKey + " in set: " + str(lastKey in BloomFilter) + '\n')
sys.stderr.flush()
'''

#OPEN shuffle channels to joins
openOutputChannels(JoinConnectionHandles, "join")
#openJoinConnections(JoinConnectionHandles)

#SHUFFLE STEP
shuffle(listOfTupples, KEY_COL_INDEX, JOIN_COUNT, JoinConnectionHandles)

#CLOSE
closeConnections(JoinConnectionHandles)


sys.stderr.write("\nSHUFFLE COMPLETE: " + NODE_NAME + '\n')
sys.stderr.flush()
    

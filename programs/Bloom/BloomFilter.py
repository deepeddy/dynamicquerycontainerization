''' Bit Array - Bit array computing with long integers.
Good for around 1,000,000 bit arrays but gets slow with a billion bits...
Mike Sweeney. June 2013.

Positives:
Works even with old versions of Python.
Only pure Python needed - no third party modules.
Binary operations such as & | ^ << and >> are fast.
Store and load are fast and compact.

Negatives:
Longs are passed by value so no in-place operations are possible.
Array changing operations are slow on large arrays.

Timing of one AND operation on bit arrays with Python 2.7 on AMD 3.8 GHz 4 core:
1,000,000 bit         15 us
10,000,000 bit       320 us
100,000,000 bit      6.2 ms
1,000,000,000 bit     86 ms    (125MB bit array objects)
'''
import math
import sys
from random import Random
import cPickle
import os
import sys
import re
import string

# Constructors for bit arrays:
bitarray = long
#s is size of bitMap
def frombin(s): return int(str(s), 2)

# Operations on bit arrays of the same size:
def union(a, b): return a & b
def issubset(a, b): return a & b == a
def subtract(a, b): return a & ~b
# use & | ^ and ~ for other bitarray operations

# Change a bit value in a bit array:
def setbit(a, n): return a | (1<<n)
def unsetbit(a, n): return a & ~(1<<n)
def toggle(a, n): return a ^ (1<<n)

# Append and pop will add or remove a bit from the end of the array:
def append(a, v): return (a<<1) | v
def pop(a):
    v = (a & 1)
    new_a = (a >> 1)
    return new_a, v

def constructBitMap(n,arr=0):
	for i in xrange(n):
		arr = append(arr, 0)
	return arr
  
  
# Query a bitarray:
def getbit(a, n): return a & (1<<n)
def countbits(a): return bin(a).count('1')
def activebits(a):
    s=bin(a)[2:][::-1]
    return [i for i,d in enumerate(s) if d == '1']
def tostring(a, w=0): return bin(a)[2:].zfill(w)

# Convert bit arrays for storage in files or databases:
import cPickle
def store(a): return cPickle.dumps(a, -1)
def load(data): return cPickle.loads(data)


#key should be a string, powerOf2 sets the positive value the hash returns
#hash returns a positive value between 0 - (2 raised to powerOf2) 
def myHash2(key, powerOf2):
  hash('string') & ((1<<powerOf2)-1)
  
#hash can return a negative #  
def absHash(key):
  return abs(hash(key))

'''  
def _analyticsdemo():
    USERS = 1000000
    from random import randint

    def randarray(n,arr=0):
        for i in xrange(n):
            arr = setbit(arr, randint(0, USERS-1))
        return arr

    admin = randarray(1000)
    upload24hr = randarray(1000)
    badpwd7days = randarray(10000)
    member2012 = randarray(100000)

    maybehacker = badpwd7days & upload24hr & ~admin & ~member2012
    print 'Possible hacker users:', activebits(maybehacker)

if __name__ == '__main__':
    _analyticsdemo()
''' 
       
class Bloom:
 """ Bloom Filter """
 def __init__(self,m,k,hash_fun):
  """
   m, size of the bitMap, should be a power of 2, e.g 1024
   k, number of hash fnctions to compute
   hash_fun, hash function to use
  """
  #length of the bitMap binary digits
  self.m = m
  # initialize the bitMap 
  # (attention a real implementation 
  #  should use an actual bit-array)
  #self.vector = [0]*m
  self.bitMap = constructBitMap(m,arr=0)
  self.k = k
  #hash function takes two parameters: (key, powerOf2), powerOf2 = m
  self.hash_fun = hash_fun
  #log(x[, base])
  self.data = {} # data structure to store the data
  self.false_positive = 0

 def insertKey(self,key):
  """ insert the key into the bitMap """
  for i in range(self.k):
   #setbit(self.bitMap, self.hash_fun(key+str(i), log(self.m,2)) % self.m)
   setbit(self.bitMap, self.hash_fun(key+str(i)) % self.m)

 def containsKey(self,key):
  """ check if key is cointained in the database
      using the filter mechanism """
  for i in range(self.k):
   if getbit(self.bitMap, self.hash_fun(key+str(i)) % self.m) == 0:
   #if (getbit(self.bitMap,self.hash_fun(key+str(i), log(self.m)) % self.m) == 0):
   #if  getbit(self.bitMap, (self.hash_fun(key+str(i) % self.m)) == 0 :
    return False # the key doesn't exist
  
  return True # the key can be in the data set

 
'''
 def get(self,key):
  """ return the value associated with key """
  if self.contains(key):
   try:
    return self.data[key] # actual lookup
   except KeyError:
    self.false_positive += 1
'''

'''
b = Bloom(10000000,2,absHash)

b.insertKey('this is a key')
sys.stderr.write("BitMap after insertion: " + tostring(b.bitMap, w=0) + '\n')
found = b.containsKey('this is a key')
sys.stderr.write("Found key: " + str(found) + '\n')
sys.stderr.flush()
'''

NODE_NAME = os.environ['SCRIPT_NAME']
KEY_COL_INDEX = int(os.environ['KEY_COL_INDEX'])
IO_BUFFER_SIZE = (2<<18) + 8

def is_match(regex, text):
	pattern = re.compile(regex)
	return pattern.search(text) is not None
  

#OPEN LIST OF INPUT  CHANNELS
#list of handles for channels, type of channel, e.g. "shuffle", "overflow".
#see manifest
def openInputChannels(handles, nodeType):
  
	sys.stderr.write("OPEN INPUT CHANNELS\n")
    
	for in_file in os.listdir('/dev/in'):
      
  		#sys.stderr.write("Inspecting file: " + in_file + '\n')
  
		if (is_match(nodeType, in_file)):
			#fp = open(os.path.join('/dev/in', in_file), 'rb', buffering = IO_BUFFER_SIZE)
			fp = open(os.path.join('/dev/in', in_file), 'r')
			handles.append(fp)
			sys.stderr.write("node " + NODE_NAME + ", connected to: " + str(handles[-1]) + '\n')
        
	sys.stderr.flush()
    
    
    
#OPEN LIST OF  OUTPUT CHANNELS
#list of handles for channels, type of channel, e.g. "shuffle", "overflow".
#see manifest
def openOutputChannels(handles, nodeType):
  
	sys.stderr.write("OPEN OUTPUT CHANNELS\n")
    
	for out_file in os.listdir('/dev/out'):
  
		if (is_match(nodeType, out_file)):
			fp = open(os.path.join('/dev/out', out_file), 'a')
			#fp = open(os.path.join('/dev/out', out_file), 'ab', buffering = IO_BUFFER_SIZE)
			handles.append(fp)
			sys.stderr.write("node " + NODE_NAME + ", connected to: " + str(handles[-1]) + '\n')
        
	sys.stderr.flush()  
    
#list open channels     
def listOpenChannels(channels):
	sys.stderr.write("LIST STATUS OF CHANNELS\n")
	if not channels:
		sys.stderr.write(NODE_NAME + " open channel list is EMPTY: " + str(channels) + '\n')
		return
	else:
		for channel in channels:
			sys.stderr.write(string.upper(NODE_NAME) + " status with channel: " + str(channel) + '\n')
            
	sys.stderr.flush()  
    
    
def closeConnections(handles):
  
	for fp in handles:
		#cPickle.dump("EOF "+ NODE_NAME, fp)
		#handles[i].write("EOF "+ NODE_NAME + '\n')
		#fp.write("EOF " + NODE_NAME + '\n')
		fp.flush()
		fp.close()
        
		#DEBUG
		sys.stderr.write("node type is: " + NODE_NAME + ", disconnected from: " + str(fp) + '\n')
		sys.stderr.flush();
                
def listChannelDirectory(directory):
	sys.stderr.write("LIST DIRECTORY CHANNELS\n")
    
	files = []
	for file in os.listdir(directory):
		files.append(file)
		sys.stderr.write(NODE_NAME + ": " + directory + " contains: " + file + '\n')
        
	sys.stderr.flush()
	return files
  


########################## MAIN 

shuffleChannels = []
#open input channels to shufflers

files = []
files = listChannelDirectory('/dev/in')
#openInputChannels(shuffleChannels,"shuffle")


listOfKeySets = []
#collect keySet for each shuffler
#for channel in shuffleChannels:
for file in files:
	#fp = open(os.path.join('/dev/in', file), 'rb', buffering = IO_BUFFER_SIZE)
	fp = open(os.path.join('/dev/in', file), 'r')
	sys.stderr.write("Blocking load on:  " + str(fp) + '\n')
	keys = cPickle.load(fp)
    #add the keys to a list of key sets
	listOfKeySets.append(keys)
	sys.stderr.write("Finished loading keys on " + str(fp) + '\n')
	sys.stderr.write( str(keys) + '\n')
	sys.stderr.flush()
	fp.close()
	#channel.close()
	#shuffleChannels.remove(channel)

#closeConnections(shuffleChannels)
#listOpenChannels(shuffleChannels)   #debug
    
#there is now a one-to-one relation between channels and keySets    
    
shuffleChannels = [] 
#open output channels to shufflers
listChannelDirectory('/dev/out')
openOutputChannels(shuffleChannels,"shuffle")
listOpenChannels(shuffleChannels)   #debug

    
#write back to each shuffler their set difference: 
#each keySet(i) - Union of all KeySets (less KeySet(i))
#for channel in shuffleChannels:
  
for i in range(0,len(listOfKeySets)):
    
	UnionLess_i = set()
      
  	for j in range(0,len(listOfKeySets)):
		if( j <> i):
			UnionLess_i = UnionLess_i | listOfKeySets[j]
              
	difference = listOfKeySets[i] - UnionLess_i              
	cPickle.dump(difference,shuffleChannels[i])
	sys.stderr.write(NODE_NAME + " dumped to : " + str(shuffleChannels[i]) + '\n')
	if not difference:
		sys.stderr.write(NODE_NAME + " empty set dumped to : " + str(shuffleChannels[i]) + '\n')
	sys.stderr.flush()  
    
	shuffleChannels[i].flush()    
	shuffleChannels[i].close()
          		




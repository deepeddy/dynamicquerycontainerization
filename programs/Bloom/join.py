#!python
import os
import math
import traceback
import sys
import cPickle
import re
import ast
import operator
import random
import mmap
import functools
import itertools
import copy
import profile
import ast

'''
  "exec": {
	"args": "join.py",

{	"device": "image", 
    "path": "swift://./ShuffleJoin/join.tar",
    "content_type": "application/x-tar"
                 	
}
'''

inp_dir = '/dev/in'
stdout = '/dev/stdout'

#GLOBAL CONSTANTS
JOIN_COUNT = int(os.environ['JOIN_COUNT'])
NODE_NAME = os.environ['SCRIPT_NAME']
KEY_COL_INDEX = int(os.environ['KEY_COL_INDEX'])
THRESHOLD = int(os.environ['THRESHOLD'])
EOF = os.environ['EOF']
IO_BUFFER_SIZE = (2<<18) + 8

#GLOBAL VARIABLES
shuffleChannels = []
overflowChannels = []
listOfTupplesR = []     #R and S relations to be joined
listOfTupplesS = []
listOfKeys = []

#INOVATION!!!
#instead of spiliing to disk when memory is full, 
#redirects to another special type of Join node. This permits scaling of data sets
#that are very large or were skewed by the shuffle to a few Join nodes
redirect = False
       

def is_match(regex, text):
	pattern = re.compile(regex)
	return pattern.search(text) is not None
  

#hash relation by key, returns hashMap  
def hashRelation(relation, key_col_index):
  
	#a (key,listOfIndexes) map of a relation.  
	#The listOfIndexes is a list of indexes for a set of tupples with the same key.
	hashMap = {} 
  
	for i in xrange(0, len(relation)):
		key = relation[i][key_col_index]  
		if (key in hashMap):
			list = hashMap[key]
			list.append(i)      	# add the new index for key
		else:
			hashMap[key] = [i]   # add key with initial list set to [i] 
            
	return hashMap
  
  
#using closure to pass variables into map function  
def _join(hashMap,hashedRelation, key_col_index):
  
  
	sys.stderr.write("ENTER _join" + '\n')
	sys.stderr.flush()
  
	def join_(tupple):
     
    
		key = tupple[key_col_index]
        
		if(key in hashMap):
			indexList = hashMap[key]
		else:
			sys.stderr.write("Tupple NOT MATCHED in HashMap: " + str(tupple) + '\n')
			sys.stderr.flush()
			return []
        
		
    
		
		#for i  in indexList:
			#joined.append(tupple + hashedRelation[i])
            
		#replace innner loop with List comprehensions for speedup
		newlist = [(tupple + hashedRelation[i]) for i in indexList]
		#newlist = [list(itertools.chain(tupple, hashedRelation[i])) for i in indexList]
		
       
		'''
		for item in newlist:
			sys.stderr.write("newlist: " + str(item) + '\n')
		sys.stderr.flush()  
		'''
            
		return newlist
	
	return join_  

'''
def join_(hashMap, hashedRelation, tupple):
  
	key = tupple[key_col_index]
        
	if(key in hashMap):
		indexList = hashMap[key]
	else:
		return joinedList
        
	#sys.stderr.write("joining tuple: " + str(tupple) + '\n')
	#sys.stderr.flush()
             
	#for i  in indexList:
		#joined.append(tupple + hashedRelation[i])
              
	#replace innner loop with List comprehensions for speedup 
	return [(tupple + hashedRelation[i]) for i in indexList]       
'''

        	
#implements a classical hash-join
#R,S are two tables of tupples to be equ-joined by a common key, 
# returns joined relation/table
def join(R, S, key_col_index): 
  
  	joined = []
    
	relationToScan = S
	relationToHash = R
    
	#scan the bigger relation and hash the smaller
	if(len(R) > len(S)):
		relationToScan = R
		relationToHash = S
	#else:
		#relationToHash = S
		#relationToScan = R
    
	hashMap = hashRelation(relationToHash, key_col_index)
	#sys.stderr.write("relation hashed: " + str(hashMap) + '\n')
	#sys.stderr.flush()
    
	mapJoin = _join(hashMap,relationToHash, key_col_index)
	joined = map(mapJoin, relationToScan) 
	sys.stderr.write("joined type: " + str(type(joined)) + ", with length " + str(len(joined)) +  '\n')
	sys.stderr.flush()
	if (joined):
		#return itertools.chain.from_iterable(joined)
		return [j for i in joined for j in i]
	else: 
		return []
    
	#[ joined.extend(el) for el in iter(temp)] 
  
	#return map(joined.extend, iter(temp))
    
	'''
	for tupple in relationToScan:
      
		key = tupple[key_col_index]
        
		if(key in hashMap):
			indexList = hashMap[key]
		else:
			continue
        
		#sys.stderr.write("joining tuple: " + str(tupple) + '\n')
		#sys.stderr.flush()
    
		#for i  in indexList:
		#	joined.append(tupple + relationToHash[i])

		#replace innner loop with List comprehensions for speedup 
		newList = [(tupple + relationToHash[i]) for i in indexList]
		joined.extend(newList)
	'''
  
  
  
#OPEN LIST OF INPUT  CHANNELS
#list of handles for channels, type of channel, e.g. "shuffle", "overflow".
#see manifest
def openInputChannels(handles, nodeType):
  
	sys.stderr.write("OPEN INPUT CHANNELS\n")
    
	for in_file in os.listdir('/dev/in'):
      
  		#sys.stderr.write("Inspecting file: " + in_file + '\n')
  
		if (is_match(nodeType, in_file)):
			fp = open(os.path.join('/dev/in', in_file), 'rb', buffering = IO_BUFFER_SIZE)
			handles.append(fp)
			sys.stderr.write("node " + NODE_NAME + ", connected to: " + str(handles[-1]) + '\n')
        
	sys.stderr.flush()
    
    
def openChannels(handles, path, mode, nodeCardinality):
  
	sys.stderr.write("OPEN CHANNELS for: " + path + '\n')
   
	for i in xrange(1, nodeCardinality + 1):
		fp = open(os.path.join(path + '-' + str(i)), mode, buffering = IO_BUFFER_SIZE)
		handles.append(fp)
		sys.stderr.write("node " + NODE_NAME + ", connected to: " + str(handles[-1]) + '\n')
        
	sys.stderr.flush()
    
    
#OPEN LIST OF  OUTPUT CHANNELS
#list of handles for channels, type of channel, e.g. "shuffle", "overflow".
#see manifest
def openOutputChannels(handles, nodeType):
  
	sys.stderr.write("OPEN OUTPUT CHANNELS\n")
    
	for out_file in os.listdir('/dev/out'):
  
		if (is_match(nodeType, out_file)):
			fp = open(os.path.join('/dev/out', out_file), 'ab', buffering = IO_BUFFER_SIZE)
			handles.append(fp)
			sys.stderr.write("node " + NODE_NAME + ", connected to: " + str(handles[-1]) + '\n')
        
	sys.stderr.flush()   
        

def obtainOverFlowNode():
  
	'''
	random.seed()   
	random.jumpahead(100)
    
	nodeID = random.choice(range(1,  JOIN_COUNT + 1))
	sys.stderr.write("obtainOverFlowNodeId: " + str(nodeID) + '\n')
	sys.stderr.flush()
	'''
    
	#OPEN handles to all overflow nodes
  
	# channels listed in order from {1 .... JOIN_COUNT}

	for out_file in os.listdir('/dev/out'):
      
		fp = open(os.path.join('/dev/out', out_file), 'ab', buffering = IO_BUFFER_SIZE)        
		fp.write("REQUEST")
		sys.stderr.write("node " + NODE_NAME + ", connected to: " + str(fp) + '\n')
        
	sys.stderr.flush()
    
def load(handle):    
	m = mmap.mmap(handle.fileno(), 1024, access=mmap.ACCESS_READ)
	start = 0
	list = []
	while True:
		end = m.find('.\n', start + 1) + 2
		if end == 1:
			sys.stderr.write("MMAP FAILURE, size of mmap: "  +  str(m.size()) + '\n')
			sys.stderr.flush()
			break
		tupple = cPickle.loads(m[start:end])
		sys.stderr.write("loading from mmap: " + str(tupple) + '\n')
		sys.stderr.flush()
		list.append(tupple)
		start = end
        
	sys.stderr.write("list size from mmap: " + str(len(list)) + '\n')
	sys.stderr.flush()    
	return list
    

'''
#deletes all elements  from a list based upon a list of indexes into the list
def deleteFromList(list, indexes)  
	newlist = []
	indexes.sort(reverse=True)
    
    for i in range(0,len(list)):
'''    
	

#LOAD RELATION PARITIONS FROM SHUFFLERS  
def loadShuffledRelations(handles, tupplesR, tupplesS):
    
	#outer LOOP thru all the shuffler connections
	while handles:
    
    
		#for j in range(0,end): 
		#interate in reverse so deleting elements will not effect subsequent indexing
		for j in range(len(handles) - 1,-1,-1):
      
			sys.stderr.write('\nLOADING FROM SHUFFLER: ' + str(handles[j]) + ' at handles index ' + str(j) + '\n')
			sys.stderr.flush()
        
			tupple = []
          
			i = 0
			#read in 1024 tupples per channel at a time
			while(i < 1024):
    
				i += 1
				try:
					tupple = cPickle.load(handles[j])
					if is_match("R", tupple[-1]):		# R relation
						tupplesR.append(tupple)
						#sys.stderr.write("MATCHED R\n")
						#sys.stderr.flush()
					elif is_match("S", tupple[-1]):	   	# S relation
						tupplesS.append(tupple)
						#sys.stderr.write("MATCHED S\n")
						#sys.stderr.flush()
					else:
						sys.stderr.write('\nCHANNEL LOAD ERROR: ' + str(tupple) + '\n')
						sys.stderr.write('\nCHANNEL LOAD ERROR: ' + str(handles[j]) + '\n')
						sys.stderr.flush()
                        
				except EOFError:
					sys.stderr.write("EOF: closing and deleting channel handle : " + str(handles[j]) + '\n')
					sys.stderr.flush()
					handles[j].close()
					del handles[j]
					break
                
			#end inner while
            
			'''    
			lines = handles[j].readlines(IO_BUFFER_SIZE)
        
			if (not lines or (lines == "")):	#EOF detected
				sys.stderr.write("closing and deleting channel handle : " + str(handles[j]) + '\n'
				sys.stderr.flush()
				handles[j].close()
				del handles[j]
				continue       
            
			for line in lines:
              
				tupple = ast.literal_eval(line.rstrip('\n'))
				#sys.stderr.write("tupple type: " + str(type(tupple)) + '\n')
				#sys.stderr.write("tupple: " + str(tupple) + '\n')
				#sys.stderr.flush()
                
				if is_match("R", tupple[-1]):		# R relation
					tupplesR.append(tupple)
					#sys.stderr.write("MATCHED R\n")
					#sys.stderr.flush()
				elif is_match("S", tupple[-1]):	   	# S relation
					tupplesS.append(tupple)
					#sys.stderr.write("MATCHED S\n")
					#sys.stderr.flush()
				else:
					sys.stderr.write('\nCHANNEL LOAD ERROR: ' + str(tupple) + '\n')
					sys.stderr.write('\nCHANNEL LOAD ERROR: ' + str(handles[j]) + '\n')
					sys.stderr.flush()
					continue
			'''
        
		'''
		while(True):    #load all tupples from this shuffler
          
			#tupples = []
			#tupple = fp.readline()
            
			try:
				#tupples = load(fp)
                #tupple = tupples[-1]
				#tupple = cPickle.load(fp)
                
				line = fp.readline().rstrip('\n')
				if((type(line) == type('')) and is_match("EOF", line)):
					fp.close()
					sys.stderr.write('CLOSED: ' + str(fp) + '\n')
					#sys.stderr.flush()
					break                #collect shuffle tupples from next channel
				else:
					tupple = eval(line)
                
			except: # catch *all* exceptions 
				exc_type, exc_value, exc_traceback = sys.exc_info()
				traceback.print_exception(exc_type, exc_value, exc_traceback)
				fp.close()
				sys.stderr.write('I/O EXECEPTION CLOSED CHANNEL: ' + str(fp) + '\n')
				sys.stderr.flush()
				break	
			
            
			#sys.stderr.write("tupple: " + str(tupple) + '\n')
			#sys.stderr.flush()
		
       
			#INOVATION!!!
			#instead of spiliing to disk when memory is full, 
			#redirects to another special type of Join node. This permits scaling of data sets
			#that are very large or were skewed by the shuffle to a few Join nodes
			global redirect
			if(not redirect):
				if(len(tupplesR) > THRESHOLD or len(tupplesS) > THRESHOLD):
					redirect = True
					#obtain and open channel to OverFlow node
					#redirect any subsequent input to the OverFlow
			#else:
				#write to OverFlow node
        
			if is_match("R", tupple[-1]):		# R relation
				tupplesR.append(tupple)
				#sys.stderr.write("MATCHED R\n")
				#sys.stderr.flush()
			elif is_match("S", tupple[-1]):	   	# S relation
				tupplesS.append(tupple)
				#sys.stderr.write("MATCHED S\n")
				#sys.stderr.flush()
			else:
				sys.stderr.write('\nCHANNEL LOAD ERROR: ' + str(fp) + '\n')
				sys.stderr.flush()
            	continue

		'''
  
        
#DEBUG
#list values in handles, tupplesR, tupplesS that correspond
#to the current state of the last input read from their respective channels
def stateOfRelation(tupples, modulo):
	sys.stderr.write('\nState of relations ************\n')
	
	i = 0
	for t in tupples :
		i += 1
		if((i % modulo) == 0):
			sys.stderr.write(str(t) + '\n')
        
	#sys.stderr.flush()   

#DEBUG
#list open channels for this join node      
def listOpenChannels(channels):
  	sys.stderr.write("LIST OF OPEN CHANNELS\n")
	for channel in channels:
		sys.stderr.write(NODE_NAME + " connected to: " + str(channel) + '\n')
	sys.stderr.flush()  
  
  
  
class EmptyChannels(Exception):
	def __init__(self, value):
		self.value = value
	def __str__(self):
		return repr(self.value)
  
 
def keyCmp(x,y): 
	if x.isdigit() and y.isdigit():
		x=int(x)
		y=int(y)
	return cmp(x,y)
      
def sort_by_key(tupples, key_col_index, reverse=False):
  
	sys.stderr.write("IN PLACE SORT\n")
	#sys.stderr.flush()
    
	'''
	Sorts tupples contents by key name (if col argument is type <str>) 
	or column index (if col argument is type <int>). 
	'''
    
	if isinstance(key_col_index, str):  
		col_index = int(key_col_index)
	else:
		col_index = key_col_index
        
	#MUCH SLOWER, but does not need:  import operator, not supported in ZeroVM Cloud
	#body = sorted(body, key =  lambda x :  x[col_index], reverse=reverse)
    #cmp=lambda x,y: cmp(x.lower(), y.lower())
    
    #IN PLACE SORT
	tupples.sort(cmp=lambda x,y: keyCmp(x,y), 
                   key=operator.itemgetter(col_index), 
                   reverse=reverse)
 
  
#                        MAIN EXECUTION SEQUENCE 

def joinMain():
  
	#open input channels to shuffler nodes
	openInputChannels(shuffleChannels,"shuffle")
	#openChannels(shuffleChannels, '/dev/in/shuffle_R', 'rb', JOIN_COUNT)
	#listOpenChannels(shuffleChannels)   #debug
    
	#open output channels to overflow nodes, ordered (1..JOIN_COUNT)
	#openChannels(overflowChannels, '/dev/out/overflow', 'ab', JOIN_COUNT)
	#listOpenChannels(overflowChannels)   #debug
    
	#open input channels from overflow nodes, ordered (1..JOIN_COUNT)
	#openChannels(overflowChannels, '/dev/in/overflow', 'rb', JOIN_COUNT)
	#listOpenChannels(overflowChannels)   #debug
    
	#openInputChannels(overflowChannels,"overflow")
	#listOpenChannels(overflowChannels)   #debug
    
	#openOutputChannels(overflowChannels,"overflow")
	#listOpenChannels(overflowChannels)   #debug

	#load R,S tupple partitions from shufflers
	loadShuffledRelations(shuffleChannels, listOfTupplesR, listOfTupplesS)

	#sort R, S paritions
	sort_by_key(listOfTupplesR, KEY_COL_INDEX, reverse=False)
	sort_by_key(listOfTupplesS, KEY_COL_INDEX, reverse=False)
        
	#debug, look at size of each partition
	sys.stderr.write("R  # of tupples: " + str(len(listOfTupplesR)) + '\n')
	sys.stderr.write("S  # of tupples: " + str(len(listOfTupplesS)) + '\n')
	sys.stderr.flush()

    #JOIN R,S partitions
	joinedTupples = join(listOfTupplesS, listOfTupplesR, KEY_COL_INDEX)
	sys.stderr.write("****************JOINED  # of tupples: " + str(len(joinedTupples)) + '\n')
	sys.stderr.flush()
	#stateOfRelation(joinedTupples, 2)

	#obtainOverFlowNode()
    
	sys.stderr.write ("\nJOIN COMPLETE LINE")
	sys.stderr.flush()
	sys.exit(0)


    #WRITE JOINED tupple to MERGER
    
    
    
# END DEFINITIONS *****************************************************************

joinMain()
#profile.run('joinMain()')
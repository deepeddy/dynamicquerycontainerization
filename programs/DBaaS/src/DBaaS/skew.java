package DBaaS;

import java.io.*;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.StringTokenizer;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

//skew program works in two modes: 
//one) to count tuples by key,
//two) based upon HASHING, list of keys per JoinNode and their respective counts
//assumes file has rows of tuples, with each attribute delimited by a ','
public class skew {

	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		
		
		//Comparator<String> comp = new Comparator<String>;

		TreeMap<String, HashMap<String,Integer[]>> map = new TreeMap<String, HashMap<String,Integer[]>>(new BucketComparator());
		

		//if 'hashed' hash key, if 'raw' count # tuples with same key
		//EXAMPLE: for column 3, using hashing to map tuples to JoinNodes, with 32 Join nodes in file natalityR1.csv
		//		$> skew 3 hashed 32 natalityR1.csv    
		//EXAMPLE: for column 8, to list all keys and their tuple count in file natalityR1.csv
		//		$> skew 8 unhashed 0 natalityR1.csv 
		if (args.length < 3) {
			System.out.println("usage: skew <keyCol> ('hashed' | 'unhashed') moduloBase [<fileName>]*");
			System.exit(1);
		}
		
		Boolean hashed = true;
		
		int keyCol = Integer.parseInt(args[0]);
		if(args[1].equals("unhashed"))
			hashed = false; 
		int moduloBase = Integer.parseInt(args[2]);
		
		System.out.println("FILE(s):");  //print the name of all relation files
		for(int i = 3; i < args.length; i++) {
			System.out.println(args[i]);
		}
		
		//iterate thru all relation files
		for(int i = 3; i < args.length; i++) {
			analyzePartition(map, keyCol, hashed, moduloBase, args[i]);
		}
		
		//print a report of tuples and keys per JoinNode, and tuples joined
		printMap(map, hashed, moduloBase);

	} // end main
	
	public static void printMap(TreeMap<String, HashMap<String,Integer[]>> map, boolean hashed, int moduloBase) {
		
		if (hashed)
			System.out.println("Tuple input totals for each hash bucket spread across modulo: " +  moduloBase);
		else
			System.out.println("Tuple input totals for each key:");
			
		PrintWriter out = null;
		//open CSV file
		try {
			out = new PrintWriter("Skew.csv");
			//write CSV header
			out.write("R Count, S Count, Total Joins\n");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		//print out map
		for( Entry<String, HashMap<String,Integer[]>> entry :  map.entrySet()) {
			
			String bucket = entry.getKey();
			HashMap<String,Integer[]> bucketMap = entry.getValue();
			
			if (hashed)
				System.out.println("JoinNode : " + bucket);
			
			int totalR = 0;
			int totalS = 0;
			int totalJoins = 0;
			for( Entry<String, Integer[]> bucketEntry :  bucketMap.entrySet()) {
				String key = bucketEntry.getKey();
				Integer[] count = bucketEntry.getValue();
				totalR += count[0]; 
				totalS += count[1];
				totalJoins += count[0] * count[1];
				if(hashed)
					System.out.println(" key: " + key + ",  R count: " + count[0] + ", S count: " + count[1]);
				else
					System.out.println(" key: " + key + ",  R count: " + count[0] + ", S count: " + count[1] + ", TOTAL Tuples " + (totalR + totalS));
			} // end inner for
			
			if (hashed)
				System.out.println(" R count: " + totalR  + ", S count: " + totalS + ", TOTAL Tuples: " + (totalR + totalS) + ", TOTAL JOINS " + totalJoins);

			
			out.write(totalR  + "," + totalS + "," + totalJoins + "\n");
			
		} // end outer for
		
		out.close();
		
	}
	

	public static void analyzePartition(TreeMap<String, HashMap<String,Integer[]>> map, int keyCol, Boolean hashed, int moduloBase, String FileName) throws IOException {

		//open file
		BufferedReader input = null;
		
		String pattern = "[^...]*R[1-9]+\\.csv";
		String relation = "S";

	     // Create a Pattern object
	     Pattern r = Pattern.compile(pattern);

	     // Now create matcher object.
	     Matcher m = r.matcher(FileName);
	      
	      if (m.find( ))
	    	  relation = "R";
	      
	      System.out.println("Relation " + relation);
	    	  
	    	  
		
		try {

			input = new BufferedReader(new FileReader(FileName));

			String tuple = null;
			String key = null;

			while((tuple = input.readLine()) != null) {

				//get key
				key =   getKeyFromTuple(keyCol, tuple);
	
				if (hashed) { //hash the key
					
				
					
					String bucket = String.valueOf( Math.abs(key.hashCode()) %  moduloBase);
					
					if (map.containsKey(bucket)) {   //map already has an entry for the mapped bucket
						
						//get list of keys for this bucket
						HashMap<String,Integer[]> listOfKeysInThisBucket = map.get(bucket);
						
						if (listOfKeysInThisBucket.containsKey(key)) {    //bucket already this key in its list of keys
							Integer[] count = listOfKeysInThisBucket.get(key);
							//listOfKeysInThisBucket.get(key)[0] += listOfKeysInThisBucket.get(key)[0]; 
							if(relation.equals("R"))
								count[0] += 1;    //[0] is holds count for relation R
							else 
								count[1] += 1;	 //[1] is holds count for relation S
							listOfKeysInThisBucket.put(key, count);
						}
						else { //add new key to listOfKeysInThisBucket
							Integer[] count = new Integer[2];
							if(relation.equals("R")) {
								count[0] = new Integer(1);    //[0] is holds count for relation R
								count[1] = new Integer(0);
							}
							else { 
								count[1] = new Integer(1);	  //[1] is holds count for relation S
								count[0] = new Integer(0);
							}
							listOfKeysInThisBucket.put(key, count);
							map.put(bucket, listOfKeysInThisBucket);
							//System.out.println("adding new tuple key " +  key  + " to Bucket " + bucket);
						}
							
					}
					else {
						//load bucket first time
						//System.out.println("key " + key + " placed in bucket " + bucket);
						HashMap<String,Integer[]> listOfKeysInThisBucket =  new HashMap<String,Integer[]>(); //get new bucket's list
						Integer[] count = new Integer[2];
						if(relation.equals("R")) {
							count[0] = new Integer(1);    //[0] is holds count for relation R
							count[1] = new Integer(0);
						}
						else {
							count[1] = new Integer(1);	  //[1] is holds count for relation S
							count[0] = new Integer(0);
						}
						listOfKeysInThisBucket.put(key, count);               //add the key to the list
						map.put(bucket, listOfKeysInThisBucket);              //add the bucket to the map
					}
				}  
				else {   //keys are not hashed, just report number number of tuples for every key  
					if (map.containsKey(key)) {
						//update  count
						HashMap<String,Integer[]> singleKeyMap = map.get(key);
						Integer[] count = singleKeyMap.get(key);    //here map has only a SINGLE key
						if(relation.equals("R"))
							count[0] += 1;    //[0] is holds count for relation R
						else 
							count[1] += 1;	  //[1] is holds count for relation S
					    singleKeyMap.put(key, count);
					}
					else {
						
						//load key into a bucket first time
						//System.out.println("add key " + key + " to map");
						HashMap<String,Integer[]> singleKeyMap =  new HashMap<String,Integer[]>();
						Integer[] count = new Integer[2];
						if(relation.equals("R")) {
							count[0] = new Integer(1);    //[0] is holds count for relation R
							count[1] = new Integer(0); 
						}
						else { 
							count[1] = new Integer(1);	  //[1] is holds count for relation S
							count[0] = new Integer(0); 
						}
						singleKeyMap.put(key, count);
						map.put(key, singleKeyMap);
					}
					
				}

			} // end while

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			input.close();
		}
		//end try


	} // end  analyzePartition


	public static String getKeyFromTuple(int column, String tuple) {

		StringTokenizer tokenizer = new StringTokenizer(tuple, ",");

		String key = null;
		for(int i = 1; i <= column; i++)
			key = tokenizer.nextToken();

		return key;

	}

} //end class skew

class BucketComparator implements Comparator<String>{
	 
    @Override
    public int compare(String e1, String e2) {
    	
    	int value = 0;
    	
    	if (isNumeric(e1) && isNumeric(e2)) {
    	
    		if (Double.parseDouble(e1) < Double.parseDouble(e2))
    			value = -1;
        
    		if (Double.parseDouble(e1) > Double.parseDouble(e2))
    			value =  1;
    		
    	}
    	else {
    		value = e1.compareTo(e2);
    	}
    	
        return value;
    }
    
    private boolean isNumeric(String str)
    {
        for (char c : str.toCharArray())
        {
            if (!Character.isDigit(c)) return false;
        }
        return true;
    }
}

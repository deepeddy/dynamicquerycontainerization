#!python
# -*- coding: utf-8 -*-
import os
import csv
import operator
import sys
import traceback
import cPickle
import io

JOIN_COUNT = int(os.environ['JOIN_COUNT'])
NODE_TYPE = os.environ['SCRIPT_NAME']
KEY_COL_INDEX = int(os.environ['KEY_COL_INDEX'])
RELATION_TAG = os.environ['RELATION_TAG']
IO_BUFFER_SIZE = (2<<18) + 8

#DECLARE List of connection handles to store handles to all join nodes 
JoinConnectionHandles = []


#READING PARAMETER INTO PYTHON SCRIPT FROM JSON FILE USING:  
#	
#		Python fragment
#		os.environ['someParameter']
#
#
#		JSON fragment
#		"exec": {
#            "path": "file://python:python",
#			 "env": {
#				"someParameter": 4
#			  }
#        }
#
#
#ALSO CONSTRUCT WHICH SEQUENCER TO CONNECT TO USING:	os.environ['SCRIPT_NAME']
#AND A MODULO AND STRING FUNCTIONS TO FORM THE PATH TO A SPECIFIC SEQUENCER USING THE CONNECTION IDS
#THAT ARE GUARANTEED TO BE PROVIDED BY THE ZEROVM NETWORK LAYER. SEE THE JSON SERVLET DOC


def csv_to_list(csv_file, delimiter=','):
	""" 
	Reads in a CSV file and returns the contents as list,
	where every row is stored as a sublist, and each element
	in the sublist represents 1 cell in the table.
  
	"""
	#sys.stderr.write(csv_file)
	with open(csv_file, 'rb', buffering=(2<<18) + 8) as fp:
		reader = csv.reader(fp, delimiter=delimiter)
		csv_list = list(reader)
		fp.close()
		return csv_list
      
	#cmp=lambda x,y: cmp(x.lower(), y.lower())
    
def sortCmp(x,y): 
	if x.isdigit() and y.isdigit():
		x=str(x)
		y=str(y)
	return cmp(x,y)

  
#sorts a list of rows/tuples by one of its columns/fields
# col must an int
def sort_by_column(rows, col, reverse=False):
  	
    
	if not isinstance(col, int ):  
		sys.stderr.write("*****FATAL**************COL INDEX NOT AN INTEGER ************************")
		sys.stderr.flush()
		sys.exit(0)
	else:
		rows = sorted(rows, cmp=lambda x,y: sortCmp(x,y), key=operator.itemgetter(col), reverse=reverse)

	return rows
  
#hash function that maps/SHUFFLES a tupple to a particular join nodeID based upon
#the value of the  key located in column 'col'
#the hash is then mapped to a node based upon modulo the number of join nodes avaliale
def mapTupple(tupple, col, modulo_base):
	keyValue = tupple[col]
	#node connections start at 1, {1 .... modulo_base}
	nodeId = (hash(keyValue) % modulo_base) + 1
    
	#sys.stderr.write ("\ntupple: " + str(tupple) + '\n')
	#sys.stderr.write ("key_col " + str(col) + ", key_value " + keyValue + '\n')
	#sys.stderr.flush()
    
	return nodeId
  


#SHUFFLE step, iterate thru, writing each tupple to a join node based upon its key,
# all tupples with the same key are shipped to the same join node
def shuffle(listOfTupples, key_col_index, JOIN_COUNT, handles):
  
	debugIndex = 0
    
	for tupple in listOfTupples:
		joinNodeId = mapTupple(tupple, key_col_index, JOIN_COUNT)
		#shuffle tupple to correct join node, tag tupple for later joining identification,
		#join must identify which relation tupple comes from
		
    
		#append RELATION type tag to end of tupple
		tupple.append(NODE_TYPE)
        
		'''
		sys.stderr.write(str(tupple[-1]) + ":, shuffleNodeId is: " + str(joinNodeId) + ", writing to: " 
                          + str(handles[joinNodeId - 1]) + '\n')
		sys.stderr.flush()
		'''
      
		#cPickle.dump(tupple,handles[joinNodeId - 1])
		JoinConnectionHandles[joinNodeId - 1].write(str(tupple) + '\n')
		#sys.stderr.write ("\nWRITTEN tupple: " + str(tupple) + '\n')
		#sys.stderr.flush();
      
		'''
		debugIndex += 1
		#if (debugIndex > 30000):
		if (debugIndex > 100):
			break;
		'''
	
          
#OPEN handles to all join nodes
def openJoinConnections(handles):
  
	# channels listed in order from {1 .... JOIN_COUNT}
	for out_file in os.listdir('/dev/out'):
      
		fp = open(os.path.join('/dev/out', out_file), 'ab', buffering = IO_BUFFER_SIZE)
		handles.append(fp)
        
		sys.stderr.write("node " + NODE_TYPE + ", connected to: " + str(handles[-1]) + '\n')
		sys.stderr.flush()

          
#send EOF messges to all joins connected to this node
def closeConnections(handles):
  
	for fp in handles:
		#cPickle.dump("EOF "+ NODE_TYPE, fp)
		#handles[i].write("EOF "+ NODE_TYPE + '\n')
		#fp.write("EOF " + NODE_TYPE + '\n')
		fp.flush()
		fp.close()
        
		#DEBUG
		sys.stderr.write("node type is: " + NODE_TYPE + ", disconnected from: " +
                       str(fp) + '\n')
		sys.stderr.flush();
      
#END  FUNCTION DEFs *************************************************         

	
                  
#GET TUPPLES from the .csv file, a list of lists(tupples), where every line is a list of csv formatted fields         
listOfTupples = csv_to_list('/dev/input', ",")


#OPEN shuffle channels to joins
openJoinConnections(JoinConnectionHandles)

#SHUFFLE STEP
shuffle(listOfTupples, KEY_COL_INDEX, JOIN_COUNT, JoinConnectionHandles)

#CLOSE
closeConnections(JoinConnectionHandles)


sys.stderr.write("\nSHUFFLE COMPLETE: " + NODE_TYPE + '\n')
sys.stderr.flush()
    

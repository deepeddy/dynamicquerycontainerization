#!python
import os
import math
import traceback
import sys
import cPickle
import re
import ast
import operator
import random

inp_dir = '/dev/in'
stdout = '/dev/stdout'

#GLOBAL CONSTANTS
JOIN_COUNT = int(os.environ['JOIN_COUNT'])
NODE_NAME = os.environ['SCRIPT_NAME']
KEY_COL_INDEX = int(os.environ['KEY_COL_INDEX'])
THRESHOLD = int(os.environ['THRESHOLD'])
EOF = os.environ['EOF']

#GLOBAL VARIABLES
shuffleChannel = []
joinChannels = []
overflowChannels = []
listOfTupplesR = []     #R and S relations to be joined
listOfTupplesS = []
listOfKeys = []

#INOVATION!!!
#instead of spiliing to disk when memory is full, 
#redirects to another special type of Join node. This permits scaling of data sets
#that are very large or were skewed by the shuffle to a few Join nodes
redirect = False
       

def is_match(regex, text):
	pattern = re.compile(regex)
	return pattern.search(text) is not None
  

#hash relation by key, returns hashMap  
def hashRelation(relation, key_col_index):
  
	#a (key,listOfIndexes) map of a relation.  
	#The listOfIndexes is a list of indexes for a set of tupples with the same key.
	hashMap = {} 
  
	for i in xrange(0, len(relation)):
		key = relation[i][key_col_index]  
		if (key in hashMap):
			list = hashMap[key]
			list.append(i)      	# add the new index for key
		else:
			hashMap[key] = [i]   # add key with initial list set to [i] 
            
	return hashMap
        	

#implements a classical hash-join
#R,S are two tables of tupples to be equ-joined by a common key, returns joined relation/table
def join(R, S, key_col_index): 
  
  	joined = []
    
	#scan the bigger relation and hash the smaller
	if(len(R) > len(S)):
		relationToScan = R
		relationToHash = S
	else:
		relationToHash = S
		relationToScan = R
    
	hashMap = hashRelation(relationToHash, key_col_index)
	#sys.stderr.write("relation hashed: " + str(hashMap) + '\n')
	#sys.stderr.flush()
    
	for tupple in relationToScan:
      
		key = tupple[key_col_index]
        
		if(key in hashMap):
			indexList = hashMap[key]
		else:
			continue
        
		#sys.stderr.write("joining tuple: " + str(tupple) + '\n')
		#sys.stderr.flush()
    
		'''
		for i  in indexList:
			joined.append(tupple + relationToHash[i])
		'''         
		#replace innner loop with List comprehensions for speedup 
		newList = [(tupple + relationToHash[i]) for i in indexList]
		joined.extend(newList)
            
	return joined
 
  
  
#OPEN LIST OF INPUT  CHANNELS
#list of handles for channels, type of channel, e.g. "shuffle", "overflow".
#see manifest
def openInputChannels(handles, type):
  
	sys.stderr.write("OPEN CHANNELS\n")
    
	for in_file in os.listdir('/dev/in'):
  
		if (is_match(type, in_file)):
			fp = open(os.path.join('/dev/in', in_file), 'rb', buffering=(2<<18) + 8)
			handles.append(fp)
			sys.stderr.write("node " + NODE_NAME + ", connected to: " + str(handles[-1]) + '\n')
        
	sys.stderr.flush()
    
    
#OPEN LIST OF  OUTPUT CHANNELS
#list of handles for channels, type of channel, e.g. "shuffle", "overflow".
#see manifest
def openOutputChannels(handles, type):
  
	sys.stderr.write("OPEN CHANNELS\n")
    
	for in_file in os.listdir('/dev/out'):
  
		if (is_match(type, in_file)):
			fp = open(os.path.join('/dev/out', in_file), 'ab', buffering=(2<<18) + 8)
			handles.append(fp)
			sys.stderr.write("node " + NODE_NAME + ", connected to: " + str(handles[-1]) + '\n')
        
	sys.stderr.flush()   
        
def obtainOverFlowNode():
  
	'''
	random.seed()   
	random.jumpahead(100)
    
	nodeID = random.choice(range(1,  JOIN_COUNT + 1))
	sys.stderr.write("obtainOverFlowNodeId: " + str(nodeID) + '\n')
	sys.stderr.flush()
	'''
    
	#OPEN handles to all overflow nodes
  
	# channels listed in order from {1 .... JOIN_COUNT}

	for out_file in os.listdir('/dev/out'):
      
		fp = open(os.path.join('/dev/out', out_file), 'ab', buffering=(2<<18) + 8)        
		fp.write("REQUEST")
		sys.stderr.write("node " + NODE_NAME + ", connected to: " + str(fp) + '\n')
        
	sys.stderr.flush()
  
  
#LOAD RELATION PARITIONS FROM SHUFFLERS  
def loadShuffledRelations(handles, tupplesR, tupplesS):
    
	for fp in handles:    #outer LOOP thru all the shuffler connections
      
		sys.stderr.write('\nSHUFFLE LOADING FROM: ' + str(fp) + '\n')
		sys.stderr.flush()
      
		while(True):    #load all tupples from this shuffler	
        
			#tupple = fp.readline()
			try:
				tupple = cPickle.load(fp)
			except: # catch *all* exceptions 
				exc_type, exc_value, exc_traceback = sys.exc_info()
				traceback.print_exception(exc_type, exc_value, exc_traceback)
				fp.close()
				sys.stderr.write('I/O EXECEPTION CLOSED CHANNEL: ' + str(fp) + '\n')
				sys.stderr.flush()
				break
        	
			if((type(tupple) == type('')) and is_match("EOF", tupple)):
				fp.close()
				sys.stderr.write('CLOSED: ' + str(fp) + '\n')
				#sys.stderr.flush()
				break                #collect shuffle tupples from next channel
            
			#sys.stderr.write("tupple: " + str(tupple) + '\n')
			#sys.stderr.flush()
		
        
			#INOVATION!!!
			#instead of spiliing to disk when memory is full, 
			#redirects to another special type of Join node. This permits scaling of data sets
			#that are very large or were skewed by the shuffle to a few Join nodes
			global redirect
			if(not redirect):
				if(len(tupplesR) > THRESHOLD or len(tupplesS) > THRESHOLD):
					redirect = True
					#obtain and open channel to OverFlow node
					#redirect any subsequent input to the OverFlow
			#else:
				#write to OverFlow node
                  		
        
			if is_match("R", tupple[-1]):		# R relation
				tupplesR.append(tupple)
				#sys.stderr.write("MATCHED R\n")
				#sys.stderr.flush()
			elif is_match("S", tupple[-1]):	   	# S relation
				tupplesS.append(tupple)
				#sys.stderr.write("MATCHED S\n")
				#sys.stderr.flush()
			else:
				sys.stderr.write('\nCHANNEL LOAD ERROR: ' + str(fp) + '\n')
				sys.stderr.flush()
            	continue
  
        
#DEBUG
#list values in handles, tupplesR, tupplesS that correspond
#to the current state of the last input read from their respective channels
def stateOfRelation(tupples):
	sys.stderr.write('\nState of relations ************\n')

	for t in tupples :
		sys.stderr.write(str(t) + '\n')
        
	#sys.stderr.flush()   

#DEBUG
#list open channels for this join node      
def listOpenChannels(channels):
  	sys.stderr.write("LIST OF OPEN CHANNELS\n")
	for channel in channels:
		sys.stderr.write(NODE_NAME + " connected to: " + str(channel) + '\n')
		#sys.stderr.flush()  
  
  
  
class EmptyChannels(Exception):
	def __init__(self, value):
		self.value = value
	def __str__(self):
		return repr(self.value)
  
 
def keyCmp(x,y): 
	if x.isdigit() and y.isdigit():
		x=int(x)
		y=int(y)
	return cmp(x,y)
      
def sort_by_key(tupples, key_col_index, reverse=False):
  
	sys.stderr.write("IN PLACE SORT\n")
	#sys.stderr.flush()
    
	'''
	Sorts tupples contents by key name (if col argument is type <str>) 
	or column index (if col argument is type <int>). 
	'''
    
	if isinstance(key_col_index, str):  
		col_index = int(key_col_index)
	else:
		col_index = key_col_index
        
	#MUCH SLOWER, but does not need:  import operator, not supported in ZeroVM Cloud
	#body = sorted(body, key =  lambda x :  x[col_index], reverse=reverse)
    #cmp=lambda x,y: cmp(x.lower(), y.lower())
    
    #IN PLACE SORT
	tupples.sort(cmp=lambda x,y: keyCmp(x,y), 
                   key=operator.itemgetter(col_index), 
                   reverse=reverse)
 
  
#                        MAIN EXECUTION SEQUENCE 

def main():
  
	#open channels to shuffler nodes
	openInputChannels(shuffleChannels,"shuffle")
	listOpenChannels(shuffleChannels)   #debug
    
	openInputChannels(overflowChannels,"overflow")
	listOpenChannels(overflowChannels)   #debug
    
	openOutputChannels(overflowChannels,"overflow")
	listOpenChannels(overflowChannels)   #debug

	#load R,S tupple partitions from shufflers
	loadShuffledRelations(shuffleChannels, listOfTupplesR, listOfTupplesS)

	#sort R, S paritions
	sort_by_key(listOfTupplesR, KEY_COL_INDEX, reverse=False)
	sort_by_key(listOfTupplesS, KEY_COL_INDEX, reverse=False)
        
	#debug, look at size of each partition
	sys.stderr.write("R  # of tupples: " + str(len(listOfTupplesR)) + '\n')
	sys.stderr.write("S  # of tupples: " + str(len(listOfTupplesS)) + '\n')
	sys.stderr.flush()

    #JOIN R,S partitions
	joinedTupples = join(listOfTupplesS, listOfTupplesR, KEY_COL_INDEX)
	sys.stderr.write("****************JOINED  # of tupples: " + str(len(joinedTupples)) + '\n')
	sys.stderr.flush()
	#stateOfRelation(joinedTupples)

	#obtainOverFlowNode()
    
	sys.stderr.write ("\nJOIN COMPLETE LINE")
	sys.stderr.flush()
	sys.exit(0)


    #WRITE JOINED tupple to MERGER
    
    
    
# END DEFINITIONS *****************************************************************

main()
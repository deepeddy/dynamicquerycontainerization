package dbaasproj;

public class UTSize {

	public static void main(String[] args) {
		// 
		double[] joinSelectivities = {0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0};
		// number of tables to join
		int[] numberOfTables = {10, 20, 30, 50, 100, 150, 200, 500}; 
		
		int k = 100; // number of columns per table
		int c = 100; // number of bytes per column
		double N = 1.0;   // number of tuples per table
		for (int i=0; i<joinSelectivities.length; i++) {
			double s = joinSelectivities[i]; // Join selectivity
			System.out.println("s= " + s);
			for (int j=1; j<10; j++) {
				N = N * 1000;
				System.out.print("N=" + N + ": ");
				for (int t=0; t<numberOfTables.length; t++) {
					int m = numberOfTables[t]; // number of tables to join
					double size = (N*Math.pow(N*s, (m-1)))*(m*k-m)*c;
					System.out.print("" + (size/1.0E15) + "PB, ");
				}
				System.out.println();								
			}
		}
	}
}

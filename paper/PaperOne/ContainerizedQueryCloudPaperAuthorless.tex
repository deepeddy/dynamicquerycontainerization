%% LyX 2.2.3 created this file.  For more info, see http://www.lyx.org/.
%% Do not edit unless you really know what you are doing.
\documentclass[10pt,letterpaper,english,10pt, conference]{IEEEtran}
\usepackage[T1]{fontenc}
\usepackage[latin9]{inputenc}
\usepackage{float}
\usepackage{url}
\usepackage{amstext}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage[numbers]{natbib}

\makeatletter

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% LyX specific LaTeX commands.
\special{papersize=\the\paperwidth,\the\paperheight}

\floatstyle{ruled}
\newfloat{algorithm}{tbp}{loa}
\providecommand{\algorithmname}{Algorithm}
\floatname{algorithm}{\protect\algorithmname}

\@ifundefined{date}{}{\date{}}
\makeatother

\usepackage{babel}
\begin{document}

\title{\textbf{Distributing an SQL query over a cluster of containers}}
\maketitle
\begin{abstract}
Emergent software container technology is now available on any cloud
and opens up new opportunities to execute and scale data intensive
applications wherever data is located. However, many traditional databases
hosted on clouds have not scaled well. In this paper, a framework
and methodology to containerize a SQL query is presented, so that,
a single SQL query can be scaled and executed by a network of cooperating
containers, achieving operator parallelism and other significant performance
gains. Results of container prototype experiments are reported and
compared to a real-world DBMS baseline. Preliminary result on a research
cloud shows up to 3-orders of magnitude performance gain for some
queries when compared to running the same query on a single VM hosted
DBMS baseline.
\end{abstract}

\begin{IEEEkeywords}
Software container, SQL database, query evaluation, operator parallelism,
cloud computing
\end{IEEEkeywords}


\section{Introduction\label{sec:Introduction}}

Container technology, such as Docker \citep{AFG2010:CloudComputing},
has become universally available on all kinds of cloud platforms.
Containers provide an OS-level virtualization \citep{B2014Containers}
that provide applications with an isolated execution environment using
cloud host resources. Running applications inside containers, rather
than in virtual machines (VMs), on a cloud can provide many benefits:
elasticity, scalability, host independence, multi-tenant use, a smaller
footprint and is overall better economy(Cloud Consolidation). 

While many new applications have been designed to run in containers,
progress as been slow to adapt existing SQL database systems to take
full advantage of container platforms. A typical installation of an
SQL database in a cloud runs the database engine on a VM, resulting
in poorer performance than otherwise on a stand-alone physical host.
There has been research on whether container technology can be used
for scaling relational queries across many containers to achieve HPC
level performance \citep{ACK2015HPC}. It is our contention that database
queries can indeed be containerized and scaled, but containerization
requires a special container architecture to meet specific needs of
SQL queries \citep{Zhang2015ContainerizedQuery}. 

In this paper, the problems of containerizing SQL queries is considered.
To our knowledge, this has not been reported in the literature. Specifically,
a framework to containerize an SQL query is presented, so that, a
single SQL query can be executed by a network of cooperating containers.
Containerizing an SQL query to execute using multiple distributed
containers faces several challenges: 1) The computation of the query's
relational operators should be divided among many containers to obtain
in-parallel performance gains; 2) Many containers must collectively
load and keep data relations all-in-memory including any intermediate
results throughout the entire query's evaluation, which provides further
evaluation speed-up by eliminating DBMS disk buffer management of
data \citep{S2008OLTPthroughLookingGlass}; 3) Orchestrate scheduling
of multiple cooperating containers across many cloud hosts, while
carefully synchronizing their interactions during the evaluation.
These issues are addressed in this paper. Contributions in this paper
are as follows:
\begin{enumerate}
\item A framework(methodology \& container architecture) for query containerization,
which for a given SQL query, derives an execution plan for a network
of specialized cooperating containers, and deploys the execution plan
using a container technology specific platform, e.g. Docker, in a
cloud.
\item An implementation of a research prototype, including containerized
query plan compiler. 
\item A set of experiments using Docker on a research cloud comparing performance
metrics against a real-world DBMS baseline to validate the framework.
Preliminary result shows up to 3-orders of magnitude performance gain
for containerization over running the same query on the standalone
DBMS baseline hosted on a OpenStack VM.
\end{enumerate}
The rest of the paper is organized as follows. In Section \ref{sec:A-Framework},
a containerized query framework is presented. Section \ref{sec:Prototype},
describes an implementation of the framework. In Section \ref{sec:AExperiments},
results from a set of test-bed experiments are discussed. In Section
\ref{sec:Related-Work}, discusses important related work, and Section
\ref{sec:Conclusions}, concludes with a summary and discussion of
future work.

\section{A Framework for Query Containerization\label{sec:A-Framework}}

For the purpose of this paper, we define a SQL query\textit{\emph{
containerized when all relational operators in the query tree have
been mapped into a cluster of cooperating networked containers that
execute the query's plan. }}

\begin{figure*}
\includegraphics[scale=0.43]{queryProcessVer2}

\caption{Query Containerization Framework Work-Flow\label{fig:Query-process}}
\end{figure*}

Fig. \ref{fig:Query-process} shows the work-flow for the query containerization
framework. (A) The SQL statement is first compiled by a relational
query optimizer, output as an optimized query tree. (B) The query
tree is then used as input to construct a digraph, representing a
set of specialized networked containers that evaluate the query. (C)
The digraph is further compiled into an execution plan, which is output
as a data serialization language (DSL) formatted file, e.g. YAML,
JSON. (D) The DSL file is used as a command stream by the underlying
container platform's orchestrations tools to allocate and schedule
containers that evaluate the query. 

\begin{figure}[h]
\includegraphics[scale=0.5]{PhysicalClusterVer3}

\caption{Containerized Query across many VMs \label{fig:Containerized-Query-across}}
\end{figure}

Fig. \ref{fig:Containerized-Query-across} shows a scenario executing
a containerized query across a cluster of containers hosted on many
cloud VMs. Shown are three types of containers: Master, Worker, and
Data-Only. Each query is progressed and orchestrated at run-time by
a master. The master begins by compiling an evaluation plan, then
dispatches the plan, formatted as a DSL configuration file to container
specific platform tools to schedule and start worker containers. Subsequently
the master progresses query evaluation with control messages to-and-from
worker containers. Query relational operations are executed by the
cooperating worker containers; each performs a specific query operation
such as join, selection, projection, as well as operations needed
to transfer data among workers. The data-only containers serve workers
as a uniform interface for storage and retrieval of data from the
underlying cloud storage. All containers run are networked together
by a virtual private LAN. 

\section{Implementation of a Prototype\label{sec:Prototype}}

This section describes an experimental research prototype implementation
of the container query framework. For a simple implementation, widely
available off-the-shelf software were used to implement the many query
system functions. Specifically, a lightweight SQL engine, SQLite,
is embedded inside the Worker container image to execute relational
operators. Containers interface to an external messaging service,
RabbitMQ, to facilitate message communication among master and workers.
These services amount to additional layers in the container images.
Plan generation, serialization and query run-time management functions,
e.g. were written as Java source code. Java's Object Database Connectivity
API (JDBC) was used to provide container functions a plug-and-play
interface for any off-the-shelf lightweight SQL engine. The following
subsections discuss specific issues related to the prototype.

\subsection{Container Images\label{subsec:Container-Images}}

The Master and Worker container images are built using a software
stack model. Both start with an Ubuntu 14.04 operating system base
layer. Subsequent layers in the images stack include a Java JVM, and
Java binaries for both Master and Worker program functions. A subsequent
layer includes RabbitMQ message interface API binaries. The message
layer permits run-time control messages to-and-from master and workers
to monitor, synchronize and progress the query. The Master image includes
our custom written query plan compiler, which interfaces to Docker
container platform orchestration tools, Docker-Compose, Docker-Swarm.
Worker functions delegate relational operations to SQLite, but manage
result data flows using special non-relational operators described
in Section \ref{subsec:DataFlow-Management-Operators}. The Data-Only
image uses the same base layer, but contains only functions to manage
data and provide a uniform interface to access relation data sets
for Workers.

\subsection{Data Flow Management Operators\label{subsec:DataFlow-Management-Operators}}

In addition to the query's relational operators executed by containers,
four non-relational operators are used to transfer intermediate results
among containers. 
\begin{itemize}
\item Pull: is used by the down-stream worker to retrieve relation or intermediate
data from an up-stream worker. 
\item \textit{\emph{Push}}: is used by the up-stream worker to send relation
or intermediate data to a down-stream worker.
\item Exchange: redistribute tuples between containers, using One-To-Many
or Many-to-One distribution patterns.
\item Merge: used to collate down-stream results of parallel operations.
\end{itemize}
Whether a push or a pull is used to transfer data between two workers
will depend on the relational operators the workers are assigned to
execute. For example, as shown in Fig. \ref{fig:Synchronizing-the-container},
a worker executing a hash-join(probe/build) may pull tuples from a
worker that produces tuples for the probe relation, while letting
another worker that produces the tuples for the build relation push
its data. 

An Exchange operator is introduced to further organize how push and
pull are used to transfer intermediate results. This operator redistributes
input data from multiple up-stream workers or data-only containers
as output to multiple down-stream workers. Exchanging involves selecting
a distribution pattern, e.g exchanging by hash key, exchanging evenly
data among recipients etc. Exchange patterns allow reading from multiple
data sources and distributing to multiple destinations. 

For example, in Fig. \ref{fig:Example-workflow} (C), an Exchange
operator is used to distribute tuples from a data-only container managing
relation $R$ to the three workers that perform the join operation
in parallel; using a One-To-Many join key hash mapping from R to the
distinct join containers . In this case the distribution pattern of
the tuples is based upon the hash values of join attribute $R.B3$.
As alluded to, the Exchange is a general redistribution operator,
not always a hashing function. Other Exchange patterns include equal-size
block re-distribution or round-robin.

\subsection{Plan Generation\label{subsec:Plan-Generation}}

\subsubsection{Generating Initial Containerized Query Plan}

Plan generation begins with an optimized query tree as initial input.
Followed by the consecutive steps in Fig. \ref{fig:Example-workflow}.
The planner may choose to add operator parallelism statically before
serialization and deployment. In step (B), the optimized query tree
is mapped 1-1 into a directed graph, $f:queryTree\text{\ensuremath{\Rrightarrow}}directedGraph$,
that represents container execution order and direction of data flows.
Specifically, each query tree operator is mapped separately into a
unique graph vertex representing a worker, $f:node_{1}\text{\ensuremath{\Rrightarrow}}vertex_{1}$,
with its specific query relational operator, conditions, predicate
and clauses derived from its mapped query tree node. A pre-order traversal
of the query tree suffices to map each node into the directed graph.

Data relations must also be mapped to data-only containers. A list
of database relations and there partitions, if any, are available
to the query planner. If the relation is partitioned, each partition
is managed by a distinct data-only container. This information is
usually available from a data catalog. Assigning data-only containers
to a relation's data partitions is an additional organization step
that proceeds query evaluation. Containerizing the data requires identifying
the data's location, schema and permissions. Once the partition size
and host location is known, containerization also attempts to collocate
data-only container on the same host where the volume resides, if
possible.

Planner compiled output is formatted as a version 3.3 Docker-Compose
file, DSL notation. The planner feeds the complied DSL file to an
container orchestration tool, Docker-Stack, that deploys the containers.
Each container image executes one of three different Java binaries:
Worker, Master, Data-Only. Each Worker in the query executes a single
relational operator mapped from the query tree. The Master sends control
messages to workers to synchronize the progress of the overall query
and data flow of intermediate relational operator output(tuples) between
workers. The Master uses message queues to communicate tasks to containers,
section, subsec. {[}\ref{subsec:Synchronization}{]}. Data-Only containers
are concerned with marshaling a query's initial database relations
for workers. Each Worker executes a single relational on its embedded
SQL engine. The embedded engine off-loads the Worker's relational
operator processing of intermediate row sets. An SQL SELECT command
with appropriate predicate and clauses e.g. \textit{``SELECT t.a,
t.b FROM intermediateTable AS t''} suffices to execute the operator,
in this example a projection $\pi$ operator. JDBC intermediate rows
sets are transferred between containers with shared edges as specified
in the query's digraph using streaming sockets. Sockets implement
the Pull, Push and Exchange operators described in section \ref{subsec:DataFlow-Management-Operators}. 

\subsubsection{Adding Operator Parallelism}

In step (B), the initial containerized graph may be revised by the
planner by adding more vertices and edges whenever a vertex's operation
needs to be distributed across many vertices in parallel. The number
of parallel join operators is referred to as Degree of Parallelism(DoP),
\citep{DMParallelIntraOperator}. An operator's DoP is assigned by
the planner. As parallel operators are added, Exchange operators are
also added to redistribute data to and from the new vertices. 

\begin{algorithm}
Input:

~~$G=<V,E>$: a digraph view of containers

~~$v$: current digraph vertex

~~$d$: operator DoP

Output: 

~~$G'=<V',E'>$: updated digraph

Method:

1.~~For $(d$ $-1)$ iterations Do

2.~~~~~create new graph vertex $v'$

3.~~~~~add inEdges to $E$ $\{(u,v')$: $\text{\ensuremath{\forall}}(u)\text{\ensuremath{\in}}In_{v}\}$
using $\text{\ensuremath{\psi}}_{1}$

4.~~~~~add outEdges to $E$ $\{(v',u)$: $\text{\ensuremath{\forall}}(u)\text{\ensuremath{\in}}Out_{v}\}$
using $\text{\ensuremath{\psi}}_{2}$

5.~~~~~add $v'$ to $V$

6.~return $G'=<V',E'>$

\caption{Adding Parallel Vertices \label{alg:Adding-Parallel-Vertices}}
\end{algorithm}

Adapting the graph with parallel vertices is performed by Algorithm
\ref{alg:Adding-Parallel-Vertices}, which takes as parameters: $G$
the query's digraph, $v$ the current digraph vertex and DoP $d$.
Two helper sets are needed: 1) set $In_{v}$ of vertices for all of
$v's$ input edges, 2) set $Out_{v}$ of vertices for all of $v's$
output edges. Steps 1 - 5 iterate $(d-1)$ times to update vertex's
operator with DoP $d$ of parallelism. Inside the loop, Step 4 creates
a new vertex $v'$ as a duplicate of $v$. Steps 3, 4 adds directed
edges to $v'$ based upon the same directed edges from $v$ using
vertex sets $In_{v}$ and $Out_{v}$. Exchange functions $\text{\ensuremath{\psi}}_{1}$
and $\text{\ensuremath{\psi}}_{2}$ are added. Exchange functions
can create new edges not common to $v$, depending on how the data
is redistributed to parallel vertices. Steps 5 adds the new vertex
$v'$ into $V$, the digraph's list of vertices. Step 6, at loop completion
returns the updated digraph $G'$. 

Fig. \ref{fig:Example-workflow} (B) and (C) show an example that
updates the join operator with DoP 3. In this case, because the operator
is an equi-join, the Exchange operator may implement a hashing function
based upon the join key that maps a tuple to one of the three containers. 

\subsubsection{Serialization}

Finally, the digraph representing the completed containerized query
plan is serialized by the planner into a configuration YAML (DSL)
file. An example configuration file is shown in Fig. \ref{fig:Example-workflow}
(D). This file communicates any semantics and relationships between
containers executing the query and other configuration input needed
to deploy and orchestrate the query. The file is composed of a hierarchical
list of container properties for each container in the digraph, reflecting
query semantics , i.e. that specify the types of containers, their
inter-relationships, cluster placement preferences, environment variables,
memory size, volumes, network identifiers and other artifacts needed
to execute the overall query across a cluster of containers.

\subsubsection{An Extended Example of Query Containerization Plan Generation}

Fig. \ref{fig:Example-workflow} illustrates the work-flow of containerizing
a SelectJoinProjectOrder(SJPO) SQL statement denoted in relational
expression form: $\delta_{R.A1,<}(\pi_{R.A1,S.B1}((\sigma_{R.A2\leq a}R)\bowtie_{R.A3=S.B3}(\sigma_{S.B2=b}S)))$,
where $R(A1,A2,A3)$ and $S(B1,B2,B3)$ are two relations; $\{\bowtie,\pi,\sigma,\delta\}$
are join, projection, selection, order-by operators, respectively.
The query tree in (A) represents the optimized query plan that would
be generated by a conventional query optimizer. Query tree nodes are
mapped 1-1 into a directed graph (B), whose vertices represent worker
containers that execute the query's relational operators. Digraph
edge direction represents both operator execution order and data flow.
The planner may choose to further adjust the digraph by dividing a
relational operator (here, the original join) among three vertices,
DoP 3, that execute in parallel (C). Parallelism is a principle performance
benefit of containerized query evaluation. Also, shown in Fig. \ref{fig:Example-workflow}
(C) is the Exchange operator, that redistributes input to multiple
containers; in this case redistributing tuples from data relations
to parallel join workers. Finally, the digraph is compiled into a
DSL file that specifies execution semantics for a container specific
platform orchestration tool (D), e.g. network id, data volumes, placement
directives, container image type etc. 

\begin{figure*}
\noindent \begin{centering}
\includegraphics[scale=0.38]{SJPO_QUERY_TREE_TO_GRAPH_TO_YAML}
\par\end{centering}
\caption{An Example of Query Containerization.\label{fig:Example-workflow}(A)
query tree; (B) mapped 1-1 into digraph; (C) add parallel operators
into digraph; (D) compile serialized deployment file.}
\end{figure*}

\subsection{Plan Deployment\label{subsec:Deployment}}

Query evaluation is started by the master by sending the YAML configuration
file as input to the platform's container orchestration tool, Docker-Compose.
The orchestrator then schedules containers described in the configuration
file assisted by the prototype's cluster manager, Docker-Swarm. Container
server/daemons on each VM respond to the orchestrator to start container
execution. Orchestration additionally creates a virtual network of
data communication channels among all containers. The private networked
messaging service, RabbitMQ, is also started to enable communications
between the master and other worker containers. 

\begin{figure}
\includegraphics[scale=0.5]{DockerDBaaSFrameworkVer3}

\caption{Synchronizing the container cluster\label{fig:Synchronizing-the-container}}
\end{figure}

\subsection{Synchronization\label{subsec:Synchronization}}

Once containers are deployed the master orchestrates the evaluation
and progress of the query using control messages. Evaluation task
control messages are passed from master to other containers with instructions
to progress the query. The master makes use of its knowledge of the
digraph and uses edge direction in the digraph to synchronize both
container execution order and operator result data flow direction
between containers. Fig. \ref{fig:Synchronizing-the-container} shows
a scenario in which the master orchestrates the evaluation of the
query with control messages. The master progresses the query by sending
control messages to a down-stream worker to start execution of its
relational operation, e.g. a join $\bowtie$. The query plan instructs
the master what type of control message and instruction payload to
send to each worker. Each worker then executes its control message
instructions. 

In general, the master can be replicated for fault tolerance. Workers
are delegated responsibility to handle autonomously some inter-container
data transfers between their peers to help distribute overall control
and avoid bottlenecks at the master. For example, a worker may sends
a message to an up-stream or down-stream worker to request \textit{\emph{pulling
or pushing}} its result data. 

Workers provide their adjacent peers and master with their state information.
When an up-stream worker completes transferring data to a down-stream
worker, both containers notify the master of any data content changes.
To aid specifying worker synchronization, the following container
data states are defined:
\begin{itemize}
\item \textit{\emph{Valid Input State}}\textit{ }\textit{\emph{(VIS)}}\textit{.}\textit{\emph{
A container is in VIS }}when its input satisfies conditions to start
execution of its designated relational operator. 
\item \textit{\emph{Valid Output State (VOS). A container is in VOS}} when
intermediate results can be sent to their down-stream workers. 
\end{itemize}
With data state transitions occurring in the workers, the execution
progress of the overall query can be viewed as \textit{\emph{data
flow engine}}, i.e. the execution of query operations proceed only
along directed edges in the digraph to successive containers coincident
with the progress of operator result data flow states. Until a container's
input data flow state (VIS) is valid, the query cannot progress along
an adjacent edge. \textit{\emph{This}} helps to simplify control of
the evaluation. Each container tracks its data input state and reports
state transitions to the master. This allows the master to monitor
the overall state of the query's progress. For example, with a hash-join,
the \textit{build phase} data flows are completed before the \textit{probe
phase} flows begin. In the prototype, a hash-join container's \textit{probe
}relation input state (VIS) remains invalid until its \textit{build}
state input data flows are also completed. 

To communicate a container's valid state transitions, the message
sent by the worker contain various fields depending on the message
type: \textit{\emph{{[}timestamp, containerID, inputstate, outputstate,
operatorAction, adjacenyList{]}}}. Because workers can help manage
their data state progress autonomously, the master can focus on progressing
the overall query with control messages; handling state transitions
exceptions or failures. Failures include a container crash or undelivered
(or lost) state transition message. 

\section{Experimental Results\label{sec:AExperiments}}

\subsection{Experiment Setups}

This section describe prototype experiments and discuss preliminary
results. To ensure experiment transparency and repeat-ability, a sufficiently
large public relational data set, provided by the MySQL community,\footnote{\url{https://dev.mysql.com/doc/employee/en/sakila-structure.html}},
serves as test-bed data relations for comparing different containerized
query configurations. Relations were transcribed into a SQLite database.
Two relations from the database, \textit{salaries} and \textit{employees},
are used in the queries with relation: \textit{salaries} size of $2,844047$
tuples,\textit{ employees} $300,024$ tuples. Both relation's tuples
were randomly permuted before experiments to avoid any inherent patterns
in the data. The experiments use SJPO, Aggregate and Simple Select
type queries, Fig. \ref{fig:Experiment-Queries}. Operator predicate
clauses were varied for different effects in each experiment.

\begin{figure}
\includegraphics[bb=70bp 620bp 550bp 990bp,clip,scale=0.55]{queriesSJPO_Table}

\caption{Table of Experiment Queries\label{fig:Experiment-Queries}}
\end{figure}

Each experiment records query performance metrics over the same data
relations, which are compared when executed as 1) a cloud hosted containerized
query vs. 2) on a VM hosted SQLite DBMS baseline. Each experiment
is repeated multiple times with operators assigned different DoPs
to investigate how parallelism effects performance. The \textit{salaries}
relation for each query experiment is run with different sized partitions,
Fig. \ref{fig:Experiment-DOPs}, to observe effects of parallelism.
All relational operators $\{\bowtie,\pi,\sigma,\delta\}$ are configured
with zero or more parallel operators. DoP for operators $\bowtie,\sigma$
also matches the experiment's number of partitions of the relation
\textit{salaries;} DoP for\textit{ $\delta,\pi$ }, also increase
with number of partitions to distribute compute overhead needed when
merging intermediate results from parallel operator nodes lower in
the digraph. This permits observing speed-up in the experiments due
to operator and disk parallelism, and further effects performance
by loading and keeping relations all-in-memory across many containers.
DoP is also reported for all experiments in Fig. \ref{fig:Experiment-DOPs}. 

\begin{figure}
\includegraphics[bb=70bp 760bp 505bp 955bp,clip,scale=0.6]{QueryExperimentDegreesVer2}

\caption{Experiment Partitions \& operator DoPs\label{fig:Experiment-DOPs}}
\end{figure}

The baseline was run at the same time on a test-bed cloud VM to avoid
workload skew when compared to many containers executing in VMs concurrently.
The experiments were performed using Docker containers scheduled across
a Docker Swarm cluster of six OpenStack VM hosts on the Chameleon
Cloud provided by TACC \footnote{Texas Advanced Computing Center at the University of Texas System's
Research campus in Austin Texas}. A Containerized-Query planner and compiler was coded for the prototype
to enable the Master to organize the deployment and execution of a
query. No special optimizations were applied by the Query Planner.
Java executables were instrumented to record operator performance
metrics . Only the total query wall-clock time metric for each experiment
is reported. 

\subsection{Query Experiment One}

The purpose of experiment-one, row one \ref{fig:Experiment-Queries},
is to compare the evaluation of a SJPO containerized query to the
same evaluated on the SQLite baseline, when indexing and sorting optimizations
are selected by the baseline's query plan. Results show that a configuration
of 6 partitions and DoP 6 for $\bowtie$, results in a only a slightly
smaller wall-clock query execution time compared to the SQLite baseline
running the query on unpartitioned relations, Fig. \ref{fig:Experiment-1}.
The associated table shows slightly improving performance up until
48 partitions, an apparent inflection point, where network and container
multi-thread overhead begins reducing performance. Experiment-one
includes a lengthy join clause, but its trailing segment \textit{``AND
(salaries.empNo == employees.empNo)''}, permits the SQLite baseline
engine's query plan to optimize the $\bowtie$ by first sorting, then
indexing on field \textit{empNo} in both relations being joined. This
optimization avoids a full-scan of both relations by the control baseline,
resulting in only marginal speed-up in the containerized query over
the baseline.

\begin{figure}[t]
\includegraphics[bb=70bp 565bp 495bp 790bp,clip,scale=0.6]{ExperimentOneVer1}\caption{Experiment One: SJPO\label{fig:Experiment-1}}
\end{figure}

\subsection{Query Experiment Two}

The purpose of experiment-two, row two \ref{fig:Experiment-Queries},
is to compare the SJPO containerized query to the baseline version,
when indexing and sorting optimizations are prohibited, forcing full-scans.
Join selectivity is also intentionally chosen to be poor by adding
a BETWEEN clause. The clause \textit{(salaries.empNo == employees.empNo)}
in query one is now absent from the join, forcing a full-scan by the
baseline SQLite engine, Fig. {[}\ref{fig:Query-Experiment-2:}{]}.
This effectively has the same overhead as a full Cross-Join, and demonstrates
the advantage of the distributed containerized query in cases of large
relations with intense relational operator overhead. Experiment-two
shows a 3-order of magnitude improvement over the baseline, 8 secs
for 24 partitions as compared to 1538 secs for the baseline. In this
case demonstrably, real-time OLTP is possible with such a containerized
query. Again, Experiment-two again shows a performance inflection
point at 48 partitions, where inter-container communication and multi-threaded
overhead begins degrading overall query wall-clock completion time.

\begin{figure}
\includegraphics[bb=70bp 570bp 510bp 790bp,clip,scale=0.6]{ExperimentTwoVer2}

\caption{Query Experiment Two: SJPO\label{fig:Query-Experiment-2:}}
\end{figure}

\subsection{Query Experiment Three}

The purpose of experiment-four, row three \ref{fig:Experiment-Queries},
is to compare a simple Select containerized query to the baseline
version. Results shows a order of magnitude improvement over the baseline,
4 secs for DoP 48 as compared to 69 secs for the baseline. In this
case demonstrably, real-time OLTP is improved by query containerization. 

\begin{figure}
\includegraphics[bb=70bp 570bp 500bp 790bp,clip,scale=0.6]{ExperimentThree}

\caption{Query Experiment Five: Simple Select \label{fig:Simple-Select}}
\end{figure}

\subsection{Query Experiment Four}

The purpose of experiment-four, row four \ref{fig:Experiment-Queries},
is to compare a aggregate containerized query to the baseline version.
Results show only marginal improvement over the baseline. An inflection
point occurs again at DoP 48, when multi-threaded and container-to-container
communication overhead begin to degrade performance. Clearly, containerizing
a query in not always an imperative.

\begin{figure}
\includegraphics[bb=70bp 570bp 500bp 790bp,clip,scale=0.6]{/home/david/nojoindbaas/paper/ExperimentFour}

\caption{Query Experiment Four: Aggregate \label{fig:Aggregate}}
\end{figure}

\subsection{Experiment Five: Transfer \& Re-Insert Latencies}

The purpose of experiment-three is to observe three operator times:
1) data result set transfer time between workers, 2) re-Insert/reloading
latencies of results sets, and 3) operator execution times. A SJPO
query run with 6 partitions and 6 DoP suffice to reveal latencies
transferring and reloading intermediate results between worker containers
Fig. {[}\ref{fig:Transfer-=000026-Re-Insert}{]}. The chart shows
that these latencies are a factor of 7 times the average relational
operator execution time. This poses a significant bottle-neck for
containerizing a query. This is an artifact to the prototype's design
implementation; using an embedded SQL engine. An implementation that
does not rely on an embedded SQL engine, but executes relational operators
directly would eliminate database reloading. Otherwise, re-Insert
database latencies observed could be mitigated by the development
of an API that improves intermediate result set reloading. Extending
the ODBC to accommodate transfer of rows sets using native internal
data structures(ResultSetInternal) at the driver level that can be
reloaded directly into a SQL database engine, avoiding the overhead
of SQL Inserts and Journaling in such an approach to reduce re-Insert
latencies. Transfer latency is unavoidable with distributed containers
and can best be addressed by extending low-level container kernel
support for inter-container communication, instead of relying on application
level channels, e.g. sockets. 

\begin{figure}
\includegraphics[bb=70bp 640bp 465bp 830bp,clip,scale=0.6]{Transfer_VS_InsertRatesNominalWorkloadVer1}

\caption{Experiment Five: Transfer \& Re-Insert Latencies\label{fig:Transfer-=000026-Re-Insert}}
\end{figure}

\section{Related Work\label{sec:Related-Work}}

Major cloud providers currently offer a number of SQL database services
in the cloud. These include Amazon's RDS\citep{amazon:RDS}, Microsoft's
Azure SQL Database\citep{microsoft:SQLAzure} and Google Cloud SQL\citep{google:GoogleCloudSQL}.
In addition, a number of academic research groups also proposed cloud
DBaaS to support relational database functionality. Examples include
VoltDB\citep{SW2013:VoltDB}, Postgres-XL\citep{postgre-xl}, Impala
\citep{KBBB2015:Impala}, and Relational Cloud \citep{CJPM2011:RelationalCloud}. 

In addition to SQL DBaaS, a number of NoSQL database services \citep{CDGH2008:Bigtable,LM2010:Cassandra,MGLRSTV2010:Dremel}{]}
are also provided by major cloud providers. These systems provide
efficient columnar storage and retrieval of large data sets, support
nested data structure, and NoSQL query types. However, these systems
do not support full SQL query and ACID data consistency. NoSQL data-stores
such as Google's Bigtable\citep{CDGH2008:Bigtable}, Apache Hadoop's
HBase\citep{Geor2011:HBaseDefinitiveGuide}, or Facebook's Cassandra\citep{LM2010:Cassandra}
are highly scalable, but their limited API and weaker consistency
models complicate relational HPC SQL application development that
can meet BigData's OLTP demands. 

Development of light-weight software container technology includes
Docker\citep{docker:Docker} and ZeroVM\citep{rackspace:ZeroVM}.
Both are open source projects. Publications about their techniques
have been sparse, except documents on the product websites and a number
of blogs published by individual developers. Performance of container
technology for data intensive applications has been investigated in\citep{TZWW2014:PerformanceLightWeightPaaS,RLPZJ2014:ZeroVM}{]}.

\section{Conclusions\label{sec:Conclusions}}

This paper presented a method and software architecture framework
for containerizing a relational SQL query, evaluated using cloud-based
commodity hosts. The method describes a query planner that compiles
a query that is evaluated by a cluster of light-weight containers.
Constructing a containerized query plan starts with mapping the query's
optimized relational tree into a digraph, which abstractly represents
the cluster of cooperating containers. Some digraph vertex operators
may be subsequently be distributed across many parallel containers
to increase evaluation performance. This allows any query relation
to be partitioned and processed in parallel. The final evaluation
plan is compiled into a data serialization language, e.g. YAML, specific
to container platform technology tools needed to deploy and execute
containers on cloud hosts.

Prototype experiments show that for some queries, performance can
be greatly improved. Other queries benefit only marginally from containerization.
This suggests that a online cloud DBMS need only containerize queries
that benefit from parallelism. But there are still research solutions
needed: 1) lower latency distribution of intermediate results, 2)
efficient dynamic parallel operator creation across many containers
to progress queries when needed, 3) better optimizations including
container placement, restart and fail-over.\bibliographystyle{plain}
\bibliography{abbrev,ieee-ctl,clouddb}

\end{document}

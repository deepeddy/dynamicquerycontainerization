import com.rabbitmq.client.Channel;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Envelope;
import com.rabbitmq.client.ShutdownSignalException;
import java.io.IOException;
import java.util.*;
import java.util.logging.*;


public class Master implements Constants {

  //private final static String[] WORKER_NAMES =  {"shuffler1", "shuffler2", "join1"};
  private static Logger logger;
  private static FileHandler fh = null;

  public static void main(String[] argv) throws Exception {

     //set up logger
    logger = Logger.getLogger("MyLog");

    try {

        // This block configure the logger with handler and formatter
        fh = new FileHandler("/var/log/Master.log");
        logger.addHandler(fh);
        SimpleFormatter formatter = new SimpleFormatter();
        fh.setFormatter(formatter);

        // the following statement is used to log any messages
        logger.info("Open Master log ");

    } catch (Exception e) {
        System.out.println(e.getMessage());
        e.printStackTrace();
        fh.close();
        System.exit(1);
    }


    //get connection to rabbit broker host 
    ConnectionFactory factory = new ConnectionFactory();
    factory.setHost("localhost"); //the rabbit broker local 
    Connection connection = factory.newConnection();

    //INITIALIZE  MESSAGE QUEUES AND CHANNELS
    Channel toAllWorkersRabbitChannel = null;
    try {
      toAllWorkersRabbitChannel = InitQueues(connection);
    } catch (Exception e) {
      logger.info("ERROR INITIALIZING MESSAGE QUEUES, "  + e.getMessage());
      fh.close();
      System.exit(1);
    }
 

    // CREATE WORKER LISTENER for acknowledgments from workers 
    MasterConsumer ACKsConsumer = new MasterConsumer(toAllWorkersRabbitChannel);
    //send messages arriving at ACK_QUE to consumer, ACKsConsumer
    toAllWorkersRabbitChannel.basicConsume(ACK_QUE, true, ACKsConsumer);

    //SEND MESSAGE to all workers using "worker" exchange routing key
    String message = "echo Alive";
    logger.info("echo Alive");
    toAllWorkersRabbitChannel.basicPublish(ALL_WORKERS_EXCHANGE, "worker", null, message.getBytes("UTF-8"));
    System.out.println(" [x] Published '" + message + "' to all worker nodes");
    logger.info(" [x] Published '" + message + "' to all worker nodes");
    //WAIT for workers to acknowledge completion of their work
    WaitWorkerACK(ACKsConsumer, 3, "workers");

    System.out.println("PROGRESS QUERY TO Shuffle Step ");
    //SEND DIRECT MESSAGE to "shuffler1"
    message = "sqlite3 -init ~/ShuffleMode /shuffle_tables/shuffle.db" +
      " 'SELECT * FROM Students WHERE age < 30.0;'" +
      "  | ssh -v root@join1 'sqlite3  -init ~/JoinMode /join_tables/join.db'";
    toAllWorkersRabbitChannel.basicPublish(ALL_WORKERS_EXCHANGE, "shuffler1", null, message.getBytes("UTF-8"));
    System.out.println(" [x] Published '" + message + "' to shuffler1 "); 
    logger.info(" [x] Published '" + message + "' to shuffler1 ");

    //SEND DIRECT MESSAGE to "shuffler2" 
    message = "sqlite3 -init  ~/ShuffleMode /shuffle_tables/shuffle.db" +
      " 'SELECT * FROM Students WHERE age >= 30.0;'" + 
      "  | ssh -v root@join1 'sqlite3  -init ~/JoinMode /join_tables/join.db'";
    toAllWorkersRabbitChannel.basicPublish(ALL_WORKERS_EXCHANGE, "shuffler2", null, message.getBytes("UTF-8"));
    System.out.println(" [x] Published '" + message + "' to shuffler2 ");
    logger.info(" [x] Published '" + message + "' to shuffler2 ");

    //Wait for shufflers to acknowledge completion of their work
    WaitWorkerACK(ACKsConsumer, 2, "shuffle");

    //BROADCAST MESSAGE to all workers with "join" exchange routing key
    System.out.println("PROGRESS QUERY TO Join Step ");
    logger.info("PROGRESS QUERY TO Join Step ");
    message = "sqlite3 -init ~/JoinMode /join_tables/join.db" +
          " 'CREATE TABLE joined AS SELECT S.name, round(S.age) AS age, C.ccid, C.courseDesc, G.points " +
          "FROM Grades AS G, Students AS S, Courses AS C WHERE S.sid = G.gsid AND G.gcid = C.ccid;'";
    toAllWorkersRabbitChannel.basicPublish(ALL_WORKERS_EXCHANGE, "join", null, message.getBytes("UTF-8"));
    System.out.println(" [x] Published '" + message + "' to join nodes ");
    logger.info(" [x] Published '" + message + "' to join nodes ");
    //WAIT for joiner to acknowledge completion
    WaitWorkerACK(ACKsConsumer, 1, "join");

    System.out.println(" CLOSING CHANNEL AND CONNECTION");
    logger.info(" CLOSING CHANNEL AND CONNECTION");
    toAllWorkersRabbitChannel.close();
    connection.close();

  } // end main

  public static Channel InitQueues(Connection rabbitConnection) throws Exception {

    Channel rabbitChannel = rabbitConnection.createChannel();

    //define exchange to communicate with all workers using publish/subscribe model
    rabbitChannel.exchangeDeclare(Constants.ALL_WORKERS_EXCHANGE, "direct");

    //establish ACK QUEUE for replies from workers
    rabbitChannel.queueDeclare(Constants.ACK_QUE, false, false, false, null);

    //workers must use ACK_QUE key to send messages to ACK_QUE queue 
    rabbitChannel.queueBind(ACK_QUE, ALL_WORKERS_EXCHANGE, ACK_QUE);


    return rabbitChannel;
  
  }

  //wait on expected NumberOfAcks  returned by workers before proceeding, sychronize threads on shared channel
  public static void WaitWorkerACK(MasterConsumer consumer, int expectedNumberOfACKs, String workerType) 
    throws Exception {

        //set number of ACKs to wait for	
	consumer.setExpectedNumOfACKs(expectedNumberOfACKs);

        System.out.println(" [*] Waiting for ACK messages from " + workerType + ". To exit press CTRL+C");
        logger.info(" [*] Waiting for ACK messages from " + workerType + ". To exit press CTRL+C");

        Channel channel = consumer.getChannel();

        synchronized (channel) { //wait for join nodes  ACK to be received
                //channel.wait(2000);
      		channel.wait();
    	}

  }// end method 


  //MasterConsumer starts consumer in child thread. Consumes delivery of ACK messages from workers
  //which indicate progress of query 
  private  static class MasterConsumer extends DefaultConsumer {

      int expectedNumberOfACKs = 0;

      int receivedACKs = 0;

      private String message = "";  //message received by consumer

      private Channel channel;

      //constructor
      public  MasterConsumer(Channel channel) {
          super(channel);
          this.channel = channel;
      }

      @Override
      public void handleShutdownSignal(java.lang.String consumerTag, ShutdownSignalException sig) {

         logger.info(" master listener received shutDown signal");
         fh.close();
      }

      @Override
      public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body)
          throws IOException {

        message = new String(body, "UTF-8");
        System.out.println(" [x] ACK RECEIVED: '" + message + "'");
        logger.info(" [x] ACK RECEIVED: '" + message + "'");

        // Update Query state 
        try { 
           updateQueryState(message);
           controlQuery();
           if(++receivedACKs == expectedNumberOfACKs)

           	//channel.basicCancel(consumerTag);
                 synchronized (channel) { //notify main thread to continue query execution
                     channel.notifyAll();
                 }
           
        }
        catch(Exception e) {
          System.out.println("Query update FAILED: " + e.toString());
          logger.info("Query update FAILED: " + e.toString());
        }

      } //end handleDelivery 

      public String getMessage() {
        return message;
      }

      public void setExpectedNumOfACKs(int num) {
        expectedNumberOfACKs = num;
        receivedACKs = 0;
      }
 
      public Channel getChannel() {
          return channel;
      }
      
  } // end MasterConsumer


  private static void updateQueryState(String ACK) {

	System.out.println(" [X] Updated Query State");
        logger.info(" [X] Updated Query State");

  }

  private static void controlQuery() {

         System.out.println(" [X] Query Controller Action Complete");
         logger.info(" [X] Query Controller Action Complete");
  }  

} // end class

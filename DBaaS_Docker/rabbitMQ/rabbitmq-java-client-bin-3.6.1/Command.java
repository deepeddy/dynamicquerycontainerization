
public class Command implements Constants {

	private msgType type;	
	private String msg;
	private String exchangeName;
	private String key;
	private String workerType;

	public Command(msgType type, String msg, String exchangeName, String key, String workerType) {
		super();
		this.type = type;
		this.msg = msg;
		this.exchangeName = exchangeName;
		this.key = key;
		this.workerType = workerType;
	}

	public msgType getType() {
		return type;
	}

	public String getMsg() {
		return msg;
	}

	public String getExchangeName() {
		return exchangeName;
	}

	public String getKey() {
		return key;
	}

	public String getWorkerType() {
		return workerType;
	}


} // end class Command

enum msgType {
	EXECUTE, WAIT 
}

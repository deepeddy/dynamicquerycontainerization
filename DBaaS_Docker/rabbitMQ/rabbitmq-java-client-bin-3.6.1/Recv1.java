import com.rabbitmq.client.*;

import java.io.IOException;

public class Recv1 {

  private final static String QUEUE_NAME = "hello";

  public static void main(String[] argv) throws Exception {

    if(argv.length != 1 && !validIP(argv[0])) {

      System.out.println("Usge: Recv <IP> ");
      System.exit(1);
    }

    ConnectionFactory factory = new ConnectionFactory();
    //factory.setHost("localhost");
    factory.setHost(argv[0]);  //the IP address where rabbitmq broker is located
    Connection connection = factory.newConnection();
    Channel channel = connection.createChannel();

    channel.queueDeclare(QUEUE_NAME, false, false, false, null);
    System.out.println(" [*] Waiting for messages. To exit press CTRL+C");

    //consume Callback
    Consumer consumer = new DefaultConsumer(channel) {

      private String message = "";

      @Override
      public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body)
          throws IOException {
        //String message = new String(body, "UTF-8");
        message = new String(body, "UTF-8");
        System.out.println(" [x] Received '" + message + "'");
      }

      public String getMessage() {
        return message;
      }

    }; //end anonymous class

    //get message off queue
    channel.basicConsume(QUEUE_NAME, true, consumer);

  } // end main

  public static boolean validIP (String ip) {
    try {
        if ( ip == null || ip.isEmpty() ) {
            return false;
        }

        String[] parts = ip.split( "\\." );
        if ( parts.length != 4 ) {
            return false;
        }

        for ( String s : parts ) {
            int i = Integer.parseInt( s );
            if ( (i < 0) || (i > 255) ) {
                return false;
            }
        }
        if ( ip.endsWith(".") ) {
            return false;
        }

        return true;
    } catch (NumberFormatException nfe) {
        return false;
    }
  } // end validIP

} // end class


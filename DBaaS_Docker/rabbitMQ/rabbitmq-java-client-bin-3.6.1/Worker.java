import com.rabbitmq.client.*;
import java.io.*;
import java.net.InetAddress;
import java.util.*;
import java.util.logging.*;

public class Worker implements Constants {

  private static String workerName;
  private static String workerQueueName;
  private static String rabbitBrokerIP;
  private static Logger logger;
  private static FileHandler fh = null;

  public static void main(String[] argv) throws Exception {

    //set up logger
    logger = Logger.getLogger("MyLogger");  

    try {  

        // This block configure the logger with handler and formatter  
        fh = new FileHandler("./Worker.log");  
        logger.addHandler(fh);
        SimpleFormatter formatter = new SimpleFormatter();  
        fh.setFormatter(formatter);  

        // the following statement is used to log any messages  
        logger.info("Start Worker log");  

    } catch (SecurityException se) {  
        se.printStackTrace();
	System.exit(1);
    } catch (IOException ioe) {  
        ioe.printStackTrace();
	System.exit(1);
    } catch(Exception e) {
        e.printStackTrace();
	System.exit(1);
    }  
   

    //logger.info("Usage: Worker <rabbitmq broker IP> <workerName> <rabbit exchange key (workerType)>");
    logger.info("# command line args found: " + argv.length);
    for(int i = 0; i < argv.length; i++) {
       logger.info(argv[i] + "  ");
       
    } 

    if(argv.length != 3) {
       //command line must provide the <rabbit broker IP>, the worker's queue name, 
       //and an exchange key to route messages to the worker type:  {shuffle, join, merger}
       System.out.println("Usage: Worker <rabbitmq broker IP> <workerName> <exchange key (workerType)>");
       logger.info("Usage: Worker <rabbitmq broker IP> <workerName> <rabbit exchange key (workerType)>");
       fh.close();
       System.exit(1);
    }

    //GET VALID IP for rabbitMQ broker server
    rabbitBrokerIP = argv[0];   //the IP address where rabbitmq broker is located
    if(!validIP(rabbitBrokerIP)) {
       logger.info(rabbitBrokerIP +  " not VALID IP literal.  Attempt to resolve to valid IP");
       
       try {
       	InetAddress address = InetAddress.getByName(rabbitBrokerIP);
        //the IP address where rabbitMQ broker is located
       	rabbitBrokerIP = address.getHostAddress();
       } catch(Exception e) {
       	logger.info(argv[0] +  e.getMessage());
       	logger.info("attempt getting host address for " + rabbitBrokerIP + " FAILED");
        fh.close();
        System.exit(1);
       }

       logger.info("address for  " + argv[0] + " resolved to: " + rabbitBrokerIP);
    }
    
  
    workerName = argv[1]; //assign worker's queue
    workerQueueName = workerName; //worker's name and its queue name are the same  
    String workerType = argv[2];
    if(!exchangeKeys.contains(workerType)) {  //validate rabbitMQ exchange key passed as command line argument
     // System.out.println("INVLALID <exchange key> given as 3rd command line argument");
     // System.out.println("Must be one of the following keys: " +  exchangeKeys.toString());
      logger.info("INVLALID <exchange key> given as 3rd command line argument");
      logger.info("Must be one of the following keys: " +  exchangeKeys.toString());
      fh.close();
      System.exit(1);
    }

    //connect to rabbitMQ broker, this may take some time if broker in master is still trying to start
    Connection rabbitConnection = null;  
    int count = 0;
    while(true) {  //keep trying to connect to rabbitMQ broker
    	try {
    		//open channel to rabbit broker
    		ConnectionFactory factory = new ConnectionFactory();
    		factory.setHost(rabbitBrokerIP);
    		factory.setPort(5672);
    		factory.setUsername("guest");
    		factory.setPassword("guest");
    		rabbitConnection = factory.newConnection();
    		break;
    	}
    	catch(Exception e) {
    		logger.info("Attempt opening connection to rabbitMQ broker. " + e.toString());
    		logger.info("Attempt opening connection to rabbitMQ broker. " + e.getMessage());
    		Thread.sleep(1000);
    		count++;
    		if(count > 20) {
			logger.info("Attempt opening connection to rabbitMQ broker for twenty seconds timed-out." );
    			System.exit(1);
                }
    		else 
    			continue;
    	} // end try
    } // end while
   
    //create channel to remote rabbit broker 
    final Channel channelToRabbit = rabbitConnection.createChannel();

    //DECLARE rabbitMQ QUEUES and BIND them to exchanges
    channelToRabbit.exchangeDeclare(ALL_WORKERS_EXCHANGE, "direct");   //master may not have yet declared Exchange

    //master may not have yet declared ACK_QUE
    channelToRabbit.queueDeclare(ACK_QUE, false, false, false, null);
    //must use ACK_QUE key to send messages to ACK_QUE queue, bind to  ALL_WORKERS_EXCHANGE
    channelToRabbit.queueBind(ACK_QUE, ALL_WORKERS_EXCHANGE, ACK_QUE); 

    //create new queue specific for THIS worker instance
    channelToRabbit.queueDeclare(workerQueueName, false, false, false, null);
    
    //MULTI-CAST to all workers of same TYPE  a  message from queue specific to THIS worker instance
    //bind local worker queue with ALL_WORKERS_EXCHANGE 
    //worker's specific  TYPE one of  {"shuffle", "join", "merge"} as the EXCHANGE key
    channelToRabbit.queueBind(workerQueueName, ALL_WORKERS_EXCHANGE, workerType);  //message sent to all workers of same type

    //MULTI_CAST sent to all workers a  message from queue specific to THIS  worker instance 
    //bind worker queue with worker type "worker", for messages sent to all workers 
    channelToRabbit.queueBind(workerQueueName, ALL_WORKERS_EXCHANGE, "worker");  //message sent to all workers of same type

    //UNI-CAST
    //also bind THIS worker queue with worker's unique name so a  unicast  message based upon its name can be 
    //sent to this worker only
    channelToRabbit.queueBind(workerQueueName, ALL_WORKERS_EXCHANGE, workerName); 

    System.out.println(" [*] Waiting for control messages. To exit press CTRL+C");
    logger.info(" [*] Waiting for initial control messages. To exit press CTRL+C");

    //Anonymous Inner class executes consumer Callback when message arrives 
    Consumer THISconsumerWorker = new DefaultConsumer(channelToRabbit) {

      private String message = "";  //message received by THIS worker 
      
      @Override
      public void handleShutdownSignal(java.lang.String consumerTag, ShutdownSignalException sig) {
        
         logger.info(workerName + " listener received shutDown signal"); 
         fh.close();
      }

      @Override
      public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body)
          throws IOException {

        message = new String(body, "UTF-8");
   //     System.out.println(" [x] Received '" + message + "'");
        logger.info(" [x] Received '" + message + "'");

        //TAKE ACTION based upon msg HERE, and send ACK back to master
        try { //acknowledge that message was received and acted upon; this syncs action w/ msg
          actAndACK(channelToRabbit, message);
        }
        catch(Exception e) {
   //       System.out.println("sendAckToMaster FAILED: " + e.toString());
          logger.info("sendAckToMaster FAILED: " + e.toString());
        }

    //    System.out.println(" [*] Waiting for control messages. To exit press CTRL+C");
        logger.info(" [*] Waiting for control messages. To exit press CTRL+C");

      } //end method

      public String getMessage() {
        return message;
      } //end handleDelivery


    }; //end anonymous class

    //start consumer msg queue listener and servicing messages
    channelToRabbit.basicConsume(workerQueueName, true, THISconsumerWorker);

  } // end main

  //execute the command send by the Master controller
  private static String execControlMsg(String msg) {

        //String cmd = "/bin/bash -c \"" + msg + "\"";
        String[] cmd  = {"/bin/bash", "-v", "-c", msg};
        //String cmd = "/bin/bash -c \"sqlite3 /join_tables/students.db 'SELECT * FROM Students;'\"";

	String outcome ="UNDEFINED";
    execute: try {
                Process p = Runtime.getRuntime().exec(cmd);
                //System.out.println("after exec()");

                BufferedReader bri = new BufferedReader
                        (new InputStreamReader(p.getInputStream()));
                BufferedReader bre = new BufferedReader
                        (new InputStreamReader(p.getErrorStream()));
                //System.out.println("exec() Readers open ");

                String line;
                while ((line = bri.readLine()) != null) {
//                        System.out.println("InputStream readline(): ");
//                        System.out.println(line);
                        logger.info(line);
                }
                bri.close();
//               System.out.println("exec() InputStream closed ");

                while ((line = bre.readLine()) != null) {
//                        System.out.println("ErrorStream readline(): ");
//                        System.out.println(line);
                        logger.info(line);
                }
                bre.close();
//                System.out.println("exec() ErrorStream closed ");

                if(p.waitFor() != 0) {
//                   System.out.println("FAILED: " + Arrays.toString(cmd) );
                   logger.info("FAILED: " + Arrays.toString(cmd) );
                   outcome = "FAILED: " + Arrays.toString(cmd);
                   break execute;
                }
                else {
//                   System.out.println("SUCCESSFULLY EXECUTED: " + Arrays.toString(cmd));
                   logger.info("SUCCESSFULLY EXECUTED: " + Arrays.toString(cmd));
                   outcome = "SUCCESSFULLY EXECUTED: " + Arrays.toString(cmd);
                }

//                System.out.println("Done.");
                logger.info("Done. " + outcome);
        } catch (Exception err) {
                err.printStackTrace();
        }

       return outcome;

  } // end method


  //execute control message and acknowledge action has been taken
  private static void actAndACK(Channel channelToRabbit, String msgRecvd) throws Exception {

    //EXECUTE CONTROL MSG CONTENT
    String outcome  = execControlMsg(msgRecvd);
 
    //acknowledge completion of message receipt and action taken
    String ACK = workerQueueName + " : received : " + msgRecvd + 
    		" : action taken completed with outcome: " + outcome;
    channelToRabbit.basicPublish(ALL_WORKERS_EXCHANGE, ACK_QUE, null, ACK.getBytes("UTF-8"));
  //  System.out.println(" [X] Sent ACK back to master:   '" + ACK + "'");
    logger.info(" [X] Sent ACK back to master:   '" + ACK + "'");

  } // end actAndACK

  private static boolean validIP (String ip) {
    try {
        if ( ip == null || ip.isEmpty() ) {
            return false;
        }

        String[] parts = ip.split( "\\." );
        if ( parts.length != 4 ) {
            return false;
        }

        for ( String s : parts ) {
            int i = Integer.parseInt( s );
            if ( (i < 0) || (i > 255) ) {
                return false;
            }
        }
        if ( ip.endsWith(".") ) {
            return false;
        }

        return true;
    } catch (NumberFormatException nfe) {
        return false;
    }
  } // end validIP

} // end class

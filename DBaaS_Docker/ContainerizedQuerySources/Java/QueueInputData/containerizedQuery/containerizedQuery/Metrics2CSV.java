package containerizedQuery;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import containerizedQuery.Constants.Log_Level;

public class Metrics2CSV {


	public static void main(String[] args) {
		
		try {
			metric2CSV(args[0], args[1]);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	} // end main
	
	public static void metric2CSV(String logFilename, String csvFilename) throws IOException {

		BufferedReader in = null;
		BufferedWriter out = null;

		//CSV file header
		String CSV_FILE_HEADER = "message,\tmilliseconds,\tcontainerID,\t...other";

		try {
			in = new BufferedReader(new FileReader(logFilename));
			out = new BufferedWriter(new FileWriter(csvFilename));

			out.write(CSV_FILE_HEADER);
			out.newLine();

			String line = "";
			while( (line = in.readLine()) != null) {

				if (line.contains("METRIC")) {
					
					String metric = line.substring(line.indexOf("METRIC"));

					String[] fields = metric.split(",");
					
					for(String field : fields) {
						out.write(field + ","); //output field
					}

					/*
					out.write(fields[0] + ",");  //message
					out.write(fields[1] + ",");  //milliseconds
					out.write(fields[2]);  		 //containerID
					*/
					
					out.newLine();
				}

			} // end while

		} catch (IOException e) {
			Master.logQueryState("metric2CSV: ERROR: " + e.getMessage(), Log_Level.quiet);
		} finally {
			in.close();
			out.flush();
			out.close();
		}

	}  // end method
	

} // end class

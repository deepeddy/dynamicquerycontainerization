package containerizedQuery;

//import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import com.rabbitmq.client.Channel;

import java.io.IOException;

/*
 * Open a socket server that accepts socket requests from data input
 * providers, which are results produced by upstream relational query operators in
 * other containers. The server socket accepts a data producer's socket
 * connection request and opens a new socket back to the producer in a separate new thread.
 */
public class MultiThreadedServer  implements Runnable, Constants {

	protected String workerID;  //each name is mapped to a Docker network IP address
	protected int serverPort;  //in ephemeral range  32768 to 61000
	protected ServerSocket serverSocket = null;
	protected boolean isStopped = false;
	protected java.sql.Connection SQLiteConnection;
	private Channel channelToRabbit;
	ArrayList<Queue<String>> queueList = null;
	ArrayList<Socket> sockets = new ArrayList<Socket>();

	//queueList is a list of queues mapped 1-1 for each input channel, queues hold contents of inputs needed merging
	public MultiThreadedServer(ArrayList<Queue<String>> queueList,  String workerID, int serverPort, java.sql.Connection SQLiteConnection, Channel channelToRabbit) {
		this.workerID = workerID;
		this.serverPort = serverPort;
		this.SQLiteConnection = SQLiteConnection;
		this.channelToRabbit = channelToRabbit;
		this.queueList = queueList;
	}
	
	public MultiThreadedServer(String workerID, int serverPort, java.sql.Connection SQLiteConnection, Channel channelToRabbit) {
		this.workerID = workerID;
		this.serverPort = serverPort;
		this.SQLiteConnection = SQLiteConnection;
		this.channelToRabbit = channelToRabbit;
	}

	public void run() {
		
		Socket clientSocket = null;
		serverSocket = openServerSocket();
		
		while (!isStopped()) {	//keep accepting in-coming socket connections from input channels
			
			try {
				serverSocket.setReceiveBufferSize(262144);
				clientSocket = serverSocket.accept();
				clientSocket.setReceiveBufferSize(262144);
				sockets.add(clientSocket);  //keep track of accepted sockets
				
				//clientSocket.setTcpNoDelay(true);
				//clientSocket.setKeepAlive(true);
			
				Worker.logQueryState("MultiThreadedServer: Server Socket on " + workerID 
						+ " created local socket address " + clientSocket.getLocalAddress().toString()
						+ " which accepted new connection request from remote input channel client address: "
						+ clientSocket.getRemoteSocketAddress().toString() + ", with bufferSize=" 
						+ clientSocket.getReceiveBufferSize()
						+ ", timeOut=" + clientSocket.getSoTimeout(), Log_Level.verbose);
				
			} catch (IOException e) {
				if (isStopped()) {
					System.out.println("Server Stopped.");
					return;
				}
				throw new RuntimeException("Error accepting input channel client connection", e);
			} // end try-catch
			
			if(queueList != null) {
				
				Queue<String> queue = new ConcurrentLinkedQueue<String>(); //create new queue for input channel
			
				if(System.getenv("QUERY").contains("STREAMING")) 
					synchronized(queueList) { 
						
						queueList.add(queue);   
						//add queue to list and open InputRowListRunnable Thread to manage input from channel
						new InputRowListRunnable(queue, workerID, clientSocket, SQLiteConnection, channelToRabbit).start();
						
						//NOTIFY other synchronizing threads that new input thread and queue have been added
						queueList.notify(); 
						
						Worker.logQueryState("MultiThreadedServer: worker " + workerID + " NOTIFY queue added, queueList size= "
								+ queueList.size(), Log_Level.quiet);
					} // sync
				else { //NOMINAL merging required all input queues to be totally filled
					queueList.add(queue);  //NOMINAL MERGE-SORT,  WHY NOT SYNCHRONIZED ???? NO SYNCHRONIZING THREADS?
					new InputRowListRunnable(queue, workerID, clientSocket, SQLiteConnection, channelToRabbit).start();
				}
				
			}
			else		
				new InputRowListRunnable(workerID, clientSocket, SQLiteConnection, channelToRabbit).start();
			
		} // end while
	
		System.out.println("Server Stopped.");
		Worker.logQueryState("MultiThreadedServer: Server Socket STOPPED", Log_Level.quiet);
		
	} // end run method
	
	public ServerSocket getServerSocket() {
		return serverSocket;
	}
	
	public ArrayList<Socket> getAcceptedSockets() {
		return sockets;
	}
	
	public int getNoOfAcceptedSockets() {
		return sockets.size();
	}
	
	//private synchronized method boolean isStopped()
	private boolean isStopped() {
		return this.isStopped;
	}

	//public synchronized void stop() {
	public synchronized void stop() {
		this.isStopped = true;
		
		try {
			this.serverSocket.close();
		} catch (IOException e) {
			throw new RuntimeException("Error closing server", e);
		} // end try-catch
		
	} // end method

	private ServerSocket openServerSocket() {
		
		ServerSocket serverSocket = null;
		
		try {
			//serverSocket = new ServerSocket(serverPort, 0, InetAddress.getByName(workerName));
			serverSocket = new ServerSocket(serverPort);
			Worker.logQueryState("Multi-Threaded Server Socket listening for query data results input on port " + serverPort  +
									" on host " + workerID, Log_Level.verbose);
		}catch (IllegalArgumentException | SecurityException e) {
			//throw new RuntimeException("Cannot open port " + serverPort, e);
			//throw new InterruptedException("Cannot open port " + serverPort + " \n" + e.toString());
			Worker.logQueryState("Multi-Threaded Server Socket failed to start on port " + serverPort + "on host " +
									workerID + " .\n" + e.toString(), Log_Level.quiet);
			//Thread.currentThread().interrupt();
			stop();  //stop the server thread
		} 
		catch (IOException e) {
			//throw new InterruptedException("Cannot open port " + serverPort + " \n" + e.toString());
			Worker.logQueryState("Multi-Threaded Server Socket failed to start on port " + serverPort + "on host " +
					workerID + " .\n" + e.toString(),  Log_Level.quiet);
			Thread.currentThread().interrupt();
		}
		
		return serverSocket;
		
	} // end method

} // end class
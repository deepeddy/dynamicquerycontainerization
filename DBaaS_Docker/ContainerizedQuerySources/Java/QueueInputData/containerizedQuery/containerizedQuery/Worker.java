package containerizedQuery;

import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
//import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.StringWriter;
//import java.io.Writer;
import java.net.InetAddress;
//import java.net.InetSocketAddress;
//import java.net.Proxy;
//import java.net.ServerSocket;
import java.net.Socket;
//import java.nio.channels.IllegalBlockingModeException;
import java.sql.*;
//import java.sql.Date;
import java.util.*;
import java.util.logging.*;

//import com.sun.rowset.CachedRowSetImpl;
//import com.sun.rowset.JdbcRowSetImpl;
//import javax.sql.rowset.JdbcRowSet;
//import org.sqlite.SQLiteConnection;
//import org.sqlite.jdbc3.JDBC3Connection;
//import org.sqlite.core.CoreConnection;
//import org.sqlite.core.CoreStatement;
//import javax.sql.rowset.RowSetFactory;
//import javax.sql.rowset.RowSetProvider;
//import javax.sql.rowset.JdbcRowSet;
//import javax.sql.rowset.RowSetMetaDataImpl;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
//import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;
import com.rabbitmq.client.ShutdownSignalException;

public class Worker implements Constants {

	//class constructor parameters
	protected String JDBC_URL;
	protected String rabbitBrokerIP;
	protected static String workerID;
	protected String workerQueueName;
	protected String workerTypeString;
	protected ContainerType workerType;
	/* running inside a container, send log message to master container  */
	protected static Log_Level LOG_LEVEL = Log_Level.verbose;   //default log level
	
	// other global class variables
	protected com.rabbitmq.client.Connection rabbitConnection = null;
	
	protected static  Channel channelToRabbit;
	protected static Logger logger;

	//MERGE-SORT op
	protected ArrayList<Queue<String>> queueList = null;
	
	protected static Connection sqliteConnection = null;
	
	/* list of nodes to output query results to */
	protected ArrayList<String> downstreamContainerIds = new ArrayList<String>(); 
	
	protected  static Worker worker = null;
	
	@SuppressWarnings("static-access")
	//CONSTRUCTOR
	protected Worker(String jDBC_URL, String rabbitBrokerIP, String workerID, String workerTypeString, String logLevel ) {
		
		super();
		JDBC_URL = jDBC_URL;
		this.rabbitBrokerIP = rabbitBrokerIP;
		this.workerID = workerID;
		this.workerTypeString = workerTypeString;
		LOG_LEVEL =   Log_Level.valueOf(logLevel);
		workerQueueName = workerID + "-Queue";
		
		try {
								
			// set global logger handle
			startLogger("./Worker.log");
			
			//get Env variable TIME_OUT and place in same named property for Dynamic IntraOp processing
			String timeOut = System.getenv("TIME_OUT");
			System.setProperty("TIME_OUT", timeOut);

			// worker type one of {"interiorCompute", "leafSelect", "apex", "dataOnly"}
			workerType = ContainerType.valueOf(workerTypeString.toUpperCase()); // See Constants.java for worker types}

			// validate rabbitMQ workerType exchange key passed as command line argument
			if (!WORKER_TYPES.contains(workerType)) {
				logQueryState("validateArgs: INVLALID <exchange key> given as 4th command line argument", Log_Level.quiet);
				logQueryState("validateArgs: Must be one of the following worker types: " + ContainerType.getNames(), Log_Level.quiet);
				throw new Exception("invalid workerType:  " + workerType);
			}

			
			// validate command line arguments
			//worker.validateArgs(argv);

			/* Open new SQLite all-in-memory database
			 * the JDBC_URL "jdbc:sqlite::memory:" will open a local in-memory only database  
			 * "jdbc:sqlite:" opens a memory cached temporary database with disk for overflow 
			 * Or connect to mounted SQLite local database file, 
			 * e.g "jdbc:sqlite:/home/david/databases/shuffle.db" 
			 */		
			
			//No need to start SQLite if Worker is executing a MERGE_SORT
			if( !OperatorType.valueOf(System.getenv("OPERATOR")).equals(OperatorType.MERGE_SORT) ) {
				
				sqliteConnection = connectSQLiteDB(JDBC_URL);
				sqliteConnection.setAutoCommit(false);
			
				/*
				Statement stmt = sqliteConnection.createStatement();
			
				//obtain ENV variables CACHE_SIZE and PAGE_SIZE and execute PRAGMA to set respective sizes
				if(Boolean.valueOf(System.getenv("DYNAMIC_INTRA_OPS_ALLOWED")) && Integer.valueOf(System.getProperty("TIME_OUT")) != 0) 
				{
					
					String CACHE_SIZE = System.getenv("CACHE_SIZE");
					String PAGE_SIZE = System.getenv("PAGE_SIZE");
				
					if(!CACHE_SIZE.equals("NOMINAL"))       //set CACHE_SIZE
						stmt.executeUpdate("PRAGMA cache_size=" + CACHE_SIZE);
					
					if(!PAGE_SIZE.equals("NOMINAL"))	//set PAGE_SIZE
						stmt.executeUpdate("PRAGMA page_size=" + PAGE_SIZE);
				}
				
				ResultSet rs = stmt.executeQuery("PRAGMA cache_size");
				rs.next();
				int cache_size = rs.getInt(1);
				rs.close();
				logQueryState("main: CACHE_SIZE= " + cache_size, Log_Level.verbose);
					
				rs = stmt.executeQuery("PRAGMA page_size");
				rs.next();
				int page_size = rs.getInt(1);
				rs.close();
				logQueryState("main: PAGE_SIZE= " + page_size, Log_Level.verbose);
				
				stmt.close();
				
				*/
				
				//DEBUG, reopen statement to see if cache_size is still set
				/*
				stmt = sqliteConnection.createStatement(); 
			    rs = stmt.executeQuery("PRAGMA cache_size");
				rs.next();
				cache_size = rs.getInt(1);
				rs.close();
				logQueryState("main: CACHE_SIZE= " + cache_size, Log_Level.verbose);
				*/
			
				if(JDBC_URL.contains(":memory:"))
					logQueryState("main: A new SQLite all-in-memory database has been created with URL " + JDBC_URL, Log_Level.verbose);
				else if(JDBC_URL.equals("jdbc:sqlite:''"))
					logQueryState("main: A new SQLite cached temporary database has been created with URL " + JDBC_URL, Log_Level.verbose);
				else
					logQueryState("main: A new SQLite database has been opened with URL " + JDBC_URL, Log_Level.verbose);

				logQueryState("main: Database URL " + sqliteConnection.getMetaData().getURL(), Log_Level.verbose);

				// logQueryState("Database UserName " + metaData.getUserName());
				//statementSQL = sqliteConnection.createStatement();
			}
			
			/* Open connection to rabbitMQ broker, declare queues and exchanges.
			 * Bind queues to exchanges. Wait for directions from Master sent to the
			 * worker queue using rabbit. Starts message Listeners that run forever.
			 * Start Socket server for container-2-container data exchange. */
			openALL_Connections(sqliteConnection);
			
		} catch (Exception e) {
			logQueryState("main: " + e.getLocalizedMessage() + "\n" + e.getMessage() + e.getClass().getCanonicalName(), Log_Level.verbose);
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			e.printStackTrace(pw);
			pw.flush();
			logQueryState("main: STACK-TRACE: " + sw.toString() + "\n" + e.getClass().getCanonicalName(), Log_Level.quiet);
			pw.close();
		}	
		
	}

	public static void main(String[] argv) {
		
		PrintStream errStream = null;
		try {
				
			errStream = new PrintStream("./localErr");
			
			if ((argv.length != 5)) {  //HANDLE ARGUMENTS ERROR
				errStream.print("validateArgs: Usage: Worker <JDBC_URL> <rabbitmq broker IP> <workerID> <rabbit exchange key(workerType)> <Log_Level>");
				errStream.print("validateArgs: # command line args found: " + argv.length);
				for (int i = 0; i < argv.length; i++) {
					errStream.print("argv[" + i + "]:  " + argv[i]);
				}
				
				errStream.print("validateArgs: ContainerMode: wrong number of command line arguments");
				throw new Exception("ContainerMode: wrong number of command line arguments");
			}
			 
			worker = new Worker(argv[0], argv[1], argv[2], argv[3], argv[4]);
			
			
		} catch (Exception e) {
			logQueryState("main: " + e.getLocalizedMessage() + "\n" + e.getMessage() + e.getClass().getCanonicalName(), Log_Level.verbose);
			StringWriter sw = new StringWriter();
		    PrintWriter pw = new PrintWriter(sw);
		    e.printStackTrace(pw);
		    pw.flush();
		    if(logger != null)
		    	logQueryState("main: STACK-TRACE: " + sw.toString() + "\n" + e.getClass().getCanonicalName(), Log_Level.quiet);
		    else	
		    	errStream.print("main: STACK-TRACE: " + sw.toString() + "\n" + e.getClass().getCanonicalName());
			pw.close();
		}
		finally {
			errStream.flush();
			errStream.close();
		}

	} // end main
	
	public void testPrintResultSet(String table, Connection connection) throws SQLException {
		
		String SQL = "SELECT * FROM " + table + " LIMIT 100";
		
		Statement stmt = connection.createStatement();
		ResultSet rs = stmt.executeQuery(SQL);
		
		printResultSetTable(rs, SQL, 20, true);
		rs.close();
		
	}

	//Message passing UTILITY
	public static byte[] serialize(Object obj) throws IOException {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		ObjectOutputStream os = new ObjectOutputStream(out);
		os.writeObject(obj);
		return out.toByteArray();
	}
	
	//Message passing UTILITY
	public static Object deserialize(byte[] data) throws IOException, ClassNotFoundException {
		ByteArrayInputStream in = new ByteArrayInputStream(data);
		ObjectInputStream is = new ObjectInputStream(in);
		return is.readObject();
	}
	
		
	protected String[] getTypes(String SQL_CreateTableDDL) {
		
		int  firstParen = SQL_CreateTableDDL.indexOf('(');
		int  lastParen = SQL_CreateTableDDL.lastIndexOf(')');
		String schema = SQL_CreateTableDDL.substring(firstParen + 1, lastParen);
		String[] types = schema.split(",");
		int colCount = types.length;
		int index = -2;
		for(int i = 0; i < colCount; i++) {
			types[i] = types[i].trim();   //remove any leading and trailing blanks in type
			types[i] = types[i].replaceFirst("\\(\\d+\\)", ""); //remove any precision clause from type, e.g. 'emp_no INT(11)' --> 'emp_no INT'
			types[i] = types[i].replaceFirst("\\w+\\s", ""); //remove field name and white space separating from type
			
			if( (index = types[i].indexOf(' ')) != -1)  //if anything follows type remove it
				types[i] = types[i].substring(0, index);
			
		}	
		
		return types;
	}

	public static synchronized String getSchema(ResultSetMetaData metaData) throws SQLException {
		
		String schema = "UNDEFINED";
		int precision = 0;
		schema =  metaData.getTableName(1) + ": [";
		
		for(int i = 1; i <= metaData.getColumnCount(); i++) {
			
			schema += metaData.getColumnName(i) +  " " + metaData.getColumnTypeName(i);
			if ((precision = metaData.getPrecision(i)) > 0) {
				schema += "(" + precision + ")";
				precision = 0;
			}
			
			if(i == metaData.getColumnCount())
				schema += "]";
			else
			   schema += ", ";
			
		} // end for 
		
		return schema;
	} // end method
		

	/* Constructs an INSERT query string from column values of the current row
	 * and meta-data in a ResultSet.
	 * Assumes only a single table is present in the ResultSet.
	 * Gets row data from the current row the ResultSet is pointed at. 
	 * Example: INSERT INTO tbl1 VALUES('foobar', 20);
	 */
	public String createINSERT(ResultSet rs) throws SQLException {

		String str = "INSERT INTO ";

		try {
			str += rs.getMetaData().getTableName(1) + " VALUES(";
			int colCount = rs.getMetaData().getColumnCount();
			
			for (int i = 1; i <= colCount; i++) {

				if (rs.getMetaData().getColumnTypeName(i).equals("VARCHAR"))
					str += "'" + rs.getString(i) + "'";
				else
					str += rs.getString(i);

				if (i == colCount) // exit and fence post condition
					str += ")";
				else
					str += ", ";
				
			} // end for
		} catch (SQLException e) {
			e.printStackTrace();
			logQueryState("createINSERT: " + e.getMessage(), Log_Level.verbose);
			throw e;
		}

		return str;
	} //end createINSERT

	
	/* Constructs a CREATE TABLE DDL statement string from  meta-data in a ResultSet.
	 * The table name is supplied as a parameter: outputTableName.
	 * Column name are composed from the tableName and a col identifier, e.g for the 
	 * table FOO with two columns, {'id', 'name'} :  FOO_id, FOO_name.
	 * This permits the TABLE's columns to be traced back to the original relation/table
	 * has the query progresses thru the relational query tree.
	 */
	public String createTableSchema(ResultSet rs, String outputTableName) throws SQLException {

		String SQLstr = "CREATE TABLE ";

		try {
			//String tableName = rs.getMetaData().getTableName(1);
			SQLstr += outputTableName + " (";
			int count = rs.getMetaData().getColumnCount();
			
			String colName = "";
			for (int i = 1; i <= count; i++) {
				//form column name by concatenating tableName and ColumnName
				//colName = outputTableName + "_" + rs.getMetaData().getColumnName(i);
				colName =  rs.getMetaData().getColumnName(i);
				SQLstr +=  colName + " " + rs.getMetaData().getColumnTypeName(i);
				
				if( !(rs.getMetaData().getPrecision(i) == 0))
					SQLstr +=  "(" + rs.getMetaData().getPrecision(i) + ")";

				if (i == count) // exit and fence post
					SQLstr += ")";
				else
					SQLstr += ", ";
			} // end for
			
		} catch (SQLException e) {
			e.printStackTrace();
			logQueryState("createTableSchema: " + e.getMessage(), Log_Level.verbose);
			throw e;
		}

		return SQLstr;
	} // end method


	protected static boolean tableExists(String tableName, Connection connection) throws SQLException {

		boolean flag = false; // indicates if table exists
		try {
			DatabaseMetaData md = connection.getMetaData();
			ResultSet rs = md.getTables(null, null, tableName, null);
			
			while(rs.next()) {
				String name = rs.getString(3); 
				if(name.equals(tableName)) {  //TABLE_NAME
					flag = true;
					break;
				}
			}
			
		} catch (SQLException sqlEx) {
			sqlEx.printStackTrace();
			logQueryState("tableExists: " +sqlEx.getMessage(), Log_Level.verbose);
			throw sqlEx;
		}
		return flag;

	} // end method

	/*
	 * Given path to log file, sets global logger handle
	 */
	public  void startLogger(String logFilePath) throws Exception {

		// set up logger
		logger = Logger.getLogger("MyLogger");

		try {
			// This block configures the logger handler and sets its formatter
			FileHandler loggerFileHandle = new FileHandler(logFilePath);
			logger.addHandler(loggerFileHandle);
			SimpleFormatter formatter = new SimpleFormatter();
			loggerFileHandle.setFormatter(formatter);

			// start log
			logger.info("startLogger: Start Worker log");

		} catch (SecurityException se) {
			se.printStackTrace();
			throw se;
		} catch (IOException ioe) {
			ioe.printStackTrace();
			throw ioe;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}

	} // end method

	/* OLD VERSION
	public  void validateArgs(String[] argv) throws Exception {

		// check correct number of arguments
		if (!ContainerMode && (argv.length != 6)) {   //HANDLE ERROR
		
		 // Command line must provide: <JDBC_URL> <rabbit broker IP> <worker's ID> <rabbit exchange key (workerType)> <list of downstream nodes>.
		 // <list of downstream nodes> is the list of nodes to output query results to.
		 // The list is comma delimited string with no spaces, that will be tokenized and separated subsequently
		 // e.g. worker1,worker2,worker3

			logQueryState("validateArgs: Usage: Worker <JDBC_URL> <rabbitmq broker IP> <workerID> <rabbit exchange key (workerType)> <list of downstream nodes>  <Log_Level>", Log_Level.quiet);
			logQueryState("validateArgs: # command line args found: " + argv.length, Log_Level.quiet);
			for (int i = 0; i < argv.length; i++) {
				logQueryState("argv[" + i + "]:  " + argv[i], Log_Level.quiet);
			}
			logQueryState("validateArgs: !ContainerMode: wrong number of command line arguments", Log_Level.quiet);
			throw new Exception("!ContainerMode: wrong number of command line arguments");
		}
		else if (ContainerMode && (argv.length != 5)) {  //HANDLE ERROR
			logQueryState("validateArgs: Usage: Worker <JDBC_URL> <rabbitmq broker IP> <workerID> <rabbit exchange key(workerType)> <Log_Level>", Log_Level.quiet);
			logQueryState("validateArgs: # command line args found: " + argv.length, Log_Level.quiet);
			for (int i = 0; i < argv.length; i++) {
				logQueryState("argv[" + i + "]:  " + argv[i], Log_Level.quiet);
			}
			logQueryState("validateArgs: ContainerMode: wrong number of command line arguments", Log_Level.quiet);
			throw new Exception("ContainerMode: wrong number of command line arguments");
		}
		
		//set LOG_LEVEL
		if(ContainerMode)
			LOG_LEVEL =   Log_Level.valueOf(argv[4]);
		else
			LOG_LEVEL =   Log_Level.valueOf(argv[5]);
		
		//SQLite database connection URL
		JDBC_URL = argv[0]; 
		rabbitBrokerIP = argv[1];
		logQueryState("validateArgs: IP address '" + argv[1] + "' for rabbitBrokerIP resolved as:  " + rabbitBrokerIP, Log_Level.verbose);

		// set global class constants and validate exchange key
		// local worker's queue name set identical to worker's ID 
		workerID = argv[2];
		workerQueueName = workerID + "-Queue";
		//workerType = argv[3]; // worker type one of {"interiorCompute", "leafSelect", "apex", "dataOnly"}
		workerType = ContainerType.valueOf(argv[3].toUpperCase()); // See Constants.java for worker types}
		
		// validate rabbitMQ workerType exchange key passed as command line argument
		if (!WORKER_TYPES.contains(workerType)) {
			logQueryState("validateArgs: INVLALID <exchange key> given as 4th command line argument", Log_Level.quiet);
			logQueryState("validateArgs: Must be one of the following worker types: " + ContainerType.getNames(), Log_Level.quiet);
			throw new Exception("invalid workerType:  " + workerType);
		}

		// must be a comma delimited list of names with no embedded spaces, e.g.
		// worker1,worker2,worker3 obtained as a LIST of command line arguments	
		if(!ContainerMode && !(argv[4].equals("null")))
			downstreamContainerIds = new ArrayList<String>(Arrays.asList(argv[4].split(",")));			

	} // end method
	*/
	
	public  void validateArgs(String[] argv) throws Exception {

		/*
		 if ((argv.length != 5)) {  //HANDLE ERROR
			logQueryState("validateArgs: Usage: Worker <JDBC_URL> <rabbitmq broker IP> <workerID> <rabbit exchange key(workerType)> <Log_Level>", Log_Level.quiet);
			logQueryState("validateArgs: # command line args found: " + argv.length, Log_Level.quiet);
			for (int i = 0; i < argv.length; i++) {
				logQueryState("argv[" + i + "]:  " + argv[i], Log_Level.quiet);
			}
			logQueryState("validateArgs: ContainerMode: wrong number of command line arguments", Log_Level.quiet);
			throw new Exception("ContainerMode: wrong number of command line arguments");
		}
		*/	
		
		//SQLite database connection URL
		JDBC_URL = argv[0]; 
		rabbitBrokerIP = argv[1];
		logQueryState("validateArgs: IP address '" + argv[1] + "' for rabbitBrokerIP resolved as:  " + rabbitBrokerIP, Log_Level.verbose);

		// set global class constants and validate exchange key
		/* local worker's queue name set identical to worker's ID */
		workerID = argv[2];
		workerQueueName = workerID + "-Queue";
		// worker type one of {"interiorCompute", "leafSelect", "apex", "dataOnly"}
		workerType = ContainerType.valueOf(argv[3].toUpperCase()); // See Constants.java for worker types}
		
		// validate rabbitMQ workerType exchange key passed as command line argument
		if (!WORKER_TYPES.contains(workerType)) {
			logQueryState("validateArgs: INVLALID <exchange key> given as 4th command line argument", Log_Level.quiet);
			logQueryState("validateArgs: Must be one of the following worker types: " + ContainerType.getNames(), Log_Level.quiet);
			throw new Exception("invalid workerType:  " + workerType);
		}

		//set LOG_LEVEL
		LOG_LEVEL =   Log_Level.valueOf(argv[4]);		

	} // end method
	
	/*
	 * Opens connection to rabbitMQ broker, declares queues and exchanges. Binds
	 * queues to specific exchanges for purposes of multi-cast and unicast
	 * control messaging. Opens Socket server for container-2-container data(ResultSet) exchange.
	 * Launches a LISTENER in a separate thread that receives Cmd&Ctrl messages from master. 
	 */
	public void  openALL_Connections(final Connection sqliteConnection) throws InterruptedException, IOException {

		// connect to rabbitMQ broker, this may take some time if broker not fully up
		ConnectionFactory factory = new ConnectionFactory();
		factory.setPort(5672);
		factory.setUsername("guest");
		factory.setPassword("guest");
		int count = 0;
		boolean valid = validIP(rabbitBrokerIP);
		while (true) { // keep trying to connect to rabbitMQ broker
			try {      //rabbitBrokerIP is an IP literal representing the network address of the rabbitMQ broker
				
				if(!valid) // resolve name to valid IP literal and reset class variable 'rabbitBrokerIP'
					rabbitBrokerIP = InetAddress.getByName(rabbitBrokerIP).getHostAddress();
				
				factory.setHost(rabbitBrokerIP); //the rabbit broker IP address literal, e,g, 10.05.16.08
				// open channel to rabbit broker
				rabbitConnection = factory.newConnection();
				break;
			} catch (Exception e) {
				logQueryState("openRabbitConnections: Attempt opening connection to rabbitMQ broker. " + e.getMessage() +
						      "\n" + e.getClass().getCanonicalName(), Log_Level.verbose);
				Thread.sleep(1000);
				count++;
				if (count > 60) {
					logQueryState("openRabbitConnections: Attempt opening connection to rabbitMQ broker for 60 seconds timed-out.",
							Log_Level.verbose);
					throw new IOException("rabbitMQ connection timeout");
				} else
					continue;
			} // end try
		} // end while
		
		logQueryState("openRabbitConnections: " + workerID + " Connection opened to rabbitMQ broker at IP " + rabbitBrokerIP, Log_Level.verbose);

		//logger.info("openRabbitConnections: right before channelToRabbit established");
		// create global channel to remote rabbit broker
		Worker.channelToRabbit = rabbitConnection.createChannel();
		logger.info("openRabbitConnections: channelToRabbit established");
		
		// DECLARE rabbitMQ QUEUES and EXCHANGES. Bind queues to exchanges.
		// Master may not have yet declared Exchange, so declare just in case.
		channelToRabbit.exchangeDeclare(ALL_WORKERS_EXCHANGE, "direct");
		logQueryState("openRabbitConnections: Declare direct ALL_WORKERS_EXCHANGE on " + workerID, Log_Level.verbose);

		// Create new queue specific to THIS worker instance.
		channelToRabbit.queueDeclare(workerQueueName, false, false, false, null);
		logQueryState("openRabbitConnections: Declare worker queue '" + workerQueueName + "-Queue'", Log_Level.verbose);

		// UNI-CAST
		// Also bind THIS worker queue with worker's unique name so a
		// message based upon its name can be sent to this worker only.
		channelToRabbit.queueBind(workerQueueName, ALL_WORKERS_EXCHANGE, workerID);
		logQueryState("openRabbitConnections: Bind worker queue '" + workerQueueName + "' with ALL_WORKERS_EXCHANGE with key '" + workerID + "'", Log_Level.verbose);
		
		//not needed for leaf containers in query tree, which read directly from mounted database partitions on disk
	    //if(JDBC_URL.contains("memory") || JDBC_URL.contains("TEST.db")) {
		//if(workerType.equals("interiorCompute") || JDBC_URL.contains("TEST.db")) {	
		MultiThreadedServer serverRunnable = null;
		//if(workerType != ContainerType.DATA_ONLY || JDBC_URL.contains("TEST.db")) {	
		if(workerType != ContainerType.DATA_ONLY) {	
			
			/* Start socket server to accept query data results from  upstream containers,
			 * in ephemeral range  32768 to 61000. */
			if(workerID.endsWith("MERGE_SORT") ) {  //handle MERGE_SORT case
				
				queueList = new ArrayList<Queue<String>>();  //list of merge queues
				
				serverRunnable = new MultiThreadedServer(queueList, workerID, DATA_PORT, sqliteConnection, channelToRabbit);
				Thread sortQueuesThread = new Thread(serverRunnable);
				sortQueuesThread.start();
				
				/* wait for server to bind to socket */
				//while( socketServer.getServerSocket() != null   &&  !(socketServer.getServerSocket().isBound()) ) { 
				while(serverRunnable.getServerSocket() != null && !(serverRunnable.getServerSocket().isBound()) ) { 
					logQueryState("openRabbitConnections MERGE_SORT: MultiThreadedServer WAITING for server socket to  bind on port " +
		                    		DATA_PORT, Log_Level.verbose);
					Thread.sleep(50);
				}
				
				logQueryState("openRabbitConnections MERGE_SORT: MultiThreadedServer STARTED & LISTENING for INPUT socket requests on port " +
	                    DATA_PORT, Log_Level.verbose);
				
				int inboundSockets = Integer.valueOf(System.getenv("SIZE_INBOUND"));
				
				if(System.getenv("QUERY").contains("STREAMING")) {
					
					//WAIT for all socket connections because STREAMING is INTERLEAVED 
					synchronized(queueList) {	

						while(queueList.size() != inboundSockets) {

							try {
								logQueryState("sortMergeStreaming:  WAITING for total of socket connections: " + inboundSockets, Log_Level.quiet);

								queueList.wait();

								logQueryState("sortMergeStreaming:  NOTIFIED of increase of total socket connections: " 
										+ serverRunnable.getNoOfAcceptedSockets() , Log_Level.quiet);
							} catch (InterruptedException e) {
								logQueryState("sortMergeStreaming: Interrupted while WAITING for all socket connections:  " + e.toString(), Log_Level.quiet);
							}

						} // while

					} // end sync

					logQueryState("sortMergeStreaming: All sockets accepted:  " + serverRunnable.getNoOfAcceptedSockets(), Log_Level.quiet);
					sortMergeStreaming(queueList, serverRunnable.getAcceptedSockets());  //Execute STREAMING MERGE_SORT
					logQueryState(QUERY_COMPLETED + " MERGE-SORTED at time, " + System.currentTimeMillis() + ", " + workerID,
						Log_Level.metrics);

					//all completed, no rabbit channel needed or consumer listener
				}  // end waiting for all STREAMING connections
				
			} else { //normal processing of a Compute Container, in-bound containers sockets attached as containers are scheduled 
				serverRunnable = new MultiThreadedServer(workerID, DATA_PORT, sqliteConnection, channelToRabbit);
		
				Thread serverThread = new Thread(serverRunnable);
				//serverThread.setPriority(Thread.MAX_PRIORITY);
				serverThread.start();
			
				/* wait for server to bind to socket */
				while( serverRunnable.getServerSocket() != null   &&  !(serverRunnable.getServerSocket().isBound()) ) { 
					logQueryState("openRabbitConnections: MultiThreadedServer WAITING for server socket to  bind on port "
							+ DATA_PORT, Log_Level.verbose);
					Thread.sleep(50);
				}
			
				logQueryState("openRabbitConnections: MultiThreadedServer STARTED & LISTENING for INPUT socket requests on port " +
                    DATA_PORT, Log_Level.verbose);
			}
			
		} // end only if Container is Compute type

		// MESSAGE QUEUE LISTENER: Anonymous Inner class overrides rabbitMQ DefaultConsumer and
		// executes consumer Callback when message arrives in CTRL queue from Master.
		Consumer consumerWorker = new DefaultConsumer(channelToRabbit) {

			@Override
			public void handleShutdownSignal(String consumerTag, ShutdownSignalException sig) {

				logQueryState("handleShutdownSignal: " + workerID + " listener received shutDown signal: \n" 
						+ sig.getLocalizedMessage(), Log_Level.quiet);
			}

			@Override
			public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body)
					throws IOException {
				
				ConvertBytes converter = new ConvertBytes();
				
				// process message that has arrived in rabbit queue
				try {
				
					Object obj = converter.convertFromBytes(body);
					
					if (obj instanceof QueryCtrlMsg) {
						/* 	Execute SQL statement sent by Master */
						QueryCtrlMsg queryCtrlMsg =  (QueryCtrlMsg)obj;
						QuerySQL payload =  queryCtrlMsg.getPayload();
						String SQL_Query = payload.getQuery();
						String workerID = payload.get_cc().getId();
						String outputTableName =  payload.getOutputTableName();
						ArrayList<String> downstreamIds = payload.getDownstreamContainerIds();
						
					    //logQueryState(payload.toString());
						logQueryState("handleDelivery: ACK receipt of query request from worker: " + workerID
								+ " at time " + System.currentTimeMillis() + ".\n" + SQL_Query +"; ", Log_Level.quiet);

						/* terminal case if worker is apex container, just output query results */
						if(downstreamIds.isEmpty()) {
							
							logQueryState("handleDelivery: " + workerID + " upsteam containers", Log_Level.verbose);
							
							if(workerID.endsWith("MERGE_SORT") && !(System.getenv("QUERY").contains("STREAMING"))) {  //MERGE FILLED SORT QUEUES
								
								logQueryState("handleDelivery: MERGE-SORT QueryCtrlMsg received, NOMINAL PROCESSING.", Log_Level.verbose);
								worker.sortMergeFullyLoadedQeues(queueList);  //QUEUES HAVE BEEN COMPLETELY FILLED, NO STREAMING
								
								//QUERY COMPLETE
								logQueryState(QUERY_COMPLETED + " MERGE-SORT APEX WORKER at time, " + System.currentTimeMillis() 
												+ ", " + workerID, Log_Level.metrics);
							}
							else if(workerID.endsWith("MERGE_SORT") && System.getenv("QUERY").contains("STREAMING")) {  //STREAMING MERGE SORT QUEUES
								logQueryState("handleDelivery: MERGE-SORT QueryCtrlMsg received, STREAM PROCESSING.", Log_Level.verbose);
							}
							else  {   //JUST PRINT QUERY RESULTS TO LOG or stdout
								java.sql.Statement statement = sqliteConnection.createStatement();
								ResultSet rs = statement.executeQuery(SQL_Query);
								final int  BATCH_SIZE = Integer.parseInt(System.getenv("BATCH_SIZE"));
								//Print rows in chunks of BATCH_SIZE. Print headers for each BATCH_SIZE rows.
								printResultSetTable(rs, SQL_Query, BATCH_SIZE, true);  
								statement.close();
								rs.close();
								
								//QUERY COMPLETE
								logQueryState(QUERY_COMPLETED + " APEX WORKER at time, " + System.currentTimeMillis() 
												+ ", " + workerID, Log_Level.metrics);
							}
							
						}
						else {
							
							logQueryState("handleDelivery: " + workerID + " starting exchanging of ResultSet", Log_Level.verbose);
							
							ComputeContainer cc =  payload.get_cc();

							Exchange exchange = payload.getParallelExchange();  //get Exchange type

							/* exchanges CRS or Array row partitions to downstream containers according to exchange type  */
							exchange.executeExchange(cc, SQL_Query, downstreamIds, sqliteConnection, outputTableName);

							logQueryState("handleDelivery: " + cc.getId() + 
									" sent Output ResultSet paritions to downstream containers {" + 
									downstreamIds + "} using TCP/IP socket", Log_Level.verbose);
						}
							
					}
					else
						logQueryState("handleDelivery:  UNKNOWN CtrlMsg type " + obj.toString(), Log_Level.quiet);
					
				} catch (TimeOutException toe) {
					logQueryState("handleDelivery: WORKER executeExchange threw query TimeOutException:\n NO further processing pending allocation of DYNAMIC INTRA OPS", Log_Level.quiet);

				} catch ( Exception e ) {
					StringWriter sw = new StringWriter();
				    PrintWriter pw = new PrintWriter(sw);
				    e.printStackTrace(pw);
				    pw.flush();
				    //logQueryState("WORKER CTRL MESSAGE FAILED: " + new String(body));
					logQueryState("handleDelivery: WORKER CtrlMsg FAILURE STACK-TRACE: \n" + 
				              e.getClass().getCanonicalName() + "\n" + sw.toString(), Log_Level.quiet);
					pw.close();
				} 

			} // end method handleDelivery
			
		}; // end anonymous class
		
		// Start consumer message queue listener to service messages sent to workerQueueName.
		channelToRabbit.basicConsume(workerQueueName, true, consumerWorker);
		logQueryState("openRabbitConnections: Rabbit Listener started. Waiting to consume queue arrivals on : " 
						+ workerQueueName, Log_Level.quiet);
		
		/* send ReadyCTRLMsg to Master to request processing instructions for leaf containers */
		//if(workerType.equalsIgnoreCase("leafSelect")) {
		if(workerType.equals(ContainerType.LEAF)) {
			ReadyCtrlMsg ready = new ReadyCtrlMsg(workerID);
			try {
				ConvertBytes converter = new ConvertBytes();
				channelToRabbit.basicPublish(ALL_WORKERS_EXCHANGE, ACK_KEY, null,
						converter.convertToBytes(ready));
				logQueryState("openRabbitConnections: Worker sent ReadyCtrlMsg to  Master:  " + ready.toString(), 
								Log_Level.verbose);
			} catch (IOException e) {
				logQueryState("openRabbitConnections: FAILED: ReadyCtrlMsg to  Master:  " + ready.toString(), 
						Log_Level.quiet);
				e.printStackTrace();
			}
		}
		
	} // end method openRabbitConnections(...)
	
	//returns an empty queue, else null to indicate no queue is empty
	//@SuppressWarnings("unused")
	protected  Queue<String> queueListEmpty(ArrayList<Queue<String>> queueList) {
	
		Queue<String> empty = null;
			
		for(int i = 0; i < queueList.size(); i++) {
			
			if (queueList.get(i).isEmpty()) {
				empty = queueList.get(i);
				break;
			}
		} // for	
		
		return empty;
		
	} // method
	
	//returns an empty queue, else null to indicate no queue is empty
	//@SuppressWarnings("unused")
	protected  boolean allQueuesFilled(ArrayList<Queue<String>> queueList) {
		
			boolean filled = true;
				
			for(int i = 0; i < queueList.size(); i++) {
				
				if (queueList.get(i).isEmpty()) {
					
				    filled = false;
				}
				
			} // for	
			
			return filled;
			
	} // method
	
		
	//waits for an empty queue to fill for just 500 ms, then moves on. Goes back to empty queues later in loop to check if they
	//are filled.
	public void waitAllQueuesFilled(ArrayList<Queue<String>> queueList, ArrayList<Socket> sockets) throws InterruptedException {

		Queue<String> q = null;
		boolean[] filledFlags = new boolean[queueList.size()];
		int n  = 0;

		while(true) {

			//check if all queues are filled
			int countFilled = 0;
			for(int i = 0; i < filledFlags.length; i++) {
				if(filledFlags[i])
					countFilled++;
			}

			if(countFilled == queueList.size())
				break;  // all queues have elements, exit loop

			//check queues in list in circular fashion,  modulo list size
			if(!filledFlags[n]) {

				q = queueList.get(n);

				synchronized(q) {

					if(q.isEmpty()) {   //check again when locked on queue monitor
						logQueryState("waitAllQueuesFilled: WAITING for queue on " + workerID + " associated with local client socket IP "
								+ sockets.get(n).getLocalSocketAddress().toString() + " to  INTIALLY FILL. ", 
								Log_Level.verbose);

						q.wait(500);   //wait 500 ms, then move on to another queue
						
					}
					
					if(!q.isEmpty())         //check again if queue has an element
						filledFlags[n] = true;
					
				} //sync
				
			} // !filledFlags[n]

			n = (n + 1) % queueList.size();  //advance n modulo queue size

		} // for

		logQueryState("waitAllQueuesFilled: ALL queues  INITIALLY have elements.", Log_Level.verbose);

	} // end waitAllQueuesFilled(...)
	
	//STREAMING PROCESSING case. Input is immediately merged as it is loaded into queues.
	public void sortMergeStreaming(ArrayList<Queue<String>> queueList, ArrayList<Socket> sockets) throws IOException, InterruptedException {
			
			//locate sort direction in Order By clause, e.g. extract "ORDER BY C2.emp_no_employees DESC"
			//from "SELECT * FROM C2 ORDER BY C2.emp_no_employees DESC;"
			String predicate = System.getenv("PREDICATE");  //test access to env variable
			String orderByClause = predicate.substring(predicate.indexOf("ORDER BY")).trim();
			int lastSpaceIndex = orderByClause.lastIndexOf(' ');
			//remove leading table name in fully qualified field name
			String sortDirectionString = orderByClause.substring(lastSpaceIndex + 1).trim();
			Queue<String> q = null;
			
			//merge algorithm ASSUMES that all queues will initially NOT be empty
			waitAllQueuesFilled(queueList, sockets);
			
			//queues are formatted to contain a formatted row with a key appended to end: (formatted row string) + key
			int rowsSorted = 0;
			int lastIndex = -1;
			
			 BufferedWriter out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("MergeSortedRows")));
			  // = new BufferedWriter(new OutputStreamWriter(System.out));
			
			//assume all queues have at least one element at this point
			//don't populate 1st queue in list (index 0), that happens first time thru loop
			ArrayList<String> headRows = new ArrayList<String>(queueList.size());  //list of 1st row in each queue
			ArrayList<String> keys = new ArrayList<String>(queueList.size());
			for(int i = 1; i < queueList.size(); i++) {  			//initialized array of keys
				String row = queueList.get(i).remove();				//dequeue element of ith queue
				lastIndex = row.lastIndexOf('|');          			//find last char of formatted row
				keys.add(row.substring(lastIndex + 1));        		//add formatted row's key to list of keys    
				headRows.add(row.substring(0, lastIndex + 1)); 		//add head of ith queue to ith element of headRows
			}
			
			//add the empty String at index 0, as a place marker for headRows[0], which is inserted first time thru loop
			keys.add(0, "UNDEFINED"); 
			headRows.add(0,"UNDEFINED");
			
			int j = 0;  //Arbitrarily start at queue in list at index 0 for currentKey and sortedRow
			String currentKeyValue =  "UNDEFINED"; 
			String sortedRow = "UNDEFINED";
			String rawRow = "UNDEFINED";
			  
			try {    //merge-sort all queues until only ONE queue is left
				
				getRowFromJthQueue:
				while(queueList.size() > 1) {

					q = queueList.get(j); 		//set jth queue as head of sort
					Socket s = sockets.get(j);
					
					//remove formatted row from selected queue
					try {	
						rawRow = q.remove();			  			  		//dequeue element of jth queue, get new row
						lastIndex = rawRow.lastIndexOf('|');          		//find last char of rawRow
						currentKeyValue = rawRow.substring(lastIndex + 1);	//update currentKey 
						sortedRow = rawRow.substring(0, lastIndex + 1); 	//update sortedRow at head of sort for all queues
						keys.set(j, currentKeyValue);        		//set jth element in keys list to row's key value     
						headRows.set(j, sortedRow); 				//set jth element of list of headRows to head of jth queue	
					} catch (NoSuchElementException nsee ) {
						
						logQueryState("sortMergeStreaming: EXCEPTION processing queues: REMOVE ROW: " + nsee.toString() , Log_Level.verbose);
						
						synchronized(q) { //lock jth queue so no input thread can update it concurrently
							
				        	try {
				        		
				        		if(q.isEmpty()) {   
				        			
				        			if(s.isClosed() || !s.isConnected()) {   //EOF was seen   
				        				
				        				queueList.remove(j);  //remove queue from list
										sockets.remove(j);    //remove associated socket
										keys.remove(j);  	  //remove key from list of keys for empty queue
										headRows.remove(j);   //remove row from headRows list
										j = 0;    			  //set queue holding new currentValue to first queue
										
										//change to 0th queue as head, get its current row from headRows[0] and key keys[0]
										currentKeyValue = keys.get(0);
										sortedRow = headRows.get(0);
										
				        				logQueryState("sortMergeStreaming:  empty queue with drained and closed socket found.",
									    		  Log_Level.quiet);
				        				
					        		}	
				        			else {
				        				logQueryState("sortMergeStreaming: WAITING: EXECEPTION REMOVING ROW, queue "
				        			    + j + ", associated with local socket "
							        	+ s.getLocalSocketAddress().toString() + " with key " + keys.get(j) + ", queue size=" + q.size()
										+ ", " + nsee.toString(), Log_Level.verbose);
				        		
				        				q.wait();   //wait for queue to fill with at least one element, e.g. 
				        		
				        				logQueryState("sortMergeStreaming: queue " + j + " associated with local socket "
				        				+ s.getLocalSocketAddress().toString() + " REFILLED  with queue size=" + q.size(),
				        				Log_Level.quiet);
				        				
				        				//don't change queues, keep jth queue as head, go back and try to get its head row again
				        				continue getRowFromJthQueue;
				        			}
				        			
				        		} // if(q.isEmpty())
				        		
				        	} catch (Exception ie) {
				        			logQueryState("sortMergeStreaming: ERROR WAITING on queue to be repopulated Interrupted: " + ie.getMessage(), Log_Level.quiet);
				        	}
				        	
						} // sync
						
					}  //try
					
					String nextKey = "UNDEFINED";
					//update currentKeyValue from selected queue i, 
					//if HeadRows(i)'s extracted key makes it the new head of the sort
					for(int i = 0, cmp = 0; i < queueList.size(); i++) {

						nextKey = keys.get(i);
						cmp = currentKeyValue.compareTo(nextKey);

						if(sortDirectionString.equals("ASC") ) {
							if(cmp > 0) {                      //if cmp == 0, values don't change
								currentKeyValue = nextKey;    	//update key at head of sort for all queues
								sortedRow = headRows.get(i);  	//updated sorted row to output
								j = i;  					 	//switch to queue holding head of current sorted value
							}
								
						}
						else  if(sortDirectionString.equals("DESC")) { // DESC
							if(cmp < 0) {						//if cmp == 0,  values don't change
								currentKeyValue = nextKey;    	//update key at head of sort for all queues
								sortedRow = headRows.get(i);  	//updated sorted row to output
								j = i;  					 	//switch to queue holding head of current sorted value					
							}
						}
						else 
							logQueryState("sortMergeStreaming: sort direction in OrderBy clause unknown: " + sortDirectionString, Log_Level.quiet);

					} //for
					
					out.write(sortedRow);  		//write out row to output stream
					out.newLine();
					//out.flush(); //JUST FOR DEBUGGING,  COMMENT OUT FOR PRODUCTION
					rowsSorted++;
					
				}  //while
				
				logQueryState("sortMergeStreaming: COMPLETED MERGING OF MULTIPLE QUEUE INPUTS." , Log_Level.verbose);
				
			} catch(IndexOutOfBoundsException ioobe ) {
				logQueryState("sortMergeStreaming: IndexOutOfBoundsException ERROR,  queueListSize=" + queueList.size() 
					+ ", keysSize=" + keys.size() + ", queue j =" + j + ", currentKeyValue=" + currentKeyValue, Log_Level.quiet);
				out.close();
				throw ioobe;  //re-throw
			} catch (Exception e ) {
				logQueryState("sortMergeStreaming: EXCEPTION, rowsSorted: " + rowsSorted + ", " + e.getMessage(), Log_Level.quiet);
				out.close();
				throw e;  //re-throw
			}
		
			//write out all elements in remaining queue which are already in sorted order
			logQueryState("sortMergeStreaming: BEGIN writing LAST queue in-order." , Log_Level.verbose);
			q = queueList.get(0);
			Socket s = sockets.get(0);
			
			//remove formatted key and row from last queue until EOF
			lastQueue:
			while(!q.isEmpty()) {

				//remove next formatted row from selected queue
				try {
					rawRow = q.remove();  		//dequeue row
					lastIndex = rawRow.lastIndexOf('|');
					sortedRow = rawRow.substring(0, lastIndex + 1);
					out.write(sortedRow);  			   //write out row to output stream
					out.newLine();
					//out.flush(); //JUST FOR DEBUGGING,  COMMENT OUT FOR PRODUCTION
					rowsSorted++;	
				} catch (Exception e ) {
					
					logQueryState("sortMergeStreaming: EXCEPTION processing last queue, REMOVE ROW:" + e.getLocalizedMessage() , Log_Level.verbose);
					
					synchronized(q) {
						
			        	try {
			        		
			        		if(q.isEmpty()) {
			        			
			        			if(s.isClosed() || !s.isConnected()) {   //EOF was seen   
									
			        				logQueryState("sortMergeStreaming:  empty queue with drained and closed socket found.",
								    		  Log_Level.verbose);
			        				break;
				        		}	
			        			else {
			        				logQueryState("sortMergeStreaming: WAITING: EXECEPTION REMOVING ROW, last queue,"
						        	+ " queue size=" + q.size()	+ ", " + e.toString(), Log_Level.verbose);
			        		
			        				q.wait();   //wait for queue to fill with at least one element, e.g. 
			        		
			        				logQueryState("sortMergeStreaming: last queue associated with local socket "
			        				+ s.getLocalSocketAddress().toString() + " REFILLED  with queue size=" + q.size(),
			        				Log_Level.verbose);
			        				
			        				continue lastQueue;  //try to remove from q again
			        			}
			        			
			        		}
			        		 
			        	} catch (Exception ie) {
			        			logQueryState("sortMergeStreaming: ERROR WAITING on queue to be repopulated Interrupted: " + ie.getMessage(), Log_Level.quiet);
			        	}
					} // sync
					
				}  //try
				
			} // outer lastQueueKey while
				
			out.flush();
			out.close();
			
			logQueryState(QUERY_COMPLETED + " STREAMING MERGE_SORT APEX WORKER at time, " + System.currentTimeMillis() 
					+ ", " + workerID, Log_Level.metrics);
			
			logQueryState("sortMergeStreaming: ROWS MERGE-SORTED and output to file: " + rowsSorted , Log_Level.verbose);
			
		} // end sortMergeStreaming
	
	//NOMINAL PROCESSING case. Input is loaded completely into queues before processing. NO streaming is used.
	public void sortMergeFullyLoadedQeues(ArrayList<Queue<String>> queueList) throws IOException {
		
		//locate sort direction in Order By clause, e.g. extract "ORDER BY C2.emp_no_employees DESC"
		//from "SELECT * FROM C2 ORDER BY C2.emp_no_employees DESC;"
		String predicate = System.getenv("PREDICATE");  //test access to env variable
		String orderByClause = predicate.substring(predicate.indexOf("ORDER BY")).trim();
		int lastSpaceIndex = orderByClause.lastIndexOf(' ');
		//remove leading table name in fully qualified field name
		String sortDirectionString = orderByClause.substring(lastSpaceIndex + 1).trim();

		 BufferedWriter out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("MergeSortedRows")));
		  // = new BufferedWriter(new OutputStreamWriter(System.out));
		 
		//queues are formatted to contain a formatted row with a key appended to end: (formatted row string) + key
		int rowsSorted = 0;
		int lastIndex = -1;
		String sortedRow = "UNDEFINED";
		String element = "UNDEFINED";
		String key = "UNDEFINED";
			
		//INTIALIZE headRows and keys
		ArrayList<String> headRows = new ArrayList<String>(queueList.size());  //list of 1st row in each queue
		ArrayList<String> keys = new ArrayList<String>(queueList.size());
		for(int i = 0; i < queueList.size(); i++) {  		
			element = queueList.get(i).remove();	//dequeue element of ith queue
			lastIndex = element.lastIndexOf('|');   //find last char of formatted row
			key = element.substring(lastIndex + 1);	//key follows last '|'
			keys.add(key);        					//add formatted row's key to list of keys
			sortedRow = element.substring(0, lastIndex + 1);
			headRows.add(sortedRow); 		//add head of ith queue to ith element of headRows
		}
		
		//index of element in keys[] selected from comparisons of all elements at head of respective sorted queues
		int j = 0;  //set current sorted value  at head of queue j
		String currentKeyValue = keys.get(j);
		int cmp = 0;
		
		try {
			
			while(queueList.size() > 1) {

				//for(int i = 0; i < keys.size(); i++) {
				for(int i = 0; i < queueList.size(); i++) {

					cmp = currentKeyValue.compareTo(keys.get(i));

					if(sortDirectionString.equals("ASC") ) { 
						if(cmp > 0) {							  //if cmp == 0, do nothing
							currentKeyValue = keys.get(i);
							j = i;  //switch queue holding current sorted value
						}
					}
					else  if(sortDirectionString.equals("DESC")) { 
						if(cmp < 0) {							 //if cmp == 0, do nothing
							currentKeyValue = keys.get(i);
							j = i;  //switch queue holding current sorted value
						}
					}
					else 
						logQueryState("sortMerge: sort direction in OrderBy clause unknown: " + sortDirectionString, Log_Level.quiet);

				} //for

				//get and output formatted row from headRows at j
				sortedRow = headRows.get(j);
				out.write(sortedRow);  //write out row to output stream
				out.newLine();
				rowsSorted++;

				//update currentKeyValue, keys and headRows from selected queue
				if(queueList.get(j).isEmpty()) {
					queueList.remove(j); 	//remove queue from list
					keys.remove(j);  	  	//also remove associated key and headRow for empty queue
					headRows.remove(j);
					j = 0;    			 	//reset queue holding currentValue
				}
				
				//update keys[j], headRows[j] and CurrentKeyValue with next element in queue j
				element = queueList.get(j).remove();				//dequeue next element of jth queue
				lastIndex = element.lastIndexOf('|');          		//find last char of formatted row
				sortedRow = element.substring(0, lastIndex + 1);
				headRows.set(j, sortedRow);							//reset headRows[j] with new value
				currentKeyValue = element.substring(lastIndex + 1); //reset currentKeyValue
				keys.set(j, currentKeyValue);                       //reset keys[j] with new value 

			} //while
			
		} catch(IndexOutOfBoundsException ioobe ) {
			
			logQueryState("sortMerge: IndexOutOfBoundsException ERROR,  queueListSize=" + queueList.size() 
				+ ", keysSize=" + keys.size() + ", queue j =" + j + ", currentKeyValue=" + currentKeyValue,  Log_Level.quiet);
			out.close();
			throw ioobe;
		}
	
		//output remaining formatted row in headRows[0]
		sortedRow = headRows.get(0);
		out.write(sortedRow);  //write out row to output stream
		out.newLine();
		rowsSorted++;
		
		//write out all elements in remaining queue in sorted order
		Queue<String> queue = queueList.get(0);
		while(!queue.isEmpty()) {
			 element = queue.remove(); 
			 lastIndex = element.lastIndexOf('|'); 
			 sortedRow = element.substring(0, lastIndex + 1);  //extract sortedRow
	         out.write(sortedRow); //remove and print row
	         out.newLine();
	         rowsSorted++;
		}
		
		out.flush();
		out.close();
		
		logQueryState("sortMerge: ROWS MERGE-SORTED and output : " + rowsSorted , Log_Level.verbose);
		
	} // end sortMerge
	
	
	/*
	private String CachedRowSetToString(CachedRowSet crs) throws SQLException {

		Worker.logQueryState("CachedRowSetToString: Listing rows from ResultSet");
		String formattedTable = "";
		int colCount = crs.getMetaData().getColumnCount();
		int rowNo = 0;

		while (crs.next()) {

			//format each row of the table for printing
			String dataRow = crs.getString(1); // fence post
			for (int col = 2; col <= colCount; col++) 
				//dataRow += " \t\t| " + rs.getString(col);
				dataRow += " \t| " + crs.getString(col);

			rowNo++;
			formattedTable += "RS Row" + rowNo +  ": " + dataRow + "\n";
		} // end while
		
		//crs.close();

		return formattedTable;
	} // end method
	*/

	//public SQLiteConnection connectSQLiteDB(String URL) {
	public Connection connectSQLiteDB(String URL) {

		//java.sql.Connection connection = null;
		//org.sqlite.SQLiteConnection connection = null;
		Connection connection = null;

		try {	
			
			Enumeration<Driver> drivers = DriverManager.getDrivers();
			while (drivers.hasMoreElements())
				logQueryState("connectSQLiteDB: available SQL driver:  " +  drivers.nextElement().toString(), Log_Level.verbose);
			/*
			Driver driver = DriverManager.getDriver(URL);
			DriverManager.registerDriver(driver);
			*/
		
			
			logQueryState("connectSQLiteDB: Database URL is " + URL, Log_Level.verbose);
			//Class.forName("org.sqlite.JDBC");
			//Class.forName("org.sqlite.SQLiteConnection");
			//Class.forName("java.sql.Connection");
			//Driver driver = DriverManager.getDriver(URL);
			//DriverManager.registerDriver(driver);
			connection = DriverManager.getConnection(URL);	
			//Statement stmt = connection.createStatement(); 
			//stmt.executeQuery("Pragma synchronous = off");
			
			/*
			if(url.contains(":memory:"))
				connection = new SQLiteConnection(URL, "");
			else {
				int last = URL.lastIndexOf('/');
				connection = new SQLiteConnection(url.substring(0, last), URL.substring(last + 1));
			}
			*/
				
			DatabaseMetaData meta = connection.getMetaData();
			logQueryState("connectSQLiteDB: The driver name is " + meta.getDriverName(), Log_Level.verbose); //SQLiteJDBC
		
		//} catch (SQLException | ClassNotFoundException e) {
		} catch (SQLException e) {	
			StringWriter sw = new StringWriter();
		    PrintWriter pw = new PrintWriter(sw);
		    e.printStackTrace(pw);
		    pw.flush();
			logQueryState("connectionSQliteDB: " +  e.getMessage() + "\n"    + sw.toString(), Log_Level.quiet);
			pw.close();
			System.out.println(e.getMessage());
			e.printStackTrace(pw);
			System.exit(1);
		}

		return connection;

	} // end method

	/*
	 * Ensures that IP string is formatted correctly with correct syntax. As a
	 * quartet of 4 numbers separated by '.' Each number in the quartet must be
	 * in the range 0 - 255
	 */
	private  boolean validIP(String ip) {

		try {
			if (ip == null || ip.isEmpty()) {
				// must use local logger, rabbit connections not yet open
				logQueryState("validIP:   IP is either empty string or null: " + ip + " FAILED", Log_Level.verbose);
				return false;
			}

			String[] parts = ip.split("\\.");

			if (parts.length != 4) {
				logQueryState("validIP:  IP not in  quartet format: '" + ip + "' FAILED", Log_Level.verbose);
				return false;
			}

			for (String s : parts) {
				int i = Integer.parseInt(s);
				if ((i < 0) || (i > 255)) {
					logQueryState("validIP: IP some quartet value out of range: " + ip + " FAILED", Log_Level.verbose);
					return false;
				}
			}
			if (ip.endsWith(".")) {
				logQueryState("validIP: IP mis-formatted: " + ip + " FAILED", Log_Level.verbose);
				return false;
			}

		} catch (NumberFormatException nfe) {
			//logger.info("broker IP with invalid quartet number: " + ip + " FAILED");
			logQueryState("validIP: IP with invalid quartet formatted number: " + ip + " FAILED", Log_Level.verbose);
			return false;
		}

		logQueryState("validIP:  IP string VALID: " + ip, Log_Level.verbose);
		return true;
	} // end validIP

	public static synchronized void logQueryState(String msg, Log_Level logLevel) {

		try { 
			
					
			if ( (channelToRabbit == null) && (logLevel.ordinal() >= LOG_LEVEL.ordinal()) ) 
				logger.info(msg);
				
			//else if(!ContainerMode  && (logLevel.ordinal() >= LOG_LEVEL.ordinal()))
			//		logger.info(msg);	
				
			else if (logLevel.ordinal() >= LOG_LEVEL.ordinal()) {
				
				logger.info(msg);
				
				/* send LOG CTRL messages to Master logger */
				LogCtrlMsg logCtrlMsg = new LogCtrlMsg(workerID, msg);
				ConvertBytes converter = new ConvertBytes();
				channelToRabbit.basicPublish(ALL_WORKERS_EXCHANGE, ACK_KEY, null,
						converter.convertToBytes(logCtrlMsg));
			} 
			
		} catch (IOException e) {
			e.printStackTrace();
			logger.info(msg);
		}

	} // end logQueryState
	
	
	/* WARNING:  this method leaves the ResultSet's cursor after the last row */
	//print headers for each 'limit' number of rows. limit is # of rows to log or  print before writing another header.
	//toLog determines is output goes to Master log or to System.out.
	public synchronized static void printResultSetTable(ResultSet rs, String SQL_Query, int limit, boolean toLog) throws SQLException {

		logQueryState("printResultSetTable: "  + toLog != null ? "to log" : "to stdout" + " for query  '" + SQL_Query + "'. ", Log_Level.verbose);
		
		if(rs == null) {
			logQueryState("printResultSetTable: for query  '" + SQL_Query + "'. IS NULL ", Log_Level.verbose);
			return;
		}
		
		rs.setFetchSize(FETCH_SIZE);
		
		ResultSetMetaData rsMetaData = rs.getMetaData();
		rsMetaData.getColumnCount();
		int columnsNumber = rsMetaData.getColumnCount();
		
		
		String headers = "|";
		//print column headers
		for (int i = 1; i <= columnsNumber; i++) {
			//if (i > 1) 
			//	headers += "|";
			headers += rsMetaData.getColumnName(i);
			headers += "\t\t| " + workerID;
		}
		
		//StringBuffer formattedTable = new StringBuffer("Final Output of Query: '" + SQL_Query + "'\n" + "\t" + headers + "\n");
		StringBuffer formattedTable = new StringBuffer("Final Output of Query: '" + SQL_Query + "'\n"  + headers + "\n");
		
		int rowNo = 0;
		String separator = "\t\t|";
		//while (rs.next() && (rowNo < limit)) {  //print rows
		while (rs.next()) {  //print rows
			
			formattedTable.append("|");
			for (int i = 1; i <= columnsNumber; i++) {
				
				formattedTable.append(rs.getString(i) + separator);
			}
			
			//formattedTable.append( "dataRow " + rowNo +  ": " + row + "\n");
			formattedTable.append('\n');
			rowNo++;
			
			
			//if(rowNo == FETCH_SIZE) {   //reset formattedTble buffer
			if(rowNo == limit) {   //reset formattedTable buffer
				
				if(toLog)
					logQueryState("printResultSetTable: " + formattedTable, Log_Level.metrics);
				else
					System.out.println(formattedTable);
				
				formattedTable.delete(0, formattedTable.length());   // clear buffer
				formattedTable.append(headers + "\n");
				rowNo = 0;
			}
			
		} // end while

		//log remaining rows
		if (toLog)
			logQueryState("printResultSetTable: " + formattedTable, Log_Level.metrics);
		else
			System.out.println(formattedTable);
		
		//rs.close();
		
	} // end method  printResultSetTable
	
	
	static HashMap<Long, Boolean> map = new HashMap<Long , Boolean>();

	public synchronized static void setThreadStatus(long id, boolean status) {
		
		map.put(id, status);
		
	} // end method
	
	public synchronized static boolean getThreadStatus(long id) {
		
		boolean status = map.get(id);
		return status;
		
	}  //end method
	
	public synchronized static void purgeThreads(ArrayList<Thread> threads) {
		
		for(Thread t : threads) {
			
			map.remove(t.getId());
		}
		
	} // end method

} // end class Worker

class SortQueue {
	
	Queue<String> queue;
	Socket socket;
	
	public SortQueue(Queue<String> queue, Socket socket) {
		super();
		this.queue = queue;
		this.socket = socket;
	}
	
	boolean isAlive() {
		return socket.isConnected();
	}
	
	Queue<String> getQueue() {
		return queue;
	}
	
}
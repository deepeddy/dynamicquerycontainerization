package containerizedQuery;

//import java.io.ObjectInputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.BufferedInputStream;
//import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
//import java.io.DataOutputStream;
import java.io.EOFException;
//import java.io.IOException;
//import java.io.OutputStreamWriter;
//import java.io.IOException;
import java.net.Socket;
import java.sql.*;
import java.util.Arrays;
//import java.util.Arrays;
//import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.Vector;
//import java.util.zip.GZIPInputStream;
//import java.util.zip.GZIPOutputStream;
//import java.util.concurrent.ConcurrentLinkedQueue;

import javax.sql.rowset.CachedRowSet;

//import org.apache.commons.io.IOUtils;

import com.rabbitmq.client.Channel;
//import com.sun.rowset.CachedRowSetImpl;

//import containerizedQuery.Constants.Log_Level;

//import containerizedQuery.Constants.Log_Level;

/**
 * READS serialized CachedRowSet in from socket attached to upstream container
 * sending its query results at input to THS worker container. WorkerRunnable
 * then loads the contents of the CRS into the local database thru the
 * SQLiteConnection. After successful table update, the Master is sent a CRTL
 * message that indicates which directed graph edge was used and which
 * container(destination) has been updated by an upstream input
 * container(origin). The input socket is then closed, that signals to the
 * upstream container that downstream loading is completed.
 */
public class InputRowListRunnable extends Thread implements Constants {

	protected Socket clientSocket = null;
	protected String serverText = "SERVER SOCKET";
	java.sql.Connection SQLiteConnection;
	Channel channelToRabbit;
	boolean DEBUG = false;
	long threadId = 0;
	String workerID = "UNDEFINED";
	String origin = "UNDEFINED";
	String destination = "UNDEFINED";
	Queue<String> queue = null;
	String localIP = "UNDEFINED";

	/* to increase PARALELLISM, a new thread is started for each input socket */
	public InputRowListRunnable(String workerID, Socket clientSocket, 
			java.sql.Connection SQLiteConnection, Channel channelToRabbit) {
		this.workerID = workerID;
		this.clientSocket = clientSocket;
		this.SQLiteConnection = SQLiteConnection;
		this.channelToRabbit = channelToRabbit;
		localIP = clientSocket.getLocalAddress().toString();
	} // end constructor

	public InputRowListRunnable(Queue<String> queue, String workerID, Socket clientSocket,
			java.sql.Connection SQLiteConnection, Channel channelToRabbit) {
		this.workerID = workerID;
		this.clientSocket = clientSocket;
		this.SQLiteConnection = SQLiteConnection;
		this.channelToRabbit = channelToRabbit;
		this.queue = queue;
		localIP = clientSocket.getLocalAddress().toString();
	} // end constructor

	/*
	 * origin and destination container pair represents an edge in the digraph
	 * visible to Master
	 */
	public void run() {

		threadId = Thread.currentThread().getId();

		try {
			// buffer and de-compress object input stream
			// GZIPInputStream gz = new
			// GZIPInputStream(clientSocket.getInputStream(), 1024000);
			// ObjectInputStream input= new ObjectInputStream(gz);

			// buffer input stream
			BufferedInputStream bufferedIn = new BufferedInputStream(clientSocket.getInputStream(), STREAM_BUFFER_SIZE);
			// ObjectInputStream input= new ObjectInputStream(bufferedIn);
			DataInputStream input = new DataInputStream(bufferedIn); // faster than ObjectOutputStream

			/* identify origin and destination of input stream */
			origin = input.readUTF(); // FROM container with ID 'origin'
			destination = input.readUTF(); // TO container with ID 'destination'
			String tableName = input.readUTF();
			String SQL_CreateTableDDL = input.readUTF(); // holds SQL statement to create the rowList's table
			String SQL_INSERT = input.readUTF(); // holds prepared INSERT statement string to insert rows into table

			Worker.logQueryState(
					workerID + " InputRowListRunnable, threadId " + threadId
							+ ": new Worker thread on " + destination + " attached to remote input socket request from " 
							+ origin + ", is reading input stream from same", Log_Level.verbose);
					/*
					 * long time = System.currentTimeMillis();
					 * Worker.logQueryState("METRIC: BEGIN READ ROWS  at time, "
					 * + time + ", " + destination + " from " + origin + ", '" +
					 * SQL_CreateTableDDL + "'; ThreadID " +
					 * Thread.currentThread().getId() +
					 * ", is output socket shutdown? " +
					 * clientSocket.isOutputShutdown(), Log_Level.metrics);
					 * 
					 * Object rowList = null; Object rowList =
					 * input.readObject(); readCRSfromDataInputStream(input,
					 * SQL_CreateTableDDL,
					 * SQLiteConnection.prepareStatement(SQL_INSERT));
					 * 
					 * time = System.currentTimeMillis(); Worker.logQueryState(
					 * "METRIC: FINISH READ ROWS at time, "+ time + ", " +
					 * destination + " from " + origin + ", '" +
					 * SQL_CreateTableDDL + "'; ThreadID " +
					 * Thread.currentThread().getId() +
					 * ", is output socket shutdown? " +
					 * clientSocket.isOutputShutdown(), Log_Level.metrics);
					 * 
					 * clientSocket.shutdownInput(); clientSocket.close();
					 * 
					 * 
					 * time = System.currentTimeMillis(); Worker.logQueryState(
					 * "METRIC: BEGIN INSERT ROWS  at time, "+ time + ", " +
					 * destination + " from " + origin + ", '" +
					 * SQL_CreateTableDDL + "'; ThreadID " +
					 * Thread.currentThread().getId(), Log_Level.metrics);
					 */

			// queue = new ConcurrentLinkedQueue<String>(); //TEST ONLY ---COMMENT OUT

			if (queue != null) {
				
				// load sort-merge queue with rows from arriving input stream
				loadQueue(input, SQL_CreateTableDDL, queue); 

			} else {

				/*
				 * loads local table with contents of rowList, BATCH_SIZE rows
				 * at a time using INSERT prepared stmt */
	
				@SuppressWarnings("unused")
				int rowsInserted = createTableFromRows(input, SQL_CreateTableDDL, SQL_INSERT, SQLiteConnection, tableName);
				
				Worker.logQueryState(workerID + " InputRowListRunnable:  CachedRowSet updated local table " + tableName,
						Log_Level.quiet);
			}

			//clientSocket.shutdownInput();
			//input.close();
			//clientSocket.close();

			/*
			 * time = System.currentTimeMillis(); Worker.logQueryState(
			 * "METRIC: FINISH INSERT ROWS  at time, "+ time + ", " +
			 * destination + " from " + origin + ", " + SQL_CreateTableDDL +
			 * "; ThreadID " + Thread.currentThread().getId() +
			 * ", rows inserted: " + rowsInserted, Log_Level.metrics);
			 */

			/*
			 * send CTRL message to Master acknowledging that loading is
			 * completed at destination container
			 */
			EdgeCtrlMsg ack = new EdgeCtrlMsg(origin, destination, "xfered table " + tableName + " over Edge");
			ConvertBytes converter = new ConvertBytes();
			channelToRabbit.basicPublish(ALL_WORKERS_EXCHANGE, ACK_KEY, null, converter.convertToBytes(ack));
			Worker.logQueryState(
					workerID + " InputRowListRunnable:  Sent ACK CTRL msg back to Master that CachedRowSet sent from : '"
							+ origin + "' --> '" + destination + "' completed loading",
					Log_Level.verbose);

		} catch (EOFException eof) {
			
			Worker.logQueryState(
					workerID + " InputRowListRunnable:  read of CachedRowSet when write-half of the socket connection is closed:  "
							+ clientSocket.isOutputShutdown() + ", connection is open: " + clientSocket.isConnected()
							+ ", " + eof.getClass().getCanonicalName(),Log_Level.quiet);
			
		} catch (Exception e) {
			
			Worker.logQueryState(workerID + " InputRowListRunnable: FATAL ERROR: in run(), threadId " + threadId + ": "
					+ e.getMessage() + ", " + e.getClass().getCanonicalName(), Log_Level.quiet);
			
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			e.printStackTrace(pw);
			pw.flush();
			
			Worker.logQueryState(workerID + " InputRowListRunnable: FATAL ERROR: in run(), threadId " + threadId + ":\n"
					+ sw.toString(), Log_Level.quiet);
			
			pw.close();
		}

	} // end run method

	public String[] getTypes(String SQL_CreateTableDDL) {

		int firstParen = SQL_CreateTableDDL.indexOf('(');
		int lastParen = SQL_CreateTableDDL.lastIndexOf(')');
		String schema = SQL_CreateTableDDL.substring(firstParen + 1, lastParen);
		String[] types = schema.split(",");
		int colCount = types.length;
		int index = -2;
		for (int i = 0; i < colCount; i++) {
			/* remove any leading and trailing  blanks in type */
			types[i] = types[i].trim(); 
			 /* remove any precision clause from type, e.g. emp_no INT(11)' --> 'emp_no INT' */
			types[i] = types[i].replaceFirst("\\(\\d+\\)", "");
			types[i] = types[i].replaceFirst("\\w+\\s", ""); /* remove field name and white space separating from type */

			if ((index = types[i].indexOf(' ')) != -1) /* if anything follows the type remove it */
				types[i] = types[i].substring(0, index);
		} //for

		return types;

	} // method

	// returns tuple position in the row's schema of OrderBy field
	// passes back a reference to sortDirection and field type
	public Object[] keyId(String createTableDDL) throws Exception {

		// [0] is the sort key's relative column position in the schema, its type is int.
		// [1] is the sort key's direction: "ASC" or "DESC", its type is String.
		// [2] is the sort key's data type as specified in the schema, this object is passed back as a String which denotes
		//     the data type.
		Object[] fieldSpecs = new Object[3];

		String predicate = System.getenv("PREDICATE"); /* test access to env  variable */

		// locate OrderBy key field and sort direction, e.g. extract "ORDER BY
		// C2.emp_no_employees DESC"
		// from "SELECT * FROM C2 ORDER BY C2.emp_no_employees DESC;"
		String orderByClause = predicate.substring(predicate.indexOf("ORDER BY")).trim();
		int lastSpaceIndex = orderByClause.lastIndexOf(' ');
		// remove leading table name in fully qualified field name
		String OrderByFieldName = orderByClause.substring(orderByClause.indexOf('.') + 1, lastSpaceIndex).trim();
		String sortDirection = orderByClause.substring(lastSpaceIndex + 1).trim();
		fieldSpecs[1] = sortDirection;

		// locate keyField's relative position in schema
		String schema = createTableDDL.substring(createTableDDL.indexOf('(') + 1, createTableDDL.lastIndexOf(')'));
		String[] fields = schema.split(",");
		String fieldSpecString = "UNDEFINED";
		int fieldColumn = -1;
		for (int i = 0; i < fields.length; i++) {
			if (fields[i].contains(OrderByFieldName)) {
				fieldSpecString = fields[i].trim();
				fieldColumn = i;
				break;
			}
		}

		if (fieldColumn == -1)
			throw new Exception("FAILED: locating keyId " + OrderByFieldName + " in table schema " + schema);
		else
			fieldSpecs[0] = fieldColumn;

		// get field type specification, remove leading field name and extra leading spaces
		fieldSpecString = fieldSpecString.substring(fieldSpecString.indexOf(' ') + 1).trim(); 
		String fieldType = "";
		char letter = ' ';
		for (int i = 0; i < fieldSpecString.length(); i++) {
			letter = fieldSpecString.charAt(i);
			if (Character.isLetter(letter))
				fieldType += letter;
			else {
				fieldSpecs[2] = fieldType;
				break; // located 1st char that's not a letter following type
						// name, which ends type
			}
		}

		return fieldSpecs;
	} // method

	public void loadQueue(DataInputStream in, String SQL_TableDDL, Queue<String> queue) throws Exception {

		int noOfRows = 0;
		String[] types = getTypes(SQL_TableDDL);
		int colCount = types.length;
		int col = 0;
		//boolean refilling = false;
		String mode = System.getenv("QUERY");
		boolean streaming = mode.contains("STREAMING");
		int fillSize = Integer.valueOf(System.getenv("FILL_SIZE"));
		//long sleepTime = Long.valueOf(System.getenv("SLEEP_TIME"));
		
		Worker.logQueryState("loadQueue(): on worker " + workerID + " MERGE_SORT mode= " + mode + ", FILL_SIZE= " + fillSize
				+ ", STREAMING is on="  + streaming, Log_Level.quiet);

		Object[] fieldSpecs = keyId(SQL_TableDDL);
		int sortKeyFieldColumn = (int) fieldSpecs[0];
		// String sortDirection = (String) fieldSpecs[1];
		// String fieldType = (String) fieldSpecs[2];

		StringBuffer buffer = new StringBuffer("|");
		String sortKey = "";

		try {

			long time = System.currentTimeMillis();
			Worker.logQueryState("METRIC: BEGIN SORT-MERGE READ ROWS  at time, " + time + ", " + origin + "==>"
					+ destination + ", queue  associated with local socket " + localIP
					+ ", at TIME=" + System.currentTimeMillis() + ", '" + SQL_TableDDL.substring(SQL_TableDDL.indexOf( '(' )) + "'; ThreadID " + Thread.currentThread().getId()
					+ ", is input socket shutdown? " + clientSocket.isInputShutdown(), Log_Level.metrics);

			while (true) { // read until EOF
			//while (!clientSocket.isClosed()) { // read until EOF
			
				//EXPLICITY test for EOF before reading next record
				 in.mark(1);
				 if (in.read() == -1)  { //EOF detected 
					 Worker.logQueryState("loadQueue() STREAMING DataInput EOF EXPLICITLY DETECTED: " + workerID 
								+ " for queue  associated with socket " + localIP
								+ " from " + origin + "===>" + destination + " at TIME=" + System.currentTimeMillis(),
								Log_Level.verbose);
					 break;
				 }
				 else
				    in.reset();

				for (col = 0; col < colCount; col++) { // for each field in a row

					switch (types[col].toUpperCase()) {
					case "INT":
						int integer = in.readInt();
						buffer.append(integer + "|");
						if (col == sortKeyFieldColumn)
							sortKey = String.valueOf(integer);
						break;
					case "SMALLINT":
						short shorter = in.readShort();
						buffer.append(shorter + "|");
						if (col == sortKeyFieldColumn)
							sortKey = String.valueOf(shorter);
						break;
					case "DATE":
						String dateString = in.readUTF();
						buffer.append(dateString + "|");
						if (col == sortKeyFieldColumn) {
							sortKey = dateString;
							// sortKey =
							// reverse.append(dateString).reverse().toString();
							// //for lexical ordering
							// reverse.delete(0, reverse.length()); //reset
							// StringBuffer to empty
						}
						// Date date = Date.valueOf(dateString);
						// prepStmt.setDate(col, date);
						// prepStmt.setString(col, dateString);
						break;
					// case "DATE":
					case "VARCHAR":
					case "TEXT":
					case "CHAR":
						String varchar = in.readUTF();
						buffer.append(varchar + "|");
						if (col == sortKeyFieldColumn)
							sortKey = varchar;
						break;
					case "REAL":
						double d = in.readInt();
						buffer.append(d + "|");
						if (col == sortKeyFieldColumn)
							sortKey = String.valueOf(d);
						break;
					default:
						Worker.logQueryState("loadQueue(): UNSUPPORTED SQLite TYPE " + types[col], Log_Level.quiet);
					} // end switch

				} // end for
				
				buffer.append(sortKey);   //add sort key to end of row
				queue.add(buffer.toString()); // add formatted row to queue
				buffer.delete(1, buffer.length()); // reset buffer for next row, keep first char '|'
				noOfRows++;
				
				if(streaming) { //STREAMING CASE
					
					//If queue became empty and other thread is in wait() state, let queue fill to at least 1000 elements
					//before sending notify() to release waiting thread. If # of elements in less than 1000, then
					//reading the data stream EOF will also release the waiting thread, by also sending a notify().
					if(queue.size() == fillSize) { 
						
						synchronized(queue) { 
							
								queue.notify(); //notify sortMerger thread that queue is not empty
								
								Worker.logQueryState("loadQueue() NOTIFY:  STREAMING " + workerID 
									+ " queue  associated with local socket " +  clientSocket.getLocalAddress() 
									+ ":" + clientSocket.getLocalPort() + ", at TIME="
									+  System.currentTimeMillis() + " from " + origin + " ===> " + destination 
									+ " is NOT EMPTY, with " + queue.size() + " elements", Log_Level.verbose);
							
						} // sync
						
					} 
					else  if(queue.size() > (fillSize * 10)) { //slow down input stream 
						
						//Worker.logQueryState("loadQueue() YIELDING:  STREAMING " + workerID
						//	+ " queue  associated with local socket " + localIP  + ", at TIME=" 
						//	+  System.currentTimeMillis() + " from " + origin + "===>" + destination 
						//	+ " is NOT EMPTY, with " + queue.size() + " elements", Log_Level.verbose);
						//
						
						Thread.yield();
						//Thread.sleep(sleepTime);
					} 
						
				} //streaming
			
			} // while
			
			Worker.logQueryState("loadQueue() EOF SEEN:  socket CLOSING: " + workerID 
					+ " for queue associated with local socket " + localIP
					+ " FILLED, from " + origin + "===>" + destination + " at TIME=" + System.currentTimeMillis()
					+ ", with queue size=" + queue.size(),
					Log_Level.quiet);
			
			in.close();
			clientSocket.close();
			
			if(streaming) { //STREAMING CASE
				synchronized(queue) {  //after EOF is seen, always send notify(), in case other thread was waiting
					
					queue.notify();
						
					Worker.logQueryState("loadQueue() EOF SEEN: STREAMING NOTIFY socket CLOSED: " + workerID 
							+ " for queue  associated with local socket " + localIP
							+ " from " + origin + "===>" + destination + " at TIME=" + System.currentTimeMillis()
							+ ", with queue size=" + queue.size(),
							Log_Level.verbose);
				} //sync
			}

		} catch (EOFException eof) {
			
			// read until EOF read
			//System.out.println(eof.toString() + " CORRECTLY signals end of data stream");
			long time = System.currentTimeMillis();
			String schema = SQL_TableDDL.substring(SQL_TableDDL.indexOf('('));
			Worker.logQueryState("METRIC: loadQueue(), CORRECTLY signals end of data stream  for queue associated with socket " 
				     + clientSocket.getLocalAddress().toString() + " \nFINISHED READING " + noOfRows 
					+ " ROWS at time, " + time + ", " + origin + "==>" + destination + ", queue size=" + queue.size()  
					+ ", schema: '" + schema + "'; ThreadID " + Thread.currentThread().getId()
					+ ", is read-half of socket shutdown? " + clientSocket.isInputShutdown(),	Log_Level.metrics);
			
			in.close();
			clientSocket.close();
			
		} catch (Exception e) {
			
			ByteArrayOutputStream stream = new ByteArrayOutputStream();
		    PrintWriter pw = new PrintWriter(stream);
		    e.printStackTrace(pw);
		    pw.flush();
			
			Worker.logQueryState("loadQueue() FAILED:  reading input stream on " + workerID  
					+ " over Socket assocaited with IP litteral " + clientSocket.getInetAddress() 
					+ " .\n"  + e.getMessage() + ", " + e.getClass().getCanonicalName() + "\n"
					+ stream.toString(), Log_Level.quiet);

			Worker.logQueryState(": loadQueue() " + noOfRows + " ROWS, at column " + col
					+ ",  with types: " + Arrays.toString(types) + ", destination " + destination + " from " + origin
					+ ", '" + SQL_TableDDL + "', ThreadID " + Thread.currentThread().getId()
					+ ", is output socket shutdown? " + clientSocket.isOutputShutdown() + e.getMessage() + ": "
					+ e.getClass().getCanonicalName(), Log_Level.quiet);

			stream.close();
			pw.close();
			in.close();
			clientSocket.close();
			
		} 

	} // method loadQueueStreaming
	
	/*
	public void printDataStream(DataInputStream in, String SQL_TableDDL, Queue<String> queue) throws Exception {

		int noOfRows = 0;
		String[] types = getTypes(SQL_TableDDL);
		int colCount = types.length;
		int col = 0;

		StringBuffer buffer = new StringBuffer("|");
		 BufferedWriter out
		   = new BufferedWriter(new OutputStreamWriter(System.out));

		try {

			long time = System.currentTimeMillis();
			Worker.logQueryState("METRIC: PRINTING ROWS  at time, " + time + ", " + destination + " from "
					+ origin + ", '" + SQL_TableDDL + "'; ThreadID " + Thread.currentThread().getId()
					+ ", is output socket shutdown? " + clientSocket.isOutputShutdown(), Log_Level.metrics);

			while (true) { // read until EOF

				for (col = 0; col < colCount; col++) { // for each field in a
														// row

					switch (types[col].toUpperCase()) {
					case "INT":
						int integer = in.readInt();
						buffer.append(integer + "|");
						break;
					case "SMALLINT":
						short shorter = in.readShort();
						buffer.append(shorter + "|");
						break;
					case "DATE":
						String dateString = in.readUTF();
						buffer.append(dateString + "|");
						break;
					// case "DATE":
					case "VARCHAR":
					case "TEXT":
					case "CHAR":
						String varchar = in.readUTF();
						buffer.append(varchar + "|");
						break;
					case "REAL":
						double d = in.readInt();
						buffer.append(d + "|");
						break;
					default:
						Worker.logQueryState("insertRows: UNSUPPORTED SQLite TYPE " + types[col], Log_Level.quiet);
					} // end switch

				} // end for

				
				out.write(buffer.toString());  // print row
				buffer.delete(1, buffer.length()); // reset buffer for next row
				noOfRows++;

			} // while

		} catch (EOFException eof) {
			// read until EOF read
			System.out.println(eof.toString() + " CORRECTLY signals end of data stream");
			long time = System.currentTimeMillis();
			Worker.logQueryState("METRIC: FINISH PRINTING " + noOfRows + " ROWS at time, " + time + ", " + destination
					+ " from " + origin + ", '" + SQL_TableDDL + "'; ThreadID " + Thread.currentThread().getId()
					+ ", is output socket shutdown? " + clientSocket.isOutputShutdown(), Log_Level.metrics);
		} catch (Exception e) {

			Worker.logQueryState(": printDataStream() " + noOfRows + " ROWS, at column " + col
					+ ",  with types: " + Arrays.toString(types) + ", destination " + destination + " from " + origin
					+ ", '" + SQL_TableDDL + "', ThreadID " + Thread.currentThread().getId()
					+ ", is output socket shutdown? " + clientSocket.isOutputShutdown() + e.getMessage() + ": "
					+ e.getClass().getCanonicalName(), Log_Level.quiet);

			System.out.println(e.getMessage() + ": " + e.getClass().getCanonicalName());
			throw e;
		}

	} // method printDataStream
	*/
	

	public int readRowsfromDataInputStream(DataInputStream in, String SQL_CreateTableDDL, PreparedStatement prepStmt)
			throws  Exception  {
		
		// extract table schema field types from CREATE TABLE DDL statement
		final int  BATCH_SIZE = Integer.parseInt(System.getenv("BATCH_SIZE"));
		int noOfRows = 0;
		String[] types = getTypes(SQL_CreateTableDDL);
		int colCount = types.length;
		int col = 0;
		String rowFields = SQL_CreateTableDDL.substring(SQL_CreateTableDDL.indexOf('('));  

		try {

			long time = System.currentTimeMillis();
			Worker.logQueryState("METRIC: readRowsfromDataInputStream: BEGIN READ ROWS  at time, " + time + ", " + origin 
			+ " ===> " + destination  + ", '" + rowFields + "', Types=" + Arrays.toString(types) 
			+ ", ThreadID " + Thread.currentThread().getId(), Log_Level.metrics);

			while (true) { // read until EOF

				for (col = 1; col <= colCount; col++) { // for each field in a row

					switch (types[col - 1].toUpperCase()) {
					case "LONG":
						// int integer = in.readInt();
						prepStmt.setLong(col, in.readLong());
						break;
					case "INTEGER":
						// int integer = in.readInt();
						prepStmt.setInt(col, in.readInt());
						break;
					case "INT":
						// int integer = in.readInt();
						prepStmt.setInt(col, in.readInt());
						break;
					case "SMALLINT":
						// short shortInt = in.readShort();
						prepStmt.setShort(col, in.readShort());
						break;
					/*
					 * case "DATE": String dateString = in.readUTF(); Date date
					 * = Date.valueOf(dateString); prepStmt.setDate(col, date);
					 * prepStmt.setString(col, dateString); break;
					 */
					case "DATE":
					case "VARCHAR":
					case "TEXT":
					case "CHAR":
						// String str = in.readUTF();
						prepStmt.setString(col, in.readUTF());
						break;
					case "REAL":
						// double real = in.readDouble();
						prepStmt.setDouble(col, in.readDouble());
						break;
					default:
						Worker.logQueryState("readRowsfromDataInputStream: UNSUPPORTED SQLite TYPE " + types[col - 1], Log_Level.quiet);
					} // end switch

				} // end for

				prepStmt.addBatch(); // add the row to the execution batch
				noOfRows++;
				if(noOfRows % BATCH_SIZE  == 0) {
					
					//Worker.logQueryState("readRowsfromDataInputStream: read " + noOfRows + " xfered from "
					//		+ origin + " ==> " + destination,  Log_Level.verbose);

					//int[] counts;
					synchronized (SQLiteConnection) { // no other thread or method can interact concurrently with the database
						//counts = prepStmt.executeBatch(); // INSERT THE ROWS
						prepStmt.executeBatch(); // INSERT THE ROWS
						SQLiteConnection.commit();
						prepStmt.clearBatch();
						//rowsInserted = counts.length;
						
						/*
						 * Arrays.sort(counts); if(Arrays.binarySearch(counts,
						 * Statement.EXECUTE_FAILED) >= 0) //iff found rowsInserted =
						 * counts.length; else Worker.logQueryState(
						 * "METRIC: INSERT EXECUTE BATCH of SOME OR ALL ROWS  FAILED, "
						 * + destination + " from " + origin + ", " + SQL_CreateTableDDL
						 * + "; ThreadID " + Thread.currentThread().getId(),
						 * Log_Level.metrics);
						 */
					} // end synchronized
				
				} // end if
				
			} // while
			
		} catch (EOFException eof) {
			
			// read until EOF read
			//System.out.println(eof.toString() + " CORRECTLY signals end of data stream");
			long time = System.currentTimeMillis();
			Worker.logQueryState("METRIC: InputRowListRunnable EOF, readRowsfromDataInputStream: CORRECTLY signals end of data stream,"
					+ "\nFINISH READ " + noOfRows + " ROWS at time, " + time + ", " + origin
					+ " ==> " + destination + ", '" + SQL_CreateTableDDL + "'; ThreadID " + Thread.currentThread().getId()
					+ ", is output socket shutdown? " + clientSocket.isOutputShutdown(), Log_Level.metrics);
			
			if(noOfRows % BATCH_SIZE  != 0) {   /* insert any remaining rows in prepared statement batch */
				synchronized (SQLiteConnection) { // no other thread or method can interact concurrently with the database
					//counts = prepStmt.executeBatch(); // INSERT THE ROWS
					prepStmt.executeBatch(); // INSERT THE ROWS
					SQLiteConnection.commit();
				}
			}
			
		} catch (Exception e) {

			ByteArrayOutputStream stream = new ByteArrayOutputStream();
		    PrintWriter pw = new PrintWriter(stream);
		    e.printStackTrace(pw);
		    pw.flush();
			
			Worker.logQueryState("readRowsfromDataInputStream() FAILED:  reading input stream on " + workerID  
					+ " over Socket assocaited with IP litteral " + clientSocket.getInetAddress() 
					+ " .\n"  + e.getMessage() + ", " + e.getClass().getCanonicalName() + "\n"
					+ stream.toString(), Log_Level.quiet);
			
			Worker.logQueryState("METRIC: FAILED readRowsfromDataInputStream() " + noOfRows + " ROWS, at column " + col
					+ ",  with types: " + Arrays.toString(types) + ", " +  origin + " ==> " + destination
					+ ", '" + SQL_CreateTableDDL + "', ThreadID " + Thread.currentThread().getId()
					+ ", is output socket shutdown? " + clientSocket.isOutputShutdown() + e.getMessage() + ": "
					+ e.getClass().getCanonicalName(), Log_Level.quiet);

			//System.out.println(e.getMessage() + ": " + e.getClass().getCanonicalName());
			stream.close();
			pw.close();
		
		} finally {
			
			in.close();
			clientSocket.close();
			//prepStmt.close();
		}
		
		return noOfRows;

	} // end method readRowsfromDataInputStream
	
	
	// insert rows into a table using a CachedRowSet crs. Returns # of rows inserted.
	@SuppressWarnings("unused")
	private int insertRows(CachedRowSet crs, PreparedStatement prepStmt, java.sql.Connection connection,
			String tableName) throws SQLException {

		int colCount = crs.getMetaData().getColumnCount();
		final int BATCH_SIZE = 300000;
		int rowNo = 0;

		if (crs.size() > 0) {
			Worker.logQueryState("insertRows, worker " + workerID + ", threadId " + threadId + ": INSERTING rows "
					+ crs.size() + " rows from CRS into table " + tableName + ", transaction level "
					+ connection.getTransactionIsolation(), Log_Level.verbose);

			int iters = 0; // initialize batch iteration counter

			// String formattedTable = "";

			// allows only one producer/worker at a time access to the table for
			// INSERTing
			// synchronized(connection) { //no other thread or method can
			// interact with the database

			// if(connection.getAutoCommit())
			// connection.setAutoCommit(false);

			while (crs.next()) {
				// format each row of the table for printing
				// String dataRow = crs.getString(1); // fence post
				// for (int col = 2; col <= colCount; col++)
				// dataRow += " \t\t| " + crs.getString(col);
				// formattedTable += "Table " + tableName + " dataRow " + rowNo
				// + ": \t" + dataRow + "\n";

				for (int col = 1; col <= colCount; col++) {
					prepStmt.setObject(col, crs.getObject(col));

					/*
					 * switch (rsMetaData.getColumnType(col)) { case
					 * Types.INTEGER: prepStmt.setInt(col, crs.getInt(col));
					 * break; case Types.SMALLINT: prepStmt.setInt(col,
					 * crs.getShort(col)); break; case Types.VARCHAR:
					 * prepStmt.setString(col, crs.getString(col)); break; case
					 * Types.FLOAT: prepStmt.setFloat(col, crs.getFloat(col));
					 * break; case Types.REAL: case Types.DECIMAL: case
					 * Types.DOUBLE: prepStmt.setDouble(col,
					 * crs.getDouble(col)); break; case Types.DATE:
					 * prepStmt.setDate(col, crs.getDate(col)); break; default:
					 * Worker.logQueryState("insertRows: UNSUPPORTED SQL TYPE "
					 * + rsMetaData.getColumnType(col)); } // end switch
					 */

				} // end for

				prepStmt.addBatch();

				if (++iters > BATCH_SIZE) { // submit batched prepared
											// statements in groups of
											// BATCH_SIZE at time
					iters = 0; // reset counter

					synchronized (connection) { // no other thread or method can
												// interact with the database
						prepStmt.executeBatch();
						connection.commit();
					}

					prepStmt.clearBatch();

				}

				rowNo++;
			} // end while

			// insert any remaining rows
			if (++iters > 0) { // fence-post case

				synchronized (connection) { // no other thread or method can
											// interact with the database
					prepStmt.executeBatch();
					connection.commit();
				}

			}

			Worker.logQueryState(workerID + " insertRows, threadId " + threadId + ": Loaded from CRS " + rowNo
					+ " rows into table " + tableName, Log_Level.verbose);

			// connection.commit(); // commits all inserts as a single
			// transaction
			// connection.setAutoCommit(true);

			prepStmt.close();
			crs.close();

			// } // end synchronize

		} // end if (crs.next())
		else
			Worker.logQueryState(workerID + " insertRows, threadId " + threadId
					+ ": CRS has NO rows to load into table " + tableName, Log_Level.quiet);

		return rowNo;

	} // end insertRows

	// INSERT ROWS into table from a List< Vector<Object> > rowList. Returns # of rows inserted.
	@SuppressWarnings("unused")
	private int insertRows(List<Vector<Object>> rowList, PreparedStatement prepStmt, java.sql.Connection connection,
			String tableName) throws SQLException {

		// ResultSetMetaData rsMetaData = schema.getMetaData();
		// int colCount = rsMetaData.getColumnCount();
		final int BATCH_SIZE = 10000;
		int rowNo = -1;

		if (!rowList.isEmpty()) {
			Worker.logQueryState(
					workerID + " insertRows, threadId " + threadId + ": INSERTING from list of row vectors "
							+ rowList.size() + "  rows from rowList array into table " + tableName,
					Log_Level.verbose);

			int iters = 0; // initialize batch iteration counter

			// String formattedTable = "";

			// allows only one producer/worker at a time access to the table for INSERTing
			// synchronized(connection) {  }  means no other thread or method can interact with the database

			// if(connection.getAutoCommit())
			// connection.setAutoCommit(false);

			while (++rowNo < rowList.size()) {

				Vector<Object> rowVector = rowList.get(rowNo);

				// format each row of the table for printing
				// String dataRow = crs.getString(1); // fence post
				// Object column = rowVector.get(0);
				// String dataRow = data.toString();
				// for (int i = 1; i < rowVector.size(); i++)
				// dataRow += " \t\t| " + ( (SQLData)rowVector.get(i)).toString();
				// formattedTable += "Table " + tableName + " dataRow " + rowNo + ": \t" + dataRow + "\n";
				// Worker.logQueryState("outputTable " + outputTableName + " dataRow " + rowNo + ": " + dataRow);

				for (int vectorIndex = 0; vectorIndex < rowVector.size(); vectorIndex++) {
					int col = vectorIndex + 1;
					prepStmt.setObject(col, rowVector.get(vectorIndex));
				} // end for

				prepStmt.addBatch();

				if (++iters == BATCH_SIZE) { // submit batched prepared
												// statements in groups of
												// BATCH_SIZE at time
					iters = 0; // reset counter

					synchronized (connection) { // no other thread or method can
												// interact with the database
						prepStmt.executeBatch();
						connection.commit();
					}
					prepStmt.clearBatch();

					// Worker.logQueryState("\n" + formattedTable);
					// formattedTable = "";
				}

			} // end while

			// insert any remaining rows
			if (iters > 0) { // fence-post case

				synchronized (connection) { // no other thread or method can
											// interact with the database
					prepStmt.executeBatch();
					connection.commit();
				}

				// Worker.logQueryState("\n" + formattedTable);
			}

			Worker.logQueryState(workerID + " insertRows, threadId " + threadId
					+ ": Loaded List of row vectors into table " + tableName, Log_Level.verbose);

			prepStmt.close();
			// connection.setAutoCommit(true);
			// } // end synchronized

		} else
			Worker.logQueryState(workerID + " insertRows, threadId " + threadId
					+ ": List of row vectors had NO rows to load into table " + tableName, Log_Level.quiet);

		return rowNo;

	} // end insertRows

	/*
	 * Uses contents of CachedRowSet to update a table in another database using a prepared stmt.
	 */
	
	// public int createTableFromRows(Object rows, String SQL_CreateTable,
	// String SQL_INSERT, java.sql.Connection connection,
	// String tableName) throws SQLException {
	public int createTableFromRows(DataInputStream input, String SQL_CreateTableDDL, String SQL_INSERT,
			java.sql.Connection connection, String tableName) throws Exception {
		/*
		 * CachedRowSet crs = null; ArrayList< Vector<Object> > rowList = null;
		 */

		// int NoOfRowsRead = 0;
		int rowsInserted = -1;

		DatabaseMetaData metaData = connection.getMetaData();
		Worker.logQueryState(
				workerID + " createTableFromRows, threadId " + threadId + " Supports batch inserts: "
						+ metaData.supportsBatchUpdates() + ", auto-commit is ON: " + connection.getAutoCommit(),
				Log_Level.verbose);

		/*
		 * if(rows instanceof CachedRowSet) {
		 * 		 crs = (CachedRowSet)rows; 
		 * 		 NoOfRows = crs.size();
		 * } 
		 * else if(rows instanceof List<?>) {
		 *  	 rowList = (ArrayList<Vector<Object>>) rows; 
		 *  	 NoOfRows = rowList.size(); 
		 * } 
		 * else {
		 * 		Worker.logQueryState(workerID + " createTableFromRows, threadId " +
		 * 			threadId + ": UNKNOWN 'rows' object type:  " + rows.toString(),
		 * 			Log_Level.quiet); return 0; 
		 * }
		 * 
		 * Worker.logQueryState(workerID + " createTableFromRows, threadId " +
		 * 		threadId + ":  'rows' object contains   " + NoOfRows + " rows.",
		 * 		Log_Level.verbose);
		 */

		PreparedStatement prepStmt = null;

		try { // create table if not already in database

			String database = metaData.getURL();

			if (tableExists(tableName, connection)) {
				Worker.logQueryState(workerID + " createTableFromRows, threadId " + threadId + ": TABLE " + tableName
						+ " ALREADY EXISTS in " + database, Log_Level.verbose);
			} else { // CREATE TABLE
				
				Worker.logQueryState(workerID + " createTableFromRows, threadId " + threadId + ": TABLE " + tableName
						+ " DOES NOT EXIST in " + database, Log_Level.verbose);
				Worker.logQueryState(
						workerID + " createTableFromRows, threadId " + threadId + ": DDL is:  " + SQL_CreateTableDDL,
						Log_Level.verbose);

				Statement statementDDL = connection.createStatement();

				try {
					synchronized (connection) {
						statementDDL.executeUpdate(SQL_CreateTableDDL); // CREATE TABLE
						connection.commit();
					}
				} catch (SQLException e) { // kludge to solve race-condition between threads competing to CREATE TABLE

					if (e.getMessage().contains(tableName))
						Worker.logQueryState(workerID + " createTableFromRows KLUDGE, threadId " + threadId
								+ ": IGNORE TABLE " + tableName + " ALREADY EXISTS in " + database, Log_Level.quiet);
					else
						throw e; // re-throw Exception
				} finally {
					statementDDL.close();
				}

				Worker.logQueryState(workerID + " createTableFromRows, threadId " + threadId
						+ ": SUCCEEDED CREATED TABLE " + tableName, Log_Level.quiet);
			}

			/* create prepared statement */
			prepStmt = connection.prepareStatement(SQL_INSERT);

			Worker.logQueryState(workerID + " createTableFromRows, threadId " + threadId
					+ ": prepared stmt parameter count " + prepStmt.getParameterMetaData().getParameterCount(),
					Log_Level.verbose);
			Worker.logQueryState(
					workerID + " createTableFromRows, threadId " + threadId + ": Table name is:  " + tableName,
					Log_Level.verbose);
			Worker.logQueryState(workerID + " createTableFromRows, threadId " + threadId
					+ ": Prepared Stmt String is:  " + SQL_INSERT, Log_Level.verbose);

			// INSERT contents from CachedRowSet or Array<Vector<Object>> into
			// table from socket DataInputStream
			rowsInserted = readRowsfromDataInputStream(input, SQL_CreateTableDDL, prepStmt);

			long time = System.currentTimeMillis();
			Worker.logQueryState("METRIC: BEGIN INSERT ROWS  at time, " + time + ", " + destination + " from " + origin
					+ ", '" + SQL_CreateTableDDL + "'; ThreadID " + Thread.currentThread().getId(), Log_Level.metrics);

			time = System.currentTimeMillis();
			Worker.logQueryState("METRIC: FINISH INSERT ROWS  at time, " + time + ", "  + origin + " ===> "  + destination 
					+ ", " + SQL_CreateTableDDL + "; ThreadID " + Thread.currentThread().getId() + ", rows inserted: "
					+ rowsInserted, Log_Level.metrics);

			/*
			 * if(rows instanceof CachedRowSet) { Worker.logQueryState(workerID
			 * + " createTableFromRows, threadId " + threadId +
			 * ": rows instanceOf CachedRowSet.", Log_Level.verbose);
			 * rowsInserted = insertRows(crs, prepStmt, connection, tableName);
			 * } else if(rows instanceof ArrayList<?>) {
			 * Worker.logQueryState(workerID + " createTableFromRows, threadId "
			 * + threadId + ": rows instanceOf ArrayList<?>.",
			 * Log_Level.verbose); rowsInserted = insertRows(rowList, prepStmt,
			 * connection, tableName); } else Worker.logQueryState(workerID +
			 * " createTableFromRows, threadId " + threadId +
			 * ": UNKNOWN 'rows' object type:  " + rows.toString(),
			 * Log_Level.quiet);
			 */

			Worker.logQueryState(workerID + " createTableFromRows, threadId " + threadId + ": " + rowsInserted
					+ " rows inserted into table " + tableName, Log_Level.quiet);

			if (DEBUG) { // display the table that is created and populated
				Statement statementSELECT = SQLiteConnection.createStatement();
				String SQL_Query = "SELECT * FROM " + tableName;
				ResultSet rs = statementSELECT.executeQuery(SQL_Query);
				Worker.printResultSetTable(rs, SQL_Query, 100, true); // print first 100 rows
				rs.close();
				statementSELECT.close();
			}

		} catch (SQLException sqlEx) {
			Worker.logQueryState(workerID + " createTableFromRows, threadId " + threadId + ": " + sqlEx.getMessage(),
					Log_Level.quiet);
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			sqlEx.printStackTrace(pw);
			pw.flush();
			Worker.logQueryState(workerID + " createTableFromRows, threadId " + threadId + ": " + sw.toString(),
					Log_Level.quiet);
			pw.close();
			throw sqlEx; // re-throw exception
		} finally {
			connection.commit();  //UNCOMMENTED THIS 05/26/19
			prepStmt.close();
			// connection.setAutoCommit(true);
		}

		return rowsInserted;

	} // end createTableFromRows

	public synchronized boolean tableExists(String tableName, java.sql.Connection connection) throws SQLException {

		boolean flag = false; // indicates if table exists
		ResultSet rs = null;

		try {
			DatabaseMetaData md = connection.getMetaData();
			rs = md.getTables(null, null, tableName, null);

			while (rs.next()) {
				String name = rs.getString(3);
				if (name.equals(tableName)) { // TABLE_NAME
					flag = true;
					break;
				}
			}

		} catch (SQLException sqlEx) {
			Worker.logQueryState(
					workerID + " tableExits, threadId " + Thread.currentThread().getId() + ": " + sqlEx.getMessage(),
					Log_Level.quiet);
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			sqlEx.printStackTrace(pw);
			pw.flush();
			pw.close();
			throw sqlEx;
		} finally {
			rs.close();
		}

		return flag;

	} // end method

} // end class

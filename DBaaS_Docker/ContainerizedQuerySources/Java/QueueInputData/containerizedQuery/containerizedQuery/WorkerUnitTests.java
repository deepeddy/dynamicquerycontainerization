package containerizedQuery;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.Socket;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.SQLTimeoutException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Queue;
import java.util.Vector;
import java.util.concurrent.ConcurrentLinkedQueue;

//import javax.sql.rowset.CachedRowSet;

import com.rabbitmq.client.Channel;
//import com.sun.rowset.CachedRowSetImpl;


class WorkerUnitTests  implements Constants {
	
	//class variables
	protected Worker worker = null;
	
	protected WorkerUnitTests(String JDBC_URL, String rabbitBrokerIP, String workerID,
			String workerTypeString, String logLevel)
	{
		worker = new Worker(JDBC_URL, rabbitBrokerIP, workerID, workerTypeString, logLevel);
		//super(JDBC_URL, rabbitBrokerIP, workerID, workerTypeString, logLevel);
	}
	
	protected WorkerUnitTests(String JDBC_URL, String rabbitBrokerIP, String workerID,
			String workerTypeString, String logLevel, String downstreamContainerList )
	{
		worker = new Worker(JDBC_URL, rabbitBrokerIP, workerID, workerTypeString, logLevel);
		//super(JDBC_URL, rabbitBrokerIP, workerID, workerTypeString, logLevel);
		
		// must be a comma delimited list of names with no embedded spaces, e.g.
		// worker1,worker2,worker3 obtained as a LIST of command line arguments	
		if((!downstreamContainerList.equals("null")))
			worker.downstreamContainerIds = new ArrayList<String>(Arrays.asList(downstreamContainerList.split(",")));			
	}
	
	public static void main(String[] argv) {
		
		WorkerUnitTests tests = new WorkerUnitTests(argv[0], argv[1], argv[2], argv[3], argv[4]);
		//WorkerUnitTests tests = new WorkerUnitTests(argv[0], argv[1], argv[2], argv[3], argv[4], argv[5]);
		
		tests.runTest(tests);
		
	}
	
	protected void runTest(WorkerUnitTests tests) {
		
		//worker = new Worker(JDBC_URL, rabbitBrokerIP, workerID, workerTypeString, logLevel);
		
		try {
			/* TEST database by CREATEing a table and INSERTING rows, then SELECTing */
			//if(DEBUG && !ContainerMode) {
			
			//final int MegaByte = 1048576;  //1024 x 1024 = 1,048,576 
			//final int GigaByte = MegaByte * 1024;
			
			//UNCOMMENT-OUT test to RUN and compile
				//tests.testDataOutputDefaultExchange("tbl1", sqliteConnection);
				//tests.testDataOutputStream("tbl1", Worker.sqliteConnection);   //test DataPutputStream with CRS
				//tests.testEmployeesCRS_DataOutputStream(sqliteConnection);
				//tests.testPrintResultSet("employees", sqliteConnection);
				//tests.testSortMerge();
				//testSortMergeStreaming();
				//tests.testEmployeesArrayListDataOutputStream(sqliteConnection);
				//tests.testArrayListPartitionedStream("tbl1", sqliteConnection); //test DataPutputStream with ArrayList
			    //tests.testTableRowByteSize("salaries", Worker.sqliteConnection, "SELECT * FROM salaries", MegaByte * 16);
			    //tests.testResultSetRowByteSize(Worker.sqliteConnection, "SELECT * FROM employees");
			
				String queryBM2 =  "SELECT employees.emp_no, salaries.salary FROM salaries, employees WHERE " +
						"CAST(julianday(salaries.to_date) - julianday(employees.hire_date) AS INT)  < 730 " + 
						"AND salaries.salary < 40000 AND employees.emp_no  < 30000 ORDER BY employees.emp_no ASC";
			
				String queryBM3 = "SELECT employees.emp_no, salaries.salary FROM salaries, employees WHERE " +
					//"salaries.emp_no = employees.emp_no " +
					"(CAST(julianday(salaries.to_date) - julianday(employees.hire_date) AS INT)  BETWEEN 1 AND 7) " +
					"AND salaries.salary < 40000 AND employees.emp_no < 30000 ORDER BY employees.emp_no DESC";
				
				String queryBM3_NoSort = "SELECT employees.emp_no, salaries.salary FROM salaries, employees WHERE " +
						//"salaries.emp_no = employees.emp_no " +
						"(CAST(julianday(salaries.to_date) - julianday(employees.hire_date) AS INT)  BETWEEN 1 AND 7) " +
						"AND salaries.salary < 50000 AND employees.emp_no < 30000";
				
				//String query = "SELECT employees.emp_no, salaries.salary FROM salaries, employees";
			    //tests.testResultSetRowByteSize(Worker.sqliteConnection, queryBM3_NoSort);
			    //tests.testRS_TimeOut(Worker.sqliteConnection,  queryBM3_NoSort, 60); 
			    
				tests.testSimpleDatabase("tbl1", Worker.sqliteConnection);
				//tests.testDynamicsDoP("employees", Worker.sqliteConnection);
				//tests.simulateXferTable("tbl1", channelToRabbit);
				
				//use Worker_IN_MEMORY_URL configuration 
				//tests.simulatePartioningTableForJOIN("tbl1", channelToRabbit);
				
			//}
			
		} catch (Exception e) {
			Worker.logQueryState("main: " + e.getLocalizedMessage() + "\n" + e.getMessage() + e.getClass().getCanonicalName(), Log_Level.verbose);
			StringWriter sw = new StringWriter();
		    PrintWriter pw = new PrintWriter(sw);
		    e.printStackTrace(pw);
		    pw.flush();
			Worker.logQueryState("main: STACK-TRACE: " + sw.toString() + "\n" + e.getClass().getCanonicalName(), Log_Level.quiet);
			pw.close();
		}	
		
	} // end method


	//TEST UTITLITY
	protected int[] getTypesRS(ResultSet rs) throws SQLException {
		
		ResultSetMetaData metaData = rs.getMetaData();
		int colCount = metaData.getColumnCount();
		int[] sqlTypes = new int[colCount];     
		for(int col = 0; col < colCount; col++) {
			//String typeName = metaData.getColumnTypeName(col + 1);
			sqlTypes[col] = metaData.getColumnType(col + 1);   //java.sql.Types are int constants
			
		}
		
		return sqlTypes;
		
	} // method
	
	/* Start a socket server to accept query data results from  upstream containers,
	 * in ephemeral range  32768 to 61000. */
	public void startSocketServer(String workerID, int DATA_PORT, java.sql.Connection sqliteConnection, 
										com.rabbitmq.client.Channel  channelToRabbit) throws InterruptedException  {
		
		MultiThreadedServer socketServer = new MultiThreadedServer(workerID, DATA_PORT, sqliteConnection, channelToRabbit);
		Thread serverThread = new Thread(socketServer);
		//serverThread.setPriority(Thread.MAX_PRIORITY);
		serverThread.start();
		
		/* wait for server to bind to socket */
		while( socketServer.getServerSocket() != null   &&  !(socketServer.getServerSocket().isBound()) ) { 
			Worker.logQueryState("openRabbitConnections: MultiThreadedServer WAITING for server socket to  bind on local port " +
                    DATA_PORT, Log_Level.verbose);
			Thread.sleep(50);
		}
		
		Worker.logQueryState("openRabbitConnections: MultiThreadedServer STARTED & LISTENING for INPUT socket requests on port " +
                DATA_PORT, Log_Level.verbose);
	}
	
	
	/* test if 'CREATE TABLE <table>' will create a ResultSet with new <table> */
	protected  void testArrayListPartitionedStream(String tbl, Connection connection) throws Exception  {
		
		String createDDL = "CREATE TABLE " + tbl + " (one varchar(20), two smallint, birthDate date)";
		Statement statement = null;
		
		try {
			statement = connection.createStatement();  // create table
			statement.executeUpdate(createDDL);
			connection.commit();
		}  catch (SQLException e) {
			Worker.logQueryState("testCreateTable: " + e.getMessage(), Log_Level.verbose);
			if(Worker.LOG_LEVEL.equals(Log_Level.verbose)) {
				e.printStackTrace();
				Worker.logQueryState("DISREGARD ERROR, TABLE ALREADSY EXISTS", Log_Level.verbose);
			}
		}
			
		try {	
			statement.executeUpdate("INSERT INTO " + tbl + " VALUES('YIKES!', 0, '1973-05-23')");
			statement.executeUpdate("INSERT INTO " + tbl + " VALUES('WOW!', 5, '1985-07-15')");
			connection.commit();
			
			ResultSet rs = statement.executeQuery("SELECT * FROM " + tbl);
			//ResultSetMetaData metaData = rs.getMetaData();
			
			
			int[] sqlTypes = getTypesRS(rs);

			
			String outputTable = "testTable";
			String CreateTableDDL = "CREATE TABLE " + outputTable + " (one varchar(20), two smallint, birthDate date)";
			int colCount = 3;
			String prepStmtString = "INSERT INTO " + outputTable + " VALUES(?, ?, ?)";
			
			//create a server socket 
			int TEST_DATA_PORT = 4326;	
			startSocketServer("testWorker", TEST_DATA_PORT, Worker.sqliteConnection, Worker.channelToRabbit); //binds on localhost
			
			Socket clientSocket = null;
			
			try {
				InetAddress IP = InetAddress.getByName("localhost");  //connect to server socket on localhost 
				
				// open streaming socket to remote container with address IP
				clientSocket = new Socket(IP.getHostAddress(), TEST_DATA_PORT);
				clientSocket.setTcpNoDelay(true);
					Worker.logQueryState("MultiThreadedServer: Server Socket accepted new connection request", Log_Level.verbose);
			} catch (IOException e) {
				clientSocket.close();
				throw new RuntimeException("Error accepting client connection", e);
			} // end try-catch
				
			
			ArrayList< Vector<Object> > rowList  = new ArrayList< Vector<Object> >(); 
			while(rs.next()) {

				Vector<Object> row = new Vector<Object>(colCount);
				for(int i = 1; i <= colCount; i++) {
					//String type = metaData.getColumnTypeName(i);
					//String className = metaData.getColumnClassName(i);
					Object obj = rs.getObject(i);
					row.add(obj);
				}	

				rowList.add(row);

			}

			OutputRowListRunnable testOutputRowListRunnable = new  OutputRowListRunnable("localhost", rowList, "testWorker",
														clientSocket, outputTable, CreateTableDDL, 
														prepStmtString, sqlTypes);
			testOutputRowListRunnable.start();
			
			//For TEST: InputRowListRunnable is started from server socket thread
			
		} catch (SQLException e) {
			Worker.logQueryState("testCreateTable: " + e.getMessage(), Log_Level.verbose);
			e.printStackTrace();
		}
		
	} // end method testArrayListPartitionedStream
	
	/*
	 * tests if table NOT EXIST, then table is CREATEd and rows INSERTed, then verify 
	 * table contents with a SELECT query that returns a ResultSet and table meta-data
	 */
	protected  void testSimpleDatabase(String tbl, Connection connection) throws SQLException {

		Statement statement = connection.createStatement();
		
		/* if table does not exit, create  it and insert rows */
		if (!Worker.tableExists(tbl, connection)) { 
												
			System.out.println("testDatabase: Create Table '" + tbl + "'");
			try {
				statement.executeUpdate("CREATE TABLE " + tbl + " (one varchar(20), two smallint)");
				Worker.logQueryState("testDatabase: CREATED TABLE '" + tbl + "'", Log_Level.verbose);
				statement.executeUpdate("INSERT INTO tbl1 VALUES('hello!',10)");
				statement.executeUpdate("INSERT INTO tbl1 VALUES('goodbye', 20)");
				statement.executeUpdate("INSERT INTO tbl1 VALUES('help', 30)");
				statement.executeUpdate("INSERT INTO tbl1 VALUES('thanks', 40)");
				statement.executeUpdate("INSERT INTO tbl1 VALUES('wiedersehen', 50)");
				statement.executeUpdate("INSERT INTO tbl1 VALUES('au revoir', 60)");
				Worker.logQueryState("testDatabase: INSERTED ROWS INTO TABLE '" + tbl + "'", Log_Level.verbose);
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		} // end if

		// list rows in table along with table meta-data
		Worker.logQueryState("testDatabase: SELECT * FROM " + tbl, Log_Level.verbose);
		
		try {
			ResultSet rs = statement.executeQuery("SELECT * FROM " + tbl);
			Worker.logQueryState("testDatabase: Table name: " + rs.getMetaData().getTableName(1), Log_Level.verbose);

			int size = 0;

			if (!rs.next())
				Worker.logQueryState("testDatabase: " + tbl + " is EMPTY", Log_Level.verbose);
			else 
				do {
					String one = rs.getString("one");
					String two = rs.getString("two");
					Worker.logQueryState(one + " | " + two, Log_Level.verbose);
					size = rs.getRow();
				} while (rs.next());
			
			Worker.logQueryState("testDatabase: " + tbl + " size is " + size + " rows", Log_Level.verbose);
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			statement.close();
		}

	} // end method
	
	
	protected  void testDynamicsDoP(String tbl, Connection connection) throws Exception {

		Statement statement = connection.createStatement();
		ContainerizedQueryUtils utils = new ContainerizedQueryUtils();
		ComputeContainer cc = new ComputeContainer("", ContainerType.COMPUTE);
		cc.setId("C1");
		//add test query to container
		Table table = new Table();
		table.setTableName(tbl);
		cc.addInputTable(table);
		cc.setOperator("SELECT");
		cc.setPredicate("");
		cc.setSelectListClause("COUNT(*)");
		String query = cc.getQuery();
		
		// list rows in table along with table meta-data
		Worker.logQueryState("testDynamics: SELECT COUNT(*) FROM " + tbl, Log_Level.verbose);
		
		try {
			long count = utils.estimateDoP(100, "SELECT COUNT(*) FROM " + tbl, 
											cc,  tbl, statement);
			Worker.logQueryState("testDynamicsDoP: " + tbl + " size is " + count + " rows", Log_Level.verbose);
			
		} catch (SQLException e) {
			Worker.logQueryState("testDynamicsDoP: " + query + " FAILED ", Log_Level.verbose);
			e.printStackTrace();
		} finally {
			statement.close();
		}

	} // end method
	
	//inner class
	//progresses a ResultSet, but subject to halting by a interrupt
	class ThreadRS extends Thread {  
		
		Connection connection = null;
		String query = null;
		
		public ThreadRS(Connection connection, String query) {
			this.connection  = connection;
			this.query = query;
			
		}
		
		/*
		public void run()
		{ 
		  try  //execute query and assign to ResultSet
		  { 
			  Statement statement  = connection.createStatement();
			  statement.setQueryTimeout(0);
			  statement.setMaxRows(0);
			  ResultSet rs =  statement.executeQuery(query);
		 
		  }
		  catch(InterruptedException exception) 
		  { 
		   // thread was interrupted during sleep or wait
		  } catch (SQLException e) {
			e.printStackTrace();
		  }
		  finally  {
		  }
		  // exit run method and terminate thread
		} // method
		*/
		
	} // inner class
	
	//Test estimating size of query's ResultSet and the time to complete the query
		protected  void testRS_TimeOut(Connection connection, String query, int timeOutSecs) {
			
		    /*
		    Thread t = new Thread() {
		            public void run() {
		                System.out.println("blah");
		            }
		        }; */   
			
			ContainerizedQueryUtils utils = null;
			ResultSet rs = null;
			
			try {
					utils = new ContainerizedQueryUtils();
					Statement statement = connection.createStatement();
					statement.setQueryTimeout(timeOutSecs);
					rs = statement.executeQuery(query);
					int rowSizeInBytes = utils.estimateRowSizeInBytes(rs); //estimate
					System.out.println("Estimated row size(bytes) for ResultSet: " + rowSizeInBytes);
					
			} catch (SQLTimeoutException timeOut) {  //get estimated time for query
				int[] estimates = null;
				try {
					rs.close();
					estimates = utils.estimateRowsInResultSet(query, .001, connection);
				} catch (SQLException e) {
					System.out.println("estimateRowsInResultSet() raised exception");
					System.out.println(e.getMessage());
					e.printStackTrace();
				}
				System.out.println("Estimated total time, " + estimates[0] + " seconds for full query:\n" + query);
				System.out.println("Estimated # of rows in full ResultSet, " + estimates[1] + " for query:\n" + query);
				
			} catch (SQLException sqlEx) {
				System.out.println(sqlEx.getMessage());
				sqlEx.printStackTrace();
				
			} finally {
				try {
					rs.close();
				} catch (SQLException e) {
					System.out.println(e.getMessage());
				}
			}
			
				
		}   //method
	
	
	//Simulates partitioning a RS into N partitions sent to N parallel intra-operators
	protected  void testResultSetRowByteSize(Connection connection, String query) {
		
		try {
				ContainerizedQueryUtils utils = new ContainerizedQueryUtils();
				Statement statement = connection.createStatement();
				ResultSet rs = statement.executeQuery(query + " LIMIT 10");
				int rowSizeInBytes = utils.estimateRowSizeInBytes(rs); //estimate
				System.out.println("Estimated row size(bytes) for ResultSet: " + rowSizeInBytes);
				rs.close();
				
				int[] estimates = utils.estimateRowsInResultSet(query, .001, connection);
				System.out.println("Estimated total time, " + estimates[0] + " seconds for full query:\n" + query);
				System.out.println("Estimated # of rows in full ResultSet, " + estimates[1] + " for query:\n" + query);
				
		} catch (SQLException sqlEx) {
				System.out.println(sqlEx.getMessage());
		}	
			
	}   //method
	
	
	//Simulates partitioning a RS into N partitions sent to N parallel intra-operators
	protected  void testTableRowByteSize(String tbl, Connection connection, String query, int inMemoryLimit) {
		
		//final int MegaByte = 1048576;  //1024 x 1024 = 1,048,576 
		//final int GigaByte = MegaByte * 1024;
		final int FetchSize = 1024 * 16;  //# of rows
		long totalBytes = 0;  //estimate of total bytes processed by CRS
		int rowsProcessed = 0;
		int NoIntraOps = 0;
		int partitionBytes = 0; //# of bytes in current partition
		
		try {
			ContainerizedQueryUtils utils = new ContainerizedQueryUtils();
			int rowSizeInBytes = utils.estimateRowSizeInBytes(tbl, connection); //estimate
			System.out.println("Estimated row size(bytes) for table " + tbl + ": " + rowSizeInBytes);
			Statement statement = connection.createStatement();
			//statement.setQueryTimeout(60);
			ResultSet rs = statement.executeQuery(query);
			rs.setFetchSize(FetchSize);
				
			while(rs.next()) {
	        	 
					//send rows to DataOutPutStream
					totalBytes += rowSizeInBytes; //update bytes and rows processed
					
					if(partitionBytes >= inMemoryLimit) {
						NoIntraOps++;
						System.out.println("Send rows to new DataOutputStream");
						partitionBytes = 0;
					}
					
					partitionBytes += rowSizeInBytes;
					totalBytes += rowSizeInBytes;
					rowsProcessed++;
					
					//System.out.println("MOVED CURSOR TO LAST ROW: " + crs.last());
					//System.out.println("LAST Row #: " + rowsProcessed);
	        	 
				} // while
				
				/*
				if(totalBytes > GigaByte ) { //have exceeded planned SQL in-memory size
					//close current DataOutPutStream 
					//open new OutPutStream to new intra-container and continue Exchange
					System.out.println("Started new intra-op for next RS partition");
					System.out.println("Total RS bytes processed: " + totalBytes);
					System.out.println("Total RS rows processed: " + rowsProcessed);
					NoIntraOps++;
				}
				*/
	        	
			
			rs.close();
			
		} catch (SQLException sqlEx) {
			System.out.println(sqlEx.getMessage());
		}
		
		System.out.println("Number of intra-ops created: " +  NoIntraOps);
		System.out.println("Total RS bytes processed: " + totalBytes);
   	 	System.out.println("Total RS  rows processed: " + rowsProcessed);
		
		
		
	}   //method
	
	protected void testSortMerge() throws IOException {
		
		ArrayList<Queue<String>> queueList = new ArrayList<Queue<String>>();
		
		//String predicate = System.getenv("PREDICATE"); // assume DESC ordering
		
		Queue<String> queue1 = new ConcurrentLinkedQueue<String>();
		queueList.add(queue1);
		queue1.add("10005"); // add string representing sortKey
		queue1.add("10005|1955-01-21|Kyoichi|Maliniak|M|1989-09-12"); // add formatted row to queue
		queue1.add("10001"); // add string representing sortKey
		queue1.add("10001|1953-09-02|Georgi|Facello|M|1986-06-26"); // add formatted row to queue

		Queue<String> queue2 = new ConcurrentLinkedQueue<String>();
		queueList.add(queue2);
		queue2.add("10006"); // add string representing sortKey
		queue2.add("10006|1953-04-20|Anneke|Preusig|F|1989-06-02"); // add formatted row to queue
		queue2.add("10002"); // add string representing sortKey
		queue2.add("10002|1964-06-02|Bezalel|Simmel|F|1985-11-21"); // add formatted row to queue
				
		Queue<String> queue3 = new ConcurrentLinkedQueue<String>();
		queueList.add(queue3);
		queue3.add("10004"); // add string representing sortKey
		queue3.add("10004|1954-05-01|Chirstian|Koblick|M|1986-12-01"); // add formatted row to queue
		queue3.add("10003"); // add string representing sortKey
		queue3.add("10003|1959-12-03|Parto|Bamford|M|1986-08-28"); // add formatted row to queue
		
		//locate sort direction in Order By clause, e.g. extract "ORDER BY C2.emp_no_employees DESC"
		//from "SELECT * FROM C2 ORDER BY C2.emp_no_employees DESC;"
		
		worker.sortMergeFullyLoadedQeues(queueList);
		
	} // end testSortMerge
	
	protected void testSortMergeStreaming()  {
		
		ArrayList<Queue<String>> queueList = new ArrayList<Queue<String>>();
		
		Queue<String> queue1 = new ConcurrentLinkedQueue<String>();
		queueList.add(queue1);
		queue1.add("|10005|1955-01-21|Kyoichi|Maliniak|M|1989-09-12|10005"); // add formatted row to queue
		queue1.add("|10001|1953-09-02|Georgi|Facello|M|1986-06-26|10001"); 	// add formatted row to queue
		queue1.add("|10000|1956-09-02|Ringo|Starr|M|1996-06-26|10000"); 	// add formatted row to queue

		Queue<String> queue2 = new ConcurrentLinkedQueue<String>();
		queueList.add(queue2);
		queue2.add("|10006|1953-04-20|Anneke|Preusig|F|1989-06-02|10006"); // add formatted row to queue
		queue2.add("|10002|1964-06-02|Bezalel|Simmel|F|1985-11-21|10002"); // add formatted row to queue
				
		Queue<String> queue3 = new ConcurrentLinkedQueue<String>();
		queueList.add(queue3);
		queue3.add("|10004|1954-05-01|Chirstian|Koblick|M|1986-12-01|10004"); // add formatted row to queue
		queue3.add("|10003|1959-12-03|Parto|Bamford|M|1986-08-28|10003"); // add formatted row to queue
		
		//locate sort direction in Order By clause, e.g. extract "ORDER BY C2.emp_no_employees DESC"
		//from "SELECT * FROM C2 ORDER BY C2.emp_no_employees DESC;" Make sure "Predicate" env variable is set to clause;
		//String predicate = System.getenv("PREDICATE"); // assume DESC ordering
		
		ArrayList<Socket> sockets = new ArrayList<Socket>();
		for(int i = 0; i < 3; i++) {
			sockets.add(new Socket());
		}
		
		try {
			worker.sortMergeStreaming(queueList, sockets);
		} catch (InterruptedException | IOException e) {
			e.printStackTrace();
		}
		
	} // end testSortMerge

	
	protected  void testDataInputStream(String tbl, Connection connection) throws Exception  {
		
		Statement statement = null;
		
		try {
			statement = connection.createStatement();  // create table
			
		}  catch (SQLException e) {
			Worker.logQueryState("testDataInputStream: " + e.getMessage(), Log_Level.verbose);
			if(Worker.LOG_LEVEL.equals(Log_Level.verbose)) {
				e.printStackTrace();
				Worker.logQueryState("DISREGARD ERROR, TABLE ALREADSY EXISTS", Log_Level.verbose);
			}
		}
			
		try {	
				
			ResultSet rs = statement.executeQuery("SELECT * FROM " + tbl);
			//CachedRowSet crs = new CachedRowSetImpl();
			//crs.populate(rs);
			
			String outputTable = "testTable";
			String outputTableDDL = "CREATE TABLE " + outputTable + " (one varchar(20), two smallint, birthDate date)";
			String prepStmtString = "INSERT INTO " + outputTable + " VALUES(?, ?, ?)";
			
			//create a server socket 
			int TEST_DATA_PORT = 4326;	
			startSocketServer("testWorker", TEST_DATA_PORT, Worker.sqliteConnection, Worker.channelToRabbit); //binds on localhost
			
			Socket clientSocket = null;
			
			try {
				InetAddress IP = InetAddress.getByName("localhost");  //connect to server socket on localhost 
				
				// open streaming socket to remote container with address IP
				clientSocket = new Socket(IP.getHostAddress(), TEST_DATA_PORT);
				clientSocket.setTcpNoDelay(true);
					Worker.logQueryState("MultiThreadedServer: Server Socket accepted new connection request", Log_Level.verbose);
			} catch (IOException e) {
				//crs.close();
				rs.close();
				clientSocket.close();
				throw new RuntimeException("Error accepting client connection", e);
			} // end try-catch
				
			OutputResultSetRunnable testOutputRunnable = new OutputResultSetRunnable("localhost", rs, "testWorker",
																	clientSocket, outputTable, outputTableDDL, prepStmtString);
			testOutputRunnable.run();
			
			//For TEST: InputRowListRunnable is started from server socket thread
			
		} catch (SQLException e) {
			Worker.logQueryState("testCreateTable: " + e.getMessage(), Log_Level.verbose);
			e.printStackTrace();
		}
	
	} // end method testDataInputStream
	
	/* test if 'CREATE TABLE <table>' will create a ResultSet with new <table> */
	protected  void testDataOutputDefaultExchange(String tbl, Connection connection) throws Exception  {
		
		String createDDL = "CREATE TABLE " + tbl + " (one varchar(20), two smallint, birthDate date)";
		Statement statement = null;
		
		try {
			statement = connection.createStatement();  // create table
			statement.executeUpdate(createDDL);
			connection.commit();
		}  catch (SQLException e) {
			Worker.logQueryState("testCreateTable: " + e.getMessage(), Log_Level.verbose);
			if(Worker.LOG_LEVEL.equals(Log_Level.verbose)) {
				e.printStackTrace();
				Worker.logQueryState("DISREGARD ERROR, TABLE ALREADSY EXISTS", Log_Level.verbose);
			}
		}
			
		try {	
			statement.executeUpdate("INSERT INTO " + tbl + " VALUES('YIKES!', 0, '1973-05-23')");
			statement.executeUpdate("INSERT INTO " + tbl + " VALUES('WOW!', 5, '1985-07-15')");
			connection.commit();
			
			String outputTable = "testTable";
			
			//create a server socket 
			//For TEST: InputRowListRunnable is started from server socket thread
			startSocketServer("testWorker", Constants.DATA_PORT, Worker.sqliteConnection, Worker.channelToRabbit); //binds on localhost
			
			ArrayList<String> downstreamContainers = new ArrayList<String>();
			downstreamContainers.add("localhost");
			downstreamContainers.add("localhost"); //add a second downstream container 
			Exchange ex = new DefaultExchange();
			//Exchange ex = new EvenPartitionExchange();  //UNCOMMENT TO RUN test on EvenPartitionExchange
			ComputeContainer cc = new ComputeContainer("testWorker", ContainerType.COMPUTE);
			ex.executeExchange(cc, "SELECT * FROM tbl1", downstreamContainers, Worker.sqliteConnection, outputTable);
			
		} catch (SQLException e) {
			Worker.logQueryState("testCreateTable: " + e.getMessage(), Log_Level.verbose);
			e.printStackTrace();
		}
		
	} // end method
	
	
	/* test if 'CREATE TABLE <table>' will create a ResultSet with new <table> */
	protected  void testEmployeesOutputByteStream(Connection connection) throws Exception  {
		
		String tbl = "employees";
		
		Statement statement = null;
		
		try {
			statement = connection.createStatement();  // create table
		}  catch (SQLException e) {
			Worker.logQueryState("testCreateTable: " + e.getMessage(), Log_Level.verbose);
			if(Worker.LOG_LEVEL.equals(Log_Level.verbose)) {
				e.printStackTrace();
				Worker.logQueryState("DISREGARD ERROR, TABLE ALREADSY EXISTS", Log_Level.verbose);
			}
		}
			
		try {	
			
			ResultSet rs = statement.executeQuery("SELECT * FROM " + tbl + " LIMIT 20");
			
			//construct SLQ types array.  java.sql.Types are int constants 
			ResultSetMetaData metaData = rs.getMetaData();
			int colCount = metaData.getColumnCount();
			int[] sqlTypes = new int[colCount];     
			for(int col = 0; col < colCount; col++) {
				sqlTypes[col] = metaData.getColumnType(col + 1);   //java.sql.Types are int constants
				
			}
			
			FileOutputStream outFile = new FileOutputStream("localFile");
			DataOutputStream out = new DataOutputStream(outFile);
			
			//ArrayList< Vector<Object> > rowList  = new ArrayList< Vector<Object> >();
			
			byte[] bytes = new byte[512];
			ByteArrayInputStream rowOfBytes = new ByteArrayInputStream(bytes);
			
			while(rs.next()) {
				//Vector<Object> row = new Vector<Object>(colCount);
				for(int i = 1; i <= colCount; i++) {
					//row.add(rs.getObject(i));
				    rowOfBytes.read(rs.getBytes(i));
			        //bytes = rs.getBytes(i);		
					//out.write(bytes, 0, bytes.length);
				}	
				//rowList.add(row);
			}
			
			rs.close();
			out.flush();
			outFile.close();
			
			FileInputStream inFile = new FileInputStream("localFile");
			DataInputStream in = new DataInputStream(inFile);
			int noOfRows = 0;
			int col = 0;
			/* create prepared statement */
			String outputTable = "testTable";
			String SQL_INSERT = "INSERT INTO " + outputTable + " VALUES(?, ?, ?, ?, ?, ?);";
			PreparedStatement prepStmt = connection.prepareStatement(SQL_INSERT);
			String outputTableDDL = "CREATE TABLE " + outputTable + " (emp_no int(11) NOT NULL, birth_date date NOT NULL, " +
					  "first_name varchar(14) NOT NULL, last_name varchar(16) NOT NULL, gender text  NOT NULL, " +
					  "hire_date date NOT NULL);";
			String[] types = worker.getTypes(outputTableDDL);
			
			try {
				
				while(true) {  //read until EOF 

					for (col = 1; col <= colCount; col++) {	  //for each field in a row
						
						switch ( types[col - 1].toUpperCase() ) {
							case "INT": 
								//int integer = in.readInt();
								prepStmt.setInt(col, in.readInt());
								break;  
							case "SMALLINT":
								//short shortInt = in.readShort();
								prepStmt.setShort(col, in.readShort());
							    break;
						/*	case "DATE": 
								String dateString = in.readUTF();
								Date date = Date.valueOf(dateString);
								prepStmt.setDate(col, date);
								prepStmt.setString(col, dateString);
								break;  */
							case "DATE":    
							case "VARCHAR": 
							case "TEXT":
							case "CHAR":
								//String str = in.readUTF();
								prepStmt.setString(col, in.readUTF());
								break;
							case "REAL":
								//double real = in.readDouble();
								prepStmt.setDouble(col, in.readDouble());
								break;
							default:  Worker.logQueryState("insertRows: UNSUPPORTED SQLite TYPE " +  
															types[col], Log_Level.quiet);
						} // end switch
				
					} // end for
	    	   
					prepStmt.execute();  //insert  the row  
					noOfRows++;
	 
			   } // while
				
				
			} catch(EOFException eof) {
				//read until EOF read
				System.out.println(eof.toString() + " CORRECTLY signals end of data stream after " + noOfRows + " rows");
			} catch (Exception e) {
				System.out.println(e.getMessage() + ": " + e.getClass().getCanonicalName());
				in.close();
				throw e;
			}
					
			/*
			String outputTable = "employeesTest";
			String outputTableDDL = "CREATE TABLE " + outputTable + " (emp_no int(11) NOT NULL, birth_date date NOT NULL, " +
					  "first_name varchar(14) NOT NULL, last_name varchar(16) NOT NULL, gender text  NOT NULL, " +
					  "hire_date date NOT NULL);";
			String prepStmtString = "INSERT INTO " + outputTable + " VALUES(?, ?, ?, ?, ?, ?);";		
	
			//create a server socket 
			int TEST_DATA_PORT = 4326;	
			startSocketServer("testWorker", TEST_DATA_PORT, sqliteConnection, channelToRabbit); //binds on localhost
			
			Socket clientSocket = null;
			
			try {
				InetAddress IP = InetAddress.getByName("localhost");  //connect to server socket on localhost 
				
				// open streaming socket to remote container with address IP
				clientSocket = new Socket(IP.getHostAddress(), TEST_DATA_PORT);
				clientSocket.setTcpNoDelay(true);
					Worker.logQueryState("MultiThreadedServer: Server Socket accepted new connection request", Log_Level.verbose);
			} catch (IOException e) {
				rs.close();
				clientSocket.close();
				throw new RuntimeException("Error accepting client connection", e);
			} // end try-catch
				
			OutputRowListRunnable testOutputRunnable = new OutputRowListRunnable("localhost", rowList, "testWorker",
										clientSocket, outputTable, outputTableDDL, prepStmtString, sqlTypes);
			testOutputRunnable.run();
			
			//For TEST: InputRowListRunnable is started from server socket thread
			 
			 */
			
		} catch (SQLException e) {
			Worker.logQueryState("testCreateTable: " + e.getMessage(), Log_Level.verbose);
			e.printStackTrace();
		}
		
	} // end method testEmployeesOutputByteStream

	
	/* test if 'CREATE TABLE <table>' will create a ResultSet with new <table> */
	protected  void testDataOutputStream(String tbl, Connection connection) throws Exception  {
		
		String createDDL = "CREATE TABLE " + tbl + " (one varchar(20) NOT NULL, two smallint NOT NULL, birthDate date NOT NULL,"
									+ " PRIMARY KEY (one) )";
		Statement statement = null;
		
		try {
			statement = connection.createStatement();  // create table
			statement.executeUpdate(createDDL);
			connection.commit();
		}  catch (SQLException e) {
			Worker.logQueryState("testCreateTable: " + e.getMessage(), Log_Level.verbose);
			if(Worker.LOG_LEVEL.equals(Log_Level.verbose)) {
				e.printStackTrace();
				Worker.logQueryState("DISREGARD ERROR, TABLE ALREADSY EXISTS", Log_Level.verbose);
			}
		}
			
		String outputTable = "testTable";
		
		try {	
			statement.executeUpdate("INSERT INTO " + tbl + " VALUES('YIKES!', 0, '1973-05-23')");
			statement.executeUpdate("INSERT INTO " + tbl + " VALUES('WOW!', 5, '1985-07-15')");
			connection.commit();
			
			ResultSet rs = statement.executeQuery("SELECT * FROM " + tbl);
			//CachedRowSet crs = new CachedRowSetImpl();
			//crs.populate(rs);
			
			String outputTableDDL = "CREATE TABLE " + outputTable + " (one varchar(20), two smallint, birthDate date)";
			String prepStmtString = "INSERT INTO " + outputTable + " VALUES(?, ?, ?)";
			
			//create a server socket 
			int TEST_DATA_PORT = 4326;	
			startSocketServer("testWorker", TEST_DATA_PORT, Worker.sqliteConnection, Worker.channelToRabbit); //binds on localhost
			
			Socket clientSocket = null;
			
			try {
				InetAddress IP = InetAddress.getByName("localhost");  //connect to server socket on localhost 
				
				// open streaming socket to remote container with address IP
				clientSocket = new Socket(IP.getHostAddress(), TEST_DATA_PORT);
				clientSocket.setTcpNoDelay(true);
					Worker.logQueryState("MultiThreadedServer: Server Socket accepted new connection request", Log_Level.verbose);
			} catch (IOException e) {
				//crs.close();
				rs.close();
				clientSocket.close();
				throw new RuntimeException("Error accepting client connection", e);
			} // end try-catch
				
			OutputResultSetRunnable testOutputRunnable = new OutputResultSetRunnable("localhost", rs, "testWorker",
																	clientSocket, outputTable, outputTableDDL, prepStmtString);
			testOutputRunnable.run();
			
			//For TEST: InputRowListRunnable is started from server socket thread
			
		} catch (SQLException e) {
			Worker.logQueryState("testCreateTable: " + e.getMessage(), Log_Level.verbose);
			e.printStackTrace();
		} finally {
			statement.executeUpdate("DROP TABLE " + tbl);
			statement.executeUpdate("DROP TABLE " + outputTable);
			connection.commit();		
		}
		
		/*
		try {	
			statement.executeUpdate("INSERT INTO " + tbl + " VALUES('YIKES!', 0, '1973-05-23')");
			statement.executeUpdate("INSERT INTO " + tbl + " VALUES('WOW!', 5, '1985-07-15')");
			connection.commit();
			ResultSet rs = statement.executeQuery("SELECT * FROM " + tbl);
			DefaultExchange exchange = new DefaultExchange();
			String DDL = exchange.createTable_DDL(rs.getMetaData(), tbl);
			String prepString = "INSERT INTO " + tbl + " VALUES(?, ?, ?)";
			CachedRowSet crs = new CachedRowSetImpl();
			crs.populate(rs);
			OutputCachedRowSetRunnable outTest = new OutputCachedRowSetRunnable("test", crs, "test", 0, "test", "test", "test");
			DataOutputStream out = new DataOutputStream(new FileOutputStream("outputStream"));
			outTest.writeCRStoDataOutputStream(out, crs);
			out.close();
			DataInputStream in = new DataInputStream(new FileInputStream("outputStream"));
			InputRowListRunnable inputTest = new InputRowListRunnable("", new Socket(), sqliteConnection, channelToRabbit);
			PreparedStatement prepStmt = sqliteConnection.prepareStatement(prepString);
			@SuppressWarnings("unused")
			int noOfRows = inputTest.readRowsfromDataInputStream(in, DDL, prepStmt);
			@SuppressWarnings("unused")
			int[] results = prepStmt.executeBatch();
			connection.commit();
			String newTable =  rs.getMetaData().getTableName(1);
			Worker.logQueryState("testCreateTableAs: CREATE TABLE stmt for " + tbl  + ", EXECUTED", Log_Level.verbose);
			Worker.logQueryState("testCreateTableAs: MetaData contains table, " + newTable, Log_Level.verbose);
		} catch (SQLException e) {
			Worker.logQueryState("testCreateTable: " + e.getMessage(), Log_Level.verbose);
			e.printStackTrace();
		}
		*/
	
		
	} // end method testDataOutputStream
	
	/* test if 'CREATE TABLE <table>' will create a ResultSet with new <table> */
	protected  void testEmployeesRS_DataOutputStream(Connection connection, String tbl) throws Exception  {
		
		Statement statement = null;
		
		try {
			statement = connection.createStatement();  // create table
		}  catch (SQLException e) {
			Worker.logQueryState("testCreateTable: " + e.getMessage(), Log_Level.verbose);
			if(Worker.LOG_LEVEL.equals(Log_Level.verbose)) {
				e.printStackTrace();
				Worker.logQueryState("DISREGARD ERROR, TABLE ALREADSY EXISTS", Log_Level.verbose);
			}
		}
			
		try {	
			
			ResultSet rs = statement.executeQuery("SELECT * FROM " + tbl + " LIMIT 20");
			//CachedRowSet crs = new CachedRowSetImpl();
			//crs.populate(rs);
			
			String outputTable = "employeesTest";
			String outputTableDDL = "CREATE TABLE " + outputTable + " (emp_no int(11) NOT NULL, birth_date date NOT NULL, " +
					  "first_name varchar(14) NOT NULL, last_name varchar(16) NOT NULL, gender text  NOT NULL, " +
					  "hire_date date NOT NULL);";
			String prepStmtString = "INSERT INTO " + outputTable + " VALUES(?, ?, ?, ?, ?, ?);";		
	
			//create a server socket 
			int TEST_DATA_PORT = 4326;	
			startSocketServer("testWorker", TEST_DATA_PORT, Worker.sqliteConnection, Worker.channelToRabbit); //binds on localhost
			
			Socket clientSocket = null;
			
			try {
				InetAddress IP = InetAddress.getByName("localhost");  //connect to server socket on localhost 
				
				// open streaming socket to remote container with address IP
				clientSocket = new Socket(IP.getHostAddress(), TEST_DATA_PORT);
				clientSocket.setTcpNoDelay(true);
					Worker.logQueryState("MultiThreadedServer: Server Socket accepted new connection request", Log_Level.verbose);
			} catch (IOException e) {
				//crs.close();
				rs.close();
				clientSocket.close();
				throw new RuntimeException("Error accepting client connection", e);
			} // end try-catch
				
			OutputResultSetRunnable testOutputRunnable = new OutputResultSetRunnable("localhost", rs, "testWorker",
																	clientSocket, outputTable, outputTableDDL, prepStmtString);
			testOutputRunnable.run();
			
			//For TEST: InputRowListRunnable is started from server socket thread
			
		} catch (SQLException e) {
			Worker.logQueryState("testCreateTable: " + e.getMessage(), Log_Level.verbose);
			e.printStackTrace();
		}
		
	} // end method testEmployeesCRS_DataOutputStream
	
	/* test if 'CREATE TABLE <table>' will create a ResultSet with new <table> */
	protected  void testEmployeesArrayListDataOutputStream(Connection connection) throws Exception  {
		
		String tbl = "employees";
		
		Statement statement = null;
		
		try {
			statement = connection.createStatement();  // create table
		}  catch (SQLException e) {
			Worker.logQueryState("testCreateTable: " + e.getMessage(), Log_Level.verbose);
			if(Worker.LOG_LEVEL.equals(Log_Level.verbose)) {
				e.printStackTrace();
				Worker.logQueryState("DISREGARD ERROR, TABLE ALREADSY EXISTS", Log_Level.verbose);
			}
		}
			
		try {	
			
			ResultSet rs = statement.executeQuery("SELECT * FROM " + tbl + " LIMIT 20");
			
			//construct SLQ types array.  java.sql.Types are int constants 
			ResultSetMetaData metaData = rs.getMetaData();
			int colCount = metaData.getColumnCount();
			int[] sqlTypes = new int[colCount];     
			for(int col = 0; col < colCount; col++) {
				sqlTypes[col] = metaData.getColumnType(col + 1);   //java.sql.Types are int constants
				
			}
			
			ArrayList< Vector<Object> > rowList  = new ArrayList< Vector<Object> >();
			
			while(rs.next()) {

				Vector<Object> row = new Vector<Object>(colCount);
				for(int i = 1; i <= colCount; i++)
					row.add(rs.getObject(i));

				rowList.add(row);
	
			}
			
			rs.close();
			
			String outputTable = "employeesTest";
			String outputTableDDL = "CREATE TABLE " + outputTable + " (emp_no int(11) NOT NULL, birth_date date NOT NULL, " +
					  "first_name varchar(14) NOT NULL, last_name varchar(16) NOT NULL, gender text  NOT NULL, " +
					  "hire_date date NOT NULL);";
			String prepStmtString = "INSERT INTO " + outputTable + " VALUES(?, ?, ?, ?, ?, ?);";		
	
			//create a server socket 
			int TEST_DATA_PORT = 4326;	
			startSocketServer("testWorker", TEST_DATA_PORT, Worker.sqliteConnection, Worker.channelToRabbit); //binds on localhost
			
			Socket clientSocket = null;
			
			try {
				InetAddress IP = InetAddress.getByName("localhost");  //connect to server socket on localhost 
				
				// open streaming socket to remote container with address IP
				clientSocket = new Socket(IP.getHostAddress(), TEST_DATA_PORT);
				clientSocket.setTcpNoDelay(true);
					Worker.logQueryState("MultiThreadedServer: Server Socket accepted new connection request", Log_Level.verbose);
			} catch (IOException e) {
				clientSocket.close();
				throw new RuntimeException("Error accepting client connection", e);
			} // end try-catch
				
			OutputRowListRunnable testOutputRunnable = new OutputRowListRunnable("localhost", rowList, "testWorker",
										clientSocket, outputTable, outputTableDDL, prepStmtString, sqlTypes);
			testOutputRunnable.run();
			
			//For TEST: InputRowListRunnable is started from server socket thread
			
		} catch (SQLException e) {
			Worker.logQueryState("testCreateTable: " + e.getMessage(), Log_Level.verbose);
			e.printStackTrace();
		}
		
	} // end method testEmployeesArrayListDataOutputStream

	/* Simulate query message from Master.
	 * Sends query thru ALL_WORKERS_EXCHANGE exchange to this worker's queue 
	 * using workerID as the exchange key. */
	protected void simulateXferTable(String tbl, Channel channelToRabbit) throws UnsupportedEncodingException, IOException {
		
		MultiThreadedServer socketServer = new MultiThreadedServer("localhost", DATA_PORT, Worker.sqliteConnection, channelToRabbit);
		new Thread(socketServer).start();
		
		String query = "SELECT * FROM " + tbl;
		ConvertBytes converter = new ConvertBytes();
		worker.downstreamContainerIds.add("localhost");   //test not in ContainerMode
		ComputeContainer cc = new ComputeContainer("dummy", ContainerType.COMPUTE);  //dummy container
		QuerySQL payload = new QuerySQL(cc, query, new DefaultExchange(), worker.downstreamContainerIds, "fooOutputTable");
		QueryCtrlMsg queryCtrlMsg = new QueryCtrlMsg(Worker.workerID, payload);
		byte[] bytes = converter.convertToBytes(queryCtrlMsg);
		channelToRabbit.basicPublish(ALL_WORKERS_EXCHANGE, Worker.workerID, null, bytes);
		Worker.logQueryState("simulateXferTable: Sent SELECT query thru ALL_WORKERS_EXCHANGE exchange to this worker's queue ", Log_Level.verbose);

	} // end method
	
	/* Simulate query message from Master.
	 * Sends query thru ALL_WORKERS_EXCHANGE exchange to this worker's queue 
	 * using workerID as the exchange key. */
	public void simulatePartioningTableForJOIN(String tbl, Channel channelToRabbit) throws UnsupportedEncodingException, IOException {
		
		MultiThreadedServer socketServer = new MultiThreadedServer("localhost", DATA_PORT, Worker.sqliteConnection, channelToRabbit);
		new Thread(socketServer).start();
		
		ConvertBytes converter = new ConvertBytes();
		worker.downstreamContainerIds.add("localhost");   //test not in ContainerMode
		
		ComputeContainer cc = new ComputeContainer("dummy", ContainerType.COMPUTE);  //dummy container
		cc.setOperator("SELECT");
		cc.setPredicate("");
		cc.setProbe(true);
		//ArrayList<Container> downstream = new ArrayList<Container>();
		//downstream.add(cc);   //needed for QuerySQL payload
		
		String query = "SELECT * FROM " + tbl;
		QuerySQL payload = new QuerySQL(cc, query, new JoinPartitionExchange(), worker.downstreamContainerIds, "fooOutputTable");
		QueryCtrlMsg queryCtrlMsg = new QueryCtrlMsg(Worker.workerID, payload);
		byte[] bytes = converter.convertToBytes(queryCtrlMsg);
		
		channelToRabbit.basicPublish(ALL_WORKERS_EXCHANGE, Worker.workerID, null, bytes);
		Worker.logQueryState("simulatePartioningTable: sent SELECT query thru ALL_WORKERS_EXCHANGE exchange to this worker's queue ", Log_Level.verbose);

	} // end method
	
	
	public void simulatePartioningTableForORDER(String tbl, Channel channelToRabbit) throws UnsupportedEncodingException, IOException {
		
		MultiThreadedServer socketServer = new MultiThreadedServer("localhost", DATA_PORT, Worker.sqliteConnection, channelToRabbit);
		new Thread(socketServer).start();
		
		ConvertBytes converter = new ConvertBytes();
		worker.downstreamContainerIds.add("localhost");   //test not in ContainerMode
		
		ComputeContainer cc = new ComputeContainer("dummy", ContainerType.COMPUTE);  //dummy container
		cc.setOperator("SELECT");
		cc.setPredicate("ORDER BY ");
		
		QuerySQL payload = new QuerySQL(cc, "SELECT * FROM Students ORDER BY age", new JoinPartitionExchange(), 
											worker.downstreamContainerIds, "fooOutputTable");
		QueryCtrlMsg queryCtrlMsg = new QueryCtrlMsg(Worker.workerID, payload);
		byte[] bytes = converter.convertToBytes(queryCtrlMsg);
		
		channelToRabbit.basicPublish(ALL_WORKERS_EXCHANGE, Worker.workerID, null, bytes);
		Worker.logQueryState("simulatePartioningTable: sent SELECT ORDER BY query thru ALL_WORKERS_EXCHANGE exchange to this worker's queue ", Log_Level.verbose);

	} // end method


}

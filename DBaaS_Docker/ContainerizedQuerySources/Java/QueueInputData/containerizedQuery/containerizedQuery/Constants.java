package containerizedQuery;

import java.util.*;

public interface Constants {

    //rabbitMQ exchange name
    static final String ALL_WORKERS_EXCHANGE = "ALL_WORKERS_EXCHANGE";
    
    /* Start socket server to accept query data results from  upstream 
	 * containers in ephemeral range  32768 to 61000. */
    static final int DATA_PORT = 4325;
    static final int STREAM_BUFFER_SIZE = 1024000;
    
    //JDBC ResultSet size of rows fetched at a time
    static final int FETCH_SIZE = 1024;
  
    static enum Log_Level {
    	verbose, quiet, metrics
    }
    
    
    static final String UNDEFINED = "UNDEFINED";
    static final String QUERY_COMPLETED = "METRIC: QUERY COMPLETED";
    static final String QUERY_START_TIME = "METRIC: QUERY START_TIME";

    //queue on master receives ACK from  worker to acknowledge operation requested and performed by worker 
    static final String ACK_QUE = "ACK_QUE";
    static final String ACK_KEY = "ACK_KEY";
    static final String QUERY_QUE = "QUERY_QUE";
    static final String QUERY_KEY= "QUERY_KEY";
    
    enum QueryNodeType {
        COMPUTE, DATA_ONLY 
    } // end enum
    
    enum ContainerType {
        COMPUTE, DATA_ONLY, MERGE, MERGE_SORT, NONE, LEAF;
        
        public static String getNames() {
        	
        	StringBuffer buffer = new StringBuffer();
        	for (ContainerType  t: ContainerType.values())
        	{
        	  buffer.append(t.toString() + " ");
        	}
        	
        	return buffer.toString();
        } 
        
    } // end enum

    enum OperatorType {
        SELECT, JOIN, PROJECT, ORDER, MERGE, MERGE_SORT
    } // end enum

    //rabbitMQ exchange keys
    //static final Set<String> exchangeKeys = new HashSet<String>() {{add("shuffle"); add("join"); add("worker"); add("merge"); }};
    //static final Set<String> WORKER_TYPES = new HashSet<String>(Arrays.asList("interiorCompute", "leafSelect", "apex", "dataOnly"));
    static final Set<ContainerType> WORKER_TYPES = new HashSet<ContainerType>(Arrays.asList( ContainerType.values()));
    
    
    
}
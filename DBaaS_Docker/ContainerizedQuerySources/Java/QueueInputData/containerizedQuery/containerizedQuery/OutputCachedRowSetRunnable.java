package containerizedQuery;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketException;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
//import java.sql.SQLException;
import java.sql.Types;

//import javax.sql.rowset.CachedRowSet;

import com.sun.rowset.CachedRowSetImpl;

//import containerizedQuery.Constants.Log_Level;

/* To increase parallelism, a new thread is created for each output socket.
* A worker uses the class and its methods to send intermediate results of a relational operator
* as a set of rows(ResultSet) to the single immediate downstream container defined in the query tree. */
class OutputCachedRowSetRunnable extends Thread implements  Constants {

	String destinationContainer;
	//CachedRowSet crs;
	ResultSet rs;
	String workerID;
	int dataPort;
	String outputTableName;
	String SQL_CreateTable;
	String SQL_INSERT;
	Socket client = null;
	InetAddress IP = null;
	final int BUFFER_SIZE = 262144;
	//static boolean DEBUG = true;

	public OutputCachedRowSetRunnable(String destinationContainer, ResultSet rs, String workerID, int dataPort,
										String outputTableName, String SQL_CreateTable, String SQL_INSERT) {
	//public OutputCachedRowSetRunnable(String destinationContainer, CachedRowSet crs, String workerID, int dataPort,
	//			String outputTableName, String SQL_CreateTable, String SQL_INSERT) {
		super();
		this.destinationContainer = destinationContainer;
		this.rs = rs;
		this.workerID = workerID;
		this.dataPort = dataPort;
		this. outputTableName =  outputTableName;
		this.SQL_CreateTable = SQL_CreateTable;
		this.SQL_INSERT = SQL_INSERT;  //prepared statement string
	}

	/*
	//TESTING CONSTRUCTOR ONLY
	public OutputCachedRowSetRunnable(String destinationContainer, ResultSet rs, String workerID, Socket client,
											String outputTableName, String SQL_CreateTable, String SQL_INSERT) {
	//public OutputCachedRowSetRunnable(String destinationContainer, CachedRowSet crs, String workerID, Socket client,
	//			String outputTableName, String SQL_CreateTable, String SQL_INSERT) {
		super();
		this.destinationContainer = destinationContainer;
		//this.crs = crs;
		this.rs = rs;
		this.workerID = workerID;
		this.client = client;
		this. outputTableName =  outputTableName;
		this.SQL_CreateTable = SQL_CreateTable;
		this.SQL_INSERT = SQL_INSERT;  //prepared statement string
	}
	*/

	public void run() {
		int NoOfRows = 0;
		//int NoOfRows = crs.size();
		//ObjectOutputStream out = null;
		DataOutputStream out = null;
		long threadID = Thread.currentThread().getId();
		//Worker.setThreadStatus(threadID, true);  //keeps track of success or failure of run()
		
		try {
	
			int count = 0;
			while (client == null) { // keep trying to connect to remote reader socket 
	
				try {
					
					IP = InetAddress.getByName(destinationContainer);
					Worker.logQueryState("OutputCachedRowSetRunnable: " + workerID + " with threadID= " + threadID + ", writing table " +
											outputTableName + " as RS to destination '" +
											destinationContainer + "' with remote IP address " + IP.getHostAddress() + ", port" +
											dataPort, Log_Level.verbose);
					
					// open streaming socket to remote container with address IP
					client = new Socket(IP.getHostAddress(), dataPort);
					//client.setTcpNoDelay(true);
					client.setSendBufferSize(BUFFER_SIZE);
					
					Worker.logQueryState("OutputCachedRowSetRunnable: socket and OutputStream connected to remote container with IP "
					        + IP.getHostAddress() + " at port " + dataPort + ", with socketSendBufferSize "
					        + client.getSendBufferSize(),  Log_Level.verbose);
					
				} catch (SocketException se) {
					Worker.logQueryState("OutputCachedRowSetRunnable: Attempt opening socket to remote container with IP  " + 
											IP + ".\n" + se.getMessage()  + " .\n" + se.toString() + ".\n" + 
											se.getClass().getCanonicalName(), Log_Level.quiet);
					Thread.sleep(5000);
					count++;
					if (count > 60) {
						Worker.logQueryState("OutputCachedRowSetRunnable: Attempt opening connection to container " + 
								destinationContainer + "' with IP address " + IP +  " for 60 seconds timed-out.",
								Log_Level.quiet);
						throw new Exception("OutputRowSetRunnable: socket connection timeout");
					} else
						continue;
				} catch (Exception e) {
					Worker.logQueryState("OutputCachedRowSetRunnable: Attempt opening socket to remote container with IP  " + 
							IP + ".\n" + e.getMessage()  + " .\n" + e.toString() + ".\n" + 
							e.getClass().getCanonicalName(), Log_Level.quiet);
					throw e;  //re-throw exception	
				} // end try-catch
				
			} // end while
			
			//buffer and compress object output stream
			//GZIPOutputStream gz = new GZIPOutputStream(client.getOutputStream(), 1024000);
			//out = new ObjectOutputStream(gz);
			
			//buffer object output stream
			BufferedOutputStream bufferedOut = new BufferedOutputStream(client.getOutputStream(), STREAM_BUFFER_SIZE);
			//out = new ObjectOutputStream(bufferedOut);
			out = new DataOutputStream(bufferedOut);  //faster than ObjectOutputStream
			
			Worker.logQueryState("METRIC: OutputCachedRowSetRunnable, START XFER OUTPUT  ROWS  at time, " 
					+  System.currentTimeMillis() + ", " 
					+ workerID + " ===> " +  destinationContainer, Log_Level.metrics);
			
			out.writeUTF(workerID);           	// FROM, origin container
			out.writeUTF(destinationContainer); // TO, destination container
			out.writeUTF(outputTableName); 		// table to be created
			out.writeUTF(SQL_CreateTable); 		// SQL Create Table statement string
			out.writeUTF(SQL_INSERT); 		    // prepared SQL INSERT statement string
			out.flush();
			
			//Worker.logQueryState("METRIC: OutputRowSetRunnable, read-end of the socket connection is closed? " 
			//		+ client.isInputShutdown() + ", " + workerID + " ===> " +  destinationContainer, Log_Level.metrics);
			
			//out.writeObject(crs);    // CRS
			//writeCRStoDataOutputStream(out, crs);
			NoOfRows  = writeRStoDataOutputStream(out, rs);
			
			Worker.logQueryState("METRIC: OutputCachedRowSetRunnable, FINISH XFER OUTPUT " + NoOfRows + "  ROWS  at time, " 
									+  System.currentTimeMillis() + ", " 
									+ workerID + " ===> " +  destinationContainer  + "  COMPLETE", Log_Level.metrics);
			
			// don't close socket here, let remote read-side socket terminate the close,  JUST send FIN
			client.shutdownOutput(); //send FIN to remote connection
			//out.close(); 
			//client.close();
			Worker.logQueryState("OutputCachedRowSetRunnable:  SOCKET  client.shutdownOutput() at TIME " + System.currentTimeMillis() +
					", " + workerID + " ===> " + destinationContainer, Log_Level.quiet);
			
		} catch (Exception  e) {
					
			ByteArrayOutputStream stream = new ByteArrayOutputStream();
		    PrintWriter pw = new PrintWriter(stream);
		    e.printStackTrace(pw);
		    pw.flush();
			
			Worker.logQueryState("OutputCachedRowSetRunnable FAILED: attempting XFER from " + workerID + " -> " + destinationContainer +
					" over Socket to IP litteral " + IP + " .\n"  + e.getMessage() + ".\n"  +
					e.getClass().getCanonicalName() + "\n" + stream.toString(), Log_Level.quiet);
			
			pw.close();
			//Worker.setThreadStatus(threadID, false);
		}
			
	} // end run
	
	//helper method to gracefully close a socket's output stream to a remote connection and await the remote's
	//acknowledgement before closing the local socket
	public void synchronizedClose(Socket sock) throws IOException {
	    InputStream is = sock.getInputStream();
	    sock.shutdownOutput(); // Sends the 'FIN' on the network
	    while (is.read() > 0) ; // "read()" returns '-1' when the 'FIN' is reached
	    sock.close(); // or is.close(); Now we can close the Socket
	}
	
	//Speedup data xfer with DataOutputStream instead of serialized ObjectOutputStream
	public int writeRStoDataOutputStream(DataOutputStream  out, ResultSet rs ) throws Exception  {
	//public void writeCRStoDataOutputStream(DataOutputStream  out, CachedRowSet crs ) throws IOException, SQLException {

		ResultSetMetaData metaData = rs.getMetaData();
		int colCount = metaData.getColumnCount();
		int rowCount = 0;
		int col = 0;
		CachedRowSetImpl crs = new CachedRowSetImpl();
		crs.populate(rs);
		//int flushSize = 10000;
		
		try {
			
			//while (rs.next()) {
			while (crs.next()) {
				//format each row of the table for printing
				//String dataRow = crs.getString(1); // fence post
				//for (int col = 2; col <= colCount; col++) 
				//	dataRow += " \t\t| " + crs.getString(col);
				//formattedTable += "Table " + tableName +  " dataRow " + rowNo +  ": \t" + dataRow + "\n";

				for (col = 1; col <= colCount; col++) {	

					switch (metaData.getColumnType(col)) {
					case Types.INTEGER:		//int integer = crs.getInt(col);
						out.writeInt(rs.getInt(col));
						//out.writeInt(crs.getInt(col));
						break;           
					case Types.SMALLINT:	//short shortInt = crs.getShort(col);
						out.writeShort(rs.getShort(col));
						//out.writeShort(crs.getShort(col));
						break;
						/*	case Types.DATE: 		String dateString = Date.valueOf(crs.getString(col)).toString();
				                        out.writeUTF(dateString);
										break; 
					case Types.DATE:	out.writeUTF(rs.getDate(col).toString());
										break; */
					case Types.DATE:					
					case Types.VARCHAR:	 	//String varchar = crs.getString(col); 
						out.writeUTF(rs.getString(col));
						//out.writeUTF(crs.getString(col));
						break;
					case Types.FLOAT:		//float shortReal = crs.getFloat(col);
						out.writeFloat(rs.getFloat(col));
						//out.writeFloat(crs.getFloat(col));
						break;
					case Types.REAL:
					case Types.DECIMAL:
					case Types.DOUBLE:  	double real = rs.getDouble(col);
					//double real = crs.getDouble(col);
					out.writeDouble(real);
					break;
					default:  Worker.logQueryState("insertRows: UNSUPPORTED SQL TYPE " +  
							metaData.getColumnType(col), Log_Level.quiet);
					} // end switch

				} // end for

				rowCount++;
				//(rowCount > flushSize)  //flush periodically
				//out.flush();
					
			} //end while
			
			out.flush();  //flush any remaining rows in the buffer
			
		}  catch (Exception e) {
			
			String row = "|";
			for(int i = 1; i <= colCount; i++) 
				row += rs.getString(i) + "|";
			
			Worker.logQueryState("OutputCachedRowSetRunnable: writeRStoDataOutputStream FAILED in row " + row 
					+ " at col " + col + ", having xfered " + rowCount + " rows from " +  workerID + " ==> "   
					+ destinationContainer + "\n" + Worker.getSchema(metaData) + "\n" +  e.getLocalizedMessage(),
					Log_Level.quiet);
		
			//re-throw
			throw e;
		} finally {
			crs.close();
		}
		
	  return rowCount;
		
	} // end writeRStoDataOutputStream	
	
} // end class OutputCachedRowSetRunnable


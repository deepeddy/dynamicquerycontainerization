package containerizedQuery;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.ConnectionFactory;
//import com.rabbitmq.client.Connection;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Envelope;
import com.rabbitmq.client.ShutdownSignalException;

import java.io.File;
import java.io.IOException;
import java.io.NotSerializableException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.concurrent.TimeoutException;
import java.util.logging.*;


public class Master implements Constants {

	/* class variables */
	protected static Logger logger;
	protected static FileHandler fh = null;
	protected static Channel rabbitChannel;
	private static MasterConsumer ACK_Consumer;
	protected ContainerizedQueryUtils utils = new ContainerizedQueryUtils();
	protected  Digraph  digraph = new Digraph();

	/* use master container mounted  local host directory  */
	//protected static final boolean containerMode = true; 
	protected static String composeVersion = "3.3";  //default docker-compose Version
	protected static Log_Level LOG_LEVEL = Log_Level.verbose;   ////default logging level, logs everything
	protected static String hostVolume = "UNDEFINED";

	//CONSTRUCTOR
	public Master(String serializedQueryFileName, String rabbitMQ_IP,  String composeFileName,
			String hostVolume, String composeVersion, String logLevel) 
	{
		
		/* serialized query text file that represents the query's relational tree. */
	    //docker-compose Version, e.g. "3.3" or "2.1"
		Master.composeVersion = composeVersion;
		LOG_LEVEL =   Log_Level.valueOf(logLevel);  //logging level string converted to ENUM type
		Master.hostVolume = hostVolume;
		
		try {
			/* master container mounts host directory in Compose.yml file
			 * and maps it to container directory "/QueryFiles/logs"  */
			//master.startLogger("/QueryFiles/logs/Master.log"); 
			startLogger(hostVolume + "/Master.log");
			
			logQueryState("serializedQueryFileName= " + serializedQueryFileName + ", rabbitMQ_IP= " + rabbitMQ_IP +
					", composeFileName= " + composeFileName + ", compose Version= " + composeVersion
					+ " hostVolume= " + hostVolume
					+ " LOG_LEVEL= " + LOG_LEVEL, Log_Level.verbose);
			
			//construct Digraph of containers
			digraph = initializeQueryDigraph(serializedQueryFileName, composeFileName);
			
			//INITIALIZE  MESSAGE QUEUES, CHANNELS and LISTENER
			initQueuesAndConsumers(rabbitMQ_IP);
			
		} catch (Exception e){
			logQueryState("InitQueuesAndConsumers: FAILED, Master exiting " + e.toString() + "\n" +
					        e.getClass().getCanonicalName(), Log_Level.quiet);
			System.exit(1);
		}
		
		logQueryState("PROGRESS QUERY to initial SELECTION step from disk with LeafComputeContainers", Log_Level.verbose);
	
	}
	
	
	public static void main(String[] argv) throws Exception {
		
		PrintStream errStream = null;
		try {
				
			errStream = new PrintStream("./localErr");
			
			if ((argv.length != 6)) {  //HANDLE ARGUMENTS ERROR
				errStream.print("validateArgs: Usage: Master <serializedQueryFileName>  <rabbitMQ_IP>  <composeFileName>  "
						+ " <hostVolume> <composeVersion>,  <Log_Level>");
				errStream.print("validateArgs: # command line args found: " + argv.length);
				for (int i = 0; i < argv.length; i++) {
					errStream.print("argv[" + i + "]:  " + argv[i]);
				}
				
				errStream.print("validateArgs: ContainerMode: wrong number of command line arguments");
				throw new Exception("ContainerMode: wrong number of command line arguments");
			}
			 
			new Master(argv[0], argv[1], argv[2], argv[3], argv[4], argv[5]);
			
			
		} catch (Exception e) {
			logQueryState("main: " + e.getLocalizedMessage() + "\n" + e.getMessage() + e.getClass().getCanonicalName(), Log_Level.verbose);
			StringWriter sw = new StringWriter();
		    PrintWriter pw = new PrintWriter(sw);
		    e.printStackTrace(pw);
		    pw.flush();
		    if(logger != null)
		    	logQueryState("main: STACK-TRACE: " + sw.toString() + "\n" + e.getClass().getCanonicalName(), Log_Level.quiet);
		    else	
		    	errStream.print("main: STACK-TRACE: " + sw.toString() + "\n" + e.getClass().getCanonicalName());
			pw.close();
		}
		finally {
			errStream.flush();
			errStream.close();
			//logQueryState(" Query complete");
			//logQueryState(" CLOSING RABBIT CHANNEL");
			//rabbitChannel.close();
		}

	} // end main
	
	/*
	 * Given path to log file, sets global logger handle
	 */
	public void startLogger(String logFilePath) throws Exception {

		// set up logger
		logger = Logger.getLogger("MyLogger");

		try {
			// This block configures the logger handler and sets its formatter
			fh = new FileHandler(logFilePath);
			logger.addHandler(fh);
			SimpleFormatter formatter = new SimpleFormatter();
			fh.setFormatter(formatter);

			// start log
			logQueryState("Start Master log", Log_Level.verbose);

		} catch (SecurityException | IOException se) {
			logQueryState("startLogger: " + se.getMessage(), Log_Level.quiet);
			se.printStackTrace();
			throw se;
		} 

	} // end method
	
	/* construct a relational query tree and container digraph */
	public Digraph initializeQueryDigraph(String SerializedQueryFile, String composeFileName) throws Exception {
		
		ContainerizedQueryUtils utils = new ContainerizedQueryUtils();
		//System.out.println(hashTable.toString());  //test if hash worked
		//utils.printSerializedTree(hashTable);
		
		QueryNode root = null;
		Digraph g = new Digraph();
		try {
			LinkedHashMap<String, String> hashTable = utils.hashTextualQuery(SerializedQueryFile);
			logQueryState("\nHASH TEXTUALIZED FORM of QUERY:\n" + hashTable.toString().replace("]","\n"), Log_Level.verbose);
			
			logQueryState("\nCONSTRUCT QUERY TREE FROM TEXTUALIZED FORM:", Log_Level.verbose);
			root = utils.buildQueryTree(hashTable, "1"); //start from root node with id = 1
			
			//print contents of query tree
			logQueryState("\nNODES IN OPTIMIZED QUERY TREE\n" + utils.printQueryTree(root), Log_Level.verbose);
			
			utils.buildDigraph(root, g, null);
			logQueryState("\nPOPULATED DIGRAPH  with containers\n" + g.toString(), Log_Level.verbose);
			
			/* Construct Compose.yml file to execute query */
			String plan = utils.buildInitialQueryPlan(g, composeFileName, SerializedQueryFile, composeVersion, LOG_LEVEL.name());
			logQueryState(composeFileName + " file\n" + plan, Log_Level.verbose);
			
			logQueryState("\nDIGRAPH  MODIFIED for parallelism with ADDED intra-op containers\n" + g.toString(), Log_Level.verbose);
			
		} catch (QueryException e) {
			logQueryState("QueryException: " + e.getMessage(), Log_Level.quiet);
			e.printStackTrace();
			throw e;
		} catch (IOException e) {
			logQueryState("IOException: " + e.getMessage(), Log_Level.quiet);
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			logQueryState("General Exception: " + e.getMessage(), Log_Level.quiet);
			e.printStackTrace();
			throw e;
		}
		
		return g;
		
	} // end method
	
	/*
	 * Sends query payload object thru 'exchange' to a bound queue using 'key'.
	 */
	public  void sendMsg(CtrlMsg ctrlMsg, String exchange, String key, Channel rabbitChannel) 
			                      throws UnsupportedEncodingException, IOException  {
		
		if(!(ctrlMsg instanceof Serializable))
			throw new NotSerializableException(ctrlMsg.toString());
		
		logQueryState("MASTER sent CtrlMsg to Exchange: " + exchange + ", using  Key: " + key + ".\n"
						+ ctrlMsg.toString(), Log_Level.quiet);
		ConvertBytes converter = new ConvertBytes();
		rabbitChannel.basicPublish(exchange, key, null, converter.convertToBytes(ctrlMsg));
		
	} //  end method
	
	//INITIALIZE  MESSAGE QUEUES, CHANNELS and LISTENER
	public void initQueuesAndConsumers(String IP) throws Exception {

			//get connection to rabbit broker host 
			com.rabbitmq.client.Connection rabbitConnection = null;
			String rabbitBrokerIP = "UNDEFINED";
			ConnectionFactory factory = new ConnectionFactory();
			factory.setPort(5672);
			factory.setUsername("guest");
			factory.setPassword("guest");
			int count = 0;
			while (true) { // keep trying to connect to rabbitMQ broker
				try {
					// IP is the symbolic name for rabbitMQ broker
					rabbitBrokerIP = InetAddress.getByName(IP).getHostAddress(); // set class variable
					factory.setHost(rabbitBrokerIP); //the rabbit broker IP address literal, e,g, 10.05.16.08
					// open channel to rabbit broker
					rabbitConnection = factory.newConnection();
					logQueryState("InitQueuesAndConsumers:  rabbitMQ  answering hostname  " +  IP  + " mappped to " + 
							rabbitBrokerIP + " successfully.", Log_Level.verbose); 
					break;
				}
				catch (IOException | TimeoutException ie) {
					logQueryState("InitQueuesAndConsumers:  rabbitMQ broker not answering hostname  " +  IP  +
							" mappped to " + rabbitBrokerIP + ", "+ ie.getMessage() + "\n" +  ie.getClass().getCanonicalName(), 
							Log_Level.verbose);
					Thread.sleep(1000);
					count++;
					if (count > 60) {
						logQueryState("InitQueuesAndConsumers: Attempt opening connection to rabbitMQ broker for 60 seconds timed-out." +
								 IP  + " mappped to " + rabbitBrokerIP + ", "+ ie.getMessage() + 
								 ie.getClass().getCanonicalName(), Log_Level.quiet);
						throw new TimeoutException("rabbitMQ connection timeout");
					} else
						continue;
				} // end try
				
			} // end while

			try {
				//toAllWorkersRabbitChannel = InitQueues(connection);
				rabbitChannel = rabbitConnection.createChannel();
				logQueryState("InitQueuesAndConsumers: created rabbitChannel Connection opened to rabbitMQ broker at IP " 
						+ rabbitBrokerIP, Log_Level.verbose);

				//define exchange to communicate with all workers using publish/subscribe model
				rabbitChannel.exchangeDeclare(Constants.ALL_WORKERS_EXCHANGE, "direct");
				logQueryState("ALL_WORKERS_EXCHANGE  declared", Log_Level.verbose);

				//establish ACK QUEUE for replies from workers
				rabbitChannel.queueDeclare(ACK_QUE, false, false, false, null);
				//workers must use ACK_QUE key to send messages to ACK_QUE queue 
				rabbitChannel.queueBind(ACK_QUE, ALL_WORKERS_EXCHANGE, ACK_KEY);
				logQueryState("ACK_QUE  declared and bound", Log_Level.verbose);

				// CREATE WORKER LISTENER for acknowledgments or requests from workers 
				ACK_Consumer = new MasterConsumer(rabbitChannel, hostVolume, digraph);
				//send messages arriving at ACK_QUE to consumer, ACKsConsumer
				rabbitChannel.basicConsume(ACK_QUE, true, ACK_Consumer);
				logQueryState("ACK_Consumer listening", Log_Level.verbose);

				//rabbitChannel.queueDeclare(QUERY_QUE, false, false, false, null);
				//workers must use ACK_QUE key to send messages to ACK_QUE queue 
				//rabbitChannel.queueBind(QUERY_QUE, ALL_WORKERS_EXCHANGE, QUERY_KEY);
			
		} catch (Exception e) {
			logQueryState("ERROR INITIALIZING MESSAGE QUEUES, "  + e.getMessage() + "\n" +  e.getClass().getCanonicalName(),
							Log_Level.quiet);
			//fh.close();
			e.printStackTrace();
			throw e;
			//rabbitChannel.close();
			//System.exit(1);
		}

	} // end InitQueues

	/* MasterConsumer starts consumer in child thread. Consumes delivery of ACK messages from workers,
	 * which indicate progress of query.
	 */
	private  class MasterConsumer extends DefaultConsumer {
		
		//private boolean firstReadyMsg = true;
		private boolean AcceptReadyMsg = true;
		private ArrayList<CtrlMsg> partitionsReadyMsgs = new ArrayList<CtrlMsg>();
		
		//includes # partitions a relation is divided into, also counts each undivided relation, i.e.
		//it is the sum of all the data components feed into the query, each specified in a DATA-ONLY node
		int NoPartitions = 1;  

		private CtrlMsg ctrlMsg = null;  //CTRL message received by consumer
		private Channel channel;
		private String hostVolume = "UNDEFINED";
	
		//constructor
		public  MasterConsumer(Channel channel, String hostVolume, Digraph  digraph) {
			super(channel);
			this.channel = channel;
			this.hostVolume = hostVolume;
			try {
				NoPartitions = digraph.getLeafComputeVertices().size();
			} catch (Exception e) {
				logQueryState(" MASTER listener FAILED to account for # partitions: " + e.getMessage(), Log_Level.quiet);
			}
		}

		@Override
		public void handleShutdownSignal(java.lang.String consumerTag, ShutdownSignalException sig) {

			logQueryState(" MASTER listener received shutDown signal", Log_Level.quiet);
			fh.close();
		}

		@Override
		public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body)
		{
			// Update Query state 
			try { 
				ConvertBytes converter = new ConvertBytes();
				ctrlMsg = (CtrlMsg)converter.convertFromBytes(body);
				
				if(ctrlMsg.getType() == CTRL_MSG_TYPE.EDGE) {
					 logQueryState("handleDelivery: WORKER EDGE MESSAGE RECVD :\n" + ((EdgeCtrlMsg)ctrlMsg).toString(), Log_Level.quiet );
					 manageQueryState(ctrlMsg);
				}
				else if(ctrlMsg.getType() == CTRL_MSG_TYPE.DYNAMIC) {
					 logQueryState("handleDelivery: DYNAMIC INTRA-OP MESSAGE RECVD :\n" + ((DynamicCtrlMsg)ctrlMsg).toString(), Log_Level.quiet );
					 manageQueryState(ctrlMsg);
				}
				else if(ctrlMsg.getType() == CTRL_MSG_TYPE.LOG) {
					LogCtrlMsg msg = (LogCtrlMsg)ctrlMsg;
					
					//always log any message that arrives from worker
					logQueryState("handleDelivery: WORKER LOG MESSAGE RECVD: " + msg.logMessage,
										Log_Level.metrics);
					
					if(msg.getLogMessage().contains(QUERY_COMPLETED)) {
						logQueryState(QUERY_COMPLETED + " Master at time, " +  System.currentTimeMillis() + ", " 
										+ msg.getContainerId(), Log_Level.metrics);
						
						//CREATE Metrics CSV file
						Metrics2CSV.metric2CSV(hostVolume + "/Master.log", hostVolume + "/Metrics.csv");
						
					}
					
				}
				else if(ctrlMsg.getType() == CTRL_MSG_TYPE.READY  && AcceptReadyMsg) {
					logQueryState("handleDelivery: WORKER READY MESSAGE RECVD: " + ((ReadyCtrlMsg)ctrlMsg).toString(),
									Log_Level.quiet);
					
					partitionsReadyMsgs.add(ctrlMsg);   //increment # READY messages seen
					
					//DO NOT START QUERY TIME UNTIL ALL LEAF CONTAINERS ARE STARTED AND READY
					if(partitionsReadyMsgs.size() == NoPartitions) {
						
						String workerID = ((ReadyCtrlMsg)ctrlMsg).getContainerId();
						logQueryState(QUERY_START_TIME + ", " +  System.currentTimeMillis() + ", last Worker container ready " 
							+ workerID + ", " +  ((ReadyCtrlMsg)ctrlMsg).toString(), Log_Level.metrics);
						
						for( CtrlMsg ctrlMsg : partitionsReadyMsgs)  //start processing on all LEAF containers 
							manageQueryState(ctrlMsg);
						
						AcceptReadyMsg = false;
						
					}
			
				}
				else 
					logQueryState("handleDelivery: RECEIVED UNEXPECTED CtrlMsg type: " + ctrlMsg.getType(), Log_Level.quiet);
				
			}
			catch(Exception e) {
				StringWriter sw = new StringWriter();
			    PrintWriter pw = new PrintWriter(sw);
			    e.printStackTrace(pw);
			    pw.flush();
			    logQueryState("handleDelivery: WORKER CTRL MESSAGE FAILED: " + (LogCtrlMsg)ctrlMsg, Log_Level.quiet);
				logQueryState("handleDelivery: WORKER CTRL MESSAGE FAILURE STACK-TRACE: " + sw.toString(), Log_Level.quiet);
				pw.close();
			}

		} //end handleDelivery 

		@SuppressWarnings("unused")
		public String getMessage() {
			return ctrlMsg.toString();
		}

		public Channel getChannel() {
			return channel;
		}

	} // end MasterConsumer


	public static synchronized void logQueryState(String msg, Log_Level logLevel) {		
		
		if(logger == null) {
			System.out.println("\nMaster Log: " +  msg);
		} else if (logLevel.ordinal() >= LOG_LEVEL.ordinal()) {
				logger.info("\nMaster Log: " +  msg);
		}		
		
	} // end method
	
	
	/* process arrivals of messages from containers. Use messages to progress the QUERY STATE.
	 * Update  the Query state by inspecting the container digraph. */
	private  void manageQueryState(CtrlMsg ctrlMsg) throws Exception {
		
		//progress state of query by checking CompletedInputs
		if(ctrlMsg instanceof EdgeCtrlMsg) {

			EdgeCtrlMsg edgeCtrlMsg = (EdgeCtrlMsg)ctrlMsg;
			ComputeContainer cc = (ComputeContainer) digraph.getContainerByID(edgeCtrlMsg.getDestinationId());
			ContainerState  state = cc.getState();
			state.addInputEdge(edgeCtrlMsg.getOriginId(), edgeCtrlMsg.getDestinationId(), digraph);

			if(state.isInputComplete(digraph)) {
				logQueryState(" manageQueryState: Container '" + cc.id + "' STATE UPDATED:\n" + state + "\n"
						+ "effected by Edge activity: " +  edgeCtrlMsg.toString(), Log_Level.verbose);
				//start query on destination container 
				String query = cc.getQuery();
				Exchange parallelEX = cc.getParallelEX();
				String outputTableName = cc.getOutputTable().getTableName();
				QuerySQL payload = new QuerySQL(cc, query, parallelEX, digraph.getDestinationIds(cc), outputTableName);
				CtrlMsg queryCtrlMsg = new QueryCtrlMsg(cc.getId(), payload);
				//publish query thru rabbitMQ to container's queue
				sendMsg(queryCtrlMsg, ALL_WORKERS_EXCHANGE, cc.id, rabbitChannel);
				logQueryState(" Master using rabbitMQ, published '" + payload + " to compute container '" + cc.id + "'\n" 
						+ (edgeCtrlMsg).toString(), Log_Level.verbose);	
			}
			else
				logQueryState(" manageQueryState: Container '" + cc.id + "' STATE UNAFFECTED:\n" + state + "\n"
						+ "effected by Edge activity: " +  edgeCtrlMsg.toString(), Log_Level.quiet);
		}
		else if(ctrlMsg instanceof DynamicCtrlMsg) {  //create, dynamic schedule(YAML) and add new intra-ops
			
			DynamicCtrlMsg dynamicCtrlMsg = (DynamicCtrlMsg)ctrlMsg;
			String ccId = dynamicCtrlMsg.getContainerId();
			int NoDynamicIntaOps  =  dynamicCtrlMsg.getNoIntraOps();
			ComputeContainer cc = (ComputeContainer)digraph.getContainerByID(ccId);
			cc.getState().resetInputState();	//reset "state" inputs of container cc
			cc.getState().setProcessing(false);
			logQueryState(" manageQueryState: container '" + cc.id + 
							"', Input State reset, InputComplete " + cc.getState().isInputComplete(digraph), Log_Level.verbose);
			
			
			//schedule new intra-ops and manage their dataFlows
			String compose_version = System.getenv("COMPOSE_VERSION");
			String working_dir = "/usr/src/containerizedQuerySources/"; // /usr/src/containerizedQuerySources
			String composeFileNamePath =  working_dir + "IntraOpComposeFile-" + ccId;
			String IntraOpComposeFileString = utils.buildDynamicIntrOperatorQueryPlan(digraph,  composeFileNamePath, ccId, NoDynamicIntaOps, compose_version, "verbose");
			
			// TEST- SCHEDULE NEW INTRA-OP CONTAINERS, watch viz to see if containers come up in Swarm
			// docker stack deploy --compose-file docker-compose.yml.EmployeesBM1_rabbit query
			// ProcessBuilder pb = new ProcessBuilder("/bin/bash","-c","export TIME_OUT=0" + "&& exec");
			logQueryState(" manageQueryState: instance of DynamicCtrlMsg to schedule intraOps containers '" + cc.id + 
					"', DYNAMIC CTRL MSG: " + dynamicCtrlMsg.toString() + "\n" +
					"GENERATING INTRA_OP FILE: \n" + IntraOpComposeFileString, Log_Level.verbose);
			
			String execCmd = "docker stack deploy --compose-file " + composeFileNamePath + " query";
			ProcessBuilder pb = new ProcessBuilder("/bin/bash","-c", execCmd);
			pb.inheritIO();
			pb.directory(new File(working_dir));
			Process process = pb.start();
			int exitCode = process.waitFor();   //wait for process to execute cmd and terminate
			
			//porcess.isAlive supported only in JDK 8
			//logQueryState(" manageQueryState: instance of DynamicCtrlMsg to schedule intraOps containers '" + cc.id + 
			//		"', Docker-Compose process is still alive : " + process.isAlive() + "\n", Log_Level.verbose);
			logQueryState(" manageQueryState: instance of DynamicCtrlMsg to schedule intraOps containers for '" + cc.id + 
					"', Docker-Compose process exit code : " + process.exitValue()   + "\n", Log_Level.verbose);
			//System.out.println("Process is still alive: " + process.isAlive());
			
		    logQueryState(" manageQueryState: instance of DynamicCtrlMsg to schedule intraOps containers for '" + cc.id + 
						"', ProcessBuilder exited with code " + exitCode + "\n", Log_Level.verbose);
			//	System.out.println("ProcessBuilder exited with code " + exitCode);
		    
		    //dynamic intra-ops should be up and running, so send QueryCtrlMsg to intra-op's in-bound sources
		    // and re-execute their queries to populate intra-ops with row data
		    ArrayList<Container> inboundList = digraph.getInboundVertices(cc);
		    
		    for(int i = 0; i < inboundList.size(); i++) {
		    	
		    	ComputeContainer inbound = (ComputeContainer)inboundList.get(i);
	
				//restart query on in-bound container 
				String query = inbound.getQuery();
				Exchange parallelEX = inbound.getParallelEX();
				ArrayList<String> destinationList = digraph.getDestinationIds(inbound);
				String destinations = "";
				for(String d : destinationList) {  //get destination ids that inbound container is pushing data to 
					destinations = d + ", ";
				}
				logQueryState(" manageQueryState: Container '" + inbound.id + "' RESTARTING query: " + 
								query.toString() + " for dynamic Intra-ops: "
								+ destinations + " with exhange protocol" + parallelEX.toString() + "\n", Log_Level.verbose);
				
				String outputTableName = inbound.getOutputTable().getTableName();
				QuerySQL payload = new QuerySQL(inbound, query, parallelEX, digraph.getDestinationIds(inbound), outputTableName);
				CtrlMsg queryCtrlMsg = new QueryCtrlMsg(inbound.getId(), payload);
				
				//publish query thru rabbitMQ to container's queue
				sendMsg(queryCtrlMsg, ALL_WORKERS_EXCHANGE, inbound.id, rabbitChannel);
				logQueryState(" Master using rabbitMQ, published '" + payload + " to inbound container '" + inbound.id + "'\n" 
							+ (ctrlMsg).toString(), Log_Level.verbose);	
		    } // for inboundList
		}
		else if(ctrlMsg instanceof ReadyCtrlMsg) {  //only sent by leaf SELECT nodes?

			ReadyCtrlMsg ready = (ReadyCtrlMsg)ctrlMsg;   
			String Id = ready.getContainerId();
			ComputeContainer cc = (ComputeContainer)digraph.getContainerByID(Id);
			
			if (digraph.isLeafComputeContainer(Id)) {
				
				if(!(cc.getState().isProcessing())) {
					cc.getState().setProcessing(true);
					//master publish cc's query, so cc can begin processing its query
					String query = cc.getQuery();
					String outputTableName = cc.getOutputTable().getTableName();
					Exchange exchange = cc.getParallelEX();
					QuerySQL payload = new QuerySQL(cc, query, exchange, digraph.getDestinationIds(cc), outputTableName);
					CtrlMsg queryCtrlMsg = new QueryCtrlMsg(cc.id, payload);
					//publish query thru rabbitMQ to container's queue
					sendMsg(queryCtrlMsg, ALL_WORKERS_EXCHANGE, cc.id, rabbitChannel);
					logQueryState(" Master using rabbitMQ, published '" + payload + " to leaf ComputeContainer '" + cc.id 
									+ "'\n" + ready.toString(), Log_Level.quiet);	
				}	//end if cc is NOT processing yet
				else
					logQueryState("Duplicate Ready CTRL msg received by leaf ComputeContainer" + cc.id 
							+ "' that is already processing\n" + ready.toString(), Log_Level.quiet); 
			}
			else
				logQueryState("Ready CTRL msg received ERRONEOUSLY by interior ComputeContainer" + cc.id + "'\n" 
								+ ready.toString(), Log_Level.quiet);  
		}
		else {
			logQueryState(": manageQueryState:  UNKNOWN CtrlMsg type " + ctrlMsg.toString(), Log_Level.quiet);
			throw new Exception(": manageQueryState:  UNKNOWN CtrlMsg type " + ctrlMsg.toString());	
		}	
	
		
	}  // end method

} // end class Master

class MasterUnitTests  implements Constants {
	
	//empty constructor,  gets arguments from command line
	public MasterUnitTests() {
		
	}
	
	//must call MasterUnitTest from command line with args needed by Master
	public static void main(String[] argv) throws Exception {
	
		Master master = null;
		PrintStream errStream = null;
		try {
				
			errStream = new PrintStream("./localErr");
			
			if ((argv.length != 6)) {  //HANDLE ARGUMENTS ERROR
				errStream.print("validateArgs: Usage: Master <serializedQueryFileName>  <rabbitMQ_IP>  <composeFileName>  "
						+ " <hostVolume> <composeVersion>,  <Log_Level>");
				errStream.print("validateArgs: # command line args found: " + argv.length);
				for (int i = 0; i < argv.length; i++) {
					errStream.print("argv[" + i + "]:  " + argv[i]);
				}
				
				errStream.print("validateArgs: ContainerMode: wrong number of command line arguments");
				throw new Exception("ContainerMode: wrong number of command line arguments");
			}
			 
			master = new Master(argv[0], argv[1], argv[2], argv[3], argv[4], argv[5]);
			testStateSatisfied(Master.rabbitChannel);
			
		} catch (Exception e) {
			Master.logQueryState("main: " + e.getLocalizedMessage() + "\n" + e.getMessage() + e.getClass().getCanonicalName(), Log_Level.verbose);
			StringWriter sw = new StringWriter();
		    PrintWriter pw = new PrintWriter(sw);
		    e.printStackTrace(pw);
		    pw.flush();
		    if(master == null)
		    	Master.logQueryState("main: STACK-TRACE: " + sw.toString() + "\n" + e.getClass().getCanonicalName(), Log_Level.quiet);
		    else	
		    	errStream.print("main: STACK-TRACE: " + sw.toString() + "\n" + e.getClass().getCanonicalName());
			pw.close();
		}
		finally {
			errStream.flush();
			errStream.close();
		}
	} // end main
	
	
	protected static void testStateSatisfied(Channel rabbitChannel) throws IOException {
		
		 // tested with SimpleJoin.txt, simulates JOIN container C1 inputComplete state satisfied
		//getComputeContainerSQL((ComputeContainer)digraph.getContainerByID("C1"), digraph);
		// send CTRL message to Master acknowledging that loading is completed at destination container
		
		ConvertBytes converter = new ConvertBytes();
		
		EdgeCtrlMsg edgeMsg = new EdgeCtrlMsg("C2", "C1", "loading complete over edge");
		rabbitChannel.basicPublish(ALL_WORKERS_EXCHANGE, ACK_KEY, null, converter.convertToBytes(edgeMsg));
		
		LogCtrlMsg logMsg = new LogCtrlMsg("C1", "SIMULATED MESSAGE FROM WORKER C1");
		rabbitChannel.basicPublish(ALL_WORKERS_EXCHANGE, ACK_KEY, null, converter.convertToBytes(logMsg));
		
		edgeMsg = new EdgeCtrlMsg("C4", "C3","loading complete over edge");
		rabbitChannel.basicPublish(ALL_WORKERS_EXCHANGE, ACK_KEY, null, converter.convertToBytes(edgeMsg));
		
		logMsg = new LogCtrlMsg("C3", "SIMULATED MESSAGE FROM WORKER C3");
		rabbitChannel.basicPublish(ALL_WORKERS_EXCHANGE, ACK_KEY, null, converter.convertToBytes(logMsg));
	
		
	} // end method
	   
	
} // end class MasterUnitTests

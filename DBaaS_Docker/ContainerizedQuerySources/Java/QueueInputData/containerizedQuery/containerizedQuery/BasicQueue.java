package containerizedQuery;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.concurrent.TimeoutException;

import com.rabbitmq.client.*;

public class BasicQueue {

	public static void main(String[] args) {
		
		String worker =  args[0];
		String workerQue = worker + "_queue";
		String sibling = args[1];
		String siblingQue = sibling + "_queue";
		
		com.rabbitmq.client.Connection rabbitConnection = null;
		ConnectionFactory factory = new ConnectionFactory();
		
		// the IP address where rabbitMQ broker is located
		InetAddress address = null;
		try {
			address = InetAddress.getByName("localhost");
		} catch (UnknownHostException e1) {
			System.out.println("Failed to resolve 'localhost'");
			e1.printStackTrace();
			System.exit(0);
		}
		
		String rabbitBrokerIP = address.getHostAddress();
		factory.setHost(rabbitBrokerIP);
		factory.setPort(5672);
		factory.setUsername("guest");
		factory.setPassword("guest");
		
		try {
			rabbitConnection = factory.newConnection();
			System.out.println("Connection opened to rabbitMQ broker at IP " + rabbitBrokerIP);
		} catch (TimeoutException | IOException e) {
			System.out.println("Failed to  create rabbit connection at " + rabbitBrokerIP);
			e.printStackTrace();
			System.exit(0);
		}
		
		// create channel for adding a queue
		Channel channel = null;
		try {
			channel = rabbitConnection.createChannel();
			System.out.println("Channel created with rabbitMQ broker at IP " + rabbitBrokerIP);
		} catch (IOException e) {
			System.out.println("Failed to create rabbit channel");
			e.printStackTrace();
			System.exit(0);
		}
		
		//create both worker and sibling message queues in case sibling process has not started
		try {
			channel.queueDeclare(workerQue,  false, false, false, null);
			channel.queueDeclare(siblingQue, false, false, false, null);
		} catch (IOException e) {
			System.out.println("Failed to create one or both queues " + worker + ", " + sibling);
			e.printStackTrace();
			System.exit(0);
		}
		
		String message = "Random number " + Math.random() * 100  + " by " + worker;
		try {
			channel.basicPublish("", workerQue, null, message.getBytes());
			System.out.println(" [x] Published '" + message + "'");
		} catch (IOException e) {
			System.out.println("Failed to publish INFO " + message + " by " + worker);
			e.printStackTrace();
			System.exit(0);
		}
		
		try {
			
			GetResponse response = null;
			while((response = channel.basicGet(siblingQue, true)) == null) {
				Thread.yield();
			}
			
			System.out.println(new String(response.getBody()));
			//System.out.println(response.toString());
			channel.queueDelete(siblingQue);
			channel.close();
			rabbitConnection.close();
			
		} catch (IOException | TimeoutException e) {
			System.out.println("Get INFO from " + sibling + " FAILED");
			e.printStackTrace();
			System.exit(0);
		} 
		
	} // end main

}

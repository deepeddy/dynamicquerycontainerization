package containerizedQuery;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
//import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
//import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Vector;

//import containerizedQuery.Constants.Log_Level;

//import containerizedQuery.Constants.Log_Level;

/* To increase parallelism, a new thread is created for each output socket.
 * A worker uses the class and its methods to send intermediate results of a relational operator
 * as a set of rows(ResultSet) to the immediate downstream container defined in the query tree. */
class OutputRowListRunnable extends Thread implements Constants {

		String destinationContainer;
		ArrayList< Vector<Object> > rowList;
		String workerID;
		Socket client = null;
		int dataPort;
		String outputTableName;
		String SQL_CreateTableDDL;
		String SQL_INSERT;
		int[] sqlTypes;

		public OutputRowListRunnable(String destinationContainer,  ArrayList< Vector<Object> > rowList, 
							String workerID, int dataPort, String outputTableName, String SQL_CreateTableDDL, String SQL_INSERT,
							int[] sqlTypes) {
			super();
			this.destinationContainer = destinationContainer;
			this.rowList = rowList;
			this.workerID = workerID;
			this.dataPort = dataPort;
			this.outputTableName =  outputTableName;
			this.SQL_CreateTableDDL = SQL_CreateTableDDL;
			this.SQL_INSERT = SQL_INSERT;    //prepared statement string
			this.sqlTypes = sqlTypes;
		}
		
		//TESTING CONSTRUCTOR ONLY
		public OutputRowListRunnable(String destinationContainer,  ArrayList< Vector<Object> > rowList, 
							String workerID, Socket client, String outputTableName, String SQL_CreateTableDDL, String SQL_INSERT,
							int[] sqlTypes) {
			super();
			this.destinationContainer = destinationContainer;
			this.rowList = rowList;
			this.workerID = workerID;
			this.client = client;
			this.outputTableName =  outputTableName;
			this.SQL_CreateTableDDL = SQL_CreateTableDDL;
			this.SQL_INSERT = SQL_INSERT;    //prepared statement string
			this.sqlTypes = sqlTypes;
	}

		public void run()  {
			int listSize = rowList.size();
			int rowsWritten = 0;
			InetAddress IP = null;
			DataOutputStream out = null;
			//ObjectOutputStream out = null;
			long threadID = Thread.currentThread().getId();
			//Worker.setThreadStatus(threadID, true);  //keeps track of success or failure of run()
			
			try {
				
				int count = 0;
				while (client == null) { // keep trying to connect to remote reader socket
					try {
						
						IP = InetAddress.getByName(destinationContainer);
						Worker.logQueryState("OutputRowListRunnable  " + workerID + " with threadID= " + threadID + ", writing table " + 
												outputTableName + " as partition with size " + listSize + " to destination '" +
												destinationContainer + "' with IP address " + IP.getHostAddress() + " at port " + dataPort, 
												Log_Level.verbose);
						
						// open streaming socket to remote container with address IP
						client = new Socket(IP.getHostAddress(), dataPort);
						//client.setTcpNoDelay(true);
						client.setSendBufferSize(262144);

						Worker.logQueryState("OutputRowListRunnable: socket and OutputStream connected to remote container with IP "
						        + IP.getHostAddress() + " at port " + dataPort, Log_Level.verbose);
						break;
					}
					catch (SocketException | UnknownHostException ex) {
						
						if(count % 10 == 0)
							Worker.logQueryState("OutputRowListRunnable: Attempt opening socket to remote container with IP  " + 
								IP + " .\n" + ex.getMessage() + ",  count " + count + ".\n" + ex.toString() + ".\n" + 
								ex.getClass().getCanonicalName(), Log_Level.verbose);
		
						Thread.sleep(500);
						count++;
						if (count > 60) {
							Worker.logQueryState("OutputRowListRunnable: Attempt opening connection to container " + 
									destinationContainer + "' with IP address " + IP +  " for 30 seconds timed-out.",
									Log_Level.quiet);
							throw new Exception("OutputRowListRunnable: socket connection timeout");
						} else
							continue;
					} // end try
				} // end while
				
				BufferedOutputStream bufferedOut = new BufferedOutputStream(client.getOutputStream(), 1024000);
				//out = new ObjectOutputStream(bufferedOut);
				out = new DataOutputStream(bufferedOut);
				
				Worker.logQueryState("METRIC: OutputRowListRunnable START XFER OUTPUT " + listSize + " ROWS at time, "
							+  System.currentTimeMillis() + ", " + workerID + " ===> " +  destinationContainer, Log_Level.metrics);
				
				out.writeUTF(workerID);           	// FROM, origin container
				out.writeUTF(destinationContainer); // TO, destination container
				out.writeUTF(outputTableName); 		// table to be created
				out.writeUTF(SQL_CreateTableDDL); 	// SQL Create Table statement string
				out.writeUTF(SQL_INSERT); 		    // SQL INSERT statement
				out.flush();
				
				//out.writeObject(rowList);           // ArrayList< Vector<Object> >  of rows
				rowsWritten = writeRowListToDataOutputStream(out, rowList, sqlTypes);
				
				Worker.logQueryState("METRIC: OutputRowListRunnable FINISH XFER OUTPUT " + rowsWritten + " ROWS at time, "
							+  System.currentTimeMillis() + ", " + workerID + " ===> " +  destinationContainer, Log_Level.metrics);
			
				// don't close socket here, let remote read-side socket terminate the close, JUST send FIN
				client.shutdownOutput();
				//out.close();
			    //client.close();
				Worker.logQueryState("OutputRowListRunnable:  SOCKET  client.shutdownOutput() at TIME " + System.currentTimeMillis() +
						", " + workerID + " -> " + destinationContainer, Log_Level.quiet);
				
			} catch (Exception e) {
				
				ByteArrayOutputStream stream = new ByteArrayOutputStream();
			    PrintWriter pw = new PrintWriter(stream);
			    e.printStackTrace(pw);
			    pw.flush();
				
				Worker.logQueryState("OutputRowListRunnable FAILED: attempting XFER from " + workerID + " -> " + destinationContainer +
						" over Socket to IP litteral " + IP + " .\n"  + e.getMessage() + ".\n"  +
						e.getClass().getCanonicalName() + "\n" + stream.toString(), Log_Level.quiet);
				
				pw.close();
				//Worker.setThreadStatus(threadID, false);
				return;
			}
			
			Worker.logQueryState("OutputRowListRunnable: partition(List) xfered " + rowsWritten + " rows from " + 
					workerID + " -> " + destinationContainer  + "  COMPLETE", Log_Level.quiet);	
			
		} // end run
		
		//Speedup data xfer with DataOutputStream instead of serialized ObjectOutputStream
		public int writeRowListToDataOutputStream(DataOutputStream  out, ArrayList< Vector<Object> > rowList, int[] sqlTypes)
				throws Exception {
				
			int size = sqlTypes.length; //number of data fields in each vector
			int rowsWritten = 0;
			
			for (int r = 0; r < rowList.size(); r++) {
				//format each row of the table for printing
				//String dataRow = crs.getString(1); // fence post
				//for (int col = 2; col <= colCount; col++) 
				//	dataRow += " \t\t| " + crs.getString(col);
				//formattedTable += "Table " + tableName +  " dataRow " + rowNo +  ": \t" + dataRow + "\n";
				
				Vector<Object> row = rowList.get(r);

				for (int i = 0; i < size; i++) {	
						
					switch (sqlTypes[i]) {
					case Types.INTEGER:		//int integer = (int) row.get(i);
					 						out.writeInt((int) row.get(i));
					 						break;           
					//case Types.SMALLINT:	short shortInt = (short) row.get(i);
					case Types.SMALLINT:	short shortInt = ((Integer) row.get(i)).shortValue();
											out.writeShort(shortInt);
											break;   
			 /*		case Types.DATE: 		Date date = (Date) row.get(i);
	                						String dateString = date.toString();
	                						out.writeUTF(dateString);
											break; */
					case Types.DATE: 						
					case Types.VARCHAR:		//String varchar = (String)row.get(i);
											out.writeUTF((String) row.get(i));
											break;
					case Types.FLOAT:		//float shortReal = (float) row.get(i);
											out.writeFloat((float) row.get(i));
											break;
					case Types.REAL:
					case Types.DECIMAL:
					case Types.DOUBLE:  	//double real = (double) row.get(i);
											out.writeDouble((double) row.get(i));
											break;
					default:  Worker.logQueryState("insertRows: UNSUPPORTED SQL TYPE " +  sqlTypes[i], Log_Level.quiet);
					} // end switch
				
				} // end for
				
				rowsWritten++;
			
		  } //end while
			
		  out.flush();  //flush any remaining rows in the buffer
		  return rowsWritten;
			
		} // end writeCRStoDataOutputStream	
		
		
	} // end class OutputRowListRunnable

package containerizedQuery;

import java.io.File;
import java.io.IOException;

public class PropertyTest {

	public static void main(String[] args) {
		
		String timeOut = System.getenv("TIME_OUT");
		System.setProperty("TIME_OUT", timeOut);
		printProperty("TIME_OUT");
		
		AnotherClass another = new AnotherClass();
		another.printProperty("TIME_OUT");
		
		int exitCode = 0;
		try {
			  ProcessBuilder builder = new ProcessBuilder("/bin/bash","-c", "ls");
			  builder = builder.directory(new File("/home/david/dynamicquerycontainerization/DBaaS_Docker"));
			  //builder.directory(null);
			  builder.inheritIO();
			  Process process = builder.start();
			  exitCode = process.waitFor();   //wait for process to execute cmd and terminate
			  //System.out.println("Process is still alive: " + process.isAlive());  //only JDK 8
			  System.out.println("Process is still alive: " + process.exitValue()); 
			  
			  // create a new list of arguments for our process
		      String[] list = {"notepad.exe", "test.txt"};
		      // create the process builder
		      ProcessBuilder pb = new ProcessBuilder(list);
		      // set the command list
		      pb.command(list);
		      // print the new command list
		      System.out.println("" + pb.command());
			
		} catch (IOException ioe) {
			System.out.println(ioe.getMessage());
		} catch (InterruptedException inter) {
			System.out.println(inter.getMessage());
		}
		
		//builder.directory(new File("/home/david"));
		
		if (exitCode == 0)
			System.out.println("ProcessBuilder exited normally, with code " + exitCode);
		else
			System.out.println("ProcessBuilder exited abnormally, with code " + exitCode);
		
	}
	
	
	public static void printProperty(String key) {
		
		System.out.println(System.getProperty(key));
		
	}

} //class

class AnotherClass {
	
	public void printProperty(String key) {
		
		System.out.println(System.getProperty(key));
		
	}
	
}
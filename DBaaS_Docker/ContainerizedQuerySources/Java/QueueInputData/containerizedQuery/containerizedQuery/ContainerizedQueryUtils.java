package containerizedQuery;

import java.io.*;

import java.sql.*;
//import com.sun.rowset.CachedRowSetImpl;

import containerizedQuery.Constants.ContainerType;
import containerizedQuery.Constants.Log_Level;

//import javax.sql.rowset.CachedRowSet;

import java.util.*;
import java.util.regex.*;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;
import java.util.Map.*;
import java.util.logging.Logger;


import org.apache.commons.io.FileUtils;
//import org.sqlite.jdbc3.JDBC3ResultSet;

import com.rabbitmq.client.*;
import com.rabbitmq.client.Connection;


public class ContainerizedQueryUtils implements Constants {
	
	/*  Reads in a serialized textual notation that represents the structure of a optimized relational 
	 *  QUERY TREE from a text file. Each line in the file represents a tree node, whose fields are 
	 *  tab delimited.  There are two(2) types of records: COMPUTE, DATA_ONLY, with different field 
	 *  definitions. A line is considered to be terminated by any one of a line-feed
	 *  ('\n'), a carriage return ('\r'), or a carriage return followed immediately by a line-feed. The
	 *  contents of the serialized file is written by a simple text editor, e.g. getEdit, NotePad.
	 *  
	 *  tab delimited FORMAT of FIELDS:
	 *  
	 *  COMPUTER TYPE: 
	 *  <ID		TYPE	OPERATOR	PREDICATE	LEFT	RIGHT>
	 *  
	 *  ID: 		identifies a query tree node
	 *  TYPE:		COMPUTE denotes a container that executes a relational operator
	 *  OPERATOR: 	identifies the relation operator executed by the node
	 *  PREDICATE: 	any predicate expression for the operator, NULL for no predicate
	 *  L:			is the left child's ID
	 *  R:			is the right child's ID   //relational ops are unary or binary only
	 *  
	 *  DATA_ONLY TYPE:
	 *  <ID		TYPE	DATABASE_URI 	TABLE>
	 *  
	 *  ID: 			identifies a query tree node
	 *  TYPE:			DATA_ONLY denotes a container only reserved for managing access to data
	 *  DATABASE:URI 	URI locating database, e.g. file://localhost/usr/sqlite/data/student_tables.db
	 *  
	 *    ID = { 1 ... n), ID = 1 is tree root node, ID = NULL denotes NO child.
	 *  
	 *  Consider the following relational expression of operators and predicates:
	 *   
	 *   Order[R.A1,<](Project[R.A1,S.B1]((Select[R.A2 < a](R)) Join[R.A3 = S.B3] (Select[S.B2 = b](S))))
	 *    
	 *  where, R(A1,A2,A3) and S(B1,B2,B3) are relations with three(3) fields each. This relational expression
	 *  can also be represented in tree form; referred to as the relational expression's QUERY TREE. 
	 *  
	 *  
	 *                  (1)     Order[R.A1,<]
	 *                            |
	 *                            |
	 *                            |
	 *                  (2)     Project[R.A1,S.B1]
	 *                            |
	 *                            |
	 *                            |
	 *                  (3)     Join[R.A3 = S.B3]
	 *                          /   \ 
	 *                         /     \
	 *                        /       \
	 *    (4)  Select[R.A2 < a]        Select[S.B2 = b]  (5)
	 *                       |         |
	 *                 (6)   R         S   (7)
	 *                     (URI,R)  (URI,S)
	 *                     
	 *   This QUERY TREE can be serialized textually using the field format above:
	 *   
	 *   ID		TYPE		OPERATOR	PREDICATE		LEFT	RIGHT
	 *   1		COMPUTE		Order		[R.A1,<]		2		null
	 *   2		COMPTUE		Project		[R.A1,S.B1]		3		null
	 *   3		COMPUTE		Join		[R.A3 = S.B3]	4		5
	 *   4		COMPUTE		Select		[R.A2 < a]		6		null
	 *   5		COMPUTE		Select		[S.B2 = b]		7		null
	 *   
	 *   ID		TYPE		DATABASE_URI								TABLE						
	 *   6      DATA_ONLY   file://${HOME}/databases/student_tables.db	R
	 *   7      DATA_ONLY   file://${HOME}/databases/student_tables.db	S
	 *   
	 *   Relations R,S are in terminal nodes with no children
	 *   
	 */
	
	private static final boolean DEBUG = false;
    static  int fieldNameSuffix = 1;
	static String workerImage = "UNDEFINED";
	static String rabbitImage = "UNDEFINED";
	static String masterImage = "UNDEFINED";

	public static void main(String[] args) {  //for TESTING
		
		ContainerizedQueryUtils utils = new ContainerizedQueryUtils();
		
		 /* "SerializedQueryFile.txt" passed in as args[0] */
		String textualQueryFile = args[0];
		String composeFileName = args[1];
		String version = args[2];
		String log_level = args[3];
		
		//start from root node with id = 1
		System.out.println("\n" + "CONSTRUCT QUERY TREE FROM TEXTUAL INPUT FORM:");
		QueryNode root = null;
		try {
			LinkedHashMap<String, String> hashTable = utils.hashTextualQuery(textualQueryFile);
			utils.printSerializedTree(hashTable); //print hashed query
			root = utils.buildQueryTree(hashTable, "1");   //start from query node 1
		} catch (Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
			return;
		}
		
		//print contents of query tree
		System.out.println("\n" + "NODES IN  QUERY TREE");
		System.out.println(utils.printQueryTree(root));
		
		Digraph g = new Digraph();
		try {
			utils.buildDigraph(root, g, null);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
			return;
		}
		
		//print digraph before construction of queryPlan
		System.out.println("\n" + "POPULATED DIGRAPH");
		System.out.println("\n" + g.toString());
		
		try {
			utils.buildInitialQueryPlan(g, composeFileName, textualQueryFile, version, log_level);
			
			//TESTING DynamicIntraOps
		/*	utils.buildDynamicIntrOperatorQueryPlan(g, "composeIntraOpFileNamePath", "C3", 8, version, "verbose");	
			String execCmd = "docker stack deploy --compose-file " + "composeIntraOpFileNamePath" + " query";
			ProcessBuilder pb = new ProcessBuilder("/bin/bash","-c", execCmd);
			pb.inheritIO();
			pb.directory(null);
			Process process = pb.start();
			int exitCode = process.waitFor();   //wait for process to execute cmd and terminate
			System.out.println("Exit code for execCmd process " + exitCode);
		*/
			//print di-graph after construction of queryPlan
			HashSet<Container> visited = new HashSet<Container>();
			String rootId = g.getRoot().id;
			System.out.println("\n" + "POPULATED DIGRAPH printed by IN-ORDER TRAVERSAL");
			utils.printDigraphInOrder(g.getContainerByID(rootId) , g, visited);
		
		} catch (Exception e) {
			System.out.println(e.getMessage());
			System.out.println("PRINTING DIGRAPH WITH NO SINGLE ROOT NODE TOP-DOWN");
			utils.printDigraphTopDown(g);
		}
		
		//print digraph after construction of queryPlan
		//System.out.println("\n" + "Print DIGRAPH after MODIFIED by INTRA-OP containers");
		//System.out.println("\n" + g.toString());
		
	} // end main	

		
	/*
	 * Recursively builds the query tree using the hashTable which contains the
	 * serialized representation of the nodes in the tree as specified in the preface
	 * to this class.
	 * 
	 * Returns the apex node of a subtree.
	 */
	public QueryNode buildQueryTree(LinkedHashMap<String, String> hashTable, String nodeKey) throws QueryException, Exception {
		
		//construct new node
		String textNode = hashTable.get(nodeKey);
		Master.logQueryState("buildQueryTree, textNode: " + textNode + "\n", Log_Level.verbose);
		QueryNode node = parseTextNode(textNode);
	
		if(node instanceof QueryComputeNode) {
			
			QueryComputeNode computeNode = (QueryComputeNode)node;
			String probeId = computeNode.getProbeId();
			
			QueryNode leftNode = null;
			
			//LEFT AND RIGHT are set in recursive calls to buildQueryTree()
			String leftKey = computeNode.getLeftId();
			if(leftKey.matches("\\d+")) {
				leftNode = buildQueryTree(hashTable, leftKey);  //recurse
				computeNode.setLeftChild(leftNode); //set left child
				if( leftNode instanceof QueryComputeNode && probeId.equals(leftNode.getId()) )
					((QueryComputeNode)leftNode).setProbe(true);  //this child is a Probe partition case
			} 
			
			String rightKey =  computeNode.getRightId();
			if(rightKey.matches("\\d+")) {
				
				QueryNode rightNode = buildQueryTree(hashTable, rightKey);  //recurse
				computeNode.setRightChild(rightNode); //set right child
				if( rightNode instanceof QueryComputeNode && probeId.equals(rightNode.getId()) )
					((QueryComputeNode)rightNode).setProbe(true);  //this child is a Probe partition case

				if(leftNode instanceof QueryComputeNode && rightNode instanceof QueryComputeNode) {
					if ( !((QueryComputeNode)leftNode).isProbe()  && !((QueryComputeNode)rightNode).isProbe() ) {
							Master.logQueryState("buildQueryTree: left and right Query nodes are BOTH 'PROBE'. leftNode: " +
									leftNode.getId() + ", rightNode: " + rightNode.getId(), Log_Level.quiet);
							throw new Exception("left and right Query nodes are BOTH 'PROBE'");
					}
					
				}
			}
			
		}  //if QueryComputeNode
		
		//else RECURSIVE TERMINAL CONDITION:  return a data-only node
		return node;
		
	} // end method buildQueryTree
	
	/* INCOMPLETE: USE  method estimateRowsInResultSet() 
	 * Estimate the DoP based upon the size of a ResultSet, rs, for a 
	 * relation R using a small representative sample of R, s.
	 * The sample s executes the query to obtain sample result set
	 * r'.  The query size of the entire relation R is estimated as
	 * 					 |rs| = |R|/|s| * |rs'|   
	 */
	public long estimateDoP(int sampleSize, String query, 
			Container cc, String R, Statement statementSQL)
			throws SQLException {
		
		long count = 0;
		ResultSet countRS = null;
		try {
			// get row size of R with  COUNT(*) 
			//SELECT COUNT(*) AS ResultSetSize FROM ( <YOUR_ORIGINAL QUERY_STRING>)
			String queryGetCount = "SELECT COUNT(*) AS count FROM " + R;
			Worker.logQueryState( "getQuerySize::  container " + cc.getId() + 
					", attempting  query " + queryGetCount,  Log_Level.verbose );
			statementSQL.setQueryTimeout(60);
			countRS = statementSQL.executeQuery(queryGetCount);
			if(countRS.next()) {
				count = countRS.getLong(1);
			}
			else  
				Worker.logQueryState("Container " + cc.getId() + 
						", " + queryGetCount + " size COUNT(*) FAILED",
						Log_Level.quiet);
		} 
		finally {
			
			try {
				countRS.close();
				statementSQL.close();
			} catch (SQLException e) {
				Worker.logQueryState(e.getMessage(), Log_Level.quiet);
				e.printStackTrace();
			}
			
		}

		return count;

	} // end method
	
	 /* Estimates the size of row in bytes of a ResultSet based upon ResultSetMetaData info, 
     * which provides the size of field sizes in bytes using a field's data type.
     * 
     * Get the designated column's specified column size. 
     * For numeric data, this is the maximum precision. 
     * For character data, this is the length in characters. 
     * For datetime datatypes, this is the length in characters of the String representation
     *  (assuming the maximum allowed precision of the fractional seconds component). 
     * For binary data, this is the length in bytes. For the ROWID datatype, this is the length in bytes.
     * 0 is returned for data types where the column size is not applicable.
     */
    public int estimateRowSizeInBytes(ResultSet rs) throws SQLException {
    	
    	 // get data base metadata
        ResultSetMetaData metaData = rs.getMetaData();
        int colCount = metaData.getColumnCount();
        /*
        String dataTypeName = "";
        String typeName = "";
        
        while(typesRS.next()) {
        	typeName = typesRS.getString(1);
        	dataTypeName = typesRS.getString(2);
        	System.out.println(typeName + ", " + dataTypeName);
        }
        */
        
        int bytes  = 0;
        String colName = "";
        String typeName = "";
        String tableName = "";
        
        int colSize = 0;  
        /* Get the designated column's specified column size. 
         * For numeric data, this is the maximum precision. 
         * For character data, this is the length in characters. 
         * For datetime datatypes, this is the length in characters of the String representation 
         * 	(assuming the maximum allowed precision of the fractional seconds component). 
         * For binary data, this is the length in bytes. 
         * For the ROWID datatype, this is the length in bytes. 
         * 0 is returned for data types where the column size is not applicable.
         */
       
     		
        //int typeName = 0;
        //int size = 0;  //column size, when applicable
        //int dataType = 0;

        // get columns
      
        for (int column = 1; column <= colCount; column++) {
        	
        	tableName = metaData.getTableName(column);
        	colName = metaData.getColumnName(column);
        	typeName = metaData.getColumnTypeName(column);
        	colSize = metaData.getPrecision(column);
        	
        	System.out.println("Table Name " + tableName + ", Column Name " + colName + ", Type Name " + typeName + ", colSize " + colSize);
        	
        	  /*
            System.out.println("col. name: " + cols.getString(4) 
            		+ ", schema: " + cols.getString(2)
            		+ ", data type: " + cols.getString("DATA_TYPE")
            		+ ", type name: " + cols.getString(6) 
            		+ ", size: " + cols.getString(7)
            		+ ", sql data type: " + cols.getString(14));
            		//+ ", data type: " + cols.getInt(5)
            		//+ ", type name: " + cols.getString(6) 
            		//+ ", size: " + cols.getInt(7));
        	   */
        	
            //typeName = cols.getString("TYPE_NAME");
            //calculate total bytes needed to store row
            if (typeName.contains("VARCHAR")) {
            	//i = typeName.indexOf('(');
            	//j = typeName.indexOf(')');
            	//size = typeName.substring(i + 1, j);
            	//length = Integer.valueOf(size);
            	bytes += colSize * 2;
            } else if (typeName.contains("TEXT")) {
            	bytes += 4;  		//2 chars
            } else if (typeName.contains("INT")) {
            	bytes += 4;      //4 byte integer
            }  
            else if (typeName.contains("DOUBLE")) {
            	bytes += 8;      //8 bytes double
            } else if (typeName.equals("DATE")) {
            	bytes += 10 * 2;    //# of chars to represent date format
            } else if (typeName.equals("TIME")) {
                bytes += 8 * 2;     //# of chars to represent time format
            }  else {
            	System.out.println("UNKNOWN TYPE: " + typeName);
            }
          
            		
            /*		SQLite does  not return correct java.sql.type code
            dataType = cols.getInt(5);
            //calculate total bytes needed to store row
            if (dataType == 1) {			//CHAR
            	size = cols.getInt(7);
            	bytes += size;
            } else if (dataType == 91) {	//VARCHAR
            	size = cols.getInt(7);
            	bytes += size;
            } else if (dataType == 4) { 	//INTEGER
            	bytes += 4;
            } else if (dataType == 8) { 	//DOUBLE
            	bytes += 8;
            } else if (dataType == 12) {	// DATE
            	bytes += 10;
            } else if (dataType == 92) {	//TIME
            	bytes += 8;
            } else {
            	System.out.println("UNKNOWN TYPE: " + typeName);
            }
            */
            
        } // for
        
        System.out.println("total bytes needed to store row: " + bytes);
        
        return bytes;
    	
    } //method
	
    
    /* Predicts the query's total # of rows that will be returned in a ResultSet.
     * 
     * 	|rs|       |R|                        |R|  
     * -----  =   -----  =====>    |rs|  =   -----  * |rs'|
     *  |rs'|      |s|						  |s|
     *  
     *  R is the relation, s is a subset sample of R, 
     *  rs is the ResultSet of query over R, rs' is the ResultSet of query over sample s, which is 
     *  calculated as a % of the size of R. 
     *  
     *  RETURNS: estimates[0] estimated time in seconds for full query 
	 *			 estimates[1] estimated # of rows in query's ResultSet
	 *			
	 *	 0 <= sampleSize <= 1, e.g. '.01', one-hundredth the size of the relation R 		 
     */
    public static int[] estimateRowsInResultSet(String query, double sampleSize, java.sql.Connection conn) throws SQLException {
    	
    	//inner class to report long running query that eats up Java Heap
    	/*
    	Thread longRunning = new  Thread() {
    				
    		public void run() {
    					
    			long heapSize = Runtime.getRuntime().totalMemory();
    			System.out.println("numberOfRowsScannedEstimator() starts with initial heapSize " + heapSize);
    					
    			while(true) {
    				heapSize = Runtime.getRuntime().totalMemory();
    				long heapFreeSize = Runtime.getRuntime().freeMemory();
    						
    				if (heapFreeSize / (double)heapSize < .05)
    					System.out.println("Only " + heapFreeSize + " remaining");
    						
    				try {
    						sleep(2000);
    					} catch (InterruptedException e) {
    							e.printStackTrace();
    					}
    						
    			 } // while
    					
    		} // end run() method
    				
    	};

    	longRunning.start();
		*/
    	
    	//parse 1st table out of FROM clause
    	int FROM_Idx = query.indexOf("FROM");
    	String afterFROM = query.substring(FROM_Idx + 4).trim();  //from FROM clause to end of query
    	//boolean b = Pattern.matches("\\s+[a-zA-Z]+.*", afterFROM);
    	//boolean b = Pattern.matches("\\w+\\b.*", afterFROM);
    	Pattern pattern = Pattern.compile("\\s\\w+\\s|,");
    	//Pattern pattern = Pattern.compile("\\s+[a-zA-Z]+.*");
    	String[] matched = pattern.split(afterFROM, 2);
    	String R = matched[0];    //assign to R the matched table name from query
    	
    	//get size of 1st table
		Statement statement = conn.createStatement();
		statement.setQueryTimeout(0);
		statement.setMaxRows(0);
		ResultSet rs =  statement.executeQuery("SELECT COUNT(*) FROM " + R);
		double sizeOfR = rs.getDouble(1);
		System.out.println("Size of Relation " + R + ": " + sizeOfR);
		int s = (int)(sampleSize * sizeOfR);  //sample size of Relation size
		System.out.println("Sample size s of Relation " + R + ": " + s + 
						", set to approx " + sampleSize + " size of " + R);
		rs.close();
		String sampleQuery = "";
	
		//CREATE SAMPLE temporary table as sample size 's' rows of table R
		/*
		DatabaseMetaData meta = conn.getMetaData(); 
		rs = meta.getTables(null, null, "temp", null); 
		if(!rs.next()){ 
			//table does not exist. 
			String createDDL = "CREATE TABLE temp AS SELECT * FROM " + R + " LIMIT " + s;
			statement.executeUpdate(createDDL);
			//statement.executeUpdate("COMMIT");
			//check that temp table has rows
			rs = statement.executeQuery("SELECT COUNT(*) FROM temp");
			System.out.println("COUNT(*) for temp: " + rs.getInt(1));
			rs.close();
		} 
			
		//execute query of on sample size 's' rows of temporary table
		sampleQuery = "SELECT COUNT(*) FROM " + afterFROM.replaceAll(R, "temp");
	    */

		if(query.contains("WHERE"))
			if(query.contains("ORDER")) {
				//locate ORDER keyword
				int ORDER_Idx = afterFROM.indexOf("ORDER");
				String ORDERclause = afterFROM.substring(ORDER_Idx);
				afterFROM = afterFROM.substring(0, ORDER_Idx);
				sampleQuery = "SELECT COUNT(*) FROM " + afterFROM + " AND " +  R + ".rowid < " + s + " " + ORDERclause;
				
			}
			else	
				sampleQuery = "SELECT COUNT(*) FROM " + afterFROM + " AND " +  R + ".rowid < " + s;
		else
			sampleQuery = "SELECT COUNT(*) FROM " + afterFROM + " WHERE " +  R + ".rowid < " + s;
	   		
		long startMilliseconds = System.currentTimeMillis();
		rs =  statement.executeQuery(sampleQuery);
		long endMilliseconds = System.currentTimeMillis();
		long sampleTime = (endMilliseconds - startMilliseconds);
		System.out.println("Time to obtain ResultSet  rs': " 
				+ (sampleTime)/1000 + " seconds");
		
		int rsPrime = rs.getInt(1) + 1;
		System.out.println("Size of ResultSet  rs': " + rsPrime);
		//estimated time to complete the original query 
		int estimatedTime = (int)(sampleTime/1000) * (int)(1 / sampleSize);  //seconds
		System.out.println("Estimated time to process full ResultSet: " + estimatedTime + " seconds");
		rs.close();
	
		//DROP temp table
		//statement.executeUpdate("DROP TABLE temp");
		//statement.executeUpdate("COMMIT");
		
		int estimatedSizeOfRS = (int)((sizeOfR / s) * rsPrime);
		System.out.println("Estimated size of ResultS  rs: " + estimatedSizeOfRS);
		
		// check accuracy of estimation
		/*
		startMilliseconds = System.currentTimeMillis();
		String originalQueryWithCountAdded = "SELECT COUNT(*) FROM " + afterFROM;
		rs = statement.executeQuery(originalQueryWithCountAdded);  //get size of rs
		endMilliseconds = System.currentTimeMillis();
		System.out.println("Check accuracy of estimation. Time to obtain full ResultSet  rs: " 
				+ (endMilliseconds - startMilliseconds)/1000 + " seconds");
		int RS_size = rs.getInt(1);
		System.out.println("Size of original query in rows: " + RS_size);
		rs.close();
		*/
		
		int[] estimates = new int[3];
		estimates[0] = estimatedTime; 		//estimates[0] estimated time in seconds for full query 
		estimates[1] = estimatedSizeOfRS; 	//value[1] estimated # of rows in query's ResultSet
		estimates[2] = (int)Math.ceil(sampleTime / 1000.0); //sample time rounded up to nearest second
		
    	return estimates;
    }
    
	 /* Estimates the size of row in bytes of a table based upon DatabaseMetaData info, 
     * which provides the size of field sizes in bytes using a field's data type.
     * 
     *   INTEGER takes 4 bytes
     *   DOUBLE takes 8 bytes
     *   CHAR takes 2 bytes per char(UTF8 encoding), given by Col. SIZE
     *   VARCHAR takes 2 bytes per char(UTF8 encoding), given by its Col. SIZE
     *   TIME length of string representation, given by Col. SIZE, usually 8
     *   DATE length of string mm/dd/yyyy, given by Col. SIZE, usually 10
     */
    public int estimateRowSizeInBytes(String tableName, java.sql.Connection conn) throws SQLException {
    	
    	 // get data base metadata
        DatabaseMetaData metaData = conn.getMetaData();
        //ResultSet typesRS = metaData.getTypeInfo();
        /*
        String dataTypeName = "";
        String typeName = "";
        while(typesRS.next()) {
        	typeName = typesRS.getString(1);
        	dataTypeName = typesRS.getString(2);
        	System.out.println(typeName + ", " + dataTypeName);
        }
        */
        int bytes  = 0;

        int length = 0;
        String typeName = "";
        int i = 0;  // first index
        int j = 0; //  second index
        String size = "";		
        //int typeName = 0;
        //int size = 0;  //column size, when applicable
        //int dataType = 0;

        // get columns
        ResultSet cols = metaData.getColumns(null, "main", tableName, "%");
        while (cols.next()) {
            // 1: catalog
            // 2: schema
            // 3: table name, java.sql.types which is numeric code. See code values.
        	//		INTEGER: 5, VARCHAR: 12, CHAR: 1, DOUBLE: 8, DATE: 91, TIME: 92
            // 4: column name
            // 5: data type (CHAR, VARCHAR, TIMESTAMP, ...)
            // 6: type name 
        	// 7: column size
            System.out.println("col. name: " + cols.getString(4) 
            		+ ", schema: " + cols.getString(2)
            		+ ", data type: " + cols.getString("DATA_TYPE")
            		+ ", type name: " + cols.getString(6) 
            		+ ", size: " + cols.getString(7)
            		+ ", sql data type: " + cols.getString(14));
            		//+ ", data type: " + cols.getInt(5)
            		//+ ", type name: " + cols.getString(6) 
            		//+ ", size: " + cols.getInt(7));
            
            typeName = cols.getString("TYPE_NAME");
            //calculate total bytes needed to store row
            if (typeName.contains("VARCHAR")) {
            	i = typeName.indexOf('(');
            	j = typeName.indexOf(')');
            	size = typeName.substring(i + 1, j);
            	length = Integer.valueOf(size);
            	bytes += length * 2;
            } else if (typeName.contains("TEXT")) {
            	bytes += 4;  		//2 chars
            } else if (typeName.contains("INT")) {
            	bytes += 4;      //4 byte integer
            }  
            else if (typeName.contains("DOUBLE")) {
            	bytes += 8;      //8 bytes double
            } else if (typeName.equals("DATE")) {
            	bytes += 10 * 2;    //# of chars to represent date format
            } else if (typeName.equals("TIME")) {
                bytes += 8 * 2;     //# of chars to represent time format
            }  else {
            	System.out.println("UNKNOWN TYPE: " + typeName);
            }

            		
            /*		SQLite does  not return correct java.sql.type code
            dataType = cols.getInt(5);
            //calculate total bytes needed to store row
            if (dataType == 1) {			//CHAR
            	size = cols.getInt(7);
            	bytes += size;
            } else if (dataType == 91) {	//VARCHAR
            	size = cols.getInt(7);
            	bytes += size;
            } else if (dataType == 4) { 	//INTEGER
            	bytes += 4;
            } else if (dataType == 8) { 	//DOUBLE
            	bytes += 8;
            } else if (dataType == 12) {	// DATE
            	bytes += 10;
            } else if (dataType == 92) {	//TIME
            	bytes += 8;
            } else {
            	System.out.println("UNKNOWN TYPE: " + typeName);
            }
            */
            
        }
        cols.close();
        //System.out.println("total bytes needed to store row: " + bytes);
        
        return bytes;
    	
    } //method
	
	/*
	 * Examines digraph to build a dynamic parallel operator query plan.
	 * Modifies the digraph for an slow operator(container) that needs
	 * to be partitioned among multiple intra-ops dynamically.
	 * Writes plan to a YAML file, e.g. "compose.yaml", which is executed by 
	 * Docker Compose orchestration tool. Returns the plan in a "big String" 
	 * for possible logging. Plan will be passed thru to Master's VM to be 
	 * executed by the VM's Docker-Compose tool.
	 * 
	 * Builds a Compose.yml configured for version "2.1" or "3.3" depending on value of parameter 'version'
	 */
	 public String buildDynamicIntrOperatorQueryPlan(Digraph g, String composeIntraOpFileNamePath, String ccId, int NoDynamicIntaOps,
			 											String version, String log_level) 	throws Exception {
		 
		//get ENV VARS
		workerImage = System.getenv("WORKER_IMAGE_NAME");
		rabbitImage = System.getenv("RABBIT_IMAGE_NAME");
		masterImage = System.getenv("MASTER_IMAGE_NAME");
			
		
		Master.logQueryState("buildDynamicIntrOperatorQueryPlan: ccId=" + ccId + ", NoDynamicIntaOps=" + NoDynamicIntaOps, Log_Level.quiet);
		String composeString = "";
		BufferedWriter writer = null;
		
		try {
			
				      writer = new BufferedWriter(new FileWriter(composeIntraOpFileNamePath)); //compose YAML file
			
			          writer.write("#docker-compose -f " + composeIntraOpFileNamePath +  " --project-name ContainerizedQuery  up -d\n");
			          writer.write("#docker stack deploy --compose-file " + composeIntraOpFileNamePath  + " query\n");
			          
			          writer.write("\nversion: '" + version + "'\n");
		              writer.newLine();
		              writer.write("services:\n");
		              writer.newLine();
		              writer.newLine();
		    
			
			/*****************************write DYNAMIC INTRA-OPERATOR COMPUTE CONTAINERS ********************************/
			/*  Modifies the digraph for an slow operator in container with containerID that needs
			 * to be partitioned among multiple intra-ops dynamically. This entails making copies of 
			 * container with containerID for each new intra-op. This requires adjusting the input and output
			 * edges of each intra-op and may require adjusting the type of Exchange type for the original container
			 * and setting the appropriate Exchange type for the new intra-op containers. The original data-sets relation
			 * sent to the original container must be distributed to the new intra-op containers.
			 * Make copy of LinkedHashSet to avoid ConcurrentModificationException with addition of new vertices 
			 * by call to createParallelOperators(...) below. May results in changes to LinkedHashSet iterator */
		      
		       /*
		      for(ComputeContainer cc :intraOpContainers) {
					    	
					    	boolean isLeaf = g.getLeafComputeVertices().contains(ccId);
					    	int degrees = cc.getDegreesParallel();
						    
						    // add intra-operator containers, when degrees parallel > 1
						    if(degrees == 1 && !isLeaf) {
						    	
						    	if(g.getParents(cc).size() > 1) 
						    		cc.setParallelEX(new EvenPartitionExchange()); //reset vertex default exchange
						    		
						    	writeComputeContainer(g, cc, writer, version, log_level);
						    }
						    else if(degrees == 1 && isLeaf) {
						    		
						    	writeComputeContainer(g, cc, writer, version, log_level);
						    }
						    else if( isLeaf && degrees > 1) { //degrees > 1
							    createLeafParallelOperators(cc, g, writer, version, log_level);  //leaf operators
							    writeComputeContainer(g, cc, writer, version, log_level);  // write modified cc too.
						    } 
						    else { //degrees > 1  && non-leaf
						    	createParallelOperators(cc, g, writer, version, log_level);  //non leaf operators
						    	writeComputeContainer(g, cc, writer, version, log_level);  // write modified cc too.
						    }
						    
		        } // end for  COMPUTER CONTAINERS 
		        */       
		              
		        ComputeContainer cc =    (ComputeContainer)g.getContainerByID(ccId);
		        ArrayList<ComputeContainer> intraOpContainers = g.addDynamicIntraOperators(cc, NoDynamicIntaOps);
		        
		        for(int i = 0; i < NoDynamicIntaOps; i++) {  //write out
		        	 
		        	writeComputeContainer(g, intraOpContainers.get(i), writer, version, log_level);
		        }
		                
		         
			  //NETWORK section
			    if(version.equals("3.3")) {
		              writer.write("networks:\n");
		              writer.write("   default:\n");
		              writer.write("      driver: overlay\n");
		              writer.write("      ipam: \n");
		              writer.write("         driver: default\n");
		              writer.write("         config: \n");
		              writer.write("            - subnet: 10.0.0.0/23\n");
			    }
			    else {
			    	 writer.write("networks:\n");
		             writer.write("   default:\n");
		             writer.write("      driver: overlay\n");
			    }
			    
			  writer.flush();  
			  writer.close();  
			  
			  composeString = FileUtils.readFileToString(new File(composeIntraOpFileNamePath), null);
			
		} catch (Exception e) {
			Master.logQueryState("buildDynamicIntrOperatorQueryPlan: " + e.getLocalizedMessage(), Log_Level.quiet);
			e.printStackTrace();
			throw e;
		} 
		
		return composeString;
		
	} //end method
	
	/*
	 * Examines digraph to build initial containerized query plan.
	 * Writes plan to YAML file, "compose.yaml", which is executed by 
	 * Docker Compose orchestration tool. Returns the plan in a "big String" 
	 * for possible logging.
	 * 
	 * Builds a Compose.yml configured for version "2.1" or "3.3" depending on value of parameter 'version'
	 */
	 public String buildInitialQueryPlan(Digraph g, String composeFileName, String textualQueryFile, String version,
											String log_level) throws Exception {
		 
		//get ENV VARS
		workerImage = System.getenv("WORKER_IMAGE_NAME");
		rabbitImage = System.getenv("RABBIT_IMAGE_NAME");
		masterImage = System.getenv("MASTER_IMAGE_NAME");
			
		String composeString = "";
		try {
			
				BufferedWriter writer = new BufferedWriter(new FileWriter(composeFileName)); //compose YAML file
			
			          writer.write("#docker-compose -f " + composeFileName +  " --project-name ContainerizedQuery  up -d\n");
			          writer.write("#docker stack deploy --compose-file " + composeFileName  + " query\n");
			          
			          writer.write("\nversion: '" + version + "'\n");
		              writer.newLine();
		              writer.write("services:\n");
		              writer.newLine();
		    	    
		    /******************************** write rabbitMQ CONTAINER **************************************/
		 	          writer.write("  rabbitMQ:\n");
		              //writer.write("    image: rabbitmq:3.6.12-management-alpine\n");
		              //writer.write("    image: rabbitmq:3.7.3-management\n");
		 	          writer.write("    image: " + rabbitImage + "\n");
 			          writer.write("    ports:\n");
 			          writer.write("     # allow access to rabbitmq monitor using HTTP over port 8080\n");
			          writer.write("     - 80:15672\n");
			          
			          if(version.equals("3.3")) {
			          writer.write("    deploy:\n");
			          writer.write("       replicas: 1\n");
			          writer.write("       restart_policy:\n");
			          writer.write("          condition: on-failure\n");
			        //writer.write("       resources:\n");
			        //writer.write("          limits:\n");
			        //writer.write("             memory: 4096M\n");
			          writer.write("       placement:\n");
			          writer.write("          constraints:\n");
			          writer.write("             - node.hostname == swarm-rabbit\n");
					  }
	      
			    if(version.equals("3.3")) {
	                  writer.write("    networks:\n");
			          writer.write("       default:\n");
		 	          writer.write("          aliases:\n");
			          writer.write("             - \"rabbitMQ\"\n");
			    }
			    	
			        writer.write("    hostname: ${HOSTNAME}\n");          
			
			    if(version.equals("2.1")) {
			          writer.write("    container_name: rabbitMQ\n");
			    
			    	  writer.write("    expose:\n");
			    	  writer.write("     # rabbitmq server port for communication with clients, supports TLS\n");
			    	  writer.write("     - 5672\n");
			          writer.write("    restart: always\n");
				}
			          writer.write("    stdin_open: true\n");
			          writer.write("    tty: true\n");
			          writer.newLine();
	
		   
		    /******************************** write DATA_ONLY CONTAINERS *****************************/
			    //first iterate thru all data-only container nodes, building an entry for each in the YAML file
			    if(version.equals("2.1")) {
				
			      for(Container v : g.getVertices()) {

				    if(v.type == ContainerType.DATA_ONLY) {	
				      DataOnlyContainer dc = (DataOnlyContainer)v;
				      writer.write("  " + dc.id + ":\n");
				      writer.write("    image: busybox:latest\n");
				      writer.write("    volumes:\n");
				      writer.write("     - " + dc.getHostVolume() + ":" + dc.getContainerVolume() + "\n");
				      writer.write("    environment:\n");
				      writer.write("      COMPOSE_PROJECT_NAME: ContainerizedQuery\n");
				      writer.write("      DATABASE: " + dc.getDataBaseFile() + "\n");
				      writer.write("      TABLE: " + dc.getTable() + "\n");
				      
				      String partitions =  Arrays.deepToString(dc.getPartitions());
				      if(partitions.equals("null"))
				      writer.write("      PARTITIONS: " + null + "\n");
				      else
				      writer.write("      PARTITIONS: \"" + partitions + "\"\n");
				      
				      String schema = dc.getSchema().toString();  //remove last char which is a "\n" newline
				      writer.write("      SCHEMA: \"" + schema.substring(0, schema.length() -1)  + "\"\n");
				      
				      writer.write("    container_name: " + dc.id + "\n");
				      writer.write("    restart: unless-stopped\n");
				      writer.newLine();	
				    } // end if

			      } // end for
				
			    } // end if
			
			/******************************** write Master CONTAINER **************************************/
			    DataOnlyContainer dc =  g.getDataOnlyVertices().get(0);  //hostVolume for finding config files
			    
			 	      writer.write("  Master:\n");
			          writer.write("    image: deepeddy/mysqlite:" + masterImage + "\n");
    			      writer.write("    volumes:\n");
    			      writer.write("       - " + dc.getHostVolume() + ":" + dc.getContainerVolume() + "\n");
    			      writer.write("       - /usr/bin/docker:/usr/bin/docker\n");  //bind to docker command and socket in host
    			      writer.write("       - /var/run/docker.sock:/var/run/docker.sock\n"); //allows Master to run docker cmds
			    
			    if(version.equals("3.3")) {
    			      writer.write("    deploy:\n");
			          writer.write("       replicas: 1\n");
			          writer.write("       restart_policy:\n");
			          writer.write("          condition: on-failure\n");
			        //writer.write("       resources:\n");
			        //writer.write("          limits:\n");
			        //writer.write("             memory: 4096M\n");
			          writer.write("       placement:\n");
			          writer.write("          constraints:\n");
			          writer.write("             - node.hostname == swarmmaster\n");
			    }
			          
			    if(version.equals("2.1")) {
     			      writer.write("    expose:\n");
     			      writer.write("     # worker server socket listening for input from upstream  container\n"); 
				      writer.write("     - 4325\n");
			    }
				
				if(version.equals("3.3")) {
			          writer.write("    networks:\n");
			          writer.write("       default:\n");
			          writer.write("          aliases:\n");
				      writer.write("             - \"Master\"\n");
				}
				
				      writer.write("    environment:\n");
				      writer.write("      DATA_PORT: 4325\n");
				      writer.write("      MASTER_IMAGE_NAME: " + masterImage + "\n");
				      writer.write("      WORKER_IMAGE_NAME: " + workerImage + "\n");
				      writer.write("      RABBIT_IMAGE_NAME: " + rabbitImage + "\n");
				      writer.write("      RabbitIP: rabbitMQ\n");
				      //writer.write("      NO_PARTITIONS:     \n");   //must be edited by user
				      writer.write("      TEXTUAL_QUERY_FILE: " + dc.getContainerVolume() + "/" + textualQueryFile + "\n");
				      writer.write("      COMPOSE_FILE_NAME: " +  dc.getContainerVolume() + "/" + composeFileName + ".generated\n");
				      writer.write("      CP: .:commons-io-1.2.jar:commons-cli-1.1.jar:rabbitmq-client.jar:sqlite-jdbc-3.19.3.jar:com.sun.rowset.jar\n");
				      writer.write("      JVM_OPTS: '-d64 -server -XX:+UseG1GC -verbose:gc -Xms2048M'\n");
				      writer.write("      COMPOSE_PROJECT_NAME: ContainerizedQuery\n");
				      writer.write("      COMPOSE_VERSION: " + version + "\n");
				      
				      
				if(version.equals("2.1")) {
				      writer.write("    depends_on:\n");
				      writer.write("      - rabbitMQ\n");
				}
				      
    			      writer.write("    working_dir: /usr/src/containerizedQuerySources/\n");
    			
    			/*  				
      				  writer.write("    command: bash -c \"/etc/init.d/rsyslog restart; java -cp $${CP} $${JVM_OPTS} containerizedQuery/Master " 
      					+ "$${SERIAL_FILE} " + "$${RabbitIP} " + composeFileName + " " + version + ";  /bin/bash -i \"\n");
      			*/
    			
      			      writer.write("    command: bash -c \" java -cp $${CP} $${JVM_OPTS} containerizedQuery/Master " 
      					+ "$${TEXTUAL_QUERY_FILE} " + "$${RabbitIP} " + " $${COMPOSE_FILE_NAME} " + dc.getContainerVolume()
      					+ " " + version + " " + log_level + ";  /bin/bash -i \" \n");
      			
      			if(version.equals("2.1"))
				      writer.write("    container_name: Master\n");
      			
				      writer.write("    stdin_open: true\n");
				      writer.write("    tty: true\n");
				      writer.newLine();	         
    			
				
			/*****************************write COMPUTE CONTAINERS ********************************/
			/* Then iterate thru all COMPUTE CONTAINERS, building an entry for each in the YAML file.
			 * make copy of LinkedHashSet to avoid ConcurrentModificationException with addition of new vertices 
			 * by call to createIntraOperators(...) below. May results in changes to LinkedHashSet iterator */
			    LinkedHashSet<ComputeContainer> vertices = new LinkedHashSet<ComputeContainer>( g.getComputeVertices());
			    
			    for(ComputeContainer cc : vertices) {   //adjust containers with intra-ops
			    	
			    	boolean isLeaf = g.getLeafComputeVertices().contains(cc);
			    	int degrees = cc.getDegreesParallel();
				    
				    // add intra-operator containers, when degrees parallel > 1
				    if(degrees == 1 && !isLeaf) {
				    	
				    	if(g.getParents(cc).size() > 1)	
							cc.setParallelEX(new EvenPartitionExchange()); //reset vertex default exchange
				    		
				    	//writeComputeContainer(g, cc, writer, version, log_level);
				    }
				    else if(degrees == 1 && isLeaf) {
				    		
				    	//writeComputeContainer(g, cc, writer, version, log_level);
				    }
				    else if( isLeaf && degrees > 1) { //degrees > 1
				    	g.addLeafIntraOperators(cc);
					    //createLeafParallelOperators(cc, g, writer, version, log_level);  //leaf operators
					    //writeComputeContainer(g, cc, writer, version, log_level);  // write modified cc too.
				    } 
				    else { //degrees > 1  && non-leaf
				    	g.addIntraOperators(cc);
				    	//createParallelOperators(cc, g, writer, version, log_level);  //non leaf operators
				    	//writeComputeContainer(g, cc, writer, version, log_level);  // write modified cc too.
				    }
				   
			    } // end for  COMPUTER CONTAINERS
			    
			    TreeSet<ComputeContainer> sortedVertices = new TreeSet<ComputeContainer>(new ContainerComp());
			    sortedVertices.addAll(g.getComputeVertices());
			    //Iterator<Container> iterator = sortedVertices.descendingIterator();
			    //vertices = new LinkedHashSet<ComputeContainer>( g.getComputeVertices());
			    //for(ComputeContainer cc : vertices)  //now write the containers
			    for(ComputeContainer cc : sortedVertices)  //now write the containers
			    	writeComputeContainer(g, cc, writer, version, log_level);  // write modified cc too.
			    
			    /*
			    for(ComputeContainer cc : vertices)  //now write the containers
			    	writeComputeContainer(g, cc, writer, version, log_level);  // write modified cc too.
			    */
			
			  //NETWORK section
			    if(version.equals("3.3")) {
		              writer.write("networks:\n");
		              writer.write("   default:\n");
		              writer.write("      driver: overlay\n");
		              writer.write("      ipam: \n");
		              writer.write("         driver: default\n");
		              writer.write("         config: \n");
		              writer.write("            - subnet: 10.0.0.0/23\n");
			    }
			    else {
			    	 writer.write("networks:\n");
		             writer.write("   default:\n");
		             writer.write("      driver: overlay\n");
			    }
			    
			
			//NETWORK section
			//writer.write("#networks:\n");
			//writer.write("# rabbit:\n");
			//writer.write("#   driver: bridge\n");
	
			//VOLUMES section
			//writer.write("#volumes:\n");
			//writer.write("# /join_tables: {}\n");
			//writer.write("# /shuffle_tables\n");
			//writer.write("# file://localhost/home/david/databases/shuffle_tables:\n");
			//writer.write("# DC-4:\n");
			//writer.write("#  database:\n");
			//writer.write("#   - file{{colon}}//localhost/${HOME}/databases/shuffle_tables:rw\n");
			    
			  writer.flush();  
			  writer.close();  
			  composeString = FileUtils.readFileToString(new File(composeFileName), null);
			 
			
		} catch (Exception e) {
			Master.logQueryState("buildInitialQueryPlan: " + e.getLocalizedMessage(), Log_Level.quiet);
			e.printStackTrace();
			throw e;
		} 
		
		return composeString;
		
	} //end method
	
	private void writeComputeContainer(Digraph g, ComputeContainer cc, BufferedWriter writer, String version, String log_level)
										throws Exception {
		String dbURL = "jdbc:sqlite:";   //set to protocol of database being used, that is JDBC compatible
		//String pragmas = ";Journal Mode=Off;Synchronous=Off;"; //specific for SQLite
		
		try {
				      writer.write("  " + cc.id + ":\n");
				      writer.write("    image : deepeddy/mysqlite:" + workerImage + "\n");
					
				    //if(cc.id.equalsIgnoreCase("C1"))
				    //	workerType = "apex";
				    String workerType = "UNDEFINED";
				    workerType = cc.getType().toString();  //obtained from ContainerType
				    
				    DataOnlyContainer dc = null; 
				    //only for leaf reader 'select' compute containers
				    //if(g.getLeafComputeVertices().contains(cc)) {
				    if(cc.getType() == ContainerType.LEAF) { 
				       //node has in-bound edge with data relation container
				       dc = g.getAssociatedDataOnlyContainer(cc);
							
				      //if(version.equals("2.1") && dc.getPartitions() == null) {
				      if(version.equals("2.1")) {
				      writer.write("    volumes_from:\n");
			          writer.write("     - " +  dc.id + "\n");
			          	//dbURL += dc.getContainerVolume() + "/" + dc.getDataBaseFile();
				      }

				      //if(version.equals("3.3") || dc.getPartitions() != null) { 
				      if(version.equals("3.3")) { 
			          writer.write("    volumes:\n");
			          writer.write("       - " + dc.getHostVolume() + ":" + dc.getContainerVolume() + "\n");
				      }
				      
				      //TO-DO: MUST BE MODIFIED TO HANDLE MORE THAN ONE TABLE ???
				      if(cc.getInputTable(0).getTableName().contains("Part_"))  // partitioned table in separate .db file
				    	  dbURL =   "'" + dbURL + dc.getContainerVolume() + "/" + cc.getInputTable(0).getTableName() + ".db" + "'";
				      else
				    	  dbURL  = "'" + dbURL +  dc.getContainerVolume() + "/" + dc.getDataBaseFile() + "'";
			          //workerType = "leafSelect";
				      //cc.setType(ContainerType.LEAF);  //adjust container type:  LEAF
				      //workerType = cc.getType().toString();
				    }
				    else {   //not a leaf compute container
				      //all-in-memory database, Surrounded in single quotes for YAML problem with training ':'
				      //dbURL = "'" + dbURL + ":memory:" + pragmas + "'";  //all in-memory
				      //dbURL = "'" + dbURL + ":memory:'";  //all in-memory
				      dbURL = dbURL + "\'\'";		 //cached to memory, but temporary database with disk if needed
				      //workerType = "interiorCompute";
				    }			    
				    
				    if(version.equals("3.3")) {
					  writer.write("    deploy:\n");
				      writer.write("       replicas: 1\n");
				      writer.write("       restart_policy:\n");
				      writer.write("          condition: on-failure\n");
				    //writer.write("       resources:\n");
				    //writer.write("          limits:\n");
				    //writer.write("             memory: 4096M\n");
				    //writer.write("       placement:\n");
				    //writer.write("          constraints:\n");
				    //writer.write("             - node.hostname != swarm-rabbit\n");
					}
					
				    if(version.equals("2.1")) {
			       	  writer.write("    expose:\n");
				      writer.write("     # worker server socket listening for input from upstream  container\n"); 
				      writer.write("     - 4325\n");
				    }
					
				    if(version.equals("3.3")) {
				      writer.write("    networks:\n");
				      writer.write("       default:\n");
				      writer.write("          aliases:\n");
				      writer.write("             - \"" + cc.getId() + "\"\n");
				    }
					
				      writer.write("    environment:\n");
				      writer.write("      DB_URL: " + dbURL + "\n");
				      writer.write("      DATA_PORT: 4325\n");
				      writer.write("      RabbitIP: rabbitMQ\n");
				      writer.write("      DYNAMIC_INTRA_OPS_ALLOWED: 'false'\n");
				      writer.write("      NO_DYNAMIC_INTRA_OPS: 0\n");
				      writer.write("      TIME_OUT: 0\n");   //in seconds
				      writer.write("      PAGE_SIZE: NOMINAL\n");
				      writer.write("      CACHE_SIZE: NOMINAL\n");
				      //writer.write("      MAX_PAGE_COUNT: NOMINAL\n");
				      writer.write("      WORKER_TYPE: " + workerType + "\n");
				      //writer.write("      SWARM_CLUSTER_SIZE: 6\n");
				      //writer.write("      SAMPLE_SIZE: .01\n");   //in seconds
				      writer.write("      OPERATOR: " + cc.getOperator() + "\n");
				      writer.write("      PREDICATE: " + cc.getPredicate() + "\n");
				      writer.write("      QUERY: " + cc.getQuery() + "\n");
				      writer.write("      BATCH_SIZE: \"20000\"\n");
				      writer.write("      EXCHANGE: " +  cc.getParallelEX().toString() + "\n");
				      writer.write("      DOWN_STREAM: \"" + g.getDestinationIds(cc).toString() + "\"\n");
				      writer.write("      UP_STREAM: \"" + g.getInboundIds(cc).toString() + "\"\n"); //may be modified by upstream intra-ops
				      writer.write("      SIZE_INBOUND: \"" + g.getNumberInboundEdges(cc) + "\"\n");
				      writer.write("      CP: .:commons-io-1.2.jar:commons-cli-1.1.jar:rabbitmq-client.jar:sqlite-jdbc-3.19.3.jar:com.sun.rowset.jar\n");
				      
				    if(cc.id.contains("MERGE_SORT")) {
				      writer.write("      JVM_OPTS: '-d64 -server -XX:+UseG1GC -verbose:gc -Xms4096M'\n");
				      writer.write("      FILL_SIZE: 10000\n");
				      writer.write("      SLEEP_TIME: 500\n");
				    }
				    else
				      writer.write("      JVM_OPTS: '-d64 -server -XX:+UseG1GC -verbose:gc -Xms2048M'\n");
				    
				      writer.write("      COMPOSE_PROJECT_NAME: ContainerizedQuery\n");
					
				    if(version.equals("2.1")) {
				      writer.write("    depends_on:\n");
				      writer.write("      - Master\n");
				      
				      if(dc != null)
					  writer.write("      - " + dc.id+ "\n");   //data dependency
				      
				      ArrayList<Container> destinationContainers = g.getDestinationVertices(cc);
					  for(Container destination : destinationContainers) {
					  writer.write("      - " + destination.id+ "\n");  //immediate downstream dependency
					  } // end for
					  
				    }
					
				      writer.write("    working_dir: /usr/src/containerizedQuerySources/\n");
				      /*
				      writer.write("    command: bash -c \"/etc/init.d/rsyslog restart; "
	                        + "java -cp $${CP} $${JVM_OPTS} containerizedQuery/Worker " +  dbURL + " $${RabbitIP} " +  cc.id
					        //+ " " + workerType + " " + destinations + ";  /bin/bash -i \"\n");
	                        + " " + workerType + ";  /bin/bash -i \"\n");
	                  */
				      String cmdStr = " command: bash -c \" java -cp $${CP} $${JVM_OPTS} containerizedQuery/Worker "
		                        + " $${DB_URL} " + " $${RabbitIP} " +  cc.id  + " " + workerType 
		                        + " " + log_level  + ";  /bin/bash -i \" \n";
				      writer.write("   " + cmdStr);
					
				    if(version.equals("2.1")) {
				      writer.write("    container_name: " + cc.id+ "\n");
				    }
				      writer.write("    stdin_open: true\n");
			  	      writer.write("    tty: true\n");
				      writer.newLine();
				    
		}
		catch (IOException ioe ) {
			ioe.printStackTrace();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
				
	} //end method writeComputeContainer
	
	/*
	private void createParallelOperators(ComputeContainer cc, Digraph g, BufferedWriter writer, 
										 String version, String log_level) throws Exception {
		
		ArrayList<ComputeContainer> intraOpContainers =  g.addIntraOperators(cc);
		
		//Each intra-operator container and possibly a MERGE container
		for(ComputeContainer intraOp :  intraOpContainers) {
			
			writeComputeContainer(g, intraOp, writer, version, log_level);
			
		} // end outer for
		
	} // end method
	*/
	
	/*
	private void createLeafParallelOperators(ComputeContainer cc, Digraph g, BufferedWriter writer, 
											String version, String log_level) throws Exception {
		
		//Modify original cc to manage the first partition before adding additional operators.
		//Add leaf intra-operators, addLeafIntraOperator(cc) has the side effect of CHANGING cc's table name
		//to be the first partition of the relation. The added intra-operators manage the remaining partitions.
		ArrayList<ComputeContainer> intraOpContainers =  g.addLeafIntraOperators(cc);

		//Each intra-operator container and possibly a MERGE container
		for(ComputeContainer intraOp :  intraOpContainers) {

			writeComputeContainer(g, intraOp, writer, version, log_level);

		} // end outer for

	} // end method
	*/
	
	/*
	 * Recursively builds a directed graph of containerNodes using a query tree as input. 
	 * The digraph is homomorphic with respect to the query tree, but has directed edges 
	 * directed from children to parents. Edge direction denotes the direction of data flow
	 * and order of node execution.
	 * 
	 * Each non-base case recursive call constructs a node of a subtree in the digraph.
	 * Recursion terminates at the base case: data-only leaf nodes. Container contents
	 * (Schema set, OutputTable, predicate) are populated POST-ORDER. 
	 */
	public LinkedHashMap<String, String> buildDigraph(QueryNode treeNode, Digraph g, ComputeContainer parentContainer) throws Exception {
		
		LinkedHashMap<String, String> schemaMap = null;
		
		//construct new digraph node
		if(treeNode instanceof QueryComputeNode) {
			
			ComputeContainer newComputeContainer =  new ComputeContainer(ContainerType.COMPUTE, (QueryComputeNode)treeNode);
			g.addVertex(newComputeContainer);
			
			if(parentContainer != null)  //not root of query tree
				g.addEdge(newComputeContainer, parentContainer);
			
			// LEFT AND RIGHT children are set in recursive calls to  buildDigraph()
			if(((QueryComputeNode) treeNode).getLeftChild() != null) {
				schemaMap = buildDigraph(((QueryComputeNode) treeNode).getLeftChild(), g, newComputeContainer);  //recurse
			} 
			
			LinkedHashMap<String, String> rightMap = null;
			if(((QueryComputeNode) treeNode).getRightChild() != null) {
				rightMap = buildDigraph(((QueryComputeNode) treeNode).getRightChild(), g, newComputeContainer);  //recurse
			} 
			
			//POST-ORDER, update compute container's SQL predicate, tables/schemas
			adjust_SQL_Clauses(newComputeContainer, g,  schemaMap, rightMap);

			if(DEBUG) //display digraph of containers POST-ORDER
				System.out.println(newComputeContainer.toString());
			else
				Master.logQueryState("buildDigraph: POST-ORDER, update compute container's SQL predicate, tables/schemas  " 
										+ newComputeContainer.toString(), Log_Level.verbose);
			
			
		}
		else if (treeNode instanceof QueryDataOnlyNode) { //leaf node, recursion terminal/base case
			DataOnlyContainer newDataContainer =  new DataOnlyContainer(ContainerType.DATA_ONLY, (QueryDataOnlyNode) treeNode);
			g.addVertex(newDataContainer);
			g.addEdge(newDataContainer, parentContainer);
			schemaMap = newDataContainer.getSchema().getFullyQualifiedNamesMappedToFields();
			
			//adjust parent container to type LEAF
			parentContainer.setType(ContainerType.LEAF);
			
		}
		
		return schemaMap;
    
	} // end method
	
	
	/* adds each element in the right map into the left map, 
	 * consolidating both maps into one. 
	 * Any duplicated field name in the right map with respect to the left, is renamed.
	 */
	@SuppressWarnings("unused")
	private LinkedHashMap<String, String> consolidateMapsVer1(LinkedHashMap<String, String> leftMap, LinkedHashMap<String, String> rightMap) {

		if(rightMap != null) {
			for(String qualifiedName : rightMap.keySet()) {

				String fieldName = rightMap.get(qualifiedName);
				if(leftMap.containsValue(fieldName)) {

					for (Entry<String, String> entry : rightMap.entrySet()) {

						if (entry.getValue().equals(fieldName)) {
							String renamedField = fieldName + fieldNameSuffix++;
							rightMap.put( entry.getKey(), renamedField); //rename field
							break; //assume 1-1 mapping 
						}
					} // end for
				} 
			} //end for
		}

		return leftMap;

	}
	
	/* adds each element in the right map into the left map, consolidating both maps into one. Any duplicated field name in
	 * the right map with respect to the left, is renamed. A list of fields that have been renamed is returned.
	 */
	@SuppressWarnings("unused")
	private ArrayList<String> consolidateMaps(LinkedHashMap<String, String> leftMap, LinkedHashMap<String, String> rightMap) {
		
		ArrayList<String> renamedList = new ArrayList<String>();

		if(rightMap != null) {
			for(String qualifiedName : rightMap.keySet()) {

				String fieldName = rightMap.get(qualifiedName);
				
				//load right map entry into left map
				if(leftMap.containsValue(fieldName)) {
					String renamedField = fieldName + fieldNameSuffix++;
					renamedList.add(renamedField);
					leftMap.put(qualifiedName, renamedField);
				} 
				else
					leftMap.put(qualifiedName, fieldName);
				
			} //end for
		}

		return renamedList;

	}
	
	/*  Adjusts the container's schema set, sets the outputTable and edits the predicate's table references with
	 * the new outputTableName. Adds each element in the right map into the left map, consolidating both maps into one.
	 * Any duplicated field name in the right map with respect to the left, is renamed. 
	 * The consolidated map is passed back thru the leftMap reference parameter.
	 */
	private void adjust_SQL_Clauses(ComputeContainer cc, Digraph g, LinkedHashMap<String, String> leftMap, 
			LinkedHashMap<String, String> rightMap) throws Exception {
		
		//get container's immediate upstream Vertices
		ArrayList<Container> upstreamContainers = g.getInboundVertices(cc);

		EnumSet<OperatorType> unaryOp = EnumSet.of(OperatorType.PROJECT, OperatorType.ORDER, OperatorType.SELECT);
		//EnumSet<OperatorType> binaryOp = EnumSet.of(OperatorType.PROJECT, OperatorType.ORDER, OperatorType.JOIN);
		EnumSet<OperatorType> binaryOp = EnumSet.of(OperatorType.JOIN);
		
		OperatorType operator = null;
		try {
			operator = OperatorType.valueOf(cc.getOperator().toUpperCase());
		}
		catch (Exception e ){
			System.out.println("adjust_SQL_Clauses: " + e.getMessage());
			e.printStackTrace();
		}

		if (unaryOp.contains(operator)) {

			Container c = upstreamContainers.get(0);
			String inputTableName = "UNDEFINED";
			String outputTableName = "UNDEFINED";
			Table inputTable;
			//Table outputTable;

			/* Replace all instances of table name in predicate(derived from query relation tree) 
			 * with  with cc's input table name AND adjust for all renamed fields.
			 * No need to manipulate predicate for leaf-compute containers. 
			 * */
			
			if( !(c.getType().equals(ContainerType.DATA_ONLY)) ) {
				
				switch(operator) {
					case SELECT  :
						String predicate = cc.getPredicate();
						if(predicate.contains("WHERE")) { //WHERE clause and fields clause following SELECT key word
							int index = predicate.indexOf("WHERE");
							if((index + 5) <= predicate.length()) { //both WHERE clause and fieldsClause in predicate string
								String pred = predicate.substring(index + 5);
								String list = predicate.substring(0, index);
								cc.setPredicate(pred);
								cc.setSelectListClause(list);
							}
						}
					case PROJECT  :
					case ORDER  :
						inputTable = ((ComputeContainer)c).getOutputTable();
						inputTableName =  inputTable.getTableName();
						cc.updateClauses(leftMap, inputTableName);	
						break;
					default: throw new Exception("adjust_SQL_Clauses: UNKNOWN unary OperatorType: " + operator);
				} // end switch

			}
			else {  //leaf-compute container case
				
				if(cc.getOperator().equalsIgnoreCase("SELECT")) {
					String predicate = cc.getPredicate();
					if(predicate.contains("WHERE")) { //contains both select and where clauses
						int index = predicate.indexOf("WHERE");
						if((index + 5) < predicate.length()) { //both WHERE clause and fieldsClause in predicate string
							cc.setPredicate(predicate.substring(index + 5));
							cc.setSelectListClause(predicate.substring(0, index));
						}
					}
				}
				
				inputTableName = ((DataOnlyContainer)c).getSchema().getRelationName();
				Schema schema = ((DataOnlyContainer)c).getSchema();
				inputTable = new Table(inputTableName, schema);
			}
			
			/* add input table */
			cc.addInputTable(inputTable);
			
			/* use cc's ID  for its output table's name */
			outputTableName = cc.id;
			/* current cc's output table's schemaSet is inherited from inputTable */
			LinkedHashSet<Schema> schemaSet = inputTable.getSchemaSet();
			/* Add output table. Make copy of SchemaSet to avoid REFERENCE problems. */
			cc.setOutputTable( new Table(outputTableName, new LinkedHashSet<Schema>(schemaSet) ) );
			
		} else if (binaryOp.contains(operator)) {

			switch(operator) {
			case JOIN:
				//add upstream input tables
				for(Container upstream : upstreamContainers) {
					cc.addInputTable(((ComputeContainer)upstream).getOutputTable());
				} // end for
				
				Table leftTable = cc.getInputTable(0);
				Table rightTable = cc.getInputTable(1);
				String leftTableName = leftTable.getTableName();
				String rightTableName = rightTable.getTableName();
				
				/* replace all instances of left map names (derived from query relation tree) in 
				 * PREDICATE with  with cc's left table name. */
				//predicate.replaceAll("\\w+[.]{1}\\w+", outputTableName + "[.]{1}\\w+");
				cc.updateClauses(leftMap, leftTableName);
				cc.updateClauses(rightMap, rightTableName);
				
				//Set SQL selectListClause for left table.
				//Enumerate list of fields in left table to be joined
				String selectListClause = "";
				for(String qualifiedName : leftMap.keySet()) {
					
					String fieldName = leftMap.get(qualifiedName);
					selectListClause += leftTableName + "." + fieldName + ", ";	
				}
				
				/* Set SQL selectListClause for right table.
				 * Enumerate list of fields in left table to be joined
				 * and RENAME ANY DUPLICATES FOUND IN RIGHT table
				 */
				for(String qualifiedName : rightMap.keySet()) {

					String fieldName = rightMap.get(qualifiedName);

					//load right map entry into left map
					if(leftMap.containsValue(fieldName)) {
						String renamedField = fieldName + "_" + qualifiedName.substring(0, qualifiedName.indexOf('.'));
						rightMap.put(qualifiedName, renamedField);
						selectListClause += rightTableName + "." + fieldName + " AS " + renamedField + ", ";	
					}
					else
						selectListClause += rightTableName + "." + fieldName + ", ";	

				} //end for
				
				//fence post problem: removed last trailing ", "
				selectListClause = selectListClause.substring(0, selectListClause.length() - ", ".length());
				
				//update container's selectListClause
				cc.setSelectListClause(selectListClause);
				
				//All fields in both tables are now guaranteed to be unique
				
				/* replace all instances of right map names (derived from query relation tree) in PREDICATE
				 * with  with cc's right table name. */
				//cc.updatePredicateWhereClauses(rightMap, rightTable.getTableName());
				
				//CONSOLIDATE BOTH left and right map into LEFT MAP
				leftMap.putAll(rightMap);
					
				/* current cc's schemaSet is inherited from both inputTables */
				String outputTableName = cc.id;
				
				//construct schema of table resulting from execution of SQL query
				/* Make copy of SchemaSet to avoid trailing REFERENCE problems in previous containers with same set.*/
				cc.setOutputTable( new Table(outputTableName, new LinkedHashSet<Schema>(leftTable.getSchemaSet()) ) );
				// add right table schema set too
				cc.getOutputTable().addSchemaSet(new LinkedHashSet<Schema>(rightTable.getSchemaSet()));  
				
				break;
			default: throw new Exception("adjust_SQL_Clauses: UNKNOWN binary OperatorType: " + operator);
			} // end switch
				
		} //end if-else

	} // end method
	
	@SuppressWarnings("unused")
	private String getOperatorSymbol(OperatorType operator) {

		String symbol = "UNDEFINED";   //undefined
		switch(operator) {
		case SELECT  :
			symbol = "SEL";
			break;
		case PROJECT  :
			symbol = "PRJ";
			break;
		case ORDER  :
			symbol = "ORD";
			break;
		case JOIN  :
			symbol = "JOIN";
			break;
		default  :
		}

		return symbol;
		
	} // end method
		
	/* only to be used on DataOnlyContainer schema */
	@SuppressWarnings("unused")
	private String prefixFieldNames(String schema, String tableName) {
		
		//remove '[' and ']'
		schema = schema.replace('[', ' ').trim();
		schema = schema.replace(']', ' ').trim();
		StringTokenizer tokenizer = new StringTokenizer(schema, ";"); //tab delimited
		
		String token = "";
		schema = "";  //reset to empty
		while(tokenizer.hasMoreTokens()) {
			token = tokenizer.nextToken(";");
			String[] subFields = token.split("\\w");
			if(subFields[0].equalsIgnoreCase("PRIMARY") || subFields[0].equalsIgnoreCase("FOREIGN")) {
				schema += token;
				continue;
			}
			String adjustedName = tableName + "_" + subFields[0]; //prefix table name to field
			token.replace(subFields[0], adjustedName);
			schema += token;
			
			if(tokenizer.hasMoreTokens())
				token += ";";
		} //end while
		
		return "[" + schema + "]";
	}
	
	/*
	 * Performs a pre-order traversal of the query tree, printing a node's contents when it is 
	 * first visited.
	 */
    public String printQueryTree(QueryNode node) {
    	
    	String queryTree = "";
		
    	if(node instanceof QueryComputeNode) {
    		QueryComputeNode compute = (QueryComputeNode)node;
    		//System.out.println(compute);  //print contents of visited node
    		queryTree += compute.toString() + "\n";  //print contents of visited node
    		
    		if(compute.leftChild != null)
    			queryTree += printQueryTree(compute.leftChild);
    	
    		if(compute.rightChild != null)
    			queryTree += printQueryTree(compute.rightChild);
    	}
    	else {
    		QueryDataOnlyNode data  = (QueryDataOnlyNode)node;
    		//System.out.println(data);  //print contents of visited node
    		queryTree += data.toString() + "\n";  //print contents of visited node
    	}
    	
    	return queryTree;
    	
	} //end method
    
	
	/* Parses the serialized representation of a query tree node, using its individual
	 * fields to construct a node in a query tree. 
	 * 
	 * Input:  the text string that represents the query node
	 * Output: a QueryNode object encapsulating the node's components
	 * Throws QueryException if ContainerType is not valid.
	 * 
	 */
    public QueryNode parseTextNode(String textualNodeString) throws Exception {

    	QueryNode node = null;
    	StringTokenizer tokenizer = new StringTokenizer(textualNodeString, "\t"); //tab delimited

    	String id = tokenizer.nextToken();
    	String typeOfNode = tokenizer.nextToken();		

    	try {
    		
    		if(typeOfNode.equals("COMPUTE")) {
    			
    			String operator = tokenizer.nextToken().toUpperCase(); //make sure op is UPPER_CASE
    			String predicate = tokenizer.nextToken();
    			String leftId = tokenizer.nextToken();
    			String rightId = tokenizer.nextToken();
    			String degrees = tokenizer.nextToken();

    			if( operator.contains("JOIN")) {
    				String probeId = tokenizer.nextToken();
    				//node = new QueryComputeNode(id, Enum.valueOf(ContainerType.class, typeOfNode), operator, predicate, leftId, rightId, degrees, probeId);
    				node = new QueryComputeNode(id, ContainerType.COMPUTE, operator, predicate, leftId, rightId, degrees, probeId);
    			}
    			else
    				node = new QueryComputeNode(id, ContainerType.COMPUTE, operator, predicate, leftId, rightId, degrees);

    		}
    		else if (typeOfNode.equals("DATA_ONLY")) {
    			String databasePath = tokenizer.nextToken();
    			String table = tokenizer.nextToken();
    			String schema = tokenizer.nextToken();
    			node = new QueryDataOnlyNode(id, ContainerType.DATA_ONLY, databasePath, table, schema);
    		}
    		else 
    			throw new QueryException("INVALID ContainerType " + typeOfNode);

    	} catch(Exception e) {
    		Master.logQueryState("parseTextNode: ERROR parsing " + textualNodeString +  "\n" 
    								+  e.getLocalizedMessage()  , Log_Level.quiet);
    		throw e;
    	}

    	return node;
    }  // end method parseSerializedNode()
	
	/* Reads in a textual representation of a relation query from a file formatted as
	 * specified  in class preface above. Each separate line represents a node in a optimized
	 * relational query tree, with each node containing its operator, predicate and links to children
	 * nodes. 
	 * 
	 * INPUT: Each line of the file is read in, then hashed into a hash-table using  node ID as the key.
	 * OUTPUT: The hash-table is returned;
	 */
	public LinkedHashMap<String, String> hashTextualQuery(String textualQueryFile) throws IOException {

		BufferedReader in = null;
		LinkedHashMap<String, String> hashTable = new LinkedHashMap<String, String>();

		in = new BufferedReader(new FileReader(textualQueryFile));
		StringBuffer queryInfo = new StringBuffer();
		String line = "";

		while(true) {

			//read in next query tree node descriptor
			if ((line = in.readLine()) == null)
				break;
			else if(line.charAt(0) == '#') 
				continue;
			else if(line.charAt(0) == '%') {
				queryInfo.append(line + "\n");
				continue;
			}
			else {
				StringTokenizer tokenizer = new StringTokenizer(line, "\t"); //tab delimited
				String key = tokenizer.nextToken();   //get nodeId
				if(key.matches("\\d+"))       //could be a comment line beginning with '#'
					hashTable.put(key, line);
			}

		}// end while
		
		Master.logQueryState("METRIC:  " + queryInfo.toString(), Log_Level.metrics);
		in.close();

		return hashTable;
		
	} //end method hashTextualQueryFromFile()
	
	
	/* print contents of digraph with IN-ORDER traversal */
	public void printDigraphInOrder( Container container, Digraph g, HashSet<Container> visited) throws Exception {
		
		if(container instanceof DataOnlyContainer) {
			if(!visited.contains(container)) {
				System.out.println((DataOnlyContainer)container);
				visited.add(container);
			}
		}
		else {
			if(!visited.contains(container)) {
				ComputeContainer cc = (ComputeContainer)container;
				System.out.println(cc);
				System.out.println(  "\n" + "DOWN_STREAM: "    + g.getDestinationIds(cc).toString()
								   + "\n" + "UP_STREAM: "      + g.getInboundIds(cc).toString()
								   + "\n" + "SIZE_INBOUND: "   + g.getNumberInboundEdges(cc)  + "\n"); 
				visited.add(container);
				ArrayList<Container> list = g.getInboundVertices(container);
				for(Container c : list) {
					printDigraphInOrder(c, g, visited);
				} // end for
			}
		} // end if-else
			
	} // end method
	
	
	/* print contents of digraph Top-Down. Make use of how containers are named
	 * from the serialization file, e.g. C1, C2, C3 ... */
	public void printDigraphTopDown(Digraph g)  {
		
		ArrayList<Container>  vertices = new ArrayList<Container>(g.getVertices());
		
		 Collections.sort(vertices);      //sorted by Container id
		
		for(Container c : vertices) {
			
			if(c instanceof DataOnlyContainer) 
					System.out.println((DataOnlyContainer)c);
			else 
					System.out.println((ComputeContainer)c);
		} // for
			
	} // end method
	
	/* Prints a facsimile of the optimized query tree, stored in the hash-table input by ID.
	 *  <ID		OPERATOR	PREDICATE	LEFT	RIGHT>
	 *  
	 *  where,
	 *  
	 *  ID: 		identifies a query tree node
	 *  OPERATOR: 	identifies the relation operator executed by the node
	 *  PREDICATE: 	any predicate expression for the operator, NULL for no predicate
	 *  LEFT:		is the left child's ID
	 *  RIGHT:		is the right child's ID   //relational ops are unary or binary only
	 */
	public void printSerializedTree( LinkedHashMap<String, String> hashTable) {
		
		String serializedTree = hashTable.toString();
		serializedTree = serializedTree.substring(1, serializedTree.length() -1);
		String[]  nodeList = serializedTree.split("\\d=");
		for(String node : nodeList) {
			System.out.println(node);
		}
		
		/*   attempt to create a graphic facsimile of tree
		try {
			
			 String line = "";
			 String ID = "";
			 String OPERATOR = "";
			 String PREDICATE = "";
			 String LEFT = "";
			 String RIGHT = "";
			 
			 String blanks =  "                                        "; //forty leading blanks 
			 
			 for(String key : hashTable.keySet()) {
					
				line = hashTable.get(key);
				StringTokenizer tokenizer = new StringTokenizer(line, "\t"); //tab delimited
				
				for(int i = 1; i < tokenizer.countTokens(); i++) {
					String token = tokenizer.nextToken();
					
					switch(i) {
					
					case 1: ID = token;
					        break;
					case 2: OPERATOR = token;
					 		System.out.println(blanks + OPERATOR + PREDICATE);
					 		if ( Character.isLetter(LEFT.charAt(0)) ) {
					 			System.out.println(blanks + "  |");
					 			System.out.println(blanks + "  " + LEFT);
					 		} 
			        		break;
					case 3: PREDICATE = token;
			         		break;
					case 4: LEFT = token;
			        		break;
					case 5: RIGHT = token;
			        		break;
					default: throw new Exception("Bad query node FIELD: " + token);
					}
					 
				} // end inner for
			
				//print graph line
				
					
			}// end outer for
			 
		} catch(Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
			System.exit(0);
		}
	  */
		
	} // end method printTree()
	
	
	protected void testSchemaReplaceFields () {
		
		// test schema method 
		Schema testSchema = new Schema("R", "[gsid INTEGER; gcid INTEGER; points REAL; PRIMARY KEY (gsid, gcid)]");
		testSchema.replaceSchemaFieldName("gsid", "xyz");
		System.out.println(testSchema.toString()); 
	
	}  // end method
	
	protected void testTableMethods () {
		
		// test table methods 
		 Schema testSchema = new Schema("Grades", "[gsid INTEGER; gcid INTEGER; points REAL; PRIMARY KEY (gsid, gcid)]");
		 Table testTable = new Table("testTable", testSchema);
		 System.out.println("Qualified names for " + testTable.getTableName());
		 
		 //ArrayList<String> list = testTable.getQualifiedFieldNames();
		 //for(String qualifiedName : list) 
		 //	 System.out.println(qualifiedName);
		 //int count = testTable.replaceTableFieldName("gsid", "xyz");
		 //System.out.println("times replaced " + count);
		 
		 QueryComputeNode treeNode = new QueryComputeNode("C1", ContainerType.COMPUTE, "SELECT",
				 "Grades.points > 2.0", "C7", null);
		 ComputeContainer cc = new  ComputeContainer(ContainerType.COMPUTE, treeNode);
		 String predicate = cc.getPredicate();
		 System.out.println("predicate before: " + predicate);
		 cc.addInputTable(testTable);
		 cc.adjustPredicateTableNames();
		 System.out.println("predicate after update: " + cc.getPredicate());
	} //end method

} // end class ContianerizedQueryUtils

class QueryNode {
	
	protected String id = "";
	protected ContainerType type = ContainerType.NONE;
	
	public QueryNode(String id, ContainerType type) {
		super();
		this.id = id;
		this.type = type;
	}
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}

	public ContainerType getType() {
		return type;
	}

	public void setType(ContainerType type) {
		this.type = type;
	}
	
	@Override
	public String toString() {
		return "QueryNode [id=" + id + ", type=" + type;
	}
	
}	// end class QueryNode

class QueryComputeNode extends QueryNode implements Constants {
	
	private String 	operator = "";
	private String 	predicate = "";
	private String 	leftId = "null";     //string representation of left child's id, if any       
	private String 	rightId = "null";    //string representation of right child's id, if any
	private String 	degreesParallel = "1";    //by default, set to 1
	private String  probeId =  UNDEFINED;   	// means No probeId
	private boolean probe = false;
	QueryNode 		leftChild = null;
	QueryNode 		rightChild = null;
	
	/* Right child must be set to null if container is attached directly to a data-only node, or predicate is unary */ 
	public QueryComputeNode(String id, ContainerType type, String operator, String predicate, String leftId, String rightId) {
		super(id, type);
		this.operator = operator;
		this.predicate = predicate;
		this.leftId = leftId;
		if(!rightId.equals("null"))
			this.rightId = rightId;  //null if container is attached directly to a data-only node, or predicate is unary
	}

	public QueryComputeNode(String id, ContainerType type, String operator, String predicate, String leftId, String rightId, 
			String degrees) {
		this(id, type, operator, predicate, leftId, rightId);
		if (!degrees.equals("null"))
			this.degreesParallel = degrees;
	}
	
	public QueryComputeNode(String id, ContainerType type, String operator, String predicate, String leftId, String rightId, 
							String degrees, String probeId) {
		this(id, type, operator, predicate, leftId, rightId);
		if (!degrees.equals("null"))
			this.degreesParallel = degrees;
		if(!probeId.equals("null"))
			this.probeId = probeId;
	}

	public boolean isProbe() {
		return probe;
	}

	public void setProbe(boolean probe) {
		this.probe = probe;
	}

	public String getProbeId() {
		return probeId;
	}
	
	public void setProbeId(String probeId) {
		this.probeId = probeId;
	}

	public String getOperator() {
		return operator;
	}
	
	public void setOperator(String operator) {
		this.operator = operator;
	}
	
	public String getDegreesParallel() {
		return degreesParallel;
	}

	public String getPredicate() {
		return predicate;
	}
	public void setPredicate(String predicate) {
		this.predicate = predicate;
	}
	
	public String getLeftId() {
		return leftId;
	}
	
	public String getRightId() {
		return rightId;
	}
	
	public QueryNode getLeftChild() {
		return leftChild;
	}
	public void setLeftChild(QueryNode leftChild) {
		this.leftChild = leftChild;
	}
	public QueryNode getRightChild() {
		return rightChild;
	}
	public void setRightChild(QueryNode rightChild) {
		this.rightChild = rightChild;
	}
	
	/*
	@Override
	public String toString() {
		return super.toString() + ", operator=" + operator + ", predicate=" + predicate + ", leftId=" + leftId
				+ ", rightId=" + rightId + ", degreesParallel=" + degreesParallel + ", sibling=" + sibling +  "]";		
	}
	*/
	
	@Override
	public String toString() {
		return super.toString() + ", operator=" + operator + ", predicate=" + predicate + ", leftId=" + leftId
				+ ", rightId=" + rightId + ", degreesParallel=" + degreesParallel +  "]";		
	}
	
} // end class QueryComputeNode

class QueryException extends Exception {

	private static final long serialVersionUID = 1L;

	public QueryException(String string) {
		super(string);
	}
	
} // end QueryException

class QueryDataOnlyNode extends QueryNode {
	
	private String databaseURL;
	private String table;
	private String[] partitions = null;
	private String schema;
	
	
	public QueryDataOnlyNode(String id, ContainerType type, String databaseURL, String table, String schema) {
		super(id, type);
		this.databaseURL = databaseURL;
		this.table = table;
		this.schema = schema;
		parsePartitions();
	}

	public String getDatabaseURI() {
		return databaseURL;
	}
	
	public String getTable() {
		return table;
	}
	
	public void setTable(String table) {
		this.table = table;
	}
	
	public String getSchema() {
		return schema;
	}

	public String[] getPartitions() {
		return partitions;
	}

	public boolean hasPartitions() {
		
		return partitions != null;
	}
	
	private void parsePartitions() {
		
		if(table.charAt(0) == '[') {
			String parts = table.substring(1, table.indexOf(']'));
			partitions = parts.split(",");
			
			//extract table name from partitions list
			int end = partitions[0].indexOf("Part_");
			table = partitions[0].substring(0, end);
		}	
		
	} // method

	@Override
	public String toString() {     
		return super.toString() + ", databasePath=" + databaseURL + ", table=" + table  + ", partitions= "
							+  Arrays.deepToString(partitions) + "]";	
	}

} // end class QueryDataOnlyNode

class ContainerState implements Serializable {

	private static final long serialVersionUID = -8148055456420035778L;
	
	private Container container;
	private boolean processing;
	private ArrayList<Edge> completedInputs = new ArrayList<Edge>();
	private ArrayList<Edge> completedOutputs = new  ArrayList<Edge>();
	
	public ContainerState(Container c, boolean processing) {
		super();
		this.processing = processing;
		this.container = c;
	}
	
	public boolean isInputComplete(Digraph dg) {
		
		//compare all input edges for this container from the Digraph to list of CompletedInputs (Edges)
		for(Edge e : dg.getInboundEdges(container)) {
			if(!completedInputs.contains(e))
				return false;
		}
		
		return true;
	}
	
	public void resetInputState() {
		completedInputs = new ArrayList<Edge>();
	}

	public boolean isOutputComplete(Digraph dg) {
		
		//compare all input edges for this container from the Digraph to list of CompletedInputs (Edges)
		for(Edge e : dg.getOutboundEdges(container)) {
			if(!completedOutputs.contains(e))
				return false;
			}
				
		return true;
	}

	public void setProcessing(boolean processing) {
		this.processing = processing;
	}
	
	public boolean isProcessing() {
		return processing;
	}

	public void addInputEdge(Edge e) {
		completedInputs.add(e);
	}
	
	public void addInputEdge(String org, String dest, Digraph g) throws Exception {
		
		boolean flag = false;
		
		for(Edge e : g.getEdges())
			if( e.getV1().id.equals(org) && e.getV2().id.equals(dest)) {	
				completedInputs.add(e);
				flag = true;
				break;
			}
		
		 if(!flag)
			 throw new Exception("addInputEdge could not find digraph edge with origin " + org + " and destination " + dest);
	} // end method
	
	public void addOutputEdge(Edge e) {
		completedOutputs.add(e);
	} // end method

	public String toString() {
		
		String str = "ContainerState [processing=" + processing + ","; 
		
		str += "\n\tcompletedInputs along Edges=";
		for(Edge edge : completedInputs)
			str +=  "\t" + edge.toString();
		
		str += "\n\tcompletedOutputs along Edges=";
		for(Edge edge : completedOutputs)
			str +=  "\t" + edge.toString();
			
		return str +  "]";
	}

	public ArrayList<Edge> getCompletedInputs() {
		return completedInputs;
	}

	public ArrayList<Edge> getCompletedOutputs() {
		return completedOutputs;
	}
	
} // end class ContainerState

class Container implements Serializable, Comparable<Container> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2448219014290080544L;
	protected String id;
	protected ContainerType type;
	
	//container ID derived from query tree
	//container type
	public Container(String id, ContainerType type) {
		this.id = id;
		this.type = type;
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public ContainerType getType() {
		return type;
	}
	
	public void setType(ContainerType type) {
		this.type = type;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Container other = (Container) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (type != other.type)
			return false;
		return true;
	}

	@Override
	public String toString() {  
		return "Container [id=" + id + ", type=" + type + "]";
	}

	@Override
	public int compareTo(Container c) {
		
		return id.compareTo(c.id) ;
	}


} // end  class Container

class Table implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2717845645234169153L;
	private String tableName = "UNDEFINED";
	private LinkedHashSet<Schema> schemaSet = new LinkedHashSet<Schema>();
	
	public Table(String tableName,  LinkedHashSet<Schema> schemaSet) {
		super();
		this.tableName = tableName;
		this.schemaSet = schemaSet;
	}
	
	public Table(String tableName, Schema schema) {
		super();
		this.tableName = tableName;
		schemaSet.add(schema);
	}
	
	public Table() {
		super();
	}
	
	public void addSchema(Schema schema) {
		schemaSet.add(schema);
	}
	
	public void addSchemaSet(LinkedHashSet<Schema> set) {
		for(Schema schema : set)
			schemaSet.add(schema);
	}
	
	
	/* Located and replaces the field name with another name(replacement) in the table's set of schemas.
	 * Returns the number of replacements. Count should be '1'. If '0' there is no such field name. If 
	 * count > 1, the set of schemas for this table has an illegal  duplicated.  
	 */
	public int replaceTableFieldName(String fieldName, String replacement) {
		
		int count = 0;
		
		for(Schema schema : schemaSet) {
			if (schema.replaceSchemaFieldName(fieldName, replacement))
				count++;
		}
		
		return count;
	} //end method
	
	/* returns list of qualified field, <relationName>.<fieldName>,  names in the table's schema set */
	public  ArrayList<String> getQualifiedFieldNames() {  // <name> <type>  ||  <name>  <type(precision)>

		ArrayList<String> fields = new ArrayList<String>();

		for(Schema schema : schemaSet) {
			
			String relationName = schema.getRelationName();
			String schemaString = schema.getSchema();
			
			/* remove schema string's starting '[' and trailing ']' */
			schemaString = schemaString.replace('[', ' ').replace(']', ' ').trim();

			StringTokenizer tokenizer = new StringTokenizer(schemaString, ";"); 

			String token = "";
			while(tokenizer.hasMoreTokens()) {

				token = tokenizer.nextToken();
				token = token.trim();
				String[] subFields = token.split("\\s");  //a whitespace character
				String fieldName = subFields[0].trim();

				if(fieldName.equalsIgnoreCase("PRIMARY") || fieldName.equalsIgnoreCase("FOREIGN")) {
					continue;
				}
				else {
					fields.add(relationName + "." + fieldName);  //name and type(precision)
				}

			} //end while
		} //end for-each

		return fields;
	} // end method
	
	/* returns list of field names in the table's schema set */
	public  ArrayList<String> getFieldNames() {  // <name> <type>  ||  <name>  <type(precision)>

		ArrayList<String> fields = new ArrayList<String>();

		for(Schema schema : schemaSet) {
			
			//String relationName = schema.getRelationName();
			String schemaString = schema.getSchema();
			
			/* remove schema string's starting '[' and trailing ']' */
			schemaString = schemaString.replace('[', ' ').replace(']', ' ').trim();

			StringTokenizer tokenizer = new StringTokenizer(schemaString, ";"); 

			String token = "";
			while(tokenizer.hasMoreTokens()) {

				token = tokenizer.nextToken();
				token = token.trim();
				String[] subFields = token.split("\\s");  //a whitespace character
				String fieldName = subFields[0].trim();

				if(fieldName.equalsIgnoreCase("PRIMARY") || fieldName.equalsIgnoreCase("FOREIGN")) {
					continue;
				}
				else {
					//fields.add(relationName + "." + fieldName);  //name and type(precision)
					fields.add(fieldName);  //name and type(precision)
				}

			} //end while
		} //end for-each

		return fields;
	} // end method
	
	public  ArrayList<String> getFieldsAndTypes() {  // <name> <type>  ||  <name>  <type(precision)>

		ArrayList<String> fields = new ArrayList<String>();

		for(Schema schema : schemaSet) {
			
			String schemaString = schema.getSchema();
			
			/* remove schema string's starting '[' and trailing ']' */
			schemaString = schemaString.replace('[', ' ').replace(']', ' ').trim();

			StringTokenizer tokenizer = new StringTokenizer(schema.getSchema() , ";"); 

			String token = "";
			while(tokenizer.hasMoreTokens()) {

				token = tokenizer.nextToken();
				String[] subFields = token.split("\\s");  //a whitespace character

				if(subFields[0].equalsIgnoreCase("PRIMARY") || subFields[0].equalsIgnoreCase("FOREIGN")) {
					continue;
				}
				else {
					fields.add(subFields[0] + " " + subFields[1]);  //name and type(precision)
				}

			} //end while
		} //end for-each

		return fields;
	} // end method

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public LinkedHashSet<Schema> getSchemaSet() {
		return schemaSet;
	}

	public void setSchemaSet(LinkedHashSet<Schema> schemaSet) {
		this.schemaSet = schemaSet;
	}

	@Override
	public String toString() {
		String str =  "Table [tableName=" + tableName + ", schemaSet= \n";
		for(Schema schema : schemaSet) {
			str += "\t" + schema.toString();
		}
		return str + " ]\n";
	} // end method
	
} // end class Table

class Schema  implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8108536521166850059L;
	private String relationName;
	private String schema;   //e.g. [sid INTEGER PRIMARY KEY; name TEX; age REAL]
	/* maps original fully qualified named: <relation>.<fieldName> --> <fieldName>
	 * needed to identify and track possible duplicate field names, e.g. in self-joins */
	private LinkedHashMap<String, String> fullyQualifiedNamesMappedToFields = new LinkedHashMap<String, String>();
	
	public Schema(String relationName, String schema) {
		super();
		this.relationName = relationName;
		this.schema = schema;
		for(String field : getFieldNames())
			fullyQualifiedNamesMappedToFields.put(relationName + "." + field, field);
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((relationName == null) ? 0 : relationName.hashCode());
		result = prime * result + ((schema == null) ? 0 : schema.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Schema other = (Schema) obj;
		if (relationName == null) {
			if (other.relationName != null)
				return false;
		} else if (!relationName.equals(other.relationName))
			return false;
		if (schema == null) {
			if (other.schema != null)
				return false;
		} else if (!schema.equals(other.schema))
			return false;
		return true;
	} // end method

	public String getRelationName() {
		return relationName;
	}

	public String getSchema() {
		return schema;
	}
	
	public LinkedHashMap<String, String> getFullyQualifiedNamesMappedToFields() {
		return fullyQualifiedNamesMappedToFields;
	}

	public void setFullyQualifiedNamesMappedToFields(LinkedHashMap<String, String> fullyQualifiedNamesMappedToFields) {
		this.fullyQualifiedNamesMappedToFields = fullyQualifiedNamesMappedToFields;
	}

	/* returns list of field names in the table's schema set */
	private  ArrayList<String> getFieldNames() {  // <name> <type>  ||  <name>  <type(precision)>

		ArrayList<String> fields = new ArrayList<String>();

		//String relationName = schema.getRelationName();
		String schemaString = schema;

		/* remove schema string's starting '[' and trailing ']' */
		schemaString = schemaString.replace('[', ' ').replace(']', ' ').trim();

		StringTokenizer tokenizer = new StringTokenizer(schemaString, ";"); 

		String token = "";
		while(tokenizer.hasMoreTokens()) {

			token = tokenizer.nextToken();
			token = token.trim();
			String[] subFields = token.split("\\s");  //a whitespace character
			String fieldName = subFields[0].trim();

			if(fieldName.equalsIgnoreCase("PRIMARY") || fieldName.equalsIgnoreCase("FOREIGN")) {
				continue;
			}
			else {
				//fields.add(relationName + "." + fieldName);  //name and type(precision)
				fields.add(fieldName);  //name and type(precision)
			}

		} //end while

		return fields;
	} // end method
	
	/* replaces all occurrences of fieldName in schema with replacement name, e.g. for  
	 * [gsid INTEGER; gcid INTEGER; points REAL; PRIMARY KEY (gsid, gcid)], if 'gsid' was 
	 * replaced, it would be twice. */
	public boolean replaceSchemaFieldName(String fieldName, String newName) {

		boolean replaced = false;
		
		/* pattern assures no embedded name is matched */
		Pattern pattern = Pattern.compile("(\\W)(" + fieldName + ")(\\W)"); 
		Matcher matcher = pattern.matcher(schema);
		
		if(matcher.find()) {
		
			//using the magic of regex groups
			schema = matcher.replaceAll("$1" + newName + "$3");
			
			replaced = true;
		}

		return replaced;
	}
	
	@Override
	public String toString() {
		return "Schema [relationName=" + relationName + ", schema=" + schema + "]\n";
	}
	
} // end class Schema

class ComputeContainer extends Container implements Serializable {

	private static final long serialVersionUID = 6322949365054016378L;
	private ContainerState state = new ContainerState(this, false);
	private String operator = "UNDEFINED";
	private String predicate = "UNDEFINED";
	private String selectListClause = " * ";
	/* usually one or at most two input tables per container, in the JOIN case, there maybe many for ORDER?? */
	private ArrayList<Table> inputTables = new ArrayList<Table>();
	private Table outputTable = null;
	private String outputSchema = ""; 
	private int degreesParallel = 1;
	private boolean probe = false;	
	private Exchange parallelEX =  new DefaultExchange();
	
	//String ID derived from query tree
	public ComputeContainer(String Id, ContainerType type) {
		super(Id, type);
	}
	
	public ComputeContainer(ContainerType type, QueryComputeNode treeNode) {
		super(treeNode.getId(), type);
		id = "C" + id;
		operator = treeNode.getOperator();
		predicate = treeNode.getPredicate();
		degreesParallel = Integer.valueOf(treeNode.getDegreesParallel());
		probe = treeNode.isProbe();
		
	} // end constructor

	public String getOutputSchema() {
		return outputSchema;
	}

	public void setOutputSchema(String outputSchema) {
		this.outputSchema = outputSchema;
	}

	public int getDegreesParallel() {
		return degreesParallel;
	}

	/**
	 * Returns a deep copy of the ComputeContainer.
	 */
	public ComputeContainer copyComputeContainer() {
		
		ComputeContainer copy = null;
		try {
			// Write the object out to a byte array
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			ObjectOutputStream out = new ObjectOutputStream(bos);
			out.writeObject(this);
			out.flush();
			out.close();

			// Make an input stream from the byte array and read
			// a copy of the Container back in.
			ObjectInputStream in = new ObjectInputStream(
					new ByteArrayInputStream(bos.toByteArray()));
			copy = (ComputeContainer)in.readObject();
		}
		catch(IOException e) {
			e.printStackTrace();
		}
		catch(ClassNotFoundException cnfe) {
			cnfe.printStackTrace();
		}
		return copy;
	}
	
	public boolean isProbe() {
		return probe;
	}

	public void setProbe(boolean probe) {
		this.probe = probe;
	}

	public ContainerState getState() {
		return state;
	}

	public void setState(ContainerState state) {
		this.state = state;
	}
	
	public void setOperator(String operator) {
		this.operator = operator;
	}

	public String getOperator() {
		return operator;
	}

	public Exchange getParallelEX() {
		return parallelEX;
	}

	public void setParallelEX(Exchange parallelEX) {
		Master.logQueryState("setParallelEX: parallelExchange for " + getId() +  " reset to exchange type "
								+ parallelEX.toString(), Log_Level.verbose);
		this.parallelEX = parallelEX;
	}

	public void setPredicate(String pred) {
		this.predicate = pred;
	}
	
	public String getPredicate() {
		return predicate;
	}
	
	public String getSelectListClause() {
		return selectListClause;
	}

	public void setSelectListClause(String selectListClause) {
		this.selectListClause = selectListClause;
	}

	/* case of container having two tables, binary operator, e.g. JOIN */
	public  void setInputTables(ArrayList<Table> tables) {
		this.inputTables = tables;
	}
	
	public ArrayList<Table> getInputTables() {
		return inputTables;
	}
	
	/* case of container only having a SINGLE table, unary operation, e.g. PROJECT */
	public Table getInputTable(int index) {
		Table table = null;
		
		try {
			table = inputTables.get(index);
		} 
		catch(ArrayIndexOutOfBoundsException e) {
			System.out.println("index= " + index + ", Container= " + id + ", operator= " + operator);
			e.printStackTrace();
		}
		
		return table;
	}
	
	public  void addInputTable(Table table) {
		this.inputTables.add(table);
	}
	
	public Table getOutputTable() {
		return outputTable;
	}

	public void setOutputTable(Table table) {
		outputTable = table;
	}
	
	/* Updates original predicate qualified names: <relation>.<field> with <table>.<field>
	 * Matching is done by examining the schema set for each table. WARNING: Duplicates must be
	 * resolved before call this method. */
	public void adjustPredicateTableNames() {
	
		for(Table table : inputTables) {
			for(String qualifiedName : table.getQualifiedFieldNames()) {
				int index = qualifiedName.indexOf('.');
				String replacement =   table.getTableName() + '.' + qualifiedName.substring(index + 1);
				predicate = predicate.replace(qualifiedName, replacement);
			}
		}
	} // end method
	
	public void eliminateFieldNameDuplicatesAcrossTables() throws Exception {
		
		for(Table fromTable : inputTables) {
			ArrayList<String> fromTableFields = fromTable.getFieldNames();
			
			for(Table comparedToTable : inputTables) {
				
				if(fromTable == comparedToTable)
					continue;   //don't compare table to itself
				else {
					ArrayList<String> comparedToTableFields = comparedToTable.getFieldNames();
					eliminateFieldNameDuplicates(fromTable, fromTableFields, comparedToTable, comparedToTableFields);
				}	
				
			}//end inner for
			
		} // end outer for
		
	} // end method
	
	/* eliminate duplicates field names between two tables: t1 and t2. The duplicate name is replaced in the predicate
	 * and  t2's schema with a new unique name */
	public void eliminateFieldNameDuplicates(Table t1, ArrayList<String> table1_fields, Table t2, ArrayList<String> table2_fields) throws Exception {

		String replacement = "";
		
		for(String fieldFromOne  : table1_fields) {


			for(String fieldFromTwo  : table2_fields) {

				if(!fieldFromOne.equals(fieldFromTwo))
					continue;   //no duplicated here
				else
					replacement = fieldFromTwo + String.valueOf(ContainerizedQueryUtils.fieldNameSuffix++);
	
				
				if(t2.replaceTableFieldName(fieldFromTwo, replacement) == 1) 		
					//update predicate for table t2
					predicate = predicate.replaceFirst("(\\W)" + t2.getTableName() + "." + fieldFromTwo + "(\\W)", 
							"$1" + t2.getTableName() + "." + replacement + "$2");
				else
					throw new Exception("eliminateFieldNameDuplicates: table " + t2.getTableName() +
							" duplicated " + fieldFromOne + " at least twice"); 

			}//end inner for
		} // end outer for

	} // end method
	
	public boolean replaceSchemaFieldName(String qualifiedFieldName, String newName) {

		boolean replaced = false;
		
		/* pattern assures no embedded name is matched */
		Pattern pattern = Pattern.compile("(\\W)(" + qualifiedFieldName + ")(\\W)"); 
		Matcher matcher = pattern.matcher(predicate);
		
		if(matcher.find()) {
		
			//using the magic of regex groups
			predicate = matcher.replaceAll("$1" + newName + "$3");
			
			replaced = true;
		}

		return replaced;
	}
	
	/* Parses out  qualified field names in the original predicate obtained from the associated
	 * query tree node. The current container's input table's (left or right child) 
	 * must be substituted for the the original relation name in the container's predicate.
	 * Also validates the table name being substituted for a schema with a matching field name. 
	 * Any duplicate field names between tables must be identified and renamed, so that names in
	 * the adjusted predicate are unique. The list of renamed fields is returned. 
	 * The predicate is updated with the substituted names.  Likewise the selectListClause is also updated.*/
	public void updateClauses(LinkedHashMap<String, String> schemaMap, String inputTableName) throws Exception  {
		
		//Pattern pattern = Pattern.compile("\\w+[.]{1}\\w+");	
		//Pattern pattern = Pattern.compile("[a-zA-Z0-9]+[.]{1}[a-zA-Z0-9]+");
		//eliminates mistaking a qualified field name for a floating point number
	//Pattern pattern = Pattern.compile("[a-z_A-Z]+[a-zA-Z_0-9]*[.]{1}[a-z_A-Z]+[a-zA-Z_0-9]*");
		//Pattern pattern = Pattern.compile("[a-z_A-Z]+[.]{1}[a-z_A-Z]+");
	//Matcher matcher = pattern.matcher(predicate.trim());
		//TreeSet<String> set = new TreeSet<String>(); //guarantees no duplicate table names in set

		//String qualifiedFieldName = "";
		
		/* for all original qualified names in the predicate, locate which input table it maps to. 
		while(matcher.find()) {  //per qualified name
			
			qualifiedFieldName = matcher.group();
			
			if(schemaMap.containsKey(qualifiedFieldName)) {

				// update original qualified name in predicate where clause
				//accounts for new field names to replacing duplicates
				String fieldName = schemaMap.get(qualifiedFieldName); 
				predicate = predicate.replaceAll(qualifiedFieldName, inputTableName + "." + fieldName );
				
			}
		} // end while
		*/
	
		/*  THIS WORKS TOO!!! */
		for(String qualifiedName : schemaMap.keySet()) {
			
			String currentField = schemaMap.get(qualifiedName);
			
			predicate = predicate.replace(qualifiedName, inputTableName + "." + currentField);
			selectListClause = selectListClause.replace(qualifiedName, inputTableName + "." + currentField);
			//predicate = predicate.replace("\\b" + qualifiedName + "\\b",  inputTableName + "." + currentField );
		} // end for
		
		
	} // end updatePredicateWhereClauses
	
	
	/* Examines schemas from left and right tables for matching qualified field name, <relationName>.<fieldName>. 
	 * Returns which table has the schema holding the original qualified field name(from the query tree)
	 * or throws Exception if the field is duplicated in both tables OR is not found in either table. If field is
	 * duplicated in each table, modify both field names and modify their respective schemas.
	 */
	@SuppressWarnings("unused")
	private String associateFieldWithTable(ArrayList<String> leftTableFields, ArrayList<String> rightTableFields,
											String fieldName) throws Exception {
		String subtituteName = "";
		
		/*
		if(leftTableFields.contains(fieldName) && rightTableFields.contains(fieldName)) { //duplicate 
		
			//String fieldName = fieldName.substring(fieldName.indexOf('.') + 1);
			String replacement =  fieldName + "_" + ContainerizedQueryUtils.fieldNameSuffix++;
			int replacementCount = inputTables.get(0).replaceFieldName(fieldName, replacement);
			if(replacementCount != 1)
				throw new Exception("associateFieldWithTable: " +  fieldName + " FOUND in  "  
							+ inputTables.get(0).getTableName() + "  " + replacementCount + " times");
			
			replacement =  fieldName + "_" + ContainerizedQueryUtils.fieldNameSuffix++;
			replacementCount = inputTables.get(1).replaceFieldName(fieldName, replacement);
			if(replacementCount != 1)
				throw new Exception("associateFieldWithTable: " +  fieldName + " FOUND in  "  
							+ inputTables.get(1).getTableName() + "  " + replacementCount + " times");
			
		} */
		
		if(leftTableFields.contains(fieldName))
			subtituteName = inputTables.get(0).getTableName();
		else if (rightTableFields.contains(fieldName))
			subtituteName = inputTables.get(1).getTableName();	
		else {
			throw new Exception("associateFieldwithTable: " +  fieldName + " Not FOUND in both table " + inputTables.get(0).getTableName() + 
		               " and table " + inputTables.get(0).getTableName());
		}
		
		return subtituteName;
	} // end method
	
	@SuppressWarnings("unused")
	private  boolean isNumeric(String str)
	{
	  return str.matches("-?\\d+(\\.\\d+)?");  //match a number with optional '-' and decimal.
	}

	/* construct SQL query string for intermediate compute container */
	public String  getQuery() throws Exception {
		
			String inputTableName = "UNDEFINED";
			
			String op = getOperator().toUpperCase();
			if( !( op.contains("SELECT") || op.contains("PROJECT") || op.contains("ORDER")
				|| op.contains("JOIN") || op.contains("MERGE_SORT")))
				throw new Exception("Container " + id + ", Operator: " + op + " UN-SUPPORTED relational op', OR container type: " + type + " not 'COMPUTE'");
			
			String predicate = getPredicate();	
			//String outputTableName = cc.getOutputTable().getTableName();
			String sql = "UNDEFINED";
			
			if(op.contains("SELECT")) {  //interior or leaf 'SELECT'
				
				if(!inputTables.isEmpty())
					inputTableName = getInputTable(0).getTableName();
				/*
				String whereClause = ""; //an empty WHERE clause is possible
				//sql = "CREATE TABLE " + outputTableName + " AS (SELECT * FROM " + inputTableName + " WHERE " + predicate + ")";
				if(predicate.contains("WHERE")) { //WHERE clause and fields clause following SELECT key word
					int index = predicate.indexOf("WHERE");
					if((index + 5) < predicate.length())
						whereClause = predicate.substring(index);  //both WHERE clause and fieldsClause in predicate string
					String fieldsClause = predicate.substring(0, index);
					sql = "SELECT " + fieldsClause + " FROM " + inputTableName + " " + whereClause;	
				}
				else
					sql = "SELECT  * FROM " + inputTableName + " WHERE " + predicate;
				*/
				
				if(predicate.equalsIgnoreCase(""))
					sql = "SELECT " + selectListClause + " FROM " + inputTableName;
				else
					sql = "SELECT " + selectListClause + " FROM " + inputTableName + " WHERE " + predicate;
			}
			else if (op.contains("JOIN")) {  // support EQUI-JOIN only
				
				//ArrayList<Table> inputTables =  getInputTables();	
				//sql = "CREATE TABLE " + outputTableName + " AS SELECT * FROM " + inputTables.get(0).getTableName();  //fence-post
				//fence-post, NO NEED TO CREATE TABLE IN PIPE-LINE
				sql = "SELECT " + selectListClause + " FROM ";
				if(!inputTables.isEmpty()) {
					sql += inputTables.get(0).getTableName();
					for(int i = 1; i < inputTables.size(); i++) {
						sql += ", " + inputTables.get(i).getTableName() ;
					}
				}
				else
					sql += "UNDEFINED";
				
				sql += " WHERE " + predicate;
			}
			else if(op.contains("PROJECT")) {
				if(!inputTables.isEmpty())
					inputTableName = getInputTable(0).getTableName();
				
				sql = "SELECT " + predicate + " FROM " + inputTableName;
			}
			else if (op.contains("ORDER")) {
				
				if(!inputTables.isEmpty())	
					inputTableName = getInputTable(0).getTableName();
				
				//sql = "SELECT * FROM " + inputTableName + " ORDER BY ";
				sql = "SELECT * FROM " + inputTableName + " " + predicate;
				
				/*
				String sortDirection = "UNDEFINED";
				int lastIndex = predicate.length() - 1;
				if(predicate.charAt(lastIndex) == '<')
					sortDirection = "DESC";
				else if (predicate.charAt(lastIndex) == '>')
					sortDirection = "ASC";
				
				int lastCommaIndex = predicate.lastIndexOf(',');
				String ORDER_CLAUSE = predicate.substring(0, lastCommaIndex).trim();
				sql += ORDER_CLAUSE + " " + sortDirection;
				*/
			}
			else if (op.contains("MERGE_SORT"))
				sql = "STREAMING";
				
		return sql;
	}

	@Override
	public String toString() {
		
		String str = "\n\nComputeContainer {  id=" + id + ", type= " + type  
					  + "\n" + state + ", " 
					  + "\nOperator= " + operator
				      + "\nPredicate= " + predicate
				      + "\nSelectListClause= " + selectListClause
				      + "\nDegreesParallel=" + degreesParallel 
				      + "\nProbe=" + probe
				      + "\nParallelEX=" + parallelEX.toString()
				      + "\nInputTables= " +  "\n";
		
		for(Table input: inputTables)
			str += "\t" + input.toString();
		
		str +=  "outputTable= " + outputTable;
		
		String SQL = "UNDEFINED";
		try {
			SQL = getQuery();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		str += "\nSQL= " + SQL + ";  }";
		
		return str; 
	} // end method

} // end ComputeContainer

class DataOnlyContainer extends Container implements Serializable {
	
	private static final long serialVersionUID = -4554289030844512323L;
	private String databaseURI;
	private String table;
	private String[] partitions = null;
	private Schema schema;
	
	public DataOnlyContainer(ContainerType type, QueryDataOnlyNode treeNode) {
		
		super("DC-" + treeNode.id, type);    //prefix container id with "DC-" to label it as data container
		databaseURI = treeNode.getDatabaseURI();
		this.schema = new Schema(treeNode.getTable(), treeNode.getSchema());
		this.table = treeNode.getTable();
		this.partitions = treeNode.getPartitions();
	}

	public String getDatabaseURI() {
		return databaseURI;
	}
	
	public String getTable() {
		return table;
	}
	
	public int getNoPartitions() {
		
		int size = 0;
		
		if(partitions != null)
			size = partitions.length;
			
		return size;
	}

	public String[] getPartitions() {
		return partitions;
	}
	
	public String getPartition(int index) throws Exception {
		if(partitions == null)
			throw new Exception("id " + id + " has NO partitions");
		else
			return partitions[index];
	}

	public String getHostVolume() {
		return databaseURI.substring(0, databaseURI.lastIndexOf('/'));
	}
	
	public String getContainerVolume() {
	
		return databaseURI.substring(databaseURI.indexOf('/'),databaseURI.lastIndexOf('/'));
	}
	
	public String getDataBaseFile() {
		int lastSlash = databaseURI.lastIndexOf('/');
		return databaseURI.substring(lastSlash + 1);
	}

	public Schema getSchema() {
		return schema;
	}
	
	@Override
	public String toString() {
		return "\n\nDataOnlyContainer [ id=" + id + ", type=" + type + ",\n" + databaseURI + ",\n" 
				+ "table=" + table + ", paritions=" + Arrays.deepToString(partitions) + ",\n"
				+ "schema=" + schema + " ]";
	}
	
} // end DataOnlyContainer

/*
 *  A directed edge between two vertices, represented as a pair (V1, V2), where the edge is 
 *  directed from V1 towards V2. 
 */
class Edge implements Serializable {
	
	private static final long serialVersionUID = 232566137818020401L;
	private final Container V1;
	private final Container V2;
	
	
	public Edge(Container v1, Container v2) {
		super();
		V1 = v1;
		V2 = v2;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((V1 == null) ? 0 : V1.hashCode());
		result = prime * result + ((V2 == null) ? 0 : V2.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Edge other = (Edge) obj;
		if (V1 == null) {
			if (other.V1 != null)
				return false;
		} else if (!V1.equals(other.V1))
			return false;
		if (V2 == null) {
			if (other.V2 != null)
				return false;
		} else if (!V2.equals(other.V2))
			return false;
		return true;
	}
	
	public Container getV1() {
		return V1;
	}
	
	public Container getV2() {
		return V2;
	}

	@Override
	public String toString() {
		
		/*
		CharSequence v1 = V1.toString().subSequence(1,V1.toString().indexOf(','));
		CharSequence v2 = V2.toString().subSequence(1,V2.toString().indexOf(','));
		
		return "\n (" + v1 + ",  " + v2 + ")";
		*/
		return "\n (" + V1.id + ",\t" + V2.id + ")";
	}
		
} // end class Edge

class QuerySQL implements Serializable {
	
	private static final long serialVersionUID = -128880298651742165L;
	private String query;
	private ArrayList<String> downstreamContainerIds;
	private String outputTableName;
	private Exchange parallelEX;
	private ComputeContainer cc;
	
	public QuerySQL(ComputeContainer cc, String query, Exchange parallelEX, ArrayList<String> downstreamContainerIds, String outputTableName) {
		super();
		this.cc = cc;
		this.query = query;
		this.downstreamContainerIds = downstreamContainerIds;
		this.outputTableName = outputTableName;
		this.parallelEX = parallelEX;
	}

	public String getQuery() {
		return query;
	}
	
	public Exchange getParallelExchange() {
		return parallelEX;
	}

	public ArrayList<String> getDownstreamContainerIds() {
		return downstreamContainerIds;
	}

	public String getOutputTableName() {
		return outputTableName;
	}

	
	public ComputeContainer get_cc() {
		return cc;
	}
	
	public void executeQuery(java.sql.Connection sqliteConnection , ResultSet rs, int resultSetSize) throws SQLException {
	
		try {
			parallelEX.executeExchange(cc, query, downstreamContainerIds, sqliteConnection, outputTableName);
		} catch (Exception e) {
			Master.logQueryState("executeQuery: FAILED " + e.getMessage(), Log_Level.quiet);
		}
	} // end method

	@Override
	public String toString() {
		
		String destinations = "";
		for(int i = 0; i <  downstreamContainerIds.size(); i++) { 
			destinations += downstreamContainerIds.get(i);
			if(i < (downstreamContainerIds.size() - 1))
				destinations += ", ";  //fence post for last iteration
		}	
			
		return "QuerySQL [container= " + cc.toString() +     "\nquery=" + query + "; \ndestination containers= {" + destinations
				+ "}, outputTableName=" + outputTableName + "]";
	}
	
} // end class

/*
 * Digraph G = <V, E>, where V is a set of vertices and E is a set of edges
 */
class Digraph {
	
	public Digraph() {
		super();
	}

	private LinkedHashSet<Container> vertices = new LinkedHashSet<Container>();
	private LinkedHashSet<Edge> edges = new LinkedHashSet<Edge>();
	
	public LinkedHashSet<Container> getVertices() {
		return vertices;
	}

	public LinkedHashSet<Edge> getEdges() {
		return edges;
	}
	
	
	/* get all leaf compute container nodes attached to Data-Only nodes.
	 * Such nodes execute read data in from their Data-Only nodes */ 
	public ArrayList<ComputeContainer> getLeafComputeVertices() throws Exception {
		
		ArrayList<ComputeContainer> leafComputeVertices = new ArrayList<ComputeContainer>();
		
		for(Edge edge : edges) {
			if(edge.getV1().type == ContainerType.DATA_ONLY)
				leafComputeVertices.add((ComputeContainer) edge.getV2());
		}
		
		if(leafComputeVertices.isEmpty())
			throw new Exception("NO  compute containers attached to any Data-Only Containers");
		else
			return leafComputeVertices;
		
	} // end method
	
	
    public ArrayList<ComputeContainer> getComputeVertices() throws Exception {
		
		ArrayList<ComputeContainer> computeVertices = new ArrayList<ComputeContainer>();
		
		for(Container vertex : vertices) {
			if(vertex.type != ContainerType.DATA_ONLY)
				computeVertices.add((ComputeContainer)vertex);
		}
		
		if(computeVertices.isEmpty())
			throw new Exception("NO compute containers in Digraph");
		else
			return computeVertices;
		
	} // end method
    
    
    public ArrayList<DataOnlyContainer> getDataOnlyVertices() throws Exception {
		
		ArrayList<DataOnlyContainer> dataVertices = new ArrayList<DataOnlyContainer>();
		
		for(Container vertex : vertices) {
			if(vertex.type == ContainerType.DATA_ONLY)
				dataVertices.add((DataOnlyContainer)vertex);
		}
		
		if(dataVertices.isEmpty())
			throw new Exception("NO data only containers in Digraph");
		else
			return dataVertices;
		
	} // end method
    
	
	public boolean isLeafComputeContainer(String containerID) throws Exception {
		
		ArrayList<ComputeContainer> leafContainers = getLeafComputeVertices();
		
		if(leafContainers.contains(getContainerByID(containerID)))
			return true;
		else
			return false;
	}
	
	public Container getRoot() throws Exception {
		
		 ArrayList<Container> list =  new ArrayList<Container>(vertices);
		 for(Edge edge: edges) {
			 
			 if(list.contains(edge.getV1()))
				 list.remove(edge.getV1());
			 
		 } // end for
		 
		 if(list.size() == 1)
			 return list.get(0);
		 else
			 throw new Exception("DIGRAPH: has Multiple ROOT(s)\n" + this.toString() + "\n");
		
	}
	
	public Container getContainerByID(String id) throws Exception {
		
		Container value = null;
		
		for(Container container : vertices)
			if(container.id.equals(id)) {
				value  = container;
				break;
			}
		
		if(value == null)
			throw new Exception("getContainerByID could not find container with ID " + id);
		else
			return value;
		
	} // end method

	/*
	 * returns true if vertex added successfully
	 */
	public boolean addVertex(Container node) {
		
		return vertices.add(node);
		
	}
	
	public boolean addEdge(Container V1, Container V2) {
		
		return edges.add(new Edge(V1, V2));
		
	}
	
	//returns true if edge is located and removed, otherwise false
	//private boolean removeEdge(Container V1, Container V2) throws Exception {
	private boolean removeEdge(Container V1, Container V2) {
		
		boolean flag = false;
		for(Edge e : edges) {
			if(e.getV1().equals(V1) && e.getV2().equals(V2)) {
				edges.remove(e);
				flag = true;
				break;
			}	
		} // end for
		
		/*
		if(!flag)
			throw new Exception("NO SUCH edge for (" + V1.getId() + ", " +  V2.getId() + ")");
	   */	
		
		return flag;
	}
	
	/*
	 * Returns the DataOnlyContainer that lies on the INBOUND edge 
	 * attached to container "node". If no such relation throws Exception.
	 */
	public DataOnlyContainer getAssociatedDataOnlyContainer(ComputeContainer node) throws Exception {
		
		
		if(node.type != ContainerType.COMPUTE  &&  !node.getOperator().toUpperCase().contains("SELECT"))
			throw new Exception("getAssociatedDataOnlyContainer: not computeContainer with operator not 'SELECT'");

		DataOnlyContainer dataOnlyContainer = null;
		
		for(Edge e : edges) {
			
			if(e.getV2().equals(node) && e.getV1() instanceof DataOnlyContainer) {
				dataOnlyContainer = (DataOnlyContainer) e.getV1();
				break;  //should only have ONE associated IN-BOUND edge with Data-Only Container
			}	
		}
		
		return dataOnlyContainer;
			
	} // end method

	
	public LinkedHashSet<Edge> getInboundEdges(Container node) {
		
		LinkedHashSet<Edge> inbounds = new LinkedHashSet<Edge>();
		
		for(Edge e : edges) {
			
			if(e.getV2().equals(node))
				inbounds.add(e);
			
		}
		
		return inbounds;
		
	} // end method
	
	
    public int getNumberInboundEdges(Container node) {
	
    	int count = 0;
    	
		for(Edge e : edges) {
			
			if(e.getV2().equals(node))
				count++;
			
		}
		
		return count;
		
	} // end method
    
	
	public LinkedHashSet<Edge> getOutboundEdges(Container node) {
		
		LinkedHashSet<Edge> outbounds = new LinkedHashSet<Edge>();
		
		for(Edge e : edges) {
			
			if(e.getV1().equals(node))
				outbounds.add(e);
			
		}
		
		return outbounds;
		
	} // end method
	
	/* get immediate parent container */
    public ArrayList<Container> getParents(Container c) {
    	
		
		ArrayList<Container> outbounds = new ArrayList<Container>();
		
		for(Edge e : edges) {
			
			if(e.getV1().equals(c)) {
				outbounds.add(e.getV2())   ;
			}
			
		}
		
		return outbounds;
		
	} // end method
	
	/* get list of ancestor containers on path to root container in digraph */ 
	public ArrayList<Container> getAncestors(Container c) {
		
		ArrayList<Container> ancestorsList = new ArrayList<Container>();
		
		//while((ancestor = getParent(c)) != null) {
		for(Container parent : getParents(c)) {
			
			if(!ancestorsList.contains(parent))  //might be multiple paths to distant ancestor
				ancestorsList.add(parent);
			}
		
		return ancestorsList;	
	}
    
	
	public ArrayList<Container> getSiblings(Container c) {
		
		ArrayList<Container> siblings = new ArrayList<Container>();
		
		//get id base for container c, e.g. C2 --> C2,  {C2-2 ... C2-n} --> C2
		String c_idBase = c.getId();
		int index = c_idBase.indexOf('-');
		if(index != -1) {                 
			c_idBase = c_idBase.substring(0, index); 
		}	
	
		String V1_idBase = "";
		int V1_index = 0;
		for(Edge edge : edges) {
			
			//get  baseId for vertex V1
			V1_idBase = edge.getV1().getId();
			V1_index = V1_idBase.indexOf('-');
		    if(V1_index != -1) {   
		    	V1_idBase = V1_idBase.substring(0, index);
		    }		
					
			//add sibling if base ids  are equal
			if( V1_idBase.equals(c_idBase) )
				siblings.add(edge.getV1()) ;
		}
    	
    	 return siblings;
    	 
    } // end method
	/*
    public ArrayList<Container> getSiblings(Container c) {
    	
    	Container parent = getParent(c);
    	ArrayList<Container> inboundVertices  =  getInboundVertices(parent);
    	 
    	 if(inboundVertices.size() < 2) 
    		 return null;
    	 
    	 ArrayList<Container> siblingsList = new ArrayList<Container>();
    	 
    	 for(Container sibling : inboundVertices) {
    		 if(!sibling.equals(c))
    			 siblingsList.add(sibling); 
    	 }
    	
    	 return siblingsList;
    	 
    } // end method
    */
	
	/* get list of destination containers for a vertex  */
	public ArrayList<Container> getDestinationVertices(Container vertex) {
		
		ArrayList<Container> destinations = new ArrayList<Container>();
		LinkedHashSet<Edge> outbound = getOutboundEdges(vertex);
		
		if (outbound.size() > 0)
			for(Edge e : outbound) 
			     destinations.add(e.getV2());
			
		
		return destinations;
		
	} // end method
	
	/* get list of destination Ids for a vertex  */
	public ArrayList<String> getDestinationIds(Container vertex) {
		
		ArrayList<String> destinationsIds = new ArrayList<String>();
		LinkedHashSet<Edge> outboundEdges = getOutboundEdges(vertex);
		
		if (outboundEdges.size() > 0)
			for(Edge e : outboundEdges) 
			     destinationsIds.add(e.getV2().getId());
			
		
		return destinationsIds;
		
	} // end method
	
	/* get list of destination Ids for a vertex  */
	public ArrayList<String> getInboundIds(Container vertex) {
		
		ArrayList<String> inboundIds = new ArrayList<String>();
		LinkedHashSet<Edge> inboundEdges = getInboundEdges(vertex);
		
		if (inboundEdges.size() > 0)
			for(Edge e : inboundEdges) 
			     inboundIds.add(e.getV1().getId());
			
		return inboundIds;
		
	} // end method
	
	/* get list of immediate in-bound containers for a vertex node */
	public ArrayList<Container> getInboundVertices(Container vertex) {
		
		ArrayList<Container> inbound = new ArrayList<Container>();
		LinkedHashSet<Edge> inboundEdges = getInboundEdges(vertex);
		
		if (inboundEdges.size() > 0) {
			for(Edge e : inboundEdges) 
			     inbound.add(e.getV1());
		}
			
		return inbound;
		
	} // end method
	
	/* get list of immediate out-bound containers for a vertex node */
	public ArrayList<ComputeContainer> getOutboundVertices(Container vertex) {
		
		ArrayList<ComputeContainer> outbound = new ArrayList<ComputeContainer>();
		LinkedHashSet<Edge> outboundEdges = getOutboundEdges(vertex);
		
		if (outboundEdges.size() > 0) {
			for(Edge e : outboundEdges) 
			     outbound.add((ComputeContainer) e.getV2());
		}	
		
		return outbound;
		
	} // end method
	
	/* Add OUT_BOUND Edges from intra-operator containers to  container's parents.
	 * If multiple parents exist, distribute intra-operators' as out-bound according to Exchange protocol. */
	@SuppressWarnings("unused")
	private void addOutBoundEdges(ComputeContainer cc, ArrayList<ComputeContainer> intraOpContainers, 
									Exchange exchange) throws Exception {
		
		ArrayList<Container> parents =  getParents(cc);
		int size = parents.size();
		
		cc.setParallelEX(exchange); //set vertex exchange for original baseId container
		
		//Add OUT_BOUND edges for intra-op containers
		if(size > 1) { //parents(0) denotes original baseId,  with in-bound edge (cc, parents(0))
			int index = 1; //so skip index at 0. Begin at index 1.
			
			while (index != 0) {
				 //remove any  edges created earlier from set = { (ccId, parentBaseId-xx) }
				removeEdge(cc,parents.get(index)); 
				index++;
				index = index % size;
			}
			
			index = 1;
			for (ComputeContainer intraOp: intraOpContainers) {
			
				addEdge(intraOp, parents.get(index));
				index++;
				index = index % size;
			
				intraOp.setParallelEX(exchange); //set vertex exchange
			}
		}
		else if (size == 1) { 
			
			for (ComputeContainer intraOp: intraOpContainers) {
				
				addEdge(intraOp, parents.get(0));
				intraOp.setParallelEX(exchange); //set vertex exchange
			}
		}
		
		/* Only Add IN-BOUND edges to each intra-operator containers if they are leaf nodes */
		if(isLeafComputeContainer(cc.getId()))  
			for(Container inbound : getInboundVertices(cc)) {
			
				for (Container intraOp: intraOpContainers) {
					addEdge(inbound, intraOp);
				}
			}
			
		
	} // end method
	
	/* Adjust DIGRAPH by adding a Vertex and Edges to and from each new intraOp containers. 
	 * The new vertices might include a MERGE vertex, that merges intraOp output. 
	 * */
	public ArrayList<ComputeContainer> addIntraOperators(ComputeContainer cc) throws Exception {
		
		int degrees = cc.getDegreesParallel();
		ArrayList<Container> inboundVertices = getInboundVertices(cc);
		ArrayList<ComputeContainer> intraOpContainers = new ArrayList<ComputeContainer>();
		ArrayList<Container> parents =  getParents(cc);
		
		//create a copy of the original compute container for every added intraOp
		for(int i = 2; i <= degrees; i++) { //each intra-operator container
			
			ComputeContainer copy = cc.copyComputeContainer();
			copy.setId(cc.getId() + "-" + i);  // id-i
			intraOpContainers.add(copy);
			addVertex(copy);
			
		}
		
		// add IN-BOUND edges to each intra-operator container
		for(Container inbound : inboundVertices) {
					
			for (Container intraOp: intraOpContainers) {
				addEdge(inbound, intraOp);
			}
		}

	    //boolean merge = false;  // for ORDER 
	    //ComputeContainer mergeContainer = null;
		//swap default exchange type for  partitioned type of in-bound containers
		
		//if needed, reset default exchange type for  partitioned type of in-bound containers
		if(cc.getOperator().equalsIgnoreCase("ORDER")) {
			
			boolean merge = false;
			ComputeContainer mergeContainer = null;
			
			if( parents.size() == 0 ) {   //add MERGE container as parent
					// add merge container
					merge = true;
					mergeContainer = new ComputeContainer("", ContainerType.MERGE_SORT);
					mergeContainer.setId(cc.getId() + "-MERGE_SORT"); 
					mergeContainer.setOperator("MERGE_SORT");
					//mergeContainer = new ComputeContainer("", ContainerType.MERGE);
					//mergeContainer.setId(cc.getId() + "-MERGE");  // id-MERGE
					//mergeContainer.setOperator("SELECT");
					mergeContainer.setSelectListClause("");
					mergeContainer.setOutputTable(new Table());   //empty OutputTable
					mergeContainer.addInputTable(cc.getOutputTable());
					mergeContainer.setPredicate(cc.getPredicate());  //set sort key and direction in predicate
					//mergeContainer.setPredicate("1 = 1");
					addVertex(mergeContainer);
					addEdge(cc, mergeContainer);  //add new parent as edge from original compute container to MERGE container
			}
		
			addOutBoundEdges(cc, intraOpContainers, new DefaultExchange());
			
			if(merge)
				intraOpContainers.add(mergeContainer); // for subsequent COMPOSE plan generation
			 
			if(inboundVertices.size() > 1)  
			
				for(int i = 0; i < inboundVertices.size(); i++) {
					ComputeContainer inbound = (ComputeContainer)inboundVertices.get(i);
					Exchange exchange = new EvenPartitionExchange();
					exchange.setDegreesParallel(inbound.getDegreesParallel()); //keep same degrees parallel
					inbound.setParallelEX(exchange); //reset vertex exchange
					Master.logQueryState("addIntraOperators: Container " + inbound.getId() +
						", reset parallelEX to EvenPartitionExchange." + inbound.toString(), Log_Level.verbose);
				}
			
		}  //end if "ORDER" 
		else if(cc.getOperator().equalsIgnoreCase("JOIN")) {
			
			for(int i = 0; i < inboundVertices.size(); i++) {
				
				ComputeContainer inbound = (ComputeContainer)inboundVertices.get(i);
				Master.logQueryState("addIntraOperators: Examine inbound container " + inbound.getId() +
						" feeding into JOIN by " + cc.getId() + " for Probe state." + inbound.toString(), Log_Level.verbose);
				
				if(inbound.isProbe()) {
					Exchange exchange = new JoinPartitionExchange();
					
					exchange.setDegreesParallel(inbound.getDegreesParallel()); //keep same degrees parallel
					inbound.setParallelEX( exchange ); //reset vertex exchange
					Master.logQueryState("addIntraOperators: Container " + inbound.getId() +
							", reset parallelEX to JoinPartitionExchange." + inbound.toString(), Log_Level.verbose);
				}
				else
					//else leave default exchange type
					Master.logQueryState("addIntraOperators: Container " + inbound.getId() +
							", exchange type unchanged." + inbound.toString(), Log_Level.verbose);
			} // end for
			
		}  // end else "JOIN"
		/*
		else {
			//other operator: PROJECTION, SELECTION
			addOutBoundEdges(cc, intraOpContainers, new DefaultExchange());
			
		}
		*/
			
		
		// Add OUT_BOUND Edges from intra-operator containers to  container's parents.
		// If multiple parents exist, distribute intra-operators' as out-bound edges evenly among parents.
		// But first remove all edges = { (ccId, parentBaseId-xx) } created  processing parent intra-ops up the tree.
		
		// Add and Adjust OUT_BOUND Edges from intra-operator containers to  container's parents (round-robin).
		// If multiple parents exist, distribute intra-operators' as out-bound edges evenly among parents.
		// But first remove all edges = { (ccId, parentBaseId-xx) } created  processing parent intra-ops up the tree. 
		int size = parents.size();
		Container parent = null;
		int index;
		if(size > 1) { //parents(0) denotes original query tree's baseId,  with in-bound edge (cc, parents(0))
				index = 1; //so skip index at 0. Begin at index 1.
					
				while (index != 0) {
					//remove any  edges created earlier from set = { (ccId, parentBaseId-xx) }
					removeEdge(cc, parents.get(index)); 
					index++;
					index = index % size;
				}
					
				//assign all new dynamicIntraOps out-bound edges round-robin to original parents
				index = 1;
				for (ComputeContainer intraOp: intraOpContainers) {
					
					parent = parents.get(index);
					addEdge(intraOp, parent);
					index++;
					index = index % size;
					
					//intraOp.setParallelEX(cc.getParallelEX());
				}
				
				//TODO: may need to also update Parent's in-bound containers
				//@SuppressWarnings("unused")
				//ArrayList<String> parentInboundIds = getInboundIds(parent);
				
		}
		else if (size == 1) { 
				index = 0;
					
				for (ComputeContainer intraOp: intraOpContainers) {
						
					addEdge(intraOp, parents.get(0));
				}
		}
		
		return intraOpContainers;
		
	} // end method
  
	
	/* Adjust DIGRAPH by adding a Vertex and Edges to and from each new dynamic intraOp containers. 
	 * The new vertices might include a MERGE vertex, that merges intraOp output. 
	 * Parameter 'NoDynamicIntraOps' indicates that the intrOps are being added dynamically at runtime. 
	 * */
	public ArrayList<ComputeContainer> addDynamicIntraOperators(ComputeContainer cc, int NoDynamicIntraOps) throws Exception {
		
		//int degrees = cc.getDegreesParallel();
		ArrayList<Container> inboundVertices = getInboundVertices(cc);
		ArrayList<ComputeContainer> dynamicIntraOpContainers = new ArrayList<ComputeContainer>();
		ArrayList<Container> parents =  getParents(cc);
		
		if(NoDynamicIntraOps > 0) {
			//create a copy of the original compute container for every added intraOp
			for(int i = 0; i < NoDynamicIntraOps; i++) { //each intra-operator container
			
				ComputeContainer copy = cc.copyComputeContainer();
				copy.setId(cc.getId() + "-DynamicIntraOp" + i);
				dynamicIntraOpContainers.add(copy);
				addVertex(copy);
			}
		} 
		
		// add IN-BOUND edges to each intra-operator container, alternatively out-bound edges for children of cc
		for(Container inbound : inboundVertices) {
			
			for (Container intraOp: dynamicIntraOpContainers) {
				addEdge(inbound, intraOp);
			}
		}
		
		boolean merge = false;  // for ORDER 
		ComputeContainer mergeContainer = null;
		//swap default exchange type for  partitioned type of in-bound containers
		if(cc.getOperator().equalsIgnoreCase("ORDER")) {
			
			if(inboundVertices.size() >  1)  {
			
				for(int i = 0; i < inboundVertices.size(); i++) {
					ComputeContainer inbound = (ComputeContainer)inboundVertices.get(i);
					Exchange exchange = new EvenPartitionExchange();
					exchange.setDegreesParallel(inbound.getDegreesParallel()); //keep same degrees parallel
					inbound.setParallelEX(exchange); //reset vertex exchange
					Master.logQueryState("addIntraOperators: Container " + inbound.getId() +
						", reset parallelEX to EvenPartitionExchange." + inbound.toString(), Log_Level.verbose);
				}
			}
			
			if(parents.size() == 0) { //i.e. has NO parent, add MERGE container as parent
				// add merge container
				merge = true;
				mergeContainer = new ComputeContainer("", ContainerType.MERGE_SORT);
				mergeContainer.setId(cc.getId() + "-MERGE_SORT"); 
				mergeContainer.setOperator("MERGE_SORT");
				mergeContainer.setSelectListClause("");
				mergeContainer.setOutputTable(new Table());   //empty OutputTable
				mergeContainer.addInputTable(cc.getOutputTable());
				mergeContainer.setPredicate(cc.getPredicate());  //set sort key and direction in predicate
				addVertex(mergeContainer);
				addEdge(cc, mergeContainer);  //add new parent as edge from original compute container to MERGE container
				parents =  getParents(cc);    //assign merge node to parents list
			}
			
		}  //end if "ORDER" 
		else if(cc.getOperator().equalsIgnoreCase("JOIN")) {
			
			for(int i = 0; i < inboundVertices.size(); i++) {
				
				ComputeContainer inbound = (ComputeContainer)inboundVertices.get(i);
				Master.logQueryState("addIntraOperators: Examine inbound container " + inbound.getId() +
						" feeding into JOIN by " + cc.getId() + " for Probe state." + inbound.toString(), Log_Level.verbose);
				
				if( inbound.isProbe() ) {
					Exchange exchange = new JoinPartitionExchange();
					exchange.setDegreesParallel(inbound.getDegreesParallel()); //keep same degrees parallel
					inbound.setParallelEX( exchange ); //reset vertex exchange
					Master.logQueryState("addIntraOperators: Container " + inbound.getId() +
							", reset parallelEX to JoinPartitionExchange." + inbound.toString(), Log_Level.verbose);
				}
				else
					//else leave default exchange type
					Master.logQueryState("addIntraOperators: Container " + inbound.getId() +
							", exchange type unchanged." + inbound.toString(), Log_Level.verbose);

			} // end for
		}  // end else "JOIN"
		
		
		//Cases for merging other than ORDER
		if(parents.size() == 0) { //add MERGE container as parent
			// add merge container
			merge = true;
			mergeContainer = new ComputeContainer("", ContainerType.MERGE);
			mergeContainer.setId(cc.getId() + "-MERGE");  
			mergeContainer.setOperator("SELECT");
			mergeContainer.setSelectListClause("*");
			mergeContainer.setOutputTable(new Table());   //empty OutputTable
			mergeContainer.addInputTable(cc.getOutputTable());
			mergeContainer.setPredicate("1 = 1");
			addVertex(mergeContainer);
			addEdge(cc, mergeContainer);  //add new parent edge from original compute container to MERGE container
			parents =  getParents(cc);    //assign merge node to parents list
		}
		
		
		// Add and Adjust OUT_BOUND Edges from intra-operator containers to  container's parents (round-robin).
		// If multiple parents exist, distribute intra-operators' as out-bound edges evenly among parents.
		// But first remove all edges = { (ccId, parentBaseId-xx) } created  processing parent intra-ops up the tree. 
		int size = parents.size();
		int index;
		if(size > 1) { //parents(0) denotes original query tree's baseId,  with in-bound edge (cc, parents(0))
			index = 1; //so skip index at 0. Begin at index 1.
			
			while (index != 0) {
				 //remove any  edges created earlier from set = { (ccId, parentBaseId-xx) }
				removeEdge(cc,parents.get(index)); 
				index++;
				index = index % size;
			}
			
			//assign all new dynamicIntraOps round-robin to original parents
			index = 1;
			for (ComputeContainer intraOp: dynamicIntraOpContainers) {
			
				addEdge(intraOp, parents.get(index));
				index++;
				index = index % size;
			
				//intraOp.setParallelEX(cc.getParallelEX());
			}
		}
		else if (size == 1) { 
			index = 0;
			
			for (ComputeContainer intraOp: dynamicIntraOpContainers) {
				
				addEdge(intraOp, parents.get(0));
			}
		}
	
		if(merge)
			dynamicIntraOpContainers.add(mergeContainer); // for subsequent COMPOSE plan generation
		
		return dynamicIntraOpContainers;
		
	} // end method addIntraOperators
	
	
	// Adjust DIGRAPH by adding a Vertex and Edges to and from each new intraOp containers cloned from 'cc',
	// where 'cc' is a leaf node.
	public ArrayList<ComputeContainer> addLeafIntraOperators(ComputeContainer cc) throws Exception  {
		//cc is a leaf "SELECT" container that reads in a relation, composed of partitions
		
		int degrees = cc.getDegreesParallel();
		ArrayList<ComputeContainer> outboundVertices = getOutboundVertices(cc);
		
		DataOnlyContainer dc = null;
		//Get the in-bound relation,  with partitions. Assume only one data-only container 
		try {
			dc = getAssociatedDataOnlyContainer(cc);
			if(dc.getNoPartitions() != degrees)
				throw new Exception("FATAL ERROR: dataOnly container # of partitions different from compute container's degree");
		} catch (Exception e) {
			Master.logQueryState("addLeafIntraOperators: Container " + cc.getId() +
					", has NO associated relation or wrong # of partitions. " + e.getLocalizedMessage(), Log_Level.verbose);
			throw e;
		}
			
		ArrayList<ComputeContainer> intraOpContainers = new ArrayList<ComputeContainer>();
		//CHANGE cc's table name to be the first partition of the relation
		Table table = cc.getInputTable(0);  //Should be only a single table, divided into partitions
		String orginalTableName = table.getTableName();
		table.setTableName(dc.getPartition(0));
	
	
		//create a copy of the original compute container for every added intraOp
		for(int i = 2; i <= degrees; i++) { //each intra-operator container
			
			ComputeContainer copy = cc.copyComputeContainer();
			copy.setId(cc.getId() + "-" + i);  // id-i
			
			//set copy's table name to matching partition name
			String partitionName = dc.getPartition(i - 1);
			copy.getInputTable(0).setTableName(partitionName);
			
			//Change the predicate to reflect the change in table name, based upon the correct partition name
			String modifiedPredicate = cc.getPredicate().replaceAll(orginalTableName, partitionName);
			copy.setPredicate(modifiedPredicate);
			
			//Change the selectListClause to reflect the change in table name, based upon the correct partition name
			String modifiedSelectListClause = cc.getSelectListClause().replaceAll(orginalTableName, partitionName);
			copy.setSelectListClause(modifiedSelectListClause);
			
			intraOpContainers.add(copy);
			addVertex(copy);   //add vertex to graph
			
		}
				
		//Change the predicate to reflect the change in table name for original container cc
		String modifiedPredicate = cc.getPredicate().replaceAll(orginalTableName, dc.getPartition(0));
		cc.setPredicate(modifiedPredicate);
		
		//Change the selectListClause to reflect the change in table name, based upon  partition[0] 
		String modifiedSelectListClause = cc.getSelectListClause().replaceAll(orginalTableName, dc.getPartition(0));
		cc.setSelectListClause(modifiedSelectListClause);
		
	
		/* OPTIMIZATION for leaf/reader nodes:  if # of intra-ops == (outboundVertices -1), then set each intra-op  
		 * exchange type to DefaultExchange and create an edge to one node in the list of cc's out-bound nodes. 
		 * Also Adjust cc's exchange type also to DefaultExchange
		 * and reduce its number of out-bound edges to one out-bound node. That is cc and its intra-ops
		 * all have a single edge to only one of the original out-bound nodes.  
		 */
		if(intraOpContainers.size() == (outboundVertices.size() -1)) {

			for(int i = 0; i <  (outboundVertices.size() -1); i++) {
				ComputeContainer intraOp = intraOpContainers.get(i);
				ComputeContainer outBound = outboundVertices.get(i + 1);
				addEdge(intraOp, outBound);  // the single edge
				if(outBound.getId().contains("-"))
					removeEdge(cc, outBound);   //remove edge from original operator
				addEdge(dc, intraOp);  	// in-bound edge is data container with partitions, making intraOp a leafNode
				intraOp.setParallelEX(new DefaultExchange()); //reset vertex exchange
			}
			
			cc.setParallelEX(new DefaultExchange()); //reset original operator/cc exchange
		}
		else if (intraOpContainers.size() > (outboundVertices.size() -1)) {

			// add IN-BOUND edges from DataOnlyContainer to each intra-operator container. 
			//This has the effect of adding each intra-operator as a new 'leaf' node.
			//Case where dc is build relation in parallel join
			for (Container intraOp: intraOpContainers) {
				addEdge(dc, intraOp);
			}
		
			if(outboundVertices.size() == 0) {   //DO NOTHING 
				/*
				//add MERGE container as parent
				ComputeContainer merge = new ComputeContainer("", ContainerType.MERGE);
				merge.setId(cc.getId() + "-MERGE");  // id-MERGE
				merge.setOperator("SELECT");
				merge.setSelectListClause("");
				merge.setOutputTable(new Table());   //empty OutputTable
				merge.addInputTable(cc.getOutputTable());
				merge.setPredicate("1 = 1");
				addVertex(merge);
					
				addEdge(cc, merge);  //add edge from original compute container to MERGE
				
				for (Container intraOp: intraOpContainers) {   //edges from intra-ops to merge container
						addEdge(intraOp, merge);
				}
					
				intraOpContainers.add(merge); // for subsequent COMPOSE plan generation
				*/
			
			}
			else if ( outboundVertices.size() == 1) {
				
				// add OUT-BOUND edges to ach intra-operator container
				for(Container outbound : outboundVertices) {	

					for (Container intraOp: intraOpContainers) {
						addEdge(intraOp, outbound);
					}
				}
				
			} else {  //  (outboundVertices > 1)   AND   (intraOpContainers.size() > outboundVertices.size() -1)
				
				// add OUT-BOUND edges to intra-operator containers
				// depends on how containers are named:   C1, C-2, C3, C4 ...
				for (int i = 0; i < intraOpContainers.size(); i++) {	
					
					//int j = (i + 3) % outboundVertices.size();
					int j = i % outboundVertices.size();
					
					addEdge(intraOpContainers.get(i), outboundVertices.get(j) );
					
					ComputeContainer outBound = outboundVertices.get(j);
					if(outBound.getId().contains("-"))
						removeEdge(cc, outBound);   //remove edge from original operator
				}
				
			}
		
		}
		
		return intraOpContainers;
		
	} // end method  addLeafIntraOperators()	

	@Override
	public String toString() {
		
		StringBuffer verticesSet = new StringBuffer("{");
		
		for(Container c : vertices) {
			verticesSet.append(c.getId() + ", ");
		}
		verticesSet.delete(verticesSet.length() -2, verticesSet.length());
		verticesSet.append("}");
		
		return "Digraph<V,E> {\n\n VERTICES: " + verticesSet.toString()  + "\n" + vertices + ",\n\n EDGES:\n" + edges + "\n\n}";
	}
	
}  // end class Digraph


enum CTRL_MSG_TYPE implements Serializable {

	EDGE, LOG, HEART_BEAT, READY, QUERY, DYNAMIC
	
} // end enumeration

class CtrlMsg implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private CTRL_MSG_TYPE type;

	public CtrlMsg(CTRL_MSG_TYPE type) {
		super();
		this.type = type;
	}
	
	public CTRL_MSG_TYPE getType() {
		return type;
	}

	@Override
	public String toString() {
		return "CtrlMsg [type=" + type + "]";
	}
	
} // end class CtrlMsg

class HeartBeatCtrlMsg extends CtrlMsg implements Serializable {
		
	private static final long serialVersionUID = -1008756273790071146L;
	
	private String containerId;

	public HeartBeatCtrlMsg(String containerId) {
		super(CTRL_MSG_TYPE.HEART_BEAT);
		this.containerId = containerId;
	}

	public String getContainerId() {
		return containerId;
	}

	@Override
	public String toString() {
		return "HeartBeat [containerName=" + containerId + "]";
	}
	
} // end class HeartBeatCtrlMsg

class DynamicCtrlMsg extends CtrlMsg implements Serializable {
	
	private static final long serialVersionUID = 7320357038063806239L;
	
	private String containerId;
	private int NoIntraOps;

	public DynamicCtrlMsg(String containerId, int NoIntraOps) {
		super(CTRL_MSG_TYPE.DYNAMIC);
		this.containerId = containerId;
		this.NoIntraOps = NoIntraOps;
	}

	public String getContainerId() {
		return containerId;
	}

	public int getNoIntraOps() {
		return NoIntraOps;
	}
	
	@Override
	public String toString() {
		return "Dynamic Intra-op Request [containerName=" + containerId + ", NoIntraOps=" + NoIntraOps + "]";
	}
	
} // end class HeartBeatCtrlMsg

class ReadyCtrlMsg extends CtrlMsg implements Serializable  {
	
	private static final long serialVersionUID = -4256867388114365205L;
	private String containerId;

	public ReadyCtrlMsg(String containerId) {
		super(CTRL_MSG_TYPE.READY);
		this.containerId = containerId;
	}

	public String getContainerId() {
		return containerId;
	}

	@Override
	public String toString() {
		return super.toString() + " [containerNameId=" + containerId + "]";
	}
	
} // end class ReadyCtrlMsg

class QueryCtrlMsg extends CtrlMsg implements Serializable {
	
	private static final long serialVersionUID = -4657846391579461414L;
	private String containerId;
	private QuerySQL payload;

	public QueryCtrlMsg(String containerId, QuerySQL payload) {
		super(CTRL_MSG_TYPE.QUERY);
		this.containerId = containerId;
		this.payload = payload;
	}

	public String getContainerId() {
		return containerId;
	}

	public QuerySQL getPayload() {
		return payload;
	}

	@Override
	public String toString() {
		return super.toString() + " [containerNameId=" + containerId + ", " + payload +  "]";
	}
	
} // end class ReadyCtrlMsg


class LogCtrlMsg extends CtrlMsg  implements Serializable {
	
	private static final long serialVersionUID = 2913669485821575149L;
	
	String containerId;
	String logMessage;

	public LogCtrlMsg(String containerId, String logMessage) {
		super(CTRL_MSG_TYPE.LOG);
		this.containerId = containerId;
		this.logMessage = logMessage;
	}	

	public String getContainerId() {
		return containerId;
	}

	public String getLogMessage() {
		return logMessage;
	}

	@Override
	public String toString() {
		return super.toString() + " [Sent by Worker= " + containerId + ", logMessage=\n" + logMessage + "]";
	}
	
} // end class LogCtrlMsg

/* Helper class used to ACKNOWLEDGE completion of socket communication between containers.
 * The message is sent by the destination container acknowledging receipt of message
 * and successful completion of processing. Origin denotes the container that sent a
 * message to the Destination container. This information is needed by the Master 
 * when inspecting query progress and the digraph.
*/
class EdgeCtrlMsg extends CtrlMsg implements Serializable {

	private static final long serialVersionUID = -5809838392402603902L;
	
	private String originId;
	private String destinationId;

	public EdgeCtrlMsg(String originId, String destinationId, String msg) {
		super(CTRL_MSG_TYPE.EDGE);
		this.originId = originId;
		this.destinationId = destinationId;
	}

	public String getOriginId() {
		return originId;
	}

	public String getDestinationId() {
		return destinationId;
	}

	@Override
	public String toString() {
		return super.toString() + " [Sent by Worker= " + originId + ":  Edge(" + originId + ", " +  destinationId + ")]";
	}
		
} // end class Edge

class ConvertBytes {
	
	public  byte[] convertToBytes(Object object) throws IOException  {
		
		if(!(object instanceof Serializable))
			throw new NotSerializableException(object.toString());
		
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		ObjectOutput out = new ObjectOutputStream(bos);
		out.writeObject(object);		
			
		return bos.toByteArray();
		
	} // end method

	public  Object convertFromBytes(byte[] bytes) throws IOException, ClassNotFoundException {
		
		ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
		ObjectInput in = new ObjectInputStream(bis);
		
		return in.readObject();
		 
	}
	
}// end class 

interface Parallel extends Serializable {
	
	abstract public void  executeExchange(ComputeContainer cc, String query, List<String> downstreamContainers,
							java.sql.Connection sqliteConnection, String outputTableName) throws Exception;
	public int getDegreesParallel();
	public void setDegreesParallel(int degreesParallel);
	abstract public String toString();
		
} // end class


abstract class Exchange implements Parallel, Constants {
	
	private static final long serialVersionUID = -2610708876425256732L;
	protected int degreesParallel = 1;
	protected boolean DEBUG = false;
	
	public void setDegreesParallel(int degreesParallel) {
		this.degreesParallel = degreesParallel;
	}
	
	abstract public void executeExchange(ComputeContainer cc, String query, List<String> downstreamContainers, 
														java.sql.Connection sqliteConnection,
														String outputTableName) throws Exception;
		
	public int getDegreesParallel() {
		return degreesParallel;
	}
	
	 //java.sql.Types are int constants
	public int[] getJavaSqlTypes(ResultSet rs) throws SQLException {
		
		ResultSetMetaData metaData = rs.getMetaData();
		int colCount = metaData.getColumnCount();
		int[] sqlTypes = new int[colCount];     
		for(int col = 0; col < colCount; col++) {
			sqlTypes[col] = metaData.getColumnType(col + 1);   //java.sql.Types are int constants
			
		}
		
		return sqlTypes;
		
	} // method getJavaSqlTypes
	
	public int getQuerySize(ComputeContainer cc, String query, Statement statementSQL, String exchange) throws SQLException {

		int count = 0;
		ResultSet countRS = null;
		try {
			// get query size with  COUNT(*) 
			//SELECT COUNT(*) AS ResultSetSize FROM ( <YOUR_ORIGINAL QUERY_STRING>)
			String queryCount = "SELECT COUNT(*) AS count "  + query.substring(query.indexOf("FROM"));
			Worker.logQueryState( "getQuerySize::  container " + cc.getId() + 
					", attempting  query " + queryCount,  Log_Level.verbose );
			statementSQL.setQueryTimeout(60);
			countRS = statementSQL.executeQuery(queryCount );
			if(countRS.next()) {
				count = countRS.getInt(1);
			}
			else  
				Worker.logQueryState(exchange + ": executeExchange :  container " + cc.getId() + 
						", obtaining  query " + query + " size with COUNT(*) FAILED",  Log_Level.quiet);

		} catch (SQLException e ) { 
			Worker.logQueryState(e.getMessage(), Log_Level.quiet);
			throw e;
		}
		finally {
			countRS.close();
			statementSQL.close();
		}

		return count;
		
	} //end method getQuerySize
	
	/* Constructs a CREATE TABLE DDL statement string from  meta-data in a ResultSet.
	 * The table name is supplied as a parameter: outputTableName.
	 * A column name is obtained from meta-data using its column identifier, 
	 * e.g for the table FOO with two columns, {'id', 'name'} : CREATE TABLE FOO (id int(8), name VarChar)
	 * This permits the TABLE's columns to be traced back to the original relation/table
	 * has the query progresses thru the relational query tree.
	 */
	public String createTable_DDL(ResultSetMetaData schema, String outputTableName) throws SQLException {

		long threadId = Thread.currentThread().getId();
		String SQLstr = "CREATE TABLE ";
		
		try {
			//String tableName = rs.getMetaData().getTableName(1);
			SQLstr += outputTableName + " (";
			int count = schema.getColumnCount();
			
			String colName = "";
			for (int i = 1; i <= count; i++) {
			
				colName = schema.getColumnName(i);
				SQLstr +=  colName + " " + schema.getColumnTypeName(i);
				
				if( !(schema.getPrecision(i) == 0))
					SQLstr +=  "(" + schema.getPrecision(i) + ")";
                /*				
				if( (schema.isNullable(i) == ResultSetMetaData.columnNoNulls)) //indicate field is NOT NULL
					SQLstr +=  "  NOT NULL";
                */
				
				if (i == count) // exit and fence post
					SQLstr += ")";
				else
					SQLstr += ", ";
				
			} // end for
			
		} catch (SQLException e) {
			e.printStackTrace();
			Worker.logQueryState("createTable_DDL, threadId " + threadId + ": " + e.getMessage(), Log_Level.quiet);
			throw e;
		}

		return SQLstr;
	} // end method  createTable_DDL
	
	
	/* prepared statement for INSERT.
	 * INSERT INTO <tableName>  VALUES(?, ?); */
	public String createPreparedStmtString(ResultSetMetaData schema, String outputTableName)
			throws SQLException {
		
		long threadId = Thread.currentThread().getId();
		String SQL_INSERT = "INSERT INTO ";

		try {
			//str += rs.getMetaData().getTableName(1) + " VALUES(";
			SQL_INSERT += outputTableName + " VALUES(";
			int colCount = schema.getColumnCount();

			for (int i = 1; i <= colCount; i++) {
				SQL_INSERT += "?";
				if (i == colCount) // exit and fence post condition
					SQL_INSERT += ");";
				else
					SQL_INSERT += ", ";
			} // end for
			
		} catch (SQLException e) {
			e.printStackTrace();
			Worker.logQueryState("createPreparedStmtString, threadId " + threadId + ": " + e.getMessage(), Log_Level.quiet);
			throw e;
		}

		return SQL_INSERT;
	} // end method createPreparedStmtString
	
	/* query has timed-out, divide current container into intra-op containers
	 * 
	 */
	public void requestDynamicIntraOps(String containerID, java.sql.Statement statement, 
					java.sql.Connection sqliteConnection, String query, int SimulateNoDynamicIntraOps) {
		
		DynamicCtrlMsg dynamic =  null;
		int NoDynamicIntraOps = -1;
		
		try {
			
			if(SimulateNoDynamicIntraOps != 0)
				NoDynamicIntraOps = SimulateNoDynamicIntraOps;    //Simulate # of dynamic intra-ops needed
			else {
				//get row and time estimation for query to complete
				//statement.setQueryTimeout(0); // reset to NO query time-out limit
				int clusterSize = Integer.valueOf(System.getenv("SWARM_CLUSTER_SIZE"));
				//  0 <=  sampleSize <= 1
				double sampleSize = Double.valueOf(System.getenv("SAMPLE_SIZE"));
			
				//get estimates of TimeToCompleteQuery, SizeOfRS, SampleSize
				int[] estimates = ContainerizedQueryUtils.estimateRowsInResultSet(query, sampleSize, sqliteConnection);
				int estimatedTimeToCompleteQuery = estimates[0];
				@SuppressWarnings("unused")
				int estimatedSizeOfRS = estimates[1];
				int estimatedSampleTime = estimates[2];   //in seconds
				// assume # of current intra-ops is to complete entire query in the time the sample completed
				NoDynamicIntraOps = estimatedTimeToCompleteQuery / estimatedSampleTime;
			
				//No of dynamic intra-ops is constrained by the number of existing containers. Empirical observation
				//suggests that concurrent intra-ops begin losing efficiency (an inflection point) when more
				//than 4 containers are executing concurrently on a cluster VM
				if(NoDynamicIntraOps > clusterSize * 4) {
					int concurrentTime = estimatedTimeToCompleteQuery / (estimatedSampleTime * clusterSize * 4);
					Worker.logQueryState("requestDynamicIntraOps: Exchange, estimated time for " + 
						NoDynamicIntraOps + " concurrent intr-ops to execute complete query is " +
						concurrentTime, Log_Level.verbose);
					NoDynamicIntraOps = clusterSize * 4;
				} else 
					Worker.logQueryState("requestDynamicIntraOps: Exchange, estimated time for " + 
						NoDynamicIntraOps + " concurrent intr-ops to execute complete query is " +
						estimatedSampleTime, Log_Level.verbose);
			}//end else
			
			//SEND request to Master for additional dynamic intra-ops  to be created to for this worker 
			dynamic = new DynamicCtrlMsg(containerID, NoDynamicIntraOps);
			ConvertBytes converter = new ConvertBytes();
			Worker.channelToRabbit.basicPublish(ALL_WORKERS_EXCHANGE, ACK_KEY, null,
					converter.convertToBytes(dynamic));
			Worker.logQueryState("requestDynamicIntraOps: DynamicCtrlMsg sent to  Master:  " + dynamic.toString(), 
							Log_Level.verbose);
		} catch (IOException ioe) {
			Worker.logQueryState("requestDynamicIntraOps: FAILED: DynamicCtrlMsg to  Master:  " + dynamic.toString()
					+  ", " + ioe.getMessage(), Log_Level.quiet);
			ioe.printStackTrace();
		} catch (SQLException sqle) {
			Worker.logQueryState("requestDynamicIntraOps: FAILED: DynamicCtrlMsg to  Master:  " 
									+ sqle.getMessage(), Log_Level.quiet);
			sqle.printStackTrace();
		}
		
		
	} // method
	
	public ResultSet getRS_wthTimeOut(ComputeContainer cc, String exchangeType, java.sql.Statement statement, 
			java.sql.Connection sqliteConnection, String query) throws TimeOutException, SQLException {
		
		String ccId = cc.getId();
		boolean dynamic_intra_ops_allowed = Boolean.valueOf(System.getenv("DYNAMIC_INTRA_OPS_ALLOWED"));
		int timeOut = Integer.valueOf(System.getProperty("TIME_OUT"));
		ResultSet rs = null;
		
		if (dynamic_intra_ops_allowed && !ccId.contains("DynamicIntraOP") && timeOut != 0) {
			
			statement.setQueryTimeout(timeOut);
			Worker.logQueryState(exchangeType + " executeExchange :  container " + ccId + 
					", Query TIME_OUT set to  :  " + timeOut, Log_Level.verbose);
		
			/*
			String CACHE_SIZE = System.getenv("CACHE_SIZE");
			String PAGE_SIZE = System.getenv("PAGE_SIZE");
		
			if(!CACHE_SIZE.equals("NOMINAL"))       //set CACHE_SIZE
				statement.executeUpdate("PRAGMA cache_size=" + CACHE_SIZE);
			
			if(!PAGE_SIZE.equals("NOMINAL"))	//set PAGE_SIZE
				statement.executeUpdate("PRAGMA page_size=" + PAGE_SIZE);
			
			rs = statement.executeQuery("PRAGMA cache_size"); //Report db CACHE and PAGE sizes
			rs.next();
			int cache_size = rs.getInt(1);
			rs.close();
			Worker.logQueryState("main: CACHE_SIZE= " + cache_size, Log_Level.verbose);
				
			rs = statement.executeQuery("PRAGMA page_size");
			rs.next();
			int page_size = rs.getInt(1);
			rs.close();
			Worker.logQueryState("main: PAGE_SIZE= " + page_size, Log_Level.verbose);
			*/
			
			/*
			String JDBC_URL = System.getProperty("JDBC_URL");  //Kludge to flush in memory cache to file based db 
			if(!JDBC_URL.contains(":memory:")) {   			   //slows down query, allowing time-out to occur 
				sqliteConnection.commit();  //write any pending transactions
				sqliteConnection.close();   //close out in-memory tables, including cache
				sqliteConnection = Worker.connectSQLiteDB(JDBC_URL); //reconnect to file based db
				statement = sqliteConnection.createStatement();      //recreate statement
				Worker.logQueryState(exchangeType + " executeExchange :  container " + ccId + 
						", reset SQLite connection to  :  " + JDBC_URL, Log_Level.verbose);
			}
			*/
			
		} else {
			statement.setQueryTimeout(0);
			//TODO: reset NONIMAL CACHE and PAGE sizes
		}
	
		statement.setMaxRows(0);
		
		try {
			
			//if(noDynamicntraOps != 0)    			//simulate TIME_OUT
			if(timeOut != 0 && dynamic_intra_ops_allowed)    {					//simulate TIME_OUT
				Worker.logQueryState("METRIC: getRS_wthTimeOut:  SIMULATE TIME_OUT on query at time " +  System.currentTimeMillis() + ", " 
						+ ccId, Log_Level.metrics);
				throw new SQLTimeoutException();
			}
			else {
			
				Worker.logQueryState("METRIC: getRS_wthTimeOut:  START query at time " +  System.currentTimeMillis() + ", " 
					+ ccId, Log_Level.metrics);
				rs =  statement.executeQuery(query);  /* get full ResultSet*/
				Worker.logQueryState("METRIC: getRS_wthTimeOut:  COMPLETE query at time " +  System.currentTimeMillis() + ", " 
					+ ccId, Log_Level.metrics);
			}
			
		} catch (SQLTimeoutException e) {
			
			Worker.logQueryState("METRIC: getRS_wthTimeOut:  query TIMED_OUT at " +  System.currentTimeMillis() + ", " 
					+ ccId, Log_Level.metrics);
			
			//reset property TIME_OUT = 0 for subsequent calls to DefaultExchange.
			//Only want query to time-out ONCE, then IntraOps are added
			System.setProperty("TIME_OUT", "0");
			
			//Drop local tables, as they will be resent by input sources.
			ArrayList<Table> tables = cc.getInputTables();
			for(Table t : tables) {
				try {
					statement.executeUpdate("DROP TABLE " + t.getTableName());
				} catch(SQLException sql) {
					Worker.logQueryState("DefaultExchange executeExchange :  container " + ccId + 
							", ERROR DROPPING TABLE " + t.toString() + ", " + sql.getMessage(), Log_Level.verbose);
				}
				Worker.logQueryState("DefaultExchange executeExchange :  container " + ccId + 
						", DROPPED TABLE " + t.toString(), Log_Level.verbose);
			}
			
			//query timed-out, divide current container up into intra-op containers
			Worker.logQueryState("DefaultExchange executeExchange :  container " + ccId + 
					", requestDynamicIntraOps() called for query:  " + query, Log_Level.verbose);
			
			int noDynamicntraOps = Integer.valueOf(System.getenv("NO_DYNAMIC_INTRA_OPS"));
			requestDynamicIntraOps(ccId, statement, sqliteConnection, query, noDynamicntraOps);
			
			//NO FURTHER PORCESSING UNTIL REPLY FROM MASTER to add additional intra-ops to digraph
			throw new TimeOutException();
		}
		
		return rs;
		
	} // method
	
	
	
} // end class Exchange


class ExchangeServerRunnable implements Runnable {
	
	private Connection conn;
	private int rsSize;
	private String workerId;
	
	public  ExchangeServerRunnable(Connection conn, int rsSize, String workerId)
	{	
		this.conn = conn;
		this.rsSize = rsSize;
		this.workerId = workerId;	
	}

	public void run() {
		
		String exchangeQueue = "exchangeQueue_" + workerId; 
		
		try {
            final Channel ch = conn.createChannel();
            
            ch.queueDeclare(exchangeQueue, false, false, false, null);
            
            StringRpcServer server = new StringRpcServer(ch, exchangeQueue)  { //anonymous class
            	
                    public String handleStringCall(String request) {
                        System.out.println(workerId + " exchangeQueue Got request: " + request);
                        terminateMainloop();
                        return Integer.toString(rsSize);
                    }
                };
                
            server.mainloop();
            
        } catch (Exception ex) {
            System.err.println(" ExchangeServerRunnable thread caught exception: " + ex);
            Worker.logQueryState(workerId + " ExchangeServerRunnable thread caught exception: " + ex, Log_Level.quiet);
            System.exit(1);
        }
	
	} // end run
	
} // end class

class DefaultExchange extends Exchange {
	
	private static final long serialVersionUID = 953030401685874808L;

	@Override
	public void executeExchange(ComputeContainer cc, String query, List<String> downstreamContainers, 
					java.sql.Connection sqliteConnection, String outputTableName) throws Exception  {
		
		java.sql.Statement statement = sqliteConnection.createStatement();
		ResultSet rs  = null;
		
		try { 
			
			Worker.logQueryState("DefaultExchange executeExchange :  container " + cc.getId() + 
					", executing query '" + query + "'. Sending to downstreamContainers:  \n" +
					downstreamContainers.toString(), Log_Level.verbose);
			
			statement = sqliteConnection.createStatement();
		
			//query might time-out and throw TimeOutException, which could prompt creation of dynamic intra-ops
			rs = getRS_wthTimeOut(cc, "DefaultExchange", statement, sqliteConnection, query);
			
			rs.setFetchSize(Constants.FETCH_SIZE * 16);  //16k rows
				
			if(DEBUG) {
			    Worker.logQueryState("DefaultExchange executeExchange :  container " + cc.getId() + 
						", ResultSet column count:  " +  rs.getMetaData().getColumnCount(), Log_Level.verbose);
				Worker.printResultSetTable(rs, query, 100, true);
				rs.close();
				rs =  statement.executeQuery(query);	
			}
			
			String SQL_CreateTable = createTable_DDL(rs.getMetaData(), outputTableName);
			String SQL_INSERT = createPreparedStmtString(rs.getMetaData(), outputTableName);
			
			int firstParen = SQL_CreateTable.indexOf('(');
			String schema = SQL_CreateTable.substring(firstParen);
			cc.setOutputSchema(schema);   //update container's Output schema
			
			Worker.logQueryState("DefaultExchange executeExchange :  container " + cc.getId() + 
					"\n, created DDL: '" + SQL_CreateTable + "'.\n PreparedStm: " + SQL_INSERT, Log_Level.verbose);
		     
		    if(downstreamContainers.size() > 1) {
		    	
		    	int colCount = rs.getMetaData().getColumnCount();
		    	int[] sqlTypes = getJavaSqlTypes(rs);
		    	
		    	ArrayList<Vector<Object>> rowList = new ArrayList<Vector<Object>>();
				
				while( rs.next() ) {  //populate ArrayList of rows

					Vector<Object> row = new Vector<Object>(colCount);
					for(int i = 1; i <= colCount; i++)
						row.add(rs.getObject(i));

					rowList.add(row);
				}
				
				//rs.close();
				
			    for(String downstream : downstreamContainers) {  //send the contents of rowList to every downstreamId
			    	
			    	OutputRowListRunnable  runnable = new  OutputRowListRunnable(downstream, rowList, cc.getId(),
							Constants.DATA_PORT, outputTableName, SQL_CreateTable, SQL_INSERT, sqlTypes);

			    	runnable.start();
			    
			    }
		    } // if  > 1
		    else if( downstreamContainers.size() == 1 ) {
		    	
		    	OutputResultSetRunnable runnable = new OutputResultSetRunnable(downstreamContainers.get(0), rs, cc.getId(), 
						Constants.DATA_PORT, outputTableName, SQL_CreateTable, SQL_INSERT);
		    	
		    	runnable.start();
		    	runnable.join();
		    }
		    else
		    	throw new Exception("DefaultExchange FAULT, downstreamContainer size = " + downstreamContainers.size());
		    
		} catch (TimeOutException toe) {
			//query timed-out, divide current container up into intra-op containers
			Worker.logQueryState("DefaultExchange executeExchange :  container " + cc.getId() + 
					", TimeOutException caught and rethrown:  " + query, Log_Level.verbose);
			throw toe;  //re-throw exception
		} catch (Exception e) {
			Worker.logQueryState("DefaultExchange executeExchange FAILED: " + cc.getId() + 
					" Xfering results to downstream container {" +
					downstreamContainers + "}: " +  e.getMessage() +"\n" + e.getClass().getCanonicalName(), Log_Level.quiet);
			e.printStackTrace();
			throw new Exception("DefaultExchange FAILED: " +  e.getMessage());
		} finally {
            if(rs != null)
            	rs.close();
			statement.close();
		}
		
		
		Worker.logQueryState("DefaultExchange executeExchange :  container " + cc.getId() + 
				", exchanging ResultSet with " + downstreamContainers.size() + " downstreamContainers", Log_Level.quiet);
    	/*
		Worker.logQueryState("DefaultExchange executeExchange: " + cc.getId() + 
								" Output entire CRS  of size= " + size + " to each downstream container {" +
								downstreamContainers + "} using TCP/IP sockets", Log_Level.quiet);
		*/		
		
	}	// end method
	
	
	public String toString() {
		//return  "DefaultExchange: degreesParallel " + degreesParallel;
		return  "DefaultExchange ";
	} // end method

} // end class  DefaultExchange

//divides table into even # of partitions =  # of downstreamContainers, if table probe attribute = true
class JoinPartitionExchange extends Exchange implements Constants {
	
	private static final long serialVersionUID = -7802460066186135473L;
	
	public JoinPartitionExchange() {
	}	
		
	//@Override
	public void executeExchange(ComputeContainer cc, String query, List<String> downstreamContainers, 
					java.sql.Connection sqliteConnection, String outputTableName) throws Exception {

		String workerId = cc.getId();
		
		/* get full ResultSet*/
		ResultSet rs =  sqliteConnection.createStatement().executeQuery(query);
		rs.setFetchSize(FETCH_SIZE); //how many rows to fetch at a time from disk
		ResultSetMetaData meta = rs.getMetaData();
		int colCount = meta.getColumnCount();
		String SQL_CreateTable = createTable_DDL(meta, outputTableName);
		String SQL_INSERT = createPreparedStmtString(meta, outputTableName);
		
		int firstParen = SQL_CreateTable.indexOf('(');
		String schema = SQL_CreateTable.substring(firstParen);
		cc.setOutputSchema(schema);
		
		try {

			int[] sqlTypes = getJavaSqlTypes(rs);
			
			if(cc.isProbe()) { //partition RS and send each downstream container a partition

				//int totalCount = getQuerySize(cc, query,  sqliteConnection.createStatement(), "JoinPartitionExchange");
			
				ArrayList<Vector<Object>> rowList = new ArrayList<Vector<Object>>();
				
				int elements = 1;
				while( rs.next()) {

					Vector<Object> row = new Vector<Object>(colCount);
					for(int i = 1; i <= colCount; i++)
						row.add(rs.getObject(i));

					rowList.add(row);
					
					elements++;

				} // end while
				
				rs.close();
				
				int PARTITION_SIZE = (int) Math.ceil( (double)elements / (double)downstreamContainers.size() );
				int fromIndex = 0;
				int toIndex = PARTITION_SIZE;
				
				int totalCount = elements;
				
				for(String downstreamId : downstreamContainers) {
				
					Worker.logQueryState("JoinPartitionExchange executeExchange :  Probe table  " + outputTableName + 
										" to be partitioned: PARITION_SIZE=" + PARTITION_SIZE, Log_Level.quiet);
				
					ArrayList<Vector<Object>> rowSubList = new ArrayList<Vector<Object>>( rowList.subList(fromIndex, toIndex));

					OutputRowListRunnable runnable = new  OutputRowListRunnable(downstreamId, rowSubList, workerId,
															Constants.DATA_PORT, outputTableName, SQL_CreateTable, 
															SQL_INSERT, sqlTypes);
					runnable.start();
					
				} // end for
				
			   
				//Worker.logQueryState("METRIC: WORKER FINISH_TIME, "+  System.currentTimeMillis() + ", " + cc, Log_Level.metrics);
				
				Worker.logQueryState("JoinPartitionExchange executeExchange: " + cc.getId() + 
						" Output ResultSet partitions to downstream containers {" 
						+ downstreamContainers + "} using TCP/IP sockets for original RS size " + totalCount, Log_Level.quiet);
			} 
			else
				Worker.logQueryState("JoinPartitionExchange executeExchange NOT PROBE ", Log_Level.verbose);
	
		} catch (Exception e) {
		 
			ByteArrayOutputStream stream = new ByteArrayOutputStream();
			PrintWriter pw = new PrintWriter(stream);
			e.printStackTrace(pw);
			pw.flush();
			pw.close();
			rs.close();
			
			Worker.logQueryState("JoinPartitionExchange executeExchange FAILED:  " +  e.toString() + ". \n" + 
									e.getClass().getCanonicalName() + "\n" + stream.toString(), Log_Level.quiet);
			throw e;  //re-throw Exception	
		}

	} // end method
	
	
	public String toString() {
		//return  "JoinPartitionExchange: degreesParallel " + Integer.valueOf(degreesParallel);
		return  "JoinPartitionExchange ";
	}
	
} // end class JoinPartitionExchange


//divides table into even # of partitions =  # of downstreamContainers
class EvenPartitionExchange extends Exchange {

	private static final long serialVersionUID = -8276392300235733170L;

	//divides the table into the number of partitions == downstreamContainers
	@Override
	public void executeExchange(ComputeContainer cc, String query, List<String> downstreamContainers, 
								java.sql.Connection sqliteConnection, String outputTableName) throws Exception {

		String workerId = cc.getId();
		
		/* get full ResultSet*/
		ResultSet rs =  sqliteConnection.createStatement().executeQuery(query);
		String SQL_CreateTable = createTable_DDL(rs.getMetaData(), outputTableName);
		String SQL_INSERT = createPreparedStmtString(rs.getMetaData(), outputTableName);
		
		int firstParen = SQL_CreateTable.indexOf('(');
		String schema = SQL_CreateTable.substring(firstParen);
		cc.setOutputSchema(schema);
		
		int rowCount = 0;
		
		try {
			
			int[] sqlTypes = getJavaSqlTypes(rs);

			int colCount = rs.getMetaData().getColumnCount();
			
			//int count  = getQuerySize(cc, query,  sqliteConnection.createStatement(), "EvenPartitionExchange");

			//divide rs into #partitions == #downstreamContainers and send to each downstream container a partition
			//write entire CRS into an ArrayList, each row in the list is a Vector of Objects,
			//with each Object being the value of a row column, Java maps SQL type to and from SQL types
			ArrayList< Vector<Object> > rowList  = new ArrayList< Vector<Object> >();
			
			while(rs.next()) {

				Vector<Object> row = new Vector<Object>(colCount);
				for(int i = 1; i <= colCount; i++)
					row.add(rs.getObject(i));

				rowList.add(row);
				rowCount++;
			}

			Worker.logQueryState("EvenPartitionExchange executeExchange :  container " + cc.getId() + 
					", exchanging ResultSet with size " + rowCount, Log_Level.quiet);
			
			rs.close();
			
			int PARTITION_SIZE = (int) Math.ceil( (double)rowCount / (double)downstreamContainers.size() );
			int fromIndex = 0;
			int toIndex = PARTITION_SIZE;

			for(String downstreamId : downstreamContainers) {

				ArrayList<Vector<Object>> rowSubList = new ArrayList<Vector<Object>>( rowList.subList(fromIndex, toIndex));

				OutputRowListRunnable runnable = new  OutputRowListRunnable(downstreamId, rowSubList, workerId,
															Constants.DATA_PORT, outputTableName, SQL_CreateTable, 
															SQL_INSERT, sqlTypes);
				runnable.start();
	
				//adjust indexes for next partition
				fromIndex += PARTITION_SIZE;
				toIndex += PARTITION_SIZE;
				if(toIndex > rowCount)  //boundary case
					toIndex = rowCount;

			} // end for
			
		}
		catch (Exception e) {
			Worker.logQueryState("EvenPartitionExchange executeExchange FAILED:  " +  e.getMessage() + ", " 
					+ e.getClass().getCanonicalName(), Log_Level.quiet);
			e.printStackTrace();
			rs.close();
			throw e;
		} finally {
				Worker.logQueryState("EvenPartitionExchange executeExchange: Output ResultSet partitions to downstream containers {" +
						downstreamContainers + "} using TCP/IP sockets with RS size " + rowCount, Log_Level.quiet); 
		}

	} // end method
	
	public String toString() {
		//return  "EvenPartitionExchange: degreesParallel " + Integer.valueOf(degreesParallel);
		return  "EvenPartitionExchange ";
	}
	
} // end class EvenPartitionExchange

/*
class SelectExchange extends Exchange {
	
	public SelectExchange() {
		degreesParallel = 1;
	}
	
	private static final long serialVersionUID = 5157770776100281261L;

	@Override
	public void executeExchange(ComputeContainer cc, String query, List<String> downstreamContainers, java.sql.Connection sqliteConnection, 
								String outputTableName) throws SQLException {
		// get full ResultSet
		//CachedRowSet crs = null;
		ResultSet rs =  sqliteConnection.createStatement().executeQuery(query);
		String SQL_CreateTable = createTable_DDL(rs.getMetaData(), outputTableName);
		String SQL_INSERT = createPreparedStmtString(rs.getMetaData(), outputTableName);
		
		try {

			rs =  sqliteConnection.createStatement().executeQuery(query);
			//crs = new CachedRowSetImpl();
			//crs.populate(rs);
			String workerId = cc.getId();
		    
			OutputResultSetRunnable runnable = new OutputResultSetRunnable(downstreamContainers.get(0), rs, workerId, 
														Constants.DATA_PORT, outputTableName, SQL_CreateTable, SQL_INSERT);
			runnable.start();
			runnable.join();
		    rs.close();
		    
		} catch (Exception e) {
			e.printStackTrace();
			rs.close();
		} 
		
		Worker.logQueryState("executeExchange: Output ResultSet partitions to downstream containers {" + downstreamContainers 
								+ "} using TCP/IP sockets", Log_Level.quiet);	
		
	}	// end method
	
	public String toString() {
		//return  "Select: degreesParallel " + degreesParallel;
		return  "Select ";
	}
	
} // end class SelectExchange
*/

class ProjectExchange extends Exchange {

	private static final long serialVersionUID = -1074865370398615916L;
	
	public ProjectExchange() {
		degreesParallel = 1;
	}

	@Override
	public void executeExchange(ComputeContainer cc, String query, List<String> downstreamContainers, java.sql.Connection sqliteConnection, 
								String outputTableName) throws SQLException, InterruptedException  {
		/* get full ResultSet*/
		//CachedRowSet crs = null;
		ResultSet rs = null;
		
		try {
			ArrayList<Thread> threads = new ArrayList<Thread>();
			
			rs =  sqliteConnection.createStatement().executeQuery(query);
			//crs = new CachedRowSetImpl();
			//crs.populate(rs);
			String SQL_CreateTable = createTable_DDL(rs.getMetaData(), outputTableName);
			String SQL_INSERT = createPreparedStmtString(rs.getMetaData(), outputTableName);
			
		    String workerId = cc.getId();
			//int PAGE_SIZE = (int) Math.ceil( (double)size / (double)downstreamContainers.size() ); 
			//crs.setPageSize(PAGE_SIZE);
		    
		    for(String downstream : downstreamContainers) {
		    	OutputResultSetRunnable runnable = new OutputResultSetRunnable(downstream, rs, workerId, 
														Constants.DATA_PORT, outputTableName, SQL_CreateTable, SQL_INSERT);
		    	Thread t = new Thread(runnable);
		    	t.start();
		    	threads.add(t);
		    }
		    
		    //wait for all downstream threads to complete before continuing and CLOSING the CRS
		    for (Thread thread : threads) {
		    	  thread.join();
		    }
		    
		} finally  {
			 try {
				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
	}	// end method
	
	public String toString() {
		//return "Project: degreesParallel " + degreesParallel;
		return "Project ";
	}
	
} // end class ProjectExchange

/*
class ParallelOperatorFactory implements Serializable {

	private static final long serialVersionUID = -90260505363443775L;

	public static Exchange createParallelExchangeOperator(String operator) throws  IllegalArgumentException {

		if (operator.equalsIgnoreCase ("JOIN")){
			return new JoinPartitionExchange();
		}
		else if(operator.equalsIgnoreCase ("ORDER")){
			return new EvenPartitionExchange();
		}
		else if(operator.equalsIgnoreCase ("SELECT")){
			return new SelectExchange();
		}
		else if(operator.equalsIgnoreCase ("PROJECT")){
			return new ProjectExchange();
		}
		else if(operator.equalsIgnoreCase ("DEFAULT")){
			return new DefaultExchange();
		}
		else
			throw new IllegalArgumentException("No such operator type");
	}
	
} // end class
*/

/*
class ParallelChannel implements Serializable {

	private static final long serialVersionUID = 3161376348866690512L;
	
	// connection to remote container used for parallel intra-operator  
	
}
*/


interface ObjectSerializer<T extends Serializable> {
	
	byte[] toBytes(T obj) throws IOException;
	
	T fromBytes(byte[] obj) throws IOException, ClassNotFoundException;
}

class CompressedSerlializer<T extends Serializable> implements ObjectSerializer<T> {

	@Override
	public byte[] toBytes(T obj) throws IOException {
		if(obj != null) {
			try (ByteArrayOutputStream bos = new ByteArrayOutputStream();
					GZIPOutputStream zos = new GZIPOutputStream(bos);
					ObjectOutputStream oos = new ObjectOutputStream(zos)) {
				oos.writeObject(obj);
				oos.flush();
				zos.finish();
				return bos.toByteArray();
			}
		} else {
			return null;
		}
	} // end method

	@SuppressWarnings("unchecked")
	@Override
	public T fromBytes(byte[] obj) throws IOException, ClassNotFoundException {
		if(obj != null) {
			try (ObjectInputStream ois =
					new ObjectInputStream(
							new GZIPInputStream(
									new ByteArrayInputStream(obj)))) {
				return (T)ois.readObject();
			}
		} else {
			return null;
		}
	}  // end method
	
} // end class CompressedSerlialize


class QueryLogger implements Constants {
	
	Logger logger = null;
    Channel  channelToRabbit = null;
    Log_Level LOG_LEVEL = null;
    boolean ContainerMode = true;
    String workerID = "UNDEFINED";
    
	public QueryLogger(Channel channelToRabbit, Logger logger, Log_Level lOG_LEVEL, boolean containerMode, String workerID) {
		super();
		this.logger = logger;
		this.channelToRabbit = channelToRabbit;
		LOG_LEVEL = lOG_LEVEL;
		ContainerMode = containerMode;
		this.workerID = workerID;
	}

	public  void logQueryState(String msg, Log_Level logLevel) {

		try { 
					
			if ( (channelToRabbit == null || !ContainerMode) && (logLevel.ordinal() >= LOG_LEVEL.ordinal()) ) {
				
				logger.info(msg);	
			}
			else if (logLevel.ordinal() >= LOG_LEVEL.ordinal()) {
				
				logger.info(msg);
				
				/* also send LOG CTRL message to Master logger */
				LogCtrlMsg logCtrlMsg = new LogCtrlMsg(workerID, msg); 
				ConvertBytes converter = new ConvertBytes();  
				channelToRabbit.basicPublish(ALL_WORKERS_EXCHANGE, ACK_KEY, null, converter.convertToBytes(logCtrlMsg));
			} 
			
		} catch (IOException e) {
			e.printStackTrace();
			logger.info(msg);
		}

	} // end logQueryState

		
}   // end class QueryLogger


class TimeOutException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 362694166704983598L;
	
}

class ContainerComp implements Comparator<ComputeContainer> {
	 
    @Override
    public int compare(ComputeContainer c1, ComputeContainer c2) {
        return c1.getId().compareTo(c2.getId());
    }
   
} 


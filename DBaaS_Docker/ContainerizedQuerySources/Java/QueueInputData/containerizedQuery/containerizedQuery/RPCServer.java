package containerizedQuery;

import com.rabbitmq.client.*;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class RPCServer {

    private String RPC_QUEUE_NAME = "rpc_queue_";
  
    public RPCServer(String workerId) {
		super();
		RPC_QUEUE_NAME = RPC_QUEUE_NAME + workerId ;
	}

	public void send_SCRS_sizeToSibling (Connection rabbitConnection, String SiblingKey, final int crsSize) throws IOException, TimeoutException {
    	
    	Channel channel;
        
        try {
           
            channel = rabbitConnection.createChannel();

            channel.queueDeclare(RPC_QUEUE_NAME, false, false, false, null);

            channel.basicQos(1);
       
            System.out.println(" [x] Awaiting Sibling request");

            Consumer consumer = new RPC_DefaultConsumer(channel, crsSize);

            channel.basicConsume(RPC_QUEUE_NAME, false, consumer); //blocks 
            
            channel.close();
            channel.queueDelete(RPC_QUEUE_NAME);
            System.out.println(" [x] RPC Server and queue " + RPC_QUEUE_NAME + " closed.");

        } finally {
        	
        }
        
    } // end getSiblingCRS_size()
    
} // end class RPCServer

class RPC_DefaultConsumer extends DefaultConsumer {
	
	private int crsSize;
	
	RPC_DefaultConsumer(Channel channel, int crsSize) {
		super(channel);
		this.crsSize = crsSize;
	}

    @Override
    public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
        AMQP.BasicProperties replyProps = new AMQP.BasicProperties
                .Builder()
                .correlationId(properties.getCorrelationId())
                .build();

        String response = "";

        try {
            String SiblingId = new String(body,"UTF-8");

            System.out.println(" sending CRS size " + crsSize + " to Sibling " + SiblingId);
            response += Integer.toString(crsSize);
        }
        catch (RuntimeException e){
            System.out.println(" SCRS_sizeToSibling ERROR: " + e.toString());
        }
        finally {
        	super.getChannel().basicPublish( "", properties.getReplyTo(), replyProps, response.getBytes("UTF-8"));

        	super.getChannel().basicAck(envelope.getDeliveryTag(), false);
        }
    }
    
} // end DefaultConsume
import com.rabbitmq.client.*;
import java.io.IOException;

public class Worker1 {

  private static String workerQueueName;
  private static String rabbitBrokerIP;

  public static void main(String[] argv) throws Exception {

    if(argv.length != 2 || !validIP(argv[0])) {

      System.out.println("Usge: Recv <IP> <queueName>");
      System.exit(1);
    }

    rabbitBrokerIP = argv[0];   //the IP address where rabbitmq broker is located
    workerQueueName = argv[1]; //assign worker's queue name

    ConnectionFactory factory = new ConnectionFactory();
    factory.setHost(rabbitBrokerIP);
    Connection connection = factory.newConnection();
    Channel channel = connection.createChannel();

    channel.queueDeclare(workerQueueName, false, false, false, null);
    System.out.println(" [*] Waiting for control messages. To exit press CTRL+C");

    //consume Callback
    Consumer consumer = new DefaultConsumer(channel) {

      private String message = "";  //message received by consumer

      @Override
      public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body)
          throws IOException {

        message = new String(body, "UTF-8");
        System.out.println(" [x] Received '" + message + "'");

        //TAKE ACTION based upon msg HERE, and send ack back to master
        try { //ack that message was received and acted upon; this syncs action w/ msg
          actAndAck(rabbitBrokerIP, workerQueueName, message);
        }
        catch(Exception e) {
          System.out.println("sendAckToMaster FAILED: " + e.toString());
        }

        System.out.println(" [*] Waiting for control messages. To exit press CTRL+C");

      } //end method

      public String getMessage() {
        return message;
      }

    }; //end anonymous class

    //get message off queue; starts consumer listener
    channel.basicConsume(workerQueueName, true, consumer);

  } // end main

  //acknowledge receipt of message and that any associated action has been taken
  private static void actAndAck(String brokerIP, String worker, String msgRecvd) throws Exception {

    final String ACK_QUE = "ACK_QUE";   //queue on master receives acknowledgments from workers
    ConnectionFactory factory = new ConnectionFactory();
    factory.setHost(brokerIP);
    Connection connection = factory.newConnection();
    Channel channel = connection.createChannel();

    //TAKE SOME ACTION BASED ON MSG CONTENT
    //TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO

    //acknowledge completion of message receipt and action taken
    //channel.queueDeclarePassive(ACK_QUE);
    channel.queueDeclare(ACK_QUE, false, false, false, null);
    String ack = worker + " : received : " + msgRecvd + " : action taken complete";
    channel.basicPublish("", ACK_QUE, null, ack.getBytes("UTF-8"));
    System.out.println(" [X] Sent Ack back to master:   '" + ack + "'");

    channel.close();
    connection.close();

  }

 private static boolean validIP (String ip) {
    try {
        if ( ip == null || ip.isEmpty() ) {
            return false;
        }

        String[] parts = ip.split( "\\." );
        if ( parts.length != 4 ) {
            return false;
        }

        for ( String s : parts ) {
            int i = Integer.parseInt( s );
            if ( (i < 0) || (i > 255) ) {
                return false;
            }
        }
        if ( ip.endsWith(".") ) {
            return false;
        }

        return true;
    } catch (NumberFormatException nfe) {
        return false;
    }
  } // end validIP

} // end class

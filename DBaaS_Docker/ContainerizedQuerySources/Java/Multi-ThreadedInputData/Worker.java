import com.rabbitmq.client.*;
import java.io.*;
import java.net.InetAddress;
import java.util.*;
import java.util.logging.*;

public class Worker implements Constants {

	// global class variables used as CONTSTANTS
	private static String workerName;
	private static String workerType;
	private static String workerQueueName;
	private static String rabbitBrokerIP;
	private static Logger logger;
	private static FileHandler fh = null;

	public static void main(String[] argv) throws Exception {

		// set global logger handle
		startLogger("./Worker.log");

		// validate command line arguments
		validateArgs(argv);
		
		/* Open connection to rabbitMQ broker, declare queues and exchanges.
		 * Bind queues to exchanges. Wait for directions from Master sent
		 * to the worker queue using rabbit. */
		openRabbitConnections();

	} // end main

	/*
	 * Open a socket server that accepts socket requests from database
	 * input providers, which are results produced from downstream query
	 * operators in other containers. The server socket accepts a 
	 * data producer's socket connection request and opens a socket
	 * to the producer in a separate new thread.
	 */
	public static void openDataChannels() {
		
	}
	
	// Execute the command sent by the Master controller.
	private static String execControlMsg(String msg) {

		// String cmd = "/bin/bash -c \"" + msg + "\"";
		String[] cmd = { "/bin/bash", "-v", "-c", msg };
		// String cmd = "/bin/bash -c \"sqlite3 /join_tables/students.db 'SELECT
		// * FROM Students;'\"";

		String outcome = "UNDEFINED";
		execute: try {
			Process p = Runtime.getRuntime().exec(cmd);
			// System.out.println("after exec()");

			BufferedReader bri = new BufferedReader(new InputStreamReader(p.getInputStream()));
			BufferedReader bre = new BufferedReader(new InputStreamReader(p.getErrorStream()));
			// System.out.println("exec() Readers open ");

			String line;
			while ((line = bri.readLine()) != null) {
				// System.out.println("InputStream readline(): ");
				// System.out.println(line);
				logger.info(line);
			}
			bri.close();
			// System.out.println("exec() InputStream closed ");

			while ((line = bre.readLine()) != null) {
				// System.out.println("ErrorStream readline(): ");
				// System.out.println(line);
				logger.info(line);
			}
			bre.close();
			// System.out.println("exec() ErrorStream closed ");

			if (p.waitFor() != 0) {
				// System.out.println("FAILED: " + Arrays.toString(cmd) );
				logger.info("FAILED: " + Arrays.toString(cmd));
				outcome = "FAILED: " + Arrays.toString(cmd);
				break execute;
			} else {
				// System.out.println("SUCCESSFULLY EXECUTED: " +
				// Arrays.toString(cmd));
				logger.info("SUCCESSFULLY EXECUTED: " + Arrays.toString(cmd));
				outcome = "SUCCESSFULLY EXECUTED: " + Arrays.toString(cmd);
			}

			// System.out.println("Done.");
			logger.info("Done. " + outcome);
		} catch (Exception err) {
			err.printStackTrace();
		}

		return outcome;

	} // end method

	// Execute control message and acknowledge action has been taken by worker.
	private static void actAndACK(Channel channelToRabbit, String msgRecvd) throws Exception {

		// EXECUTE CONTROL MSG CONTENT
		String outcome = execControlMsg(msgRecvd);

		// acknowledge completion of message receipt and action taken
		String ACK = workerQueueName + " : received : " + msgRecvd + " : action taken completed with outcome: "
				+ outcome;
		channelToRabbit.basicPublish(ALL_WORKERS_EXCHANGE, ACK_QUE, null, ACK.getBytes("UTF-8"));
		// System.out.println(" [X] Sent ACK back to master: '" + ACK + "'");
		logger.info(" [X] Sent ACK back to master:   '" + ACK + "'");

	} // end actAndACK
	
	/*
	 * Given path to log file, sets global logger handle
	 */
	public static void startLogger(String logFilePath) throws Exception {

		// set up logger
		logger = Logger.getLogger("MyLogger");

		try {
			// This block configures the logger handler and sets its formatter
			fh = new FileHandler(logFilePath);
			logger.addHandler(fh);
			SimpleFormatter formatter = new SimpleFormatter();
			fh.setFormatter(formatter);

			// start log
			logger.info("Start Worker log");

		} catch (SecurityException se) {
			se.printStackTrace();
			throw se;
		} catch (IOException ioe) {
			ioe.printStackTrace();
			throw ioe;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}

	} // end method

	public static void validateArgs(String[] argv) throws Exception {

		// check correct number of arguments
		if (argv.length != 3) {
			// command line must provide the <rabbit broker IP>, the worker's
			// queue name,
			// and an exchange key to route messages to the worker type:
			// {shuffle, join, merger}
			System.out.println("Usage: Worker <rabbitmq broker IP> <workerName> <exchange key (workerType)>");
			logger.info("Usage: Worker <rabbitmq broker IP> <workerName> <rabbit exchange key (workerType)>");
			logger.info("# command line args found: " + argv.length);
			for (int i = 0; i < argv.length; i++) {
				logger.info("argv[" + i + ":  " + argv[i]);
			}
			fh.close();
			throw new Exception("wrong number of command line arguments");
		} // end if

		// GET VALID IP for rabbitMQ broker server
		// argv[0] holds the IP address where rabbitMQ broker is located
		if (!validIP(argv[0])) {
			logger.info(argv[0] + " not VALID IP literal.  Attempting to resolve to valid IP");
			try {
				InetAddress address = InetAddress.getByName(argv[0]);
				// the IP address where rabbitMQ broker is located
				rabbitBrokerIP = address.getHostAddress(); // set class variable
			} catch (Exception e) {
				logger.info(argv[0] + e.getMessage());
				logger.info("attempt getting host address for " + argv[0] + " FAILED");
				fh.close();
				throw new Exception("invalid IP for rabbit broker");
			}
		} // end if
		
		logger.info("IP address " + argv[0] + " assigned as rabbitBrokerIP");

		// set global class constants and validate exchange key
		workerName = argv[1]; // worker's name
		workerQueueName = argv[1]; // local worker's queue name identical to
									// worker's name
		workerType = argv[2]; // worker type one of {"shuffle", "join", "merge"}

		// validate rabbitMQ exchange key passed as command line argument
		if (!exchangeKeys.contains(workerType)) {
			System.out.println("INVLALID <exchange key> given as 3rd command line argument");
			System.out.println("Must be one of the following keys: " + exchangeKeys.toString());
			logger.info("INVLALID <exchange key> given as 3rd command line argument");
			logger.info("Must be one of the following keys: " + exchangeKeys.toString());
			fh.close();
			System.exit(1);
		}

	} // end method

	/*
	 * Opens connection to rabbitMQ broker, declares queues and exchanges. Binds
	 * queues to specific exchanges for purposes of multi-cast and unicast
	 * control messaging
	 */
	public static void openRabbitConnections() throws InterruptedException, IOException {

		// connect to rabbitMQ broker, this may take some time if broker in
		// Master is still trying to start.
		Connection rabbitConnection = null;
		int count = 0;
		while (true) { // keep trying to connect to rabbitMQ broker
			try {
				// open channel to rabbit broker
				ConnectionFactory factory = new ConnectionFactory();
				factory.setHost(rabbitBrokerIP);
				factory.setPort(5672);
				factory.setUsername("guest");
				factory.setPassword("guest");
				rabbitConnection = factory.newConnection();
				break;
			} catch (Exception e) {
				logger.info("Attempt opening connection to rabbitMQ broker. " + e.toString());
				logger.info("Attempt opening connection to rabbitMQ broker. " + e.getMessage());
				Thread.sleep(1000);
				count++;
				if (count > 20) {
					logger.info("Attempt opening connection to rabbitMQ broker for twenty seconds timed-out.");
					System.exit(1);
				} else
					continue;
			} // end try
		} // end while

		// create channel to remote rabbit broker
		final Channel channelToRabbit = rabbitConnection.createChannel();

		// DECLARE rabbitMQ QUEUES and EXCHANGES. Bind queues to exchanges.
		// Master may not have yet declared Exchange, so declare just in case.
		channelToRabbit.exchangeDeclare(ALL_WORKERS_EXCHANGE, "direct");

		// Master may not have yet declared ACK_QUE, so declare just in case
		channelToRabbit.queueDeclare(ACK_QUE, false, false, false, null);
		// Use ACK_QUE key to send messages to ACK_QUE queue, which acknowledges
		// to master receipt of commands and messages sent by master to a 
		// worker. Bind ACK_QUE to ALL_WORKERS_EXCHANGE.
		channelToRabbit.queueBind(ACK_QUE, ALL_WORKERS_EXCHANGE, ACK_QUE);

		// Create new queue specific for THIS worker instance.
		channelToRabbit.queueDeclare(workerQueueName, false, false, false, null);

		// MULTI-CAST to all workers of same TYPE a message from queue specific
		// to THIS worker instance by binding local worker queue to
		// ALL_WORKERS_EXCHANGE. Worker's specific TYPE is one of 
		// {"shuffle", "join", "merge"} used as an EXCHANGE KEY.
		channelToRabbit.queueBind(workerQueueName, ALL_WORKERS_EXCHANGE, workerType);

		// MULTI_CAST sent to all workers a message from queue specific to THIS
		// worker instance  by binding local worker queue with 
		// worker type "worker", for messages sent to all workers.
		channelToRabbit.queueBind(workerQueueName, ALL_WORKERS_EXCHANGE, "worker");

		// UNI-CAST
		// Also bind THIS worker queue with worker's unique name so a
		// message based upon its name can be sent to this worker only.
		channelToRabbit.queueBind(workerQueueName, ALL_WORKERS_EXCHANGE, workerName);

		System.out.println(" [*] Waiting for control messages. To exit press CTRL+C\n");
		logger.info(" [*] Waiting for initial control messages. To exit press CTRL+C");

		// Anonymous Inner class  overrides rabbitMQ DefaultConsumer and
		// executes consumer Callback when message arrives.
		Consumer THISconsumerWorker = new DefaultConsumer(channelToRabbit) {

			private String message = ""; // message received by THIS worker

			@Override
			public void handleShutdownSignal(java.lang.String consumerTag, ShutdownSignalException sig) {

				logger.info(workerName + " listener received shutDown signal");
				fh.close();
			}

			@Override
			public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties,
					byte[] body) throws IOException {

				message = new String(body, "UTF-8");
				// System.out.println(" [x] Received '" + message + "'");
				logger.info(" [x] Received '" + message + "'");

				// TAKE ACTION based upon msg HERE, and send ACK back to master
				try { 
					// acknowledge that message was received and acted upon.
					// This allows the master synchronize command execution
					// order.
					actAndACK(channelToRabbit, message);
				} catch (Exception e) {
					// System.out.println("sendAckToMaster FAILED: " +
					// e.toString());
					logger.info("sendAckToMaster FAILED: " + e.toString());
				}

				// System.out.println(" [*] Waiting for control messages. To
				// exit press CTRL+C");
				logger.info(" [*] Waiting for control messages. To exit press CTRL+C");

			} // end method

			@SuppressWarnings("unused")
			public String getMessage() {
				return message;
			} // end handleDelivery

		}; // end anonymous class

		// Start consumer message queue listener and servicing messages.
		channelToRabbit.basicConsume(workerQueueName, true, THISconsumerWorker);

	}

	/*
	 * Ensures that IP string is formatted correctly with correct syntax.
	 */
	private static boolean validIP(String ip) {

		try {
			if (ip == null || ip.isEmpty()) {
				logger.info("broker IP is either empty string or null: " + ip + " FAILED");
				return false;
			}

			String[] parts = ip.split("\\.");
			if (parts.length != 4) {
				logger.info("broker IP missing quartet formatting: " + ip + " FAILED");
				return false;
			}

			for (String s : parts) {
				int i = Integer.parseInt(s);
				if ((i < 0) || (i > 255)) {
					logger.info("broker IP missing quartet out of range: " + ip + " FAILED");
					return false;
				}
			}
			if (ip.endsWith(".")) {
				logger.info("broker IP mis-formatted: " + ip + " FAILED");
				return false;
			}

		} catch (NumberFormatException nfe) {
			logger.info("broker IP with invalid quartet number: " + ip + " FAILED");
			return false;
		}

		logger.info("broker IP string VALID: " + ip);
		return true;
	} // end validIP
	

} // end class Worker

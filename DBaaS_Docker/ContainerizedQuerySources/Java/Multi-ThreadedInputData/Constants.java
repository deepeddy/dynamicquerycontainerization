import java.util.*;

public interface Constants {

    //rabbitMQ exchange name
    static final String ALL_WORKERS_EXCHANGE = "allWorkersExchange";

    //queue on master receives ACK from  worker to acknowledge operation requested and performed by worker 
    static final String ACK_QUE = "ackQue";

    //rabbitMQ exchange keys
    static final Set<String> exchangeKeys = new HashSet<String>() {{add("shuffle"); add("join"); add("worker"); add("merge"); }};

}

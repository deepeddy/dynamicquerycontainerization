import java.io.*;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.StringTokenizer;

public class ContainerizedQueryUtils {
	
	/*  Reads in a serialized textual notation that represents the structure of a optimized relational 
	 *  QUERY TREE from a text file. Each line in the file represents a tree node, whose fields are 
	 *  tab delimited.  There are two(2) types of records: COMPUTE, DATA_ONLY, with different field 
	 *  definitions. A line is considered to be terminated by any one of a line-feed
	 *  ('\n'), a carriage return ('\r'), or a carriage return followed immediately by a line-feed. The
	 *  contents of the serialized file is written by a simple text editor, e.g. getEdit, NotePad.
	 *  
	 *  tab delimited FORMAT of FIELDS:
	 *  
	 *  COMPUTER TYPE: 
	 *  <ID		TYPE	OPERATOR	PREDICATE	LEFT	RIGHT>
	 *  
	 *  ID: 		identifies a query tree node
	 *  TYPE:		COMPUTE denotes a container that executes a relational operator
	 *  OPERATOR: 	identifies the relation operator executed by the node
	 *  PREDICATE: 	any predicate expression for the operator, NULL for no predicate
	 *  L:			is the left child's ID
	 *  R:			is the right child's ID   //relational ops are unary or binary only
	 *  
	 *  DATA_ONLY TYPE:
	 *  <ID		TYPE	DATABASE_URI 	TABLE>
	 *  
	 *  ID: 			identifies a query tree node
	 *  TYPE:			DATA_ONLY denotes a container only reserved for managing access to data
	 *  DATABASE:URI 	URI locating database, e.g. file://localhost/usr/sqlite/data/student_tables.db
	 *  
	 *    ID = { 1 ... n), ID = 1 is tree root node, ID = NULL denotes NO child.
	 *  
	 *  Consider the following relational expression of operators and predicates:
	 *   
	 *   Order[R.A1,<](Project[R.A1,S.B1]((Select[R.A2 < a](R)) Join[R.A3 = S.B3] (Select[S.B2 = b](S))))
	 *    
	 *  where, R(A1,A2,A3) and S(B1,B2,B3) are relations with three(3) fields each. This relational expression
	 *  can also be represented in tree form; referred to as the relational expression's QUERY TREE. 
	 *  
	 *  
	 *                  (1)     Order[R.A1,<]
	 *                            |
	 *                            |
	 *                            |
	 *                  (2)     Project[R.A1,S.B1]
	 *                            |
	 *                            |
	 *                            |
	 *                  (3)     Join[R.A3 = S.B3]
	 *                          /   \ 
	 *                         /     \
	 *                        /       \
	 *    (4)  Select[R.A2 < a]        Select[S.B2 = b]  (5)
	 *                       |         |
	 *                 (6)   R         S   (7)
	 *                     (URI,R)  (URI,S)
	 *                     
	 *   This QUERY TREE can be serialized textually using the field format above:
	 *   
	 *   ID		TYPE		OPERATOR	PREDICATE		LEFT	RIGHT
	 *   1		COMPUTE		Order		[R.A1,<]		2		null
	 *   2		COMPTUE		Project		[R.A1,S.B1]		3		null
	 *   3		COMPUTE		Join		[R.A3 = S.B3]	4		5
	 *   4		COMPUTE		Select		[R.A2 < a]		6		null
	 *   5		COMPUTE		Select		[S.B2 = b]		7		null
	 *   
	 *   ID		TYPE		DATABASE_URI								TABLE						
	 *   6      DATA_ONLY   file://${HOME}/databases/student_tables.db	R
	 *   7      DATA_ONLY   file://${HOME}/databases/student_tables.db	S
	 *   
	 *   Relations R,S are in terminal nodes with no children
	 *   
	 */

	
	public static void main(String[] args) {
		
		ContainerizedQueryUtils utils = new ContainerizedQueryUtils();
		
		LinkedHashMap<String, String> hashTable = utils.hashSerializedQuery("SerializedQueryFile.txt");
		//System.out.println(hashTable.toString());  //test if hash worked
		//utils.printSerializedTree(hashTable);
		
		//start from root node with id = 1
		System.out.println("\n" + "CONSTRUCT QUERY TREE FROM SERIALIZED FORM:");
		QueryNode root = null;
		try {
			root = utils.buildQueryTree(hashTable, "1");
		} catch (QueryException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		
		//print contents of query tree
		System.out.println("\n" + "NODES IN OPTIMIZED QUERY TREE");
		utils.printQueryTree(root);
		
		Digraph g = utils.new Digraph();
		utils.buildDigraph(root, g, null);
		System.out.println("\n" + g.toString());
		
		utils.buildInitialQueryPlan(g);
		
	}
	

	private class QueryNode {
    	
		protected String id = "";
		protected ContainerType type = ContainerType.NONE;
    	

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + getOuterType().hashCode();
			result = prime * result + ((id == null) ? 0 : id.hashCode());
			result = prime * result + ((type == null) ? 0 : type.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			QueryNode other = (QueryNode) obj;
			if (!getOuterType().equals(other.getOuterType()))
				return false;
			if (id == null) {
				if (other.id != null)
					return false;
			} else if (!id.equals(other.id))
				return false;
			if (type != other.type)
				return false;
			return true;
		}

		@Override
		public String toString() {
			return "QueryNode [id=" + id + ", type=" + type;
		}
    	
		public String getId() {
			return id;
		}
		public void setId(String id) {
			this.id = id;
		}

		public ContainerType getType() {
			return type;
		}

		public void setType(ContainerType type) {
			this.type = type;
		}

		private ContainerizedQueryUtils getOuterType() {
			return ContainerizedQueryUtils.this;
		}
    	
    }	// end inner class QueryNode
	
	private class QueryComputeNode extends QueryNode {
		
		private String operator = "";
    	private String predicate = "";
    	private String leftId = "";     //string representation of left child's id, if any       
    	private String rightId = "";    //string representation of right child's id, if any
    	private QueryNode leftChild = null;
    	private QueryNode rightChild = null;
    	
    	public String getOperator() {
			return operator;
		}
		public void setOperator(String operator) {
			this.operator = operator;
		}
		public String getPredicate() {
			return predicate;
		}
		public void setPredicate(String predicate) {
			this.predicate = predicate;
		}
		
		public String getLeftId() {
			return leftId;
		}
		public void setLeftId(String leftId) {
			this.leftId = leftId;
		}
		public String getRightId() {
			return rightId;
		}
		public void setRightId(String rightId) {
			this.rightId = rightId;
		}
		public QueryNode getLeftChild() {
			return leftChild;
		}
		public void setLeftChild(QueryNode leftChild) {
			this.leftChild = leftChild;
		}
		public QueryNode getRightChild() {
			return rightChild;
		}
		public void setRightChild(QueryNode rightChild) {
			this.rightChild = rightChild;
		}
		
		@Override
		public String toString() {
			return super.toString() + ", operator=" + operator + ", predicate=" + predicate + ", leftId=" + leftId
					+ ", rightId=" + rightId + "]";		
		}
		
	} // end class QueryComputeNode
	
	private class QueryException extends Exception {

		private static final long serialVersionUID = 1L;

		public QueryException(String string) {
			super(string);
		}
		
	} // end QueryException
	
	private class QueryDataOnlyNode extends QueryNode {
		
		private String databaseURI;
		private String table;
		
		public String getDatabaseURI() {
			return databaseURI;
		}
		public void setDatabaseURI(String databaseURI) {
			this.databaseURI = databaseURI;
		}
		public String getTable() {
			return table;
		}
		public void setTable(String table) {
			this.table = table;
		}
		
		@Override
		public String toString() {     
			return super.toString() + ", databaseURI=" + databaseURI + ", table=" + table  + "]";	
		}

	} // end class
	
	public enum ContainerType {
	    COMPUTE, DATA_ONLY, NONE 
	}
	
	private class Container {

		protected String id;
    	protected ContainerType type;    
    	
		public Container(String id, ContainerType type) {
			this.id = id;
			this.type = type;
		}
		
		
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + getOuterType().hashCode();
			result = prime * result + ((id == null) ? 0 : id.hashCode());
			result = prime * result + ((type == null) ? 0 : type.hashCode());
			return result;
		}


		@Override
		public boolean equals(Object obj) {
			
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			
			Container other = (Container) obj;
			if (!getOuterType().equals(other.getOuterType()))
				return false;
			if (id == null) {
				if (other.id != null)
					return false;
			} else if (!id.equals(other.id))
				return false;
			if (type != other.type)
				return false;
			
			return true;
		} // end method


		@Override
		public String toString() {
			return "\nV:" + id + ",\t" + type;
		}


		private ContainerizedQueryUtils getOuterType() {
			return ContainerizedQueryUtils.this;
		}

	} // end ContainerNode
	
	private class ComputeContainer extends Container {
		
    	public ComputeContainer(ContainerType type, QueryComputeNode treeNode) {
			super(treeNode.getId(), type);
			id = "C" + id;
			this.operator = treeNode.getOperator();
			this.predicate = treeNode.getPredicate();
		}
    	
		@Override
		public String toString() {
			return super.toString() + ", " + operator + "(" + predicate + ")";
		}


		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			ComputeContainer other = (ComputeContainer) obj;
			if (!getOuterType().equals(other.getOuterType()))
				return false;
			if (operator == null) {
				if (other.operator != null)
					return false;
			} else if (!operator.equals(other.operator))
				return false;
			if (predicate == null) {
				if (other.predicate != null)
					return false;
			} else if (!predicate.equals(other.predicate))
				return false;
			return true;
		}


		private String operator;
    	private String predicate;
		private ContainerizedQueryUtils getOuterType() {
			return ContainerizedQueryUtils.this;
		}
      
	} // end ComputeNode

	private class DataOnlyContainer extends Container {
    	
    	public DataOnlyContainer(ContainerType type, QueryDataOnlyNode treeNode) {
    		
			super("DC-" + treeNode.id, type);    //prefix container id with "DC-" to label it as data container
			databaseURI = treeNode.getDatabaseURI();
			table = treeNode.table;
		}

		private String databaseURI;
		private String table;

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (!super.equals(obj))
				return false;
			if (getClass() != obj.getClass())
				return false;
			DataOnlyContainer other = (DataOnlyContainer) obj;
			if (!getOuterType().equals(other.getOuterType()))
				return false;
			if (databaseURI == null) {
				if (other.databaseURI != null)
					return false;
			} else if (!databaseURI.equals(other.databaseURI))
				return false;
			if (table == null) {
				if (other.table != null)
					return false;
			} else if (!table.equals(other.table))
				return false;
			return true;
		}

		@Override
		public String toString() {
			return super.toString() + ", " + databaseURI + ", " + table;
			//return "\nV:"+ id + ", " + type +  ", " + relationName;
		}

		private ContainerizedQueryUtils getOuterType() {
			return ContainerizedQueryUtils.this;
		}
		
	} // end DataOnlyNode
	
	/*
	 *  A directed edge between two vertices, represented as a pair (V1, V2), where the edge is 
	 *  directed from V1 towards V2. 
	 */
	private class Edge {
		
		private final Container V1;
		private final Container V2;
		
		public Edge(Container v1, Container v2) {
			super();
			V1 = v1;
			V2 = v2;
		}

		@Override
		public boolean equals(Object obj) {
			return this == obj;
		}
		
		public Container getV1() {
			return V1;
		}
		
		public Container getV2() {
			return V2;
		}

		/*
		@Override
		public String toString() {
			
			CharSequence v1 = V1.toString().subSequence(0,V1.toString().length() -1);
			CharSequence v2 = V2.toString().subSequence(0,V2.toString().length() -1);
			
			return "\n (V:" + v1 + ",  V:" + v2 + ")";
		}
		*/
		
		@Override
		public String toString() {
			
			/*
			CharSequence v1 = V1.toString().subSequence(1,V1.toString().indexOf(','));
			CharSequence v2 = V2.toString().subSequence(1,V2.toString().indexOf(','));
			
			return "\n (" + v1 + ",  " + v2 + ")";
			*/
			return "\n (" + V1.id + ",\t" + V2.id + ")";
		}
			
	} // end class Edge
	
	/*
	 * Digraph G = <V, E>, where V is a set of vertices and E is a set of edges
	 */
	private class Digraph {
		
		Digraph() {
			super();
		}

		private LinkedHashSet<Container> vertices = new LinkedHashSet<Container>();
		private LinkedHashSet<Edge> edges = new LinkedHashSet<Edge>();

		public LinkedHashSet<Container> getVertices() {
			return vertices;
		}

		public LinkedHashSet<Edge> getEdges() {
			return edges;
		}

		/*
		 * returns if node added successfully
		 */
		public boolean addVertex(Container node) {
			
			return vertices.add(node);
			
		}
		
		public boolean addEdge(Container V1, Container V2) {
			
			return edges.add(new Edge(V1, V2));
			
		}
		
		/*
		 * Returns the data relation container that lies on the INBOUND edge 
		 * attached to container "node". If no such relation exists returns null.
		 */
		public DataOnlyContainer getRelation(Container node) {

			DataOnlyContainer relation = null;
			
			for(Edge e : edges) {
				
				if(e.getV2().equals(node) && e.getV1() instanceof DataOnlyContainer)
					relation = (DataOnlyContainer) e.getV1();
			}
			
			return relation;
				
		} // end method
		
		public LinkedHashSet<Edge> getInboundEdges(Container node) {
			
			LinkedHashSet<Edge> inbounds = new LinkedHashSet<Edge>();
			
			for(Edge e : edges) {
				
				if(e.getV2().equals(node))
					inbounds.add(e);
				
			}
			
			return inbounds;
			
		} // end method
		
		public LinkedHashSet<Edge> getOutboundEdges(Container node) {
			
			LinkedHashSet<Edge> outbounds = new LinkedHashSet<Edge>();
			
			for(Edge e : edges) {
				
				if(e.getV2().equals(node))
					outbounds.add(e);
				
			}
			
			return outbounds;
			
		} // end method

		@Override
		public String toString() {
			return "Digraph<V,E> {\n\n VERTICES:\n" + vertices + ",\n\n EDGES:\n" + edges + "\n\n}";
		}
		
	}  // end class Digraph
	
	
	/*
	 * Recursively builds the query tree using the hashTable which contains the
	 * serialized representation of the nodes in the tree as specified in the preface
	 * to this class.
	 * 
	 * Returns the apex node of a subtree
	 */
	public QueryNode buildQueryTree(LinkedHashMap<String, String> hashTable, String nodeKey) throws QueryException {
		
		//construct new node
		String serializedNode = hashTable.get(nodeKey);
		System.out.println(serializedNode);
		QueryNode node = parseSerializedNode(serializedNode);
		
		if(node instanceof QueryComputeNode) {
			
			//LEFT AND RIGHT are set in recurse calls to buildQueryTree()
			String leftKey = ((QueryComputeNode) node).getLeftId();
			if(leftKey.matches("\\d+")) {
				QueryNode leftNode = buildQueryTree(hashTable, leftKey);  //recurse
				((QueryComputeNode) node).setLeftChild(leftNode); //set left child
			} 
			
			String rightKey = ((QueryComputeNode) node).getRightId();
			if(rightKey.matches("\\d+")) {
				QueryNode rightNode = buildQueryTree(hashTable, rightKey);  //recurse
				((QueryComputeNode) node).setRightChild(rightNode); //set right child
			}
			
		}  //if QueryComputeNode
		else 
			;
			//System.out.println("Adding left data node");
		
		return node;
	}
	
	/*
	 * Examines digraph to build initial containerized query plan.
	 * Writes plan to YAML file, "compose.yaml", which is executed by Docker Compose orchestration tool
	 * 
	 */
	public void buildInitialQueryPlan(Digraph g) {   
		
		BufferedWriter writer = null;   //compose YAML file
		try {
			writer = new BufferedWriter(new FileWriter("compose.yaml"));
			//new URI("file://localhost/home/david/help.txt");
			//new FileWriter("file://localhost/home/david/help.txt");
			writer.write("#docker-compose -f docker-compose.yml.sameHostVer2 --project-name ContainerizedQuery  --no-recreate  up -d\n");
			
		    writer.write("version: '2'\n");
		    writer.newLine();
		    writer.write("services:\n");
		    writer.newLine();
		   
			//first iterate thru all data-only container nodes, building an entry for each in the YAML file
			for(Container node : g.vertices) {
				
				if(node.type == ContainerType.DATA_ONLY) {		
					DataOnlyContainer dc = (DataOnlyContainer)node;
					writer.write("  " + dc.id + ":\n");
					writer.write("    image : 'busybox:latest'\n");
					writer.write("    volumes:\n");
					
					String uri = dc.databaseURI;     //full database URI, e.g. file://usr/databases/student_tables.db
					int lastIndexOf = uri.lastIndexOf('/');
					String volume = uri.substring(0, lastIndexOf);
					String database = uri.substring(lastIndexOf + 1);
					writer.write("     - " + volume + " : " + volume.substring(volume.lastIndexOf('/')) + "\n");
					//writer.write("     - ${HOME}/databases/" + dc.relationName + " : " + "/" + dc.relationName + "\n");
					writer.write("    environment:\n");
					writer.write("      COMPOSE_PROJECT_NAME : ContainerizedQuery\n");
					writer.write("      DATABASE : " + database + "\n");
					writer.write("      TABLE : " + dc.table + "\n");
					writer.write("    container_name : " + dc.id + "\n");
					writer.write("    restart: unless-stopped\n");
					writer.newLine();	
				}
				
			} // end for
			
			//then iterate thru all compute container nodes, building an entry for each in the YAML file
			for(Container node : g.vertices) {
				
				if(node.type == ContainerType.COMPUTE) {					
					ComputeContainer c = (ComputeContainer)node;
					writer.write("  " + c.id + ":\n");
					writer.write("    image : 'deepeddy/mysqlite:worker'\n");
				
					DataOnlyContainer dc = g.getRelation(node); 
					if (dc != null) { //node has in-bound edge with data relation container
						writer.write("    volumes from:\n");
						writer.write("     - " +  dc.id  + "\n");
					}
					
					writer.write("    expose:\n");
					writer.write("     - 22\n");
					writer.write("    environment:\n");
					writer.write("      COMPOSE_PROJECT_NAME : ContainerizedQuery\n");
					writer.write("    command: bash -c '/usr/sbin/sshd; /bin/bash -i'\n");
					writer.write("    container_name : " + node.id + "\n");
					writer.write("    stdin_open : true\n");
					writer.write("    tty : true\n");
					writer.newLine();
				} //end if
				
			} // end for
			
			writer.write("networks:\n");
			writer.write("  default:\n");
			writer.write("    driver: bridge\n");
		
			writer.flush();
			writer.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	} //end method
	
	/*
	 * Recursively builds a directed graph of containerNodes using a query tree as input. 
	 * The digraph is homomorphic with respect to the query tree, but has directed edges 
	 * directed from children to parents. Edge direction denotes the direction of data flow
	 * and order of node execution.
	 * 
	 * Returns the apex node of a subtree
	 */
	public void buildDigraph(QueryNode treeNode, Digraph g, Container previousContainerNode) {
		
		//construct new digraph node
		if(treeNode instanceof QueryComputeNode) {
			ComputeContainer newContainerNode =  new ComputeContainer(ContainerType.COMPUTE, (QueryComputeNode) treeNode);
			g.addVertex(newContainerNode);
			
			if(previousContainerNode != null)
				g.addEdge((Container)newContainerNode, previousContainerNode);
			
			// LEFT AND RIGHT children are set in recursive calls to  buildInitialPlan()
			if(((QueryComputeNode) treeNode).getLeftChild() != null) {
				buildDigraph(((QueryComputeNode) treeNode).getLeftChild(), g, newContainerNode);  //recurse
			} 
			
			if(((QueryComputeNode) treeNode).getRightChild() != null) {
				buildDigraph(((QueryComputeNode) treeNode).getRightChild(), g, newContainerNode);  //recurse
			} 
			
		}
		else if (treeNode instanceof QueryDataOnlyNode) {
			DataOnlyContainer newContainerNode =  new DataOnlyContainer(ContainerType.DATA_ONLY, (QueryDataOnlyNode) treeNode);
			g.addVertex(newContainerNode);
			g.addEdge((Container)newContainerNode, previousContainerNode);
			
		}
		
		/*
		//could create a data-only container node and its edge 
		if(!treeNode.getLeftId().equals("null") && treeNode.getLeftId().matches("[a-zA-Z]+")) {
			DataOnlyContainer newDataNode =  new DataOnlyContainer(ContainerType.DATA_ONLY, treeNode.getLeftId());
			g.addEdge(newDataNode, newContainerNode);
			g.addVertex(newDataNode);
		}
		*/
    
	} //buildinitialPlan
	
	/*
	 * Performs a pre-order traversal of the query tree, printing a node's contents when it is 
	 * first visited.
	 */
    public void printQueryTree(QueryNode node) {
		
    	if(node instanceof QueryComputeNode) {
    		QueryComputeNode compute = (QueryComputeNode)node;
    		System.out.println(compute);  //print contents of visited node
    		
    		if(compute.leftChild != null)
    			printQueryTree(compute.leftChild);
    	
    		if(compute.rightChild != null)
    			printQueryTree(compute.rightChild);
    	}
    	else {
    		QueryDataOnlyNode data  = (QueryDataOnlyNode)node;
    		System.out.println(data);  //print contents of visited node
    	}
    	
	} //end method

	
	/* Parses the serialized representation of a query tree node, using its individual
	 * fields to construct a node in a query tree. 
	 * 
	 * Input:  the serialized string that represents the query node
	 * Output: a QueryNode object encapsulating the node's components
	 * Throws QueryException if ContainerType is not valid.
	 * 
	 */
	public QueryNode parseSerializedNode(String serializedNode) throws QueryException {
		
		QueryNode node = null;
		StringTokenizer tokenizer = new StringTokenizer(serializedNode, "\t"); //tab delimited
		String id = tokenizer.nextToken();
		String typeOfContainer = tokenizer.nextToken();
		
		if(typeOfContainer.equals("COMPUTE")) {
			node = new QueryComputeNode();
			node.setType(ContainerType.COMPUTE);
		}
		else if (typeOfContainer.equals("DATA_ONLY")) {
			node = new QueryDataOnlyNode();
			node.setType(ContainerType.DATA_ONLY);
		}
		else 
			throw new QueryException("INVALID ContainerType " + typeOfContainer);
					
		node.setId(id);  													//set ID	
		
		if(node instanceof QueryComputeNode) {
			((QueryComputeNode) node).setOperator(tokenizer.nextToken());    //set OPERATOR
			((QueryComputeNode) node).setPredicate(tokenizer.nextToken());   //set PREDICATE 
			((QueryComputeNode) node).setLeftId(tokenizer.nextToken());
			((QueryComputeNode) node).setRightId(tokenizer.nextToken());
		}
		else {
			((QueryDataOnlyNode) node).setDatabaseURI(tokenizer.nextToken());   //set URI
			((QueryDataOnlyNode) node).setTable(tokenizer.nextToken());    		//set table
			
		}
 		
		return node;
		
	}  // end method parseSerializedNode()
	
	/* Reads in a serialized textual representation of a relation query from a file formatted as
	 * specified  in class preface above. Each separate line represents a node in a optimized
	 * relational query tree, with each node containing its operator, predicate and links to children
	 * nodes.
	 * 
	 * Each line of the file is read in, then hashed into a hash-table using  node ID as the key.
	 * 
	 * The hash-table is returned;
	 */
	public LinkedHashMap<String, String> hashSerializedQuery(String fileName) {
		
		BufferedReader in = null;
		LinkedHashMap<String, String> hashTable = new LinkedHashMap<String, String>();
		
		try {
			 in = new BufferedReader(new FileReader(fileName));
			 String line = "";
			 
			 while(true) {
					
					  //read in next query tree node descriptor
					if ((line = in.readLine()) == null)
						break;
					else {
						StringTokenizer tokenizer = new StringTokenizer(line, "\t"); //tab delimited
						String key = tokenizer.nextToken();
						hashTable.put(key, line);
					}
					
			}// end while
			 
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.exit(0);
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(0);
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(0);
		} 
	
		
		//close query file and return hash-table
		try {
			in.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return hashTable;
		
	} //end method hashSerializedQueryFromFile()
	
	/* Prints a facsimile of the optimized query tree, stored in the hash-table input by ID.
	 *  <ID		OPERATOR	PREDICATE	LEFT	RIGHT>
	 *  
	 *  where,
	 *  
	 *  ID: 		identifies a query tree node
	 *  OPERATOR: 	identifies the relation operator executed by the node
	 *  PREDICATE: 	any predicate expression for the operator, NULL for no predicate
	 *  LEFT:		is the left child's ID
	 *  RIGHT:		is the right child's ID   //relational ops are unary or binary only
	 */
	public void printSerializedTree( LinkedHashMap<String, String> hashTable) {
		
		String serializedTree = hashTable.toString();
		serializedTree = serializedTree.substring(1, serializedTree.length() -1);
		String[]  nodeList = serializedTree.split("\\d=");
		for(String node : nodeList) {
			System.out.println(node);
		}
		
		/*
		try {
			
			 String line = "";
			 String ID = "";
			 String OPERATOR = "";
			 String PREDICATE = "";
			 String LEFT = "";
			 String RIGHT = "";
			 
			 String blanks =  "                                        "; //forty leading blanks 
			 
			 for(String key : hashTable.keySet()) {
					
				line = hashTable.get(key);
				StringTokenizer tokenizer = new StringTokenizer(line, "\t"); //tab delimited
				
				for(int i = 1; i < tokenizer.countTokens(); i++) {
					String token = tokenizer.nextToken();
					
					switch(i) {
					
					case 1: ID = token;
					        break;
					case 2: OPERATOR = token;
					 		System.out.println(blanks + OPERATOR + PREDICATE);
					 		if ( Character.isLetter(LEFT.charAt(0)) ) {
					 			System.out.println(blanks + "  |");
					 			System.out.println(blanks + "  " + LEFT);
					 		} 
			        		break;
					case 3: PREDICATE = token;
			         		break;
					case 4: LEFT = token;
			        		break;
					case 5: RIGHT = token;
			        		break;
					default: throw new Exception("Bad query node FIELD: " + token);
					}
					 
				} // end inner for
			
				//print graph line
				
					
			}// end outer for
			 
		} catch(Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
			System.exit(0);
		}
	  */
		
	} // end method printTree()

} // end class

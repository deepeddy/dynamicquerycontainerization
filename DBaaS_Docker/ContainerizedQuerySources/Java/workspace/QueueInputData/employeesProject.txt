#  All employees that worked less than 20 years
#
# SELECT employees.emp_no, salaries.salary FROM salaries, employees WHERE  
#    CAST(julianday(salaries.to_date) - julianday(employees.hire_date) AS INT)  < 7300
#    AND (salaries.emp_no == employees.emp_no) 
#    AND salaries.salary < 200000 AND employees.emp_no  < 50000 ORDER BY employees.emp_no ASC;
#
#	Type		Op		Predicate							L	R		Degree	Probe(JOIN)
#1	COMPUTE		ORDER	ORDER BY employees.emp_no DESC		2	null	2
1	COMPUTE		PROJECT	employees.emp_no, salaries.salary	2	null	1
2	COMPUTE		JOIN	(CAST(julianday(salaries.to_date) - julianday(employees.hire_date) AS INT) < 730) AND (employees.emp_no == salaries.emp_no)		3	4	6	3
3	COMPUTE		SELECT	salaries.salary < 200000			5	null	6
4	COMPUTE		SELECT	employees.emp_no  < 50000			6	null	1
5	DATA_ONLY	${HOME}/databases/employees6SalariesPartitions/employees.db	[salariesPart_1,salariesPart_2,salariesPart_3,salariesPart_4,salariesPart_5,salariesPart_6]	[emp_no int(11); salary int(11); from_date date; to_date date]
6	DATA_ONLY	${HOME}/databases/employees6SalariesPartitions/employees.db	employees	[emp_no int(11) PRIMARY; birth_date date; first_name  varchar(14); last_name  varchar(16); gender text; hire_date date]
#8   DATA_ONLY	${HOME}/databases/salariesPart_1.db		salariesPart_1	[emp_no int(11); salary int(11); from_date date; to_date date]

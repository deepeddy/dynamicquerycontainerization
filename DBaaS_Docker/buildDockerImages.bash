#!/bin/bash
clear
echo "---------Starting Script---------"
echo
echo "---------Building Worker image---------"
docker build -t "deepeddy/mysqlite:worker" -f ./Dockerfile.worker --no-cache --pull .
echo "--------- Push image---------"
docker push deepeddy/mysqlite:worker
echo "---------Building Master image---------"
docker build -t "deepeddy/mysqlite:master" -f ./Dockerfile.master --no-cache --pull .
echo "--------- Push image---------"
docker push deepeddy/mysqlite:master
echo
echo "---------Finished Script---------"

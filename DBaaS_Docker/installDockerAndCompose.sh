#!/bin/bash
echo "usage: <host name> <sudo passwd>"
echo "installs Docker engine and Docker-Compose on top of Ubuntu image"
echo "1st argument is " $1
echo "2nd argument is " $2
passwd=$2
host=$1
echo $passwd | sudo -S sed -i '1s/^/127.0.0.1 '$host'\n/' /etc/hosts

echo "INSTALL Docker engine"
sudo apt-get -y update

sudo apt-get -y install \
   linux-image-extra-$(uname -r) \
   linux-image-extra-virtual 

echo "Install packages to allow apt to use a repository over HTTPS"
sudo apt-get -y install \
  apt-transport-https \
  ca-certificates \
  curl \
  software-properties-common

echo "Add Docker’s official GPG key"
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

echo "Add repository"
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"

sudo apt-get -y update

echo "INSTALL DOCKER"
sudo apt-get -y install docker-ce=17.06.2~ce-0~ubuntu

echo "INSTALL Docker-Compose"
sudo chgrp cc /usr/local/bin
sudo chmod g=rwx /usr/local/bin

sudo curl -L https://github.com/docker/compose/releases/download/1.16.0/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose

sudo chmod +x /usr/local/bin/docker-compose

sudo usermod -aG docker $USER

exit 0

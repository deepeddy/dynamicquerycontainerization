CREATE TABLE Students (sid INTEGER PRIMARY KEY, name TEX , age REAL);
CREATE TABLE Grades (gsid INTEGER, gcid INTEGER, points REAL, PRIMARY KEY (gsid, gcid));
CREATE TABLE  Courses (ccid INTEGER PRIMARY KEY, courseDesc TEXT);


INSERT INTO Students VALUES (1, 'Alice', 25.67);
INSERT INTO Students VALUES (2, 'Chris', 12.34);
INSERT INTO Students VALUES (3, 'Bob', 30.0);


INSERT INTO Courses VALUES (448, 'DB Fun');
INSERT INTO Courses VALUES (348, 'Less Cool');
INSERT INTO Courses VALUES (542, 'More Fun');

INSERT INTO Grades VALUES (2, 448, 4.0);
INSERT INTO Grades VALUES (3, 348, 2.5);
INSERT INTO Grades VALUES (3, 448, 3.1);
INSERT INTO Grades VALUES (1, 542, 3.5);

package examples;

import java.util.Scanner;

public class FutureVale {

	public int present;
	public int rate;
	public int years;
	public int futureValue;

	public static void main(String[] args){

		Scanner input = new Scanner(System.in);

		System.out.println("present value ");
		double present = input.nextDouble();

		System.out.println("interest rate");
		double rate = input.nextDouble();

		System.out.println("number of years ");
		int years = input.nextInt(); 

	}

	public static double values (double p, double r, int t) {
		double value1 = p; // assign the variable p to present
		double value2 = r; // assign the variable r to rate
		int value3 = t; // assign the variable t to years
		return value3;
	}

	public static double futureValue (double p, double r, int t){
		double futureValue = p * Math.pow( (1.0 + r/100), t);  
		return futureValue;
	} 


}
package quotient;

public class QuotientTable {
	
	  public static void main(String[] args) {

		  calcQuotientTable(5);     
		  

      }   

		    public static void calcQuotientTable(int N )  {   //count is the size of the table from 1 to N

		      //nested loops printing out table here

		        for(int k = 1; k <=N; k++)   //print 1st row

		               System.out.print(" " + k);

		        for(int i = 2; i <= N; i++) {    //loop thru rows 2 .. N

		              System.out.println();    // next row

		              System.out.print(" "  + i);      // 1st element labels row

		              for(int j = 2; j <= N; j++) {  //for every outer loop value, loop inner 2 ...n

		                   System.out.print(" " + j/i);        //integer division truncates any remainder

		              }

		        }

		     }   // end method

}

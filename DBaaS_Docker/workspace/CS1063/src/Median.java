
public class Median {

	public static void main(String[] args) {
		
		
		int x = 5;
		int y = 8;
		int z = 2;
		System.out.println("x " + x + ", y " + y + ", z " + z);
		System.out.println("median is " + median(x,y,z));
		

	}

	public static int median(int x, int y, int z){
		
		int median = 0;
		
		if(x < y) {
			
			if (z < x)      // z < x < y
				median = x;
			else if( z > y) // x < y < z
				median = y;
			else    		// x < z < y
				median = z;
		}
		else {  // y < x 
			
			if (z < y)      // z < y < x 
				median = x;
			else if( z > x) // y < x < z
				median = y;
			else    		// y < z < x
				median = z;
		} 
		
		return median;
		
	} // end method
	
} // end class

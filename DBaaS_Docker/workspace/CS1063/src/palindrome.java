
public class palindrome {

	public static void main(String[] args) {
		System.out.println("'racecar' is a palindrome: "+ isPalindrome("racecar"));

	}

	public static String reversed(String str) {
		
		String reversed = "";
		for(int i = 0; i < str.length(); i++) {
			reversed = str.charAt(i) + reversed;
		}
		
		return reversed;
	} // end reversed 
	
	
	public static boolean isPalindrome(String str) {
		
		 return str.equals(reversed(str));
	} // end isPalindrome
	
} // end class

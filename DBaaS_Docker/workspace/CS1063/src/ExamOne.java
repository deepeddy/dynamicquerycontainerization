
public class ExamOne {
	/*
	CS1063  Fall 2016 Exam One.

	You may use books, notes, example code, etc.

	Type of Problems:

	1) write a method named "myName()" that prints your name. 

	2) write a method named "numericExpression()" that computes a numeric expression and prints its result.

	3) write a method named "xGraphic()" that prints a character pattern.

	4) write a method named "squareOfNumbersOneToFive()" that uses a loop to calculate and print a values.

	5) write a method named "divisionTable()" that prints the quotient of all number combinations
	   from 1 .. 10 using a NESTED loops.

	The test is be representative of the lab assignments, lab0, lab1, lab2. 
	Don't forget using REMAINDER and INTEGER DIVISION in lab2.  
	The test also reflects the interactive assignments we have been doing in class.
	*/
	
	public static void main(String[] args) {
		
		numericExpression();
		
		System.out.println();
		xGraphic();
		
		System.out.println();
		squareOfNumbersOneToFive();
		
		System.out.println();
		System.out.println();
		divisionTable();

	}

	
	public static void myName() {
		//YOUR CODE
	}
	
	public static void numericExpression() {
		
		//on a single line
		//add numbers 1 ... 10. divide this sum by 3 to create a quotient using integer division, 
		//then compute modulo (remainder) 7 of this quotient
		//print the final number only
	
		int number = (1 + 2 + 3 + 4 + 5 + 6 + 7 + 8 + 9 + 10) / 3 % 7;
		System.out.println(number);	
		
	} // end numericExpression()
	
	public static void xGraphic() {
		
		System.out.println("X   X");
		System.out.println(" X X");
		System.out.println("  X");
		System.out.println(" X X");
		System.out.println("X   X");
		
	} // end xGraphic()
	
	
	public static void squareOfNumbersOneToFive() {
		
		for(int i = 1; i <= 5; i++) {
			System.out.print(i * i + " ");
			
		}
		
	} // end squareOfNumbersOneToFive()
	
	public static void divisionTable() {
		
		for(int i = 1; i <= 10; i++){
			System.out.print(i + "\t");
			for(int j = 1; j <= 10; j++) {
				System.out.print((j / i) + "\t");
			}
			System.out.println();
		}
		
	}
	
	
} //end ExamOne

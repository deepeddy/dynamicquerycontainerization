/*
 * 
 * "WHAT IS PRINTED BY" type question for FINAL EXAM CS1063 Fall 2015
 * 
 * QUESTION:   STATE WHAT IS PRINTED BY THE FOLLOWING CODE.
 */
public class StringMystery {
	
	public static void main (String[] args) {
		int count = loopMystery(9);
		System.out.println(count + " squares printed");
	} // end main

	//returns the number of numbers printed in the loop body
	public static int loopMystery(int iterations) {

		int count = 0;  //how many times loop body is entered 
		
		for(int i = 0; i < iterations; i = i + 2) {
			
			count++;
			
			if(i < (iterations - 2))
				System.out.print((i * i) + ",");
			else
				System.out.println((i * i));
				
			
		} // end for
		
		
		return count;
		
    } // end loopMystery method

}

/*
 * 	   "WRITING CODE PROBLEM" type question. FINAL EXAM CS 1063 FALL 2015
 * 		THIS PROBLEM INVOLVES WRITING A METHOD THAT TAKES: a Scanner object as a parameter.
 * 
 *      Write a method names "groceryList", that takes two parameters:  1st) type: java.util.Scanner for
 *      user input, 2nd) type: int for the number of items on the grocery list. The methods returns the total cost
 *      of all the items.
 *        
 *      Input the user's list one line at a time for each grocery item on the list and the item's
 *      estimated cost to buy the item. Keep a running-total of the accumulated cost of all items
 *      up to the point. Print the running-total after each user input. Format cost with only two(2)
 *      digits to the right of the decimal, HINT: use printf() with the formatting string "$%.2f".
 *      Example:
 *       
 *       			> milk 		3.46
 *       			> $3.46
 *                  > fish  	7.98
 *                  > $11.44
 *                  > apples 	2.46
 *                  > $13.9 
 *                  > butter	1.98
 *                  > $15.88
 *                  
 *      The list should have between 4 and 6 items. Store each item as it is input by the user in an 
 *      array of Strings, i.e. String[] items.
 *      The items array of also the size of the list. Remember the method should also return the total 
 *      accumulated cost of buying all the grocery items. Items should be a single word with no spaces
 *      (not a phrase). Cost should be a decimal number, e.g. 3.46, as in the example. When user input is
 *      completed, print a single line with all the items separated by "," followed by a newline with the
 *      TOTAL cost of all items. Example
 *      
 *       		milk, fish, apples, butter
 *       		Total $15.88
 *       
 *       Take care not to print a "," after the last item (the fence-loop situation) 
 * 
 */
//FINAL EXAM 1063 QUESTION SOLUTION KEY ************************* DO NOT PRINT ON EXAM
//DO NOT PRINT ON EXAM, SOLUTION ONLY, DO NOT PRINT ON EXAM, SOLUTION ONLY, DO NOT PRINT ON EXAM, SOLUTION ONLY
//**************************************  DO NOT USE BELLOW *****************************************
import java.util.Scanner;

public class GroceryList {

	public static void main(String[] args) {
		
		Scanner console = new Scanner(System.in);
		groceryList(console, 5);

	} // end main
	
	public static double groceryList(Scanner console, int listSize) {
		
		System.out.println("Input a list of grocery items, one item at a time, and its estimated cost");
	
		String[] items = new String[listSize];
		double sum = 0.0;
		for(int i = 0; i < listSize; i++) {
			
			String item = console.next();
			items[i] = item;				//save item
		    Double cost = console.nextDouble();
		    sum += cost;					//accumulate cost
			System.out.printf("$%.2f \n", sum);	//print cost formatted on newline
			
		} //end for
		
		//fence-loop
		for(int i = 0; i < listSize -1; i++) {	//print items on same line, comma delimited.
			System.out.print(items[i] + ", ");
		}
		System.out.println(items[listSize -1]); //print last item
		
		System.out.printf("Total $%.2f", sum);  //print total cost after list of items
		
		return sum;  //return total cost
		
	} // end method

} // end class

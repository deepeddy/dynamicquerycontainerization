package index;

import global.GlobalConst;
import global.RID;
import global.SearchKey;

/**
 * A HashScan object is created only through the function openScan() in the
 * HashIndex class. It supports the getNext interface which will simply retrieve
 * the next record in the file.
 */
public class HashScan implements GlobalConst {

  /**
   * Constructs an equality scan by initializing the iterator state.
   */
  protected HashScan(HashIndex index, SearchKey key) {
    throw new UnsupportedOperationException("Not implemented");
  }

  /**
   * Called by the garbage collector when there are no more references to the
   * object; closes the scan if it's still open.
   */
  protected void finalize() throws Throwable {
    throw new UnsupportedOperationException("Not implemented");
  }

  /**
   * Closes the index scan, releasing any pinned pages.
   */
  public void close() {
    throw new UnsupportedOperationException("Not implemented");
  }

  /**
   * Returns true if there are more entries to scan, false otherwise.
   */
  public boolean hasNext() {
    throw new UnsupportedOperationException("Not implemented");
  }

  /**
   * Gets the next entry's record in the index scan.
   * 
   * @throws IllegalStateException if the scan has no more entries
   */
  public RID getNext() {
    throw new UnsupportedOperationException("Not implemented");
  }

} // public class HashScan implements GlobalConst

package index;

import global.GlobalConst;
import global.RID;
import global.SearchKey;

/**
 * <h3>Minibase Hash Index</h3>
 * This unclustered index implements static hashing as described on pages 371 to
 * 373 of the textbook (3rd edition).
 */
public class HashIndex implements GlobalConst {

  /**
   * Opens an index file given its name, or creates a new index file if the name
   * doesn't exist; a null name produces a temporary index file which requires
   * no disk manager entry.
   */
  public HashIndex(String fileName) {
    throw new UnsupportedOperationException("Not implemented");
  }

  /**
   * Called by the garbage collector when there are no more references to the
   * object; deletes the index file if it's temporary.
   */
  protected void finalize() throws Throwable {
    throw new UnsupportedOperationException("Not implemented");
  }

  /**
   * Deletes the index file from the database, freeing all of its pages.
   */
  public void deleteFile() {
    throw new UnsupportedOperationException("Not implemented");
  }

  /**
   * Inserts a new entry into the index file.
   * 
   * @throws IllegalArgumentException if the entry is too large
   */
  public void insertEntry(SearchKey key, RID rid) {
    throw new UnsupportedOperationException("Not implemented");
  }

  /**
   * Deletes the specified entry from the index file.
   * 
   * @throws IllegalArgumentException if the entry doesn't exist
   */
  public void deleteEntry(SearchKey key, RID rid) {
    throw new UnsupportedOperationException("Not implemented");
  }

  /**
   * Initiates a scan of the entire index file in bucket order.
   */
  public BucketScan openScan() {
    return new BucketScan(this);
  }

  /**
   * Initiates an equality scan of the index file.
   */
  public HashScan openScan(SearchKey key) {
    return new HashScan(this, key);
  }

  /**
   * Returns the name of the index file.
   */
  public String toString() {
    throw new UnsupportedOperationException("Not implemented");
  }

  /**
   * Prints a high-level view of the directory, namely which buckets are
   * allocated and how many entries are stored in each one. Sample output:
   * 
   * <pre>
   * IX_Customers
   * ------------
   * 0000000 : 35
   * 0000001 : 39
   * 0000010 : 27
   * ...
   * 1111111 : 42
   * ------------
   * Total : 1500
   * </pre>
   */
  public void printSummary() {
    throw new UnsupportedOperationException("Not implemented");
  }

} // public class HashIndex implements GlobalConst

package index;

import global.Page;
import global.PageId;
import global.RID;
import global.SearchKey;


/**
 * A base class for index pages that automatically stores records in ascending
 * order by key value. SortedPage supports variable-length records by using a
 * slot directory, with the slots at the front and the records in the back, both
 * growing and shrinking into and from the free space in the middle of the page.
 */
class SortedPage extends SortedPageBase {

  /**
   * Default constructor; creates a sorted page with default values.
   */
  public SortedPage() {
    super();
  }

  /**
   * Constructor that wraps an existing sorted page.
   */
  public SortedPage(Page page) {
    super(page);
  }

  /**
   * Gets the number of entries on the page.
   */
  public short getEntryCount() {
    throw new UnsupportedOperationException("Not implemented");
  }
  
  /**
   * Gets the amount of free space (in bytes).
   */
  public short getFreeSpace() {
    throw new UnsupportedOperationException("Not implemented");
  }

  /**
   * Gets the next page's id.
   */
  public PageId getNextPage() {
    throw new UnsupportedOperationException("Not implemented");
  }

  /**
   * Sets the next page's id.
   */
  public void setNextPage(PageId pageno) {
    throw new UnsupportedOperationException("Not implemented");
  }

  /**
   * Gets the entry at the given slot number.
   * 
   * @throws IllegalArgumentException if the slot number is invalid
   */
  public DataEntry getEntryAt(int slotno) {
    throw new UnsupportedOperationException("Not implemented");
  }

  /**
   * Gets the key at the given slot number.
   * 
   * @throws IllegalArgumentException if the slot number is invalid
   */
  public SearchKey getKeyAt(int slotno) {
    throw new UnsupportedOperationException("Not implemented");
  }

  /**
   * Gets the key of the first entry on the page.
   * 
   * @throws IllegalStateException if the page is empty
   */
  public SearchKey getFirstKey() {
    throw new UnsupportedOperationException("Not implemented");
  }

  /**
   * Inserts a new record into the page in sorted order.
   * 
   * @return true if inserting made this page dirty, false otherwise
   * @throws IllegalStateException if insufficient space
   */
  public boolean insertEntry(DataEntry entry) {
    throw new UnsupportedOperationException("Not implemented");
  }

  /**
   * Deletes a record from the page, compacting the free space (including the
   * slot directory).
   * 
   * @return true if deleting made this page dirty, false otherwise
   * @throws IllegalArgumentException if the entry doesn't exist
   */
  public boolean deleteEntry(DataEntry entry) {
    throw new UnsupportedOperationException("Not implemented");
  }

  /**
   * Searches for the first entry that matches the given search key.
   * 
   * @return the slot number of the entry, or -1 if not found
   */
  public int findEntry(SearchKey key) {
    throw new UnsupportedOperationException("Not implemented");
  }

  /**
   * Searches for the next entry that matches the given search key, and stored
   * after the given slot.
   * 
   * @return the slot number of the entry, or -1 if not found
   */
  public int nextEntry(SearchKey key, int slotno) {
    throw new UnsupportedOperationException("Not implemented");
  }
  
  //overrides method in base class SortedPageBase to add a sorted order to the SLOT directory
  //which permits efficient search of indexes based upon their key values
  public RID insertRecord(byte record[])  {
		return null;
  }

} // class SortedPage extends Page

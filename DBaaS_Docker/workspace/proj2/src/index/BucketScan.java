package index;

import global.GlobalConst;
import global.RID;
import global.SearchKey;

/**
 * Scans an entire hash index file, bucket by bucket; created only through the
 * openScan() method in the HashIndex class.
 */
public class BucketScan implements GlobalConst {

  /**
   * Constructs a bucket scan by initializing the iterator state.
   */
  protected BucketScan(HashIndex index) {
    throw new UnsupportedOperationException("Not implemented");
  }

  /**
   * Called by the garbage collector when there are no more references to the
   * object; closes the scan if it's still open.
   */
  protected void finalize() throws Throwable {
    throw new UnsupportedOperationException("Not implemented");
  }

  /**
   * Closes the index scan, releasing any pinned pages.
   */
  public void close() {
    throw new UnsupportedOperationException("Not implemented");
  }

  /**
   * Returns true if there are more entries to scan, false otherwise.
   */
  public boolean hasNext() {
    throw new UnsupportedOperationException("Not implemented");
  }

  /**
   * Gets the next entry's record in the index scan.
   * 
   * @throws IllegalStateException if the scan has no more entries
   */
  public RID getNext() {
    throw new UnsupportedOperationException("Not implemented");
  }

  /**
   * Gets the key of the last RID returned.
   */
  public SearchKey getLastKey() {
    throw new UnsupportedOperationException("Not implemented");
  }

  /**
   * Returns the hash value for the bucket containing the next RID, or maximum
   * number of buckets if none.
   */
  public int getNextHash() {
    throw new UnsupportedOperationException("Not implemented");
  }

} // public class BucketScan implements GlobalConst

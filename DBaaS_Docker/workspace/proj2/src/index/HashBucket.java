package index;


/**
 * A bucket is a linked list of SortedPages (overflow pages).
 */
class HashBucket extends SortedPage {

  /**
   * Gets the number of entries in the entire bucket.
   */
  public int countEntries() {
    throw new UnsupportedOperationException("Not implemented");
  }

  /**
   * Inserts a new record into the hash bucket.
   * 
   * @return true if inserting made this page dirty, false otherwise
   */
  public boolean insertEntry(DataEntry entry) {
    throw new UnsupportedOperationException("Not implemented");
  }

  /**
   * Deletes a record from the page, compacting the free space (including the
   * slot directory).
   * 
   * @return true if deleting made this page dirty, false otherwise
   * @throws IllegalArgumentException if the entry doesn't exist
   */
  public boolean deleteEntry(DataEntry entry) {
    throw new UnsupportedOperationException("Not implemented");
  }

} // class HashBucket extends SortedPage

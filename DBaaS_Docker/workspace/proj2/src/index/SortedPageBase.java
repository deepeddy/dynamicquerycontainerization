package index;

import global.*;
import java.io.IOException;


// This class servers as the base class for SortedPage
// It is adopted from a DISASSEMBLED version of heap/HFpage
// available from  online from:  
// https://github.com/Hossam-Mahmoud/MinibaseProject_Wisconsin/blob/master/HeapFile/src/heap/HFPage.java
// Several Tuple methods were removed and disassembled variables labeled correctly
//
// subclass SortedPage OVERRIDES insertRecord(), which in addition to inserting a record in the page, ensures that
// the SLOTS in the SLOT Directory are sorted in  ASCENDING ORDER. This amounts to adding  a new slot in the 
// list of slots at a position which reflects the its order to other slots with respect to the index keys values
// that they point to.  Overriding insertRecord was the chief design decision in selection a base class that
// managed insertion and deletion of records into a heapFile page along with managing the page's SLOT directory.
// This saved time and allowed development to focus on maintaining slots in sorted ascending order.  This ordering
// allows the subclass SortedPage to efficiently search for records/tuples based upon a key. SortedPages is 
// intended to be used to organize relational INDEX files.
//
// Although adapted, some changes to methods were required so that they represent to HFPage specification 
// accurately, i.e. insertRecord() now returns 'null' instead of throwing a IOException
//
// Some class variable modifiers were changes to PROTECTED, to allow subclass SortedPage access

public class SortedPageBase extends Page implements  GlobalConst {

	public static final int SIZE_OF_SLOT = 4;
	public static final int DPFIXED = 20;
	public static final int SLOT_CNT = 0;
	public static final int USED_PTR = 2;
	public static final int FREE_SPACE = 4;
	public static final int TYPE = 6;
	public static final int PREV_PAGE = 8;
	public static final int NEXT_PAGE = 12;
	public static final int CUR_PAGE = 16;
	protected short slotCnt;
	protected short usedPtr;
	protected short freeSpace;
	protected short type;
	protected PageId prevPage;
	protected PageId nextPage;
	protected PageId curPage;

	public SortedPageBase() {
		prevPage = new PageId();
		nextPage = new PageId();
		curPage = new PageId();
	}

	public SortedPageBase(Page page) {
		prevPage = new PageId();
		nextPage = new PageId();
		curPage = new PageId();
		super.data = page.getData();
	}

	public void openHFpage(Page page) {
		super.data = page.getData();
	}

	public void init(PageId pageid, Page page) throws IOException {
		super.data = page.getData();
		slotCnt = 0;
		Convert.setShortValue(slotCnt, 0, super.data);
		curPage.pid = pageid.pid;
		Convert.setIntValue(curPage.pid, 16, super.data);
		nextPage.pid = prevPage.pid = -1;
		Convert.setIntValue(prevPage.pid, 8, super.data);
		Convert.setIntValue(nextPage.pid, 12, super.data);
		usedPtr = 1024;
		Convert.setShortValue(usedPtr, 2, super.data);
		freeSpace = 1004;
		Convert.setShortValue(freeSpace, 4, super.data);
	}

	public byte[] getHFpageArray() {
		return super.data;
	}

	public void dumpPage() throws IOException {
		curPage.pid = Convert.getIntValue(16, super.data);
		nextPage.pid = Convert.getIntValue(12, super.data);
		usedPtr = Convert.getShortValue(2, super.data);
		freeSpace = Convert.getShortValue(4, super.data);
		slotCnt = Convert.getShortValue(0, super.data);
		System.out.println("dumpPage");
		System.out.println("curPage= " + curPage.pid);
		System.out.println("nextPage= " + nextPage.pid);
		System.out.println("usedPtr= " + usedPtr);
		System.out.println("freeSpace= " + freeSpace);
		System.out.println("slotCnt= " + slotCnt);
		int i = 0;
		int j = 20;
		for (; i < slotCnt; i++) {
			short length = Convert.getShortValue(j, super.data);
			short addr = Convert.getShortValue(j + 2, super.data);
			System.out.println("slotNo " + i + " offset= " + addr);
			System.out.println("slotNo " + i + " length= " + length);
			j += 4;
		}

	}

	public PageId getPrevPage() throws IOException {
		prevPage.pid = Convert.getIntValue(8, super.data);
		return prevPage;
	}

	public void setPrevPage(PageId pageid) throws IOException {
		prevPage.pid = pageid.pid;
		Convert.setIntValue(prevPage.pid, 8, super.data);
	}

	public PageId getNextPage() throws IOException {
		nextPage.pid = Convert.getIntValue(12, super.data);
		return nextPage;
	}

	public void setNextPage(PageId pageid) throws IOException {
		nextPage.pid = pageid.pid;
		Convert.setIntValue(nextPage.pid, 12, super.data);
	}

	public PageId getCurPage() throws IOException {
		curPage.pid = Convert.getIntValue(16, super.data);
		return curPage;
	}

	public void setCurPage(PageId pageid) throws IOException {
		curPage.pid = pageid.pid;
		Convert.setIntValue(curPage.pid, 16, super.data);
	}

	public short getType() throws IOException {
		type = Convert.getShortValue(6, super.data);
		return type;
	}

	public void setType(short s) throws IOException {
		type = s;
		Convert.setShortValue(type, 6, super.data);
	}

	public short getSlotCnt() throws IOException {
		slotCnt = Convert.getShortValue(0, super.data);
		return slotCnt;
	}

	public void setSlot(int i, int j, int k)  {
		int l = 20 + i * 4;
		Convert.setShortValue((short) j, l, super.data);
		Convert.setShortValue((short) k, l + 2, super.data);
	}
	
	// a slot is composed of 4 bytes, the first 2 bytes are short representing the length of the record the
	//slot addresses, the 2nd 2 bytes is the address of the record
	//slot is  (recordLength, recordAddr)

	public short getSlotLength(int i) {
		int j = 20 + i * 4;
		short length = Convert.getShortValue(j, super.data);
		return length;
	}

	public short getSlotOffset(int i) throws IOException {
		int j = 20 + i * 4;
		short addr = Convert.getShortValue(j + 2, super.data);
		return addr;
	}

	
	//overridden in subclass SortedPage to add a sorted order to the SLOT directory
	//which permits efficient search of indexes based upon their key values
	public RID insertRecord(byte record[])  {
		RID rid = new RID();
		int i = record.length;
		int j = i + 4;
		freeSpace = Convert.getShortValue(4, super.data);
		if (j > freeSpace)
			return null;
		slotCnt = Convert.getShortValue(0, super.data);
		int k;
		for (k = 0; k < slotCnt; k++) {
			short length = getSlotLength(k);
			if (length == -1)
				break;
		}

		if (k == slotCnt) {
			freeSpace -= j;
			Convert.setShortValue(freeSpace, 4, super.data);
			slotCnt++;
			Convert.setShortValue(slotCnt, 0, super.data);
		} else {
			freeSpace -= i;
			Convert.setShortValue(freeSpace, 4, super.data);
		}
		usedPtr = Convert.getShortValue(2, super.data);
		usedPtr -= i;
		Convert.setShortValue(usedPtr, 2, super.data);
		setSlot(k, i, usedPtr);
		System.arraycopy(record, 0, super.data, usedPtr, i);
		curPage.pid = Convert.getIntValue(16, super.data);
		rid.pageno.pid = curPage.pid;
		rid.slotno = k;
		return rid;
	}

	public void deleteRecord(RID rid) throws IOException,
			InvalidSlotNumberException {
		int i = rid.slotno;
		short length = getSlotLength(i);
		slotCnt = Convert.getShortValue(0, super.data);
		if (i >= 0 && i < slotCnt && length > 0) {
			short addr = getSlotOffset(i);
			usedPtr = Convert.getShortValue(2, super.data);
			int j = usedPtr + length;
			int k = addr - usedPtr;
			System.arraycopy(super.data, usedPtr, super.data, j, k);
			int l = 0;
			int i1 = 20;
			for (; l < slotCnt; l++) {
				if (getSlotLength(l) >= 0) {
					int j1 = getSlotOffset(l);
					if (j1 < addr) {
						j1 += length;
						Convert.setShortValue((short) j1, i1 + 2, super.data);
					}
				}
				i1 += 4;
			}

			usedPtr += length;
			Convert.setShortValue(usedPtr, 2, super.data);
			freeSpace = Convert.getShortValue(4, super.data);
			freeSpace += length;
			Convert.setShortValue(freeSpace, 4, super.data);
			setSlot(i, -1, 0);
			return;
		} else {
			throw new InvalidSlotNumberException("HEAPFILE: INVALID_SLOTNO");
		}
	}

	public RID firstRecord() throws IOException {
		RID rid = new RID();
		slotCnt = Convert.getShortValue(0, super.data);
		int i;
		for (i = 0; i < slotCnt; i++) {
			short length = getSlotLength(i);
			if (length != -1)
				break;
		}

		if (i == slotCnt) {
			return null;
		} else {
			rid.slotno = i;
			curPage.pid = Convert.getIntValue(16, super.data);
			rid.pageno.pid = curPage.pid;
			return rid;
		}
	}

	public RID nextRecord(RID rid) throws IOException {
		RID rid1 = new RID();
		slotCnt = Convert.getShortValue(0, super.data);
		int i = rid.slotno;
		for (i++; i < slotCnt; i++) {
			short length = getSlotLength(i);
			if (length != -1)
				break;
		}

		if (i >= slotCnt) {
			return null;
		} else {
			rid1.slotno = i;
			curPage.pid = Convert.getIntValue(16, super.data);
			rid1.pageno.pid = curPage.pid;
			return rid1;
		}
	}

	

	public int available_space() throws IOException {
		freeSpace = Convert.getShortValue(4, super.data);
		return freeSpace - 4;
	}

	public boolean empty() throws IOException {
		slotCnt = Convert.getShortValue(0, super.data);
		for (int i = 0; i < slotCnt; i++) {
			short length = getSlotLength(i);
			if (length != -1)
				return false;
		}

		return true;
	}

	protected void compact_slot_dir() throws IOException {
		int i = 0;
		int j = -1;
		boolean flag = false;
		slotCnt = Convert.getShortValue(0, super.data);
		freeSpace = Convert.getShortValue(4, super.data);
		for (; i < slotCnt; i++) {
			short length = getSlotLength(i);
			if (length == -1 && !flag) {
				flag = true;
				j = i;
			} else if (length != -1 && flag) {
				short addr = getSlotOffset(i);
				setSlot(j, length, addr);
				setSlot(i, -1, 0);
				for (j++; getSlotLength(j) != -1; j++)
					;
			}
		}

		if (flag) {
			freeSpace += 4 * (slotCnt - j);
			slotCnt = (short) j;
			Convert.setShortValue(freeSpace, 4, super.data);
			Convert.setShortValue(slotCnt, 0, super.data);
		}
	}

}

@SuppressWarnings("serial")
class InvalidSlotNumberException extends Exception {
	
	public InvalidSlotNumberException(String msg) {
		super(msg);
	}
	
}
	
	
	
	
	


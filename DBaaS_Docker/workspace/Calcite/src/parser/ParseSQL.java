

package parser;


import java.util.HashMap;
import java.util.Map;

import javax.json.Json;
import javax.json.JsonObjectBuilder;
import javax.json.JsonString;
import javax.json.JsonValue;

import org.apache.calcite.adapter.jdbc.JdbcSchema;
import org.apache.calcite.avatica.util.Casing;
import org.apache.calcite.rel.RelRoot;
import org.apache.calcite.schema.Schema;
import org.apache.calcite.schema.SchemaPlus;
import org.apache.calcite.schema.Table;
import org.apache.calcite.sql.SqlNode;
import org.apache.calcite.sql.parser.SqlParseException;
import org.apache.calcite.sql.parser.SqlParser;
import org.apache.calcite.sql.parser.SqlParser.ConfigBuilder;
import org.apache.calcite.tools.FrameworkConfig;
import org.apache.calcite.tools.Frameworks;
import org.apache.calcite.tools.Planner;
import org.apache.calcite.tools.RelConversionException;
import org.apache.calcite.tools.ValidationException;

public class ParseSQL {

	public static void main(String[] args) {
		
		
		try {
			
			// register the JDBC driver 
	        String sDriverName = "org.sqlite.JDBC";
	        Class.forName(sDriverName);
			
			
	        JsonObjectBuilder builder = Json.createObjectBuilder();
	 
	        
	        builder.add("jdbcDriver", "org.sqlite.JDBC")
	                        .add("jdbcUrl", "jdbc:sqlite://home/david/Dropbox/DBaaS_Docker_Notes/calcite/students.db")
	                            .add("jdbcUser", "root")
	        						.add("jdbcPassword", "root");
	         
	     	         
	        Map<String, JsonValue> JsonObject = builder.build();
	        //operand: map argument for JdbcSchema.Factory().create(rootSchema, "students",  operand) 
			Map<String, Object> operand = new HashMap<String, Object>();
			
			//have to explicitly extract JsonString(s) from builder object and load into operand map
			for(String key : JsonObject.keySet()) {
				JsonString value = (JsonString) JsonObject.get(key);
				operand.put(key, value.getString());
			}
			
			final SchemaPlus rootSchema = Frameworks.createRootSchema(true);
			Schema schema =  new JdbcSchema.Factory().create(rootSchema, "students",  operand);
			rootSchema.add("students", schema);
			
			//output tables to validate database schema has been read in correctly
			for(String tableName : schema.getTableNames()){
				System.out.println(tableName);
				Table table = schema.getTable(tableName);
				System.out.println(table.toString());
			}
			 
			//build a FrameworkConfig using defaults where values aren't required
			Frameworks.ConfigBuilder configBuilder  =  Frameworks.newConfigBuilder();
			
			//set defaultSchema
			configBuilder.defaultSchema(rootSchema);
			
			//build configuration
			FrameworkConfig frameworkdConfig = configBuilder.build();
			
			//use SQL parser config builder to ignore case of quoted identifier
			SqlParser.configBuilder(frameworkdConfig.getParserConfig()).setQuotedCasing(Casing.UNCHANGED).build();
			//use SQL parser config builder to set SQL case sensitive = false
			SqlParser.configBuilder(frameworkdConfig.getParserConfig()).setCaseSensitive(false).build();
			
			//get planner
			Planner planner =  Frameworks.getPlanner(frameworkdConfig);
			//parse SQL statement
			SqlNode sql_node = planner.parse("SELECT * FROM \"Students\" WHERE age > 15.0");
			System.out.println("\n" + sql_node.toString());
			//validate SQL
			SqlNode sql_validated = planner.validate(sql_node);
			//get associated relational expression
			RelRoot relationalExpression = planner.rel(sql_validated);
			relationalExpression.toString();
			
		} catch (SqlParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (RelConversionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ValidationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		

	}  // end main

} // end class

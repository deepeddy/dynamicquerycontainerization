

package parser;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.Properties;

import javax.json.Json;
import javax.json.JsonBuilderFactory;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonReader;
import javax.json.JsonString;
import javax.json.JsonStructure;
import javax.json.JsonValue;

import org.apache.calcite.adapter.jdbc.JdbcSchema;
import org.apache.calcite.avatica.util.Casing;
import org.apache.calcite.jdbc.CalciteConnection;
import org.apache.calcite.model.ModelHandler;
import org.apache.calcite.rel.RelRoot;
import org.apache.calcite.schema.Schema;
import org.apache.calcite.schema.SchemaPlus;
import org.apache.calcite.schema.Table;
import org.apache.calcite.sql.SqlNode;
import org.apache.calcite.sql.parser.SqlParseException;
import org.apache.calcite.sql.parser.SqlParser.Config;
import org.apache.calcite.tools.FrameworkConfig;
import org.apache.calcite.tools.Frameworks;
import org.apache.calcite.tools.Planner;
import org.apache.calcite.tools.RelConversionException;
import org.apache.calcite.tools.ValidationException;

public class ParseSQLVer1 {

	public static void main(String[] args) {
		
		
		try {
			
			//JsonObjectBuilder operand = Json.createObjectBuilder();
			//operand.add("jdbcDriver","com.sqlite.jdbc.Driver");
			
			// register the driver 
	        String sDriverName = "org.sqlite.JDBC";
	        Class.forName(sDriverName);
			
			
	        JsonObjectBuilder operandBuilder = Json.createObjectBuilder();
	 
	        
	        operandBuilder.add("jdbcDriver", "org.sqlite.JDBC")
	                        .add("jdbcUrl", "jdbc:sqlite://home/david/Dropbox/DBaaS_Docker_Notes/calcite/students.db")
	                            .add("jdbcUser", "root")
	        						.add("jdbcPassword", "root");
	         
	     
	         
	        //Object operandObj = operandBuilder.build();
	        Map<String, JsonValue> operandObj = operandBuilder.build();
	       //Map<String, Object> operandMap = (Map<String, Object>) operandObj;
			
			
			//JsonReader reader = Json.createReader(new FileReader("students.json"));
			//JsonReader reader = Json.createReader(new FileReader("foo.json"));
			//JsonObject object = reader.readObject();
			//reader.close();
			//Map<String, Object> operand = (Map<String, Object>) (object.get("operand"));
			//String JdbcUrl = (String)operand.get("jdbcUrl");
			//Class<? extends JsonValue> c = operand.getClass();
			//JsonValue.ValueType type = operand.getValueType();
			
			//JsonObject operandObject = (JsonObject) operand;
			//Map<String, JsonValue> map = (java.util.Map<java.lang.String,JsonValue>)operandObject;
			Map<String, Object> operand = new HashMap<String, Object>();
			
			for(String key : operandObj.keySet()) {
				JsonString value = (JsonString) operandObj.get(key);
				//String value2str = value.getString();
				//Object obj = (Object)value2str;
				operand.put(key, value.getString());
			}
			
			//Map<String, Object> operandMap =  operand.getValueType();
			//String url =  operandMap.get("jdbcUrl").toString();
			//Map<String, JsonValue> operandMap = (Map<String, JsonValue>) object.get("operand");
			//JsonObjectBuilder operandBuilder = Json.createObjectBuilder();
			final SchemaPlus rootSchema = Frameworks.createRootSchema(true);
			Schema schema =  new JdbcSchema.Factory().create(rootSchema, "students",  operand);
			for(String tableName : schema.getTableNames()){
				System.out.println(tableName);
				Table table = schema.getTable(tableName);
				System.out.println(table.toString());
			}
			 
			//build a FrameworkConfig using defaults where values aren't required
			Frameworks.ConfigBuilder configBuilder  =  Frameworks.newConfigBuilder();
			//set defaultSchema
			configBuilder.defaultSchema(rootSchema);
			//Connection connection = DriverManager.getConnection("jdbc:sqlite:/home/david/Dropbox/DBaaS_Docker_Notes/calcite/students.db");
		
			FrameworkConfig config = configBuilder.build();
			Config parserConfig = config.getParserConfig();
			Casing casing = parserConfig.unquotedCasing();
			//don't change case of any identifier in the SQL statement
			casing =  Casing.UNCHANGED;
			
			/*
			org.apache.calcite.jdbc.Driver driver = new org.apache.calcite.jdbc.Driver();
			Properties info = new Properties();
			info.setProperty("jdbcUser", "root");
			info.setProperty("jdbcPassword", "root");
			
			Connection connection = driver.connect("jdbc:sqlite:/home/david/Dropbox/DBaaS_Docker_Notes/calcite/students.db", info); 
			//CalciteConnection calciteConnection = (CalciteConnection) connection; 
			
			CalciteConnection calciteConnection = connection.unwrap(CalciteConnection.class);
			
			ModelHandler model = new ModelHandler(calciteConnection, "students.json");
			*/
			
			//String schema = connection.getSchema();
			//String catalog = connection.getCatalog();
			
			//SchemaPlus defaultSchema = JdbcSchema.create(null, name, dataSource, catalog, schema);
			configBuilder.defaultSchema(rootSchema);
		
			Planner planner =  Frameworks.getPlanner(config);
			SqlNode sql_node = planner.parse("SELECT * FROM Students WHERE age > 15.0");
			System.out.println("\n" + sql_node.toString());
			SqlNode sql_validated = planner.validate(sql_node);
			RelRoot relationalExpression = planner.rel(sql_validated);
			relationalExpression.toString();
		} catch (SqlParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (RelConversionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ValidationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		

	}  // end main

} // end class

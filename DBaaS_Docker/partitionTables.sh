#!/bin/bash
echo paritions table into N partitions, each partition is stored in its own .db file AS tableName.db

echo usage: 1st param: table name, 2nd param is database to attach, 3rd param is NoOfPartitions to create
table=$1
echo $table
#database="$table.db"
db2=$2
echo $db2
NoOfPartions=$3
echo $NoOfPartions

counter=1
while [ $counter -le $3 ]
do
echo iteration $counter
database="$table""Part_""$counter"".db"
tablePart="$table""Part_""$counter"

echo $database
echo $tablePart
echo sqlite3 $database "ATTACH '$db2' AS db2;  CREATE TABLE $tablePart AS SELECT * FROM db2.$tablePart;"

sqlite3 $database "ATTACH '$db2' AS db2;  CREATE TABLE $tablePart AS SELECT * FROM db2.$tablePart;"


((counter++))
done
echo All done

exit 0
